//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ReShade 3.0 effect file
// visit facebook.com/MartyMcModding for news/updates
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// SSRTGI ported and improved by Marty McFly
// 1.1
// CC BY-NC-ND 3.0 licensed.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "common.fx"

cbuffer SSAOBuffer : register(b0)
{
	matrix projMatrix;
	matrix invProjMatrix;
	float invWidth;
	float invHeight;
	float aspectRatio;
	float dummy;
}

Texture2D depthTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D colorTexture : register(t2);
Texture2D noiseTexture : register(t3);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// UI variables
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

static const float SSRTGI_Z_THRESHOLD_AO = 0.02; // Z Threshold AO
static const float SSRTGI_Z_THRESHOLD_GI = 0.02; // Z Threshold GI
static const float SSRTGI_Z_THRESHOLD_BN = 0.02; // Z Threshold Bent Normal

static const float SSRTGI_SAMPLE_RADIUS = 0.5; // Sample radius of SSRTGI, higher means more large-scale occlusion

#define NUM_RAYS 	32
#define NUM_STEPS	4


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Vertex Shader
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

struct SSRTGI_VSOUT
{
	float4              position    : SV_Position;
	float2              texcoord    : TEXCOORD0;
};

struct ssrtgiData
{
	float 	ao;
	float3 	gi;
	float3 	bn;
	float 	thresholdao;
	float 	thresholdgi;
	float 	thresholdbn;
};


SSRTGI_VSOUT VS(in uint id : SV_VertexID)
{
	SSRTGI_VSOUT IN;

	IN.position.x = (float)(id / 2) * 4.0f - 1.0f;
	IN.position.y = (float)(id % 2) * 4.0f - 1.0f;
	IN.position.z = 0.0f;
	IN.position.w = 1.0f;

	IN.texcoord.x = (float)(id / 2) * 2.0f;
	IN.texcoord.y = 1.0f - (float)(id % 2) * 2.0f;
	return IN;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Functions
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

float GetDepth(float2 coords)
{
	return depthTexture.Sample(LinearClampSampler, coords).r;
}

float GetDepthLOD(float2 coords, float mipLevel)
{
	return depthTexture.SampleLevel(LinearClampSampler, coords, mipLevel).r;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

float3 GetPosition(in float2 coords)
{
	float4 ssPosition = float4(2.0 * coords.x - 1.0, 2.0 * (1.0 - coords.y) - 1.0, GetDepth(coords), 1.0);
	ssPosition = mul(invProjMatrix, ssPosition);
	return ssPosition.xyz / ssPosition.w;
}

float3 GetPositionLOD(in float2 coords, float mipLevel)
{
	float4 ssPosition = float4(2.0 * coords.x - 1.0, 2.0 * (1.0 - coords.y) - 1.0, GetDepthLOD(coords, mipLevel), 1.0);
	ssPosition = mul(invProjMatrix, ssPosition);
	return ssPosition.xyz / ssPosition.w;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

float2 GetPositionToUV(in float3 pos)
{
	float4 csPosition = mul(projMatrix, float4(pos, 1.0));
	csPosition.xyz /= csPosition.w;
	csPosition.y = -csPosition.y;
	return 0.5 + csPosition.xy * 0.5;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

float3 GetNormal(in float2 coords)
{
	return normalize(normalTexture.Sample(LinearClampSampler, coords).xyz);
}


float4 GetColorLOD(in float2 coords, in float mipLevel)
{
	return colorTexture.SampleLevel(LinearClampSampler, coords, mipLevel);
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool checkUV(in float2 uv)
{
	return all(saturate(-uv * uv + uv));
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

float3x3 GetOrthonormalMat(float3 N)
{
	float3 UP = float3(0.707, 0.707, 0);
	UP = lerp(UP.xyz, UP.zxy, saturate(1.0 - 10.0*dot(UP.xyz, N.xyz)));
	float3 T = normalize(cross(UP, N));
	float3 B = cross(N, T);
	return float3x3(T, B, N);
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Pixel Shaders
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

float4 PS(SSRTGI_VSOUT IN): SV_Target
{
	float3 N = GetNormal(IN.texcoord);

	float3x3 mTBN = GetOrthonormalMat(N);

	float3 P = GetPosition(IN.texcoord.xy).xyz;

	float depth = -P.z;
	P += -P * 0.002;

	float3 vRand = noiseTexture.Sample(LinearWrapSampler, IN.position.xy / 64.0).xyz;

	float rayStep = SSRTGI_SAMPLE_RADIUS * sqrt(depth) / NUM_STEPS;
	vRand.z /= NUM_RAYS;

	ssrtgiData SSRTGI;
	SSRTGI.ao = 0.0;
	SSRTGI.gi = 0.0;
	SSRTGI.bn = N * 0.000001;
	SSRTGI.thresholdao = SSRTGI_Z_THRESHOLD_AO / rayStep;
	SSRTGI.thresholdgi = SSRTGI_Z_THRESHOLD_GI / rayStep;
	SSRTGI.thresholdbn = SSRTGI_Z_THRESHOLD_BN / rayStep;

	rayStep *= vRand.x + 0.05;

	float2 Phi;
	sincos(vRand.y * 3.1415927 * 10.0, Phi.y, Phi.x);

	[loop]
	for (float iRay = 1; iRay <= NUM_RAYS; ++iRay)
	{
		float3 R; //vogel distribution 3D
		R.y = iRay / NUM_RAYS - vRand.z;
		R.xz = Phi.xy * sqrt(1.0 - R.y*R.y);
		R = mul(R.xzy, mTBN);

		Phi = mul(Phi, float2x2(-0.73737, -0.67549, 0.67549, -0.73737));

		float2 rayUV = IN.texcoord.xy;
		float rayLength = rayStep;
		float rayIntersect = -1000000000.0;

		int jStep = 0;
		float3 rayPosition = P;

		do {
			rayPosition += R * rayLength;
			rayUV = GetPositionToUV(rayPosition);
			float3 V = GetPositionLOD(rayUV, 0) - rayPosition;

			if (V.z > 0)
			{
				rayIntersect = rayLength - sqrt(dot(V, V));
				jStep += NUM_STEPS;
			}

			rayLength += rayStep;

		} while (++jStep <= NUM_STEPS && checkUV(rayUV));

		float validUV = checkUV(rayUV);
		SSRTGI.ao += saturate(rayIntersect*SSRTGI.thresholdao + validUV);
		//SSRTGI.bn += saturate(-rayIntersect*SSRTGI.thresholdbn - validUV) * R;
		SSRTGI.gi += saturate(rayIntersect*SSRTGI.thresholdgi + validUV) * GetColorLOD(rayUV.xy, 0).rgb * saturate(dot(R,N));
	}

	SSRTGI.ao = saturate(1 - SSRTGI.ao / NUM_RAYS);
	//SSRTGI.bn = normalize(SSRTGI.bn) * 0.5 + 0.5;
	SSRTGI.gi = saturate(SSRTGI.gi / NUM_RAYS * 4.0);

	return float4(SSRTGI.gi, SSRTGI.ao);
}

void GetBlurWeight(in float4 tempKey, in float4 centerKey, in float surfacealignment, inout float weight)
{
	float depthdiff = abs(tempKey.w - centerKey.w);
	float normaldiff = saturate(1.0 - dot(tempKey.xyz, centerKey.xyz));

	weight = saturate(0.15 / surfacealignment - depthdiff) * saturate(0.65 - normaldiff);
	weight = saturate(weight * 4.0) * 2.0;
}

void GetBlurKeyAndSample(in float2 texcoord, in float inputscale, inout float4 tempsample, inout float4 key)
{
	tempsample = GetColorLOD(texcoord.xy * inputscale, 0);
	key = float4(GetNormal(texcoord.xy), GetPosition(texcoord.xy).z);
}

#define BLUR_COMP_SWIZZLE xyzw
float4 BlurFilter(in float2 texcoord, in float inputscale, in float radius, in int blursteps)
{
	float4 tempsample;
	float4 centerkey, tempkey;
	float  centerweight = 1.0, tempweight;
	float4 blurcoord = 0.0;

	GetBlurKeyAndSample(texcoord.xy, inputscale, tempsample, centerkey);
	float surfacealignment = saturate(-dot(centerkey.xyz, normalize(float3(texcoord.xy*2.0 - 1.0, 1.0)*centerkey.w)));

	float4 blurSum = tempsample.BLUR_COMP_SWIZZLE;
	float2 blurOffsets[8] = { float2(1.5,0.5), float2(-1.5,-0.5), float2(-0.5,1.5), float2(0.5,-1.5), float2(1.5,2.5), float2(-1.5,-2.5), float2(-2.5,1.5), float2(2.5,-1.5) };

	[loop]
	for (int iStep = 0; iStep < blursteps; iStep++)
	{
		float2 sampleCoord = texcoord.xy + blurOffsets[iStep] * float2(invWidth, invHeight) * radius / inputscale;

		GetBlurKeyAndSample(sampleCoord, inputscale, tempsample, tempkey);
		GetBlurWeight(tempkey, centerkey, surfacealignment, tempweight);

		blurSum += tempsample.BLUR_COMP_SWIZZLE * tempweight;
		centerweight += tempweight;
	}

	blurSum.BLUR_COMP_SWIZZLE /= centerweight;

	//blurSum.xyz = centerkey.xyz * 0.5 + 0.5;

	return blurSum;
}


float4 PS_BlurX(in SSRTGI_VSOUT IN) : SV_Target
{
	return BlurFilter(IN.texcoord, 1.0, 1.0, 8);
}


float4 PS_BlurY(in SSRTGI_VSOUT IN) : SV_Target
{
	return BlurFilter(IN.texcoord, 1.0, 0.75, 4);
}
