#include "common.fx"

cbuffer WorldMatrixBuffer : register(b0)
{
	matrix World;
}

cbuffer ViewMatrixBuffer : register(b1)
{
	matrix View;
}

cbuffer ProjMatrixBuffer : register(b2)
{
	matrix Projection;
}

cbuffer MaterialBuffer : register(b3)
{
	float3 diffuseColor;
	float roughness;
	float metalic;
	bool hasDiffuseMap;
	bool hasNormalMap;
	bool hasSpecularMap;
}

struct VS_INPUT
{
	float4 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 Binormal : BINORMAL0;
};

struct VS_OUTPUT
{
	float4 Position	: SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

Texture2D diffuseTexture : register(t0);


VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output;
	output.Position = mul(Projection, mul(View, mul(World, input.Position)));
	output.TexCoord = input.TexCoord;
	return output;
}

float PS(VS_OUTPUT input) : SV_TARGET
{
	if (hasDiffuseMap)
	{
		float4 diffuseTex = diffuseTexture.Sample(LinearWrapSampler, input.TexCoord);

		clip(diffuseTex.a - 0.1);
	}
	
	return input.Position.z / input.Position.w;
}