struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
#if REDLINE_VERTEX
	VS_OUTPUT VS(uint id:SV_VertexID)
	{
		VS_OUTPUT output;
		output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
		output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
		output.Pos.z = 0.0f;
		output.Pos.w = 1.0f;

		output.TexCoord.x = (float)(id / 2) * 2.0f;
		output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
		return output;
	}
#endif


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
#if REDLINE_PIXEL
	Texture2D baseTexture : register(t0);

	#include "common.fx"

	float4 GaussianBlur(float2 centreUV, float2 halfPixelOffset, float2 pixelOffset)
	{
		float4 colOut = float4(0, 0, 0, 0);

		////////////////////////////////////////////////;
		// Kernel width 7 x 7
		//
		const int stepCount = 2;
		//
		const float gWeights[stepCount] = {
			0.44908,
			0.05092
		};
		const float gOffsets[stepCount] = {
			0.53805,
			2.06278
		};
		////////////////////////////////////////////////;

		for (int i = 0; i < stepCount; i++)
		{
			float2 texCoordOffset = gOffsets[i] * pixelOffset;
			float4 col = baseTexture.Sample(LinearClampSampler, centreUV + texCoordOffset) + baseTexture.Sample(LinearClampSampler, centreUV - texCoordOffset);
			colOut += gWeights[i] * col;
		}

		return colOut;
	}

	float4 PS(VS_OUTPUT input) : SV_Target
	{
		float w;
		float h;
		baseTexture.GetDimensions(w, h);
		return GaussianBlur(input.TexCoord, float2(0, 0), float2(0.0, 1.0 / h));
	}
#endif
