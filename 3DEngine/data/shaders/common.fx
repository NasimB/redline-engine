#ifndef COMMON_INCLUDE
#define COMMON_INCLUDE 1

// Common shader include file
// Everything set here must be independant from specific shader declarations

#define PI 3.14159265359
#define EPSILON 1e-6

SamplerState LinearClampSampler : register(s0);
SamplerState PointClampSampler : register(s1);
SamplerState LinearWrapSampler : register(s2);
SamplerState PointWrapSampler : register(s3);
SamplerComparisonState ShadowComparaisonSampler : register(s4);

#define GAMMA 2.2
inline float3 LinearToGamma(float3 linearColor)
{
	return pow(linearColor, 1 / GAMMA);
}

inline float3 GammaToLinear(float3 gammaColor)
{
	return pow(gammaColor, GAMMA);
}

inline float4 LinearToGamma(float4 linearColor)
{
	return float4(LinearToGamma(linearColor.rgb), linearColor.a);
}

inline float4 GammaToLinear(float4 gammaColor)
{
	return float4(GammaToLinear(gammaColor.rgb), gammaColor.a);
}



float GetLinearDepth(float2 coords, Texture2D<float> depthTexture)
{
	return depthTexture.Sample(LinearClampSampler, coords).r;
}

float3 GetPosition(in float2 coords, in matrix invProjMatrix, Texture2D<float> depthTexture)
{
	float4 ssPosition = float4(2.0 * coords.x - 1.0, 2.0 * (1.0 - coords.y) - 1.0, GetLinearDepth(coords, depthTexture), 1.0);
	ssPosition = mul(invProjMatrix, ssPosition);
	return ssPosition.xyz / ssPosition.w;
}


inline float DotClamped(float3 a, float3 b)
{
	return max(dot(a, b), 0.0);
}

// Transforms 2D UV by scale/bias property
#define TRANSFORM_TEX(tex, tileOffset) (tex.xy * tileOffset.zw + tileOffset.xy)

#endif
