#define ENABLE_ALPHA_TEST 1

#include "common.fx"

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
#if REDLINE_VERTEX
	cbuffer WorldMatrixBuffer : register(b0)
	{
		matrix World;
	}

	cbuffer ViewMatrixBuffer : register(b1)
	{
		matrix View;
	}

	cbuffer ProjMatrixBuffer : register(b2)
	{
		matrix Projection;
	}

	struct VS_INPUT
	{
		float4 Pos : POSITION;
		float2 TexCoord : TEXCOORD0;
		float3 Normal : NORMAL0;
		float3 Tangent : TANGENT0;
		float3 Binormal : BINORMAL0;
	};


	VS_OUTPUT VS(VS_INPUT input)
	{
		VS_OUTPUT output;
		output.Pos = mul(Projection, mul(View, mul(World, input.Pos)));
		output.TexCoord = input.TexCoord;
		output.Normal = mul(View, mul(World, float4(input.Normal, 0.0f))).xyz;
		output.Tangent = mul(View, mul(World, float4(input.Tangent, 0.0f))).xyz;
		output.Binormal = mul(View, mul(World, float4(input.Binormal, 0.0f))).xyz;
		return output;
	}
#endif

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
#if REDLINE_PIXEL
	cbuffer MaterialBuffer : register(b0)
	{
		float3 diffuseColor;
		float roughness;
		float metallic;
		bool hasDiffuseMap;
		bool hasNormalMap;
		bool hasSpecularMap;
	}

	Texture2D diffuseTexture : register(t0);
	Texture2D normalTexture : register(t1);

	struct PS_OUTPUT
	{
		float Depth : SV_Target0;
		float4 Normal : SV_Target1;
		float4 Color : SV_Target2;
	};


	PS_OUTPUT PS(VS_OUTPUT input)
	{
		PS_OUTPUT output;

		// Color
		float4 diffuseTex = GammaToLinear(diffuseTexture.Sample(LinearWrapSampler, input.TexCoord));
		diffuseTex = lerp(float4(1, 1, 1, 1), diffuseTex, hasDiffuseMap ? 1.0 : 0.0);

		#if ENABLE_ALPHA_TEST
			clip(diffuseTex.a - 0.1);
		#endif

		output.Color = float4(diffuseColor * diffuseTex.rgb, roughness);

		// Depth
		output.Depth = input.Pos.z;

		// Normal
		float3 normalTangentSpace = normalTexture.Sample(LinearWrapSampler, input.TexCoord).xyz;
		float3x3 tbnMatrix = float3x3(normalize(input.Tangent), normalize(input.Binormal), normalize(input.Normal));
		output.Normal.xyz = normalize(mul((normalTangentSpace * 2.0) - 1.0, tbnMatrix));
		output.Normal.xyz = lerp(input.Normal, output.Normal.xyz, hasNormalMap ? 1.0 : 0.0);
		output.Normal.w = metallic;
		return output;
	}
#endif
