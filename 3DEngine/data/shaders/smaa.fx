cbuffer SMAABuffer : register(b0)
{
	float4 SMAA_RT_METRICS : packoffset(c0);
};

#define SMAA_HLSL_4
#define SMAA_PRESET_HIGH
#include "SMAA.hlsl"

Texture2D    InputTexture : register(t0); // color and edge
Texture2D    AreaTexture : register(t1); // area tex
Texture2D    SearchTexture : register(t2); // search tex

struct VS_OUTPUT_EDGE
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
	float4 offset[3] : TEXCOORD1;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT_EDGE VS_EdgeDetection(uint id:SV_VertexID)
{
	VS_OUTPUT_EDGE output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;

	SMAAEdgeDetectionVS(output.TexCoord, output.offset);
	return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS_EdgeDetection(VS_OUTPUT_EDGE input) : SV_Target
{
	float2 edge = SMAALumaEdgeDetectionPS(input.TexCoord, input.offset, InputTexture);
	return float4(edge, 0, 0);
}




struct VS_OUTPUT_BLEND
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
	float2 pixcoord : TEXCOORD1;
	float4 offset[3] : TEXCOORD2;
	
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT_BLEND VS_BlendingWeight(uint id:SV_VertexID)
{
	VS_OUTPUT_BLEND output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;

	SMAABlendingWeightCalculationVS(output.TexCoord, output.pixcoord, output.offset);
	return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS_BlendingWeight(VS_OUTPUT_BLEND input) : SV_Target
{
	float4 weights = SMAABlendingWeightCalculationPS(input.TexCoord, input.pixcoord, input.offset, InputTexture, AreaTexture, SearchTexture, 1);
	return weights;
}





struct VS_OUTPUT_NBLEND
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
	float4 offset : TEXCOORD1;

};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT_NBLEND VS_NeighborhoodBlend(uint id:SV_VertexID)
{
	VS_OUTPUT_NBLEND output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;

	SMAANeighborhoodBlendingVS(output.TexCoord, output.offset);
	return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS_NeighborhoodBlend(VS_OUTPUT_NBLEND input) : SV_Target
{
	float4 output = SMAANeighborhoodBlendingPS(input.TexCoord, input.offset, InputTexture, AreaTexture);
	return output;
}