#ifndef SHADOW_COMMON_INCLUDE
#define SHADOW_COMMON_INCLUDE 1

#include "common.fx"

#define SHADOW_QUALITY_MEDIUM 1
#define SHADOW_PCSS 0

float ShadowContribution(Texture2D<float> shadowTex, float2 projectedTexCoords, float depth)
{
	float pixelOffset = 1.0 / shadowResolution;
	static const float bias = 0.0001;
	float shadow = 0.0f;

#if SHADOW_QUALITY_HIGH
	const float samples = 7.0 * 7.0;
	const float offset = 3.0;
#elif SHADOW_QUALITY_MEDIUM
	const float samples = 5.0 * 5.0;
	const float offset = 2.0;
#else
	const float samples = 1.0;
	const float offset = 0.0;
#endif

#if SHADOW_PCSS
	float blockerDepth = 0.0;
	float blockerCount = 0.0;

	[loop]
	for (float y = -offset; y <= offset; y += 1.0)
	{
		for (float x = -offset; x <= offset; x += 1.0)
		{
			float shadowMapDepth = shadowTex.SampleLevel(LinearClampSampler, projectedTexCoords + float2(x, y) * pixelOffset, 0);

			if (shadowMapDepth < depth - bias)
			{
				blockerDepth += shadowMapDepth;
				blockerCount += 1.0;
			}
		}
	}

	blockerDepth /= blockerCount;

	const float lightSize = 0.01;

	float radius = max((depth - blockerDepth) * lightSize / blockerDepth, pixelOffset);
#else
	float radius = pixelOffset;
#endif

	for (float y = -offset; y <= offset; y += 1.0)
	{
		for (float x = -offset; x <= offset; x += 1.0)
		{
			float2 projectedTexCoordsPenumbra = projectedTexCoords + float2(x, y) * radius;
			shadow += shadowTex.SampleCmpLevelZero(ShadowComparaisonSampler, projectedTexCoordsPenumbra, depth - bias);
		}
	}

	return shadow / samples;
}

float IsShadowInRange(matrix lightViewMatrix, matrix lightProjectionMatrix, float4 position, out float2 projectedTexCoords, out float depth)
{
	float4 ShadowProjection = mul(lightProjectionMatrix, mul(lightViewMatrix, position));

	// Texcoord range test
	projectedTexCoords = float2((ShadowProjection.x / ShadowProjection.w / 2.0f) + 0.5f, (-ShadowProjection.y / ShadowProjection.w / 2.0f) + 0.5f);
	if (saturate(projectedTexCoords.x) != projectedTexCoords.x || saturate(projectedTexCoords.y) != projectedTexCoords.y)
		return 0.0;

	depth = ShadowProjection.z / ShadowProjection.w;
	return 1.0;
}

float GetShadowFromCascade(float3 vsPosition)
{
	float4 wsPosition = mul(viewToWorldMatrix, float4(vsPosition, 1.0f));

	float2 projectedTexCoords = (float2)0.0;
	float depth = 0.0;
	int cascade = NUM_CASCADES;

	[unroll]
	for (int i = NUM_CASCADES - 1; i >= 0; i--)
	{
		float2 projCoords;
		float depthTmp;
		float isInRange = IsShadowInRange(lightView, lightProjection[i], wsPosition, projCoords, depthTmp);
		cascade = lerp(cascade, i, isInRange);
		projectedTexCoords = lerp(projectedTexCoords, projCoords, isInRange);
		depth = lerp(depth, depthTmp, isInRange);
	}

	if (cascade >= NUM_CASCADES)
	{
		return 1.0;
	}

	projectedTexCoords = TRANSFORM_TEX(projectedTexCoords, splitTexCoord[cascade]);
	return ShadowContribution(shadowMap, projectedTexCoords, depth);
}
#endif
