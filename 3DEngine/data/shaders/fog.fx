//
// Atmospheric scattering "GroundFromAtmosphere" shader
//
// Author: Sean O'Neil
// Ported to HLSL by Nasim BOUGUERRA
//
// Copyright (c) 2004 Sean O'Neil
//

#include "common.fx"

cbuffer ConstantBuffer : register(b0)
{
	matrix viewToWorldMatrix;
	matrix invProjMatrix;
	int EnvNumMipMaps;
	float ambient_intensity;
}

cbuffer SkyboxVSBuffer : register(b1)
{
	float3 v3CameraPos;			// The camera's current position
	float4 v3InvWavelength;		// 1 / pow(wavelength, 4) for the red, green, and blue channels
	float fCameraHeight;		// The camera's current height
	float fCameraHeight2;		// fCameraHeight^2
	float fOuterRadius;			// The outer (atmosphere) radius
	float fOuterRadius2;		// fOuterRadius^2
	float fInnerRadius;			// The inner (planetary) radius
	float fInnerRadius2;		// fInnerRadius^2
	float fKrESun;				// Kr * ESun
	float fKmESun;				// Km * ESun
	float fKr4PI;				// Kr * 4 * PI
	float fKm4PI;				// Km * 4 * PI
	float fScale;				// 1 / (fOuterRadius - fInnerRadius)
	float fScaleDepth;			// Where the average atmosphere density is found
	float fScaleOverScaleDepth;	// (1.0f / (m_fOuterRadius - m_fInnerRadius)) / m_fRayleighScaleDepth
}

cbuffer SkyboxPSBuffer : register(b2)
{
	float2 g; // X: G, Y: G^2
	float3 lightDirection;
	float3 cameraPosition;
}

#if SHADOW_ENABLED
cbuffer ShadowsBuffer : register(b3)
{
	matrix lightView;
	matrix lightProjection[MAX_CASCADES];
	float4 splitTexCoord[MAX_CASCADES];
	float shadowResolution;
}
#endif

Texture2D baseTexture : register(t0);
Texture2D<float> depthTexture : register(t1);

#if SHADOW_ENABLED
Texture2D<float> shadowMap : register(t4);
#endif


#if SHADOW_ENABLED
#include "shadow_common.fx"
#endif


// The number of sample points taken along the ray
static const int nSamples = 16;
static const float fSamples = (float)nSamples;

// The scale equation calculated by Vernier's Graphical Analysis
float scale(float fCos)
{
	float x = 1.0f - fCos;
	return fScaleDepth * exp(-0.00287f + x * (0.459f + x * (3.83f + x * (-6.80f + x * 5.25f))));
}


struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
	return output;
}


float4 PS(VS_OUTPUT input) : SV_Target
{
	float3 position = GetPosition(input.TexCoord, invProjMatrix, depthTexture);

	float3 color = baseTexture.Sample(LinearClampSampler, input.TexCoord).rgb;

	// Get the ray from the camera to the vertex, and its length (which is the far point of the ray passing through the atmosphere)
	float3 v3Pos = (position - cameraPosition) * 2e-6;
	v3Pos.y += fInnerRadius;
	float3 v3Ray = v3Pos - v3CameraPos;
	float fFar = length(v3Ray);
	v3Ray /= fFar;

	// Calculate the ray's starting position, then calculate its scattering offset
	float3 v3Start = v3CameraPos;
	float fDepth = exp((fInnerRadius - fCameraHeight) / fScaleDepth);
	float fCameraAngle = 1.0f; // dot(-v3Ray, v3Pos) / length(v3Pos);
	float fLightAngle = dot(lightDirection, v3Pos) / length(v3Pos);
	float fCameraScale = scale(fCameraAngle);
	float fLightScale = scale(fLightAngle);
	float fCameraOffset = fDepth * fCameraScale;
	float fTemp = (fLightScale + fCameraScale);

	// Initialize the scattering loop variables
	float fSampleLength = fFar / fSamples;
	float fScaledLength = fSampleLength * fScale;
	float3 v3SampleRay = v3Ray * fSampleLength;
	float3 v3SamplePoint = v3Start + v3SampleRay * 0.5f;

	// Now loop through the sample rays
	float3 v3FrontColor = float3(0.0f, 0.0f, 0.0f);
	float3 v3Attenuate = float3(0.0f, 0.0f, 0.0f);

	float3 startWorldPos = cameraPosition;
	float3 sampleWorldPos = ((position - cameraPosition) / fSamples);

	for (float i = 0; i < fSamples; i++)
	{
#if SHADOW_ENABLED
		float3 worldPos = startWorldPos + sampleWorldPos * i;
		float sunShadow = GetShadowFromCascade(worldPos);
#else
		float sunShadow = 1.0;
#endif

		float fHeight = length(v3SamplePoint);
		float fDepth = exp(fScaleOverScaleDepth * (fInnerRadius - fHeight));
		float fScatter = fDepth * fTemp - fCameraOffset;
		v3Attenuate = exp(-fScatter * (v3InvWavelength.rgb * fKr4PI + fKm4PI));
		v3FrontColor += v3Attenuate * (fDepth * fScaledLength) * sunShadow;

		v3SamplePoint += v3SampleRay;
	}

	float3 gl_FrontColor = v3FrontColor * (v3InvWavelength.rgb * fKrESun + fKmESun);

	// Calculate the attenuation factor for the ground
	float3 gl_FragColor = gl_FrontColor + color * max(v3Attenuate, 1.0 - saturate((0.1 + lightDirection.y) * 5.0f));
	return float4(gl_FragColor, 1.0f);
}
