//
// Ambient Obscurance with Indirect Lighting "MXAO" 3.4.3 by Marty McFly
// CC BY-NC-ND 3.0 licensed.
//

#include "common.fx"

#define MXAO_MIPLEVEL_AO		0 // [0 to 2] Miplevel of AO texture. 0 = fullscreen, 1 = 1/2 screen width/height, 2 = 1/4 screen width/height and so forth. Best results: IL MipLevel = AO MipLevel + 2
#define MXAO_TWO_LAYER			0 // [0 or 1] Splits MXAO into two separate layers that allow for both large and fine AO.

//#define QUALITY_HIGH 1
#define QUALITY_MEDIUM 1
//#define QUALITY_LOW 1


static const float MXAODepthScale = 1.0;

static const float MXAO_SSAO_AMOUNT = 1.00; // Intensity of AO effect. Can cause pitch black clipping if set too high.
static const float MXAO_SAMPLE_RADIUS = 5.0; // Sample radius of MXAO, higher means more large-scale occlusion with less fine-scale details.

#if (MXAO_TWO_LAYER != 0)
static const float MXAO_SAMPLE_RADIUS_SECONDARY = 0.2; // Multiplier of Sample Radius for fine geometry. A setting of 0.5 scans the geometry at half the radius of the main AO.
static const float MXAO_AMOUNT_COARSE = 1.0; // Intensity of large and small scale AO / IL.
static const float MXAO_AMOUNT_FINE = 1.0; // Intensity of large and small scale AO / IL.
#endif

static const float MXAO_SAMPLE_NORMAL_BIAS = 0.2; // Occlusion Cone bias to reduce self-occlusion of surfaces that have a low angle to each other.

static const float MXAO_FADE_DEPTH_START = 0.4; // Distance where MXAO starts to fade out. 0.0 = camera, 1.0 = sky. Must be less than Fade Out End.
static const float MXAO_FADE_DEPTH_END = 0.6; // Distance where MXAO completely fades out. 0.0 = camera, 1.0 = sky. Must be greater than Fade Out Start.

cbuffer SSAOBuffer : register(b0)
{
	matrix projMatrix;
	matrix invProjMatrix;
	float invWidth;
	float invHeight;
	float aspectRatio;
	float dummy;
}

Texture2D depthTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D colorTexture : register(t2);

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD1;
	float samples : TEXCOORD2;
};

#if REDLINE_VERTEX
VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.position.x = (float)(id / 2) * 4.0f - 1.0f;
	output.position.y = (float)(id % 2) * 4.0f - 1.0f;
	output.position.z = 0.0f;
	output.position.w = 1.0f;

	output.texcoord.x = (float)(id / 2) * 2.0f;
	output.texcoord.y = 1.0f - (float)(id % 2) * 2.0f;

#if QUALITY_HIGH
	output.samples = 64;
#elif QUALITY_MEDIUM
	output.samples = 24;
#else
	output.samples = 4;
#endif
	return output;
}
#endif



float GetDepth(float2 coords)
{
	return depthTexture.Sample(LinearClampSampler, coords).r;
}

float3 GetPosition(in float2 coords)
{
	float4 ssPosition = float4(2.0 * coords.x - 1.0, 2.0 * (1.0 - coords.y) - 1.0, GetDepth(coords), 1.0);
	ssPosition = mul(invProjMatrix, ssPosition);
	return ssPosition.xyz / ssPosition.w;
}

float3 GetNormal(in float2 coords)
{
	return normalTexture.Sample(LinearClampSampler, coords).xyz;
}


float GetDepthLOD(float2 coords, float mipLevel)
{
	return depthTexture.SampleLevel(LinearClampSampler, coords, mipLevel).r;
}

float3 GetPositionLOD(in float2 coords, float mipLevel)
{
	float4 ssPosition = float4(2.0 * coords.x - 1.0, 2.0 * (1.0 - coords.y) - 1.0, GetDepthLOD(coords, mipLevel), 1.0);
	ssPosition = mul(invProjMatrix, ssPosition);
	return ssPosition.xyz / ssPosition.w;
}

float3 GetNormalLOD(in float2 coords, float mipLevel)
{
	return normalTexture.SampleLevel(LinearClampSampler, coords, mipLevel).xyz;
}

float4 GetColorLOD(in float2 coords, in float mipLevel)
{
	return colorTexture.SampleLevel(LinearClampSampler, coords, mipLevel);
}

void SetupAOParameters(in VS_OUTPUT MXAO, in float scaled_depth, in float layer_id, out float scaled_radius, out float falloff_factor)
{
	scaled_radius = 0.25 * MXAO_SAMPLE_RADIUS / (MXAO.samples * (scaled_depth + 2.0));
	falloff_factor = -1.0 / (MXAO_SAMPLE_RADIUS * MXAO_SAMPLE_RADIUS);

#if(MXAO_TWO_LAYER != 0)
	scaled_radius *= lerp(1.0, MXAO_SAMPLE_RADIUS_SECONDARY + 1e-6, layer_id);
	falloff_factor *= lerp(1.0, 1.0 / (MXAO_SAMPLE_RADIUS_SECONDARY * MXAO_SAMPLE_RADIUS_SECONDARY + 1e-6), layer_id);
#endif
}

float3 GetNormalFromDepth(float2 texcoord)
{
	float3 single_pixel_offset = float3(invWidth, invHeight, 0);

	float3 position = GetPosition(texcoord);

	float3 position_delta_x1 = -position + GetPosition(texcoord + single_pixel_offset.xz);
	float3 position_delta_x2 = position - GetPosition(texcoord - single_pixel_offset.xz);
	float3 position_delta_y1 = -position + GetPosition(texcoord + single_pixel_offset.zy);
	float3 position_delta_y2 = position - GetPosition(texcoord - single_pixel_offset.zy);

	position_delta_x1 = lerp(position_delta_x1, position_delta_x2, abs(position_delta_x1.z) > abs(position_delta_x2.z));
	position_delta_y1 = lerp(position_delta_y1, position_delta_y2, abs(position_delta_y1.z) > abs(position_delta_y2.z));

	return normalize(cross(position_delta_y1, position_delta_x1));
}


#if REDLINE_PIXEL
float4 PS(VS_OUTPUT input) : SV_Target
{
	float finalAO = 0.0;

	float3 position = GetPositionLOD(input.texcoord.xy, 0);
	float3 normal = GetNormal(input.texcoord.xy);

	float sample_jitter = dot(floor(input.position.xy % 4 + 0.1), float2(0.0625, 0.25)) + 0.0625;

    float  layer_id = (input.position.x + input.position.y) % 2.0;

	position += normal * abs(position.z) * 0.001;

	float scaled_radius;
	float falloff_factor;
    SetupAOParameters(input, position.z, layer_id, scaled_radius, falloff_factor);

	float2 sample_uv, sample_direction;
	sincos(2.3999632 * 16 * sample_jitter, sample_direction.x, sample_direction.y); // 2.3999632 * 16
	sample_direction *= scaled_radius; 

    [loop]
    for(int i = 0; i < input.samples; i++)
    {                
		sample_uv = input.texcoord.xy + sample_direction.xy * float2(1.0, aspectRatio) * (i + sample_jitter);
		sample_direction.xy = mul(sample_direction.xy, float2x2(0.76465, -0.64444, 0.64444, 0.76465)); // cos/sin 2.3999632 * 16

		float sample_mip = saturate(scaled_radius * i * 20.0) * 3.0;

		float3 occlusion_vector = -position + GetPositionLOD(sample_uv, sample_mip + MXAO_MIPLEVEL_AO);
		float  occlusion_distance_squared = dot(occlusion_vector, occlusion_vector);
		float  occlusion_normal_angle = dot(occlusion_vector, normal) * rsqrt(occlusion_distance_squared);

		float sample_occlusion = saturate(1.0 + falloff_factor * occlusion_distance_squared) * saturate(occlusion_normal_angle - MXAO_SAMPLE_NORMAL_BIAS);
		finalAO += sample_occlusion;
    }

	finalAO = saturate(finalAO / ((1.0 - MXAO_SAMPLE_NORMAL_BIAS) * input.samples) * 2.0);
	finalAO = sqrt(finalAO); // AO denoise

	#if(MXAO_TWO_LAYER != 0)
		finalAO *= lerp(MXAO_AMOUNT_COARSE, MXAO_AMOUNT_FINE, layer_id);
	#endif

	return finalAO;
}
#endif


void GetBlurWeight(in float4 tempKey, in float4 centerKey, in float surfacealignment, inout float weight)
{
	float depthdiff = abs(tempKey.w - centerKey.w);
	float normaldiff = saturate(1.0 - dot(tempKey.xyz, centerKey.xyz));

	weight = saturate(0.15 / surfacealignment - depthdiff) * saturate(0.65 - normaldiff);
	weight = saturate(weight * 4.0) * 2.0;
}

void GetBlurKeyAndSample(in float2 texcoord, in float inputscale, inout float4 tempsample, inout float4 key)
{
	tempsample = GetColorLOD(texcoord.xy * inputscale, 0);
	key = float4(GetNormalLOD(texcoord.xy, 0), GetPositionLOD(texcoord.xy, 0).z * MXAODepthScale);
}

float4 BlurFilter(in float2 texcoord, in float inputscale, in float radius, in int blursteps)
{
	float4 tempsample;
	float4 centerkey, tempkey;
	float  centerweight = 1.0, tempweight;
	float4 blurcoord = 0.0;

	GetBlurKeyAndSample(texcoord.xy, inputscale, tempsample, centerkey);
	float surfacealignment = saturate(-dot(centerkey.xyz, normalize(float3(texcoord.xy*2.0 - 1.0, 1.0)*centerkey.w)));

	#define BLUR_COMP_SWIZZLE rgba

	float4 blurSum = tempsample.BLUR_COMP_SWIZZLE;
	float2 blurOffsets[8] = { float2(1.5,0.5), float2(-1.5,-0.5), float2(-0.5,1.5), float2(0.5,-1.5), float2(1.5,2.5), float2(-1.5,-2.5), float2(-2.5,1.5), float2(2.5,-1.5) };

	[loop]
	for (int iStep = 0; iStep < blursteps; iStep++)
	{
		float2 sampleCoord = texcoord.xy + blurOffsets[iStep] * float2(invWidth, invHeight) * radius / inputscale;

		GetBlurKeyAndSample(sampleCoord, inputscale, tempsample, tempkey);
		GetBlurWeight(tempkey, centerkey, surfacealignment, tempweight);

		blurSum += tempsample.BLUR_COMP_SWIZZLE * tempweight;
		centerweight += tempweight;
	}

	blurSum.BLUR_COMP_SWIZZLE /= centerweight;

	return blurSum;
}


#if REDLINE_PIXEL
float4 PS_BlurX(VS_OUTPUT input) : SV_Target
{
	return BlurFilter(input.texcoord, 1.0, 1.0, 8);
}
#endif


#if REDLINE_PIXEL
float4 PS_BlurY(VS_OUTPUT input) : SV_Target
{
	float4 aoil = BlurFilter(input.texcoord, 1.0, 0.75, 4);
	aoil *= aoil; // AO denoise
	aoil = 1.0 - pow(1.0 - aoil, MXAO_SSAO_AMOUNT * 4.0);
	return 1.0 - aoil;
}
#endif