//
// Screen Space Ambient Occlusion shader
//
// Author: Nasim BOUGUERRA
// 

#include "common.fx"

static const float SSAODepthScale = 0.001;
static const float SSAORadius = 0.02;
static const float SSAOStrength = 1.0;

static const float SSAOBlurSharpness = 5.00; // MXAO sharpness, higher means AO blurs less across geometry edges but may leave some noisy areas.
static const int SSAOBlurSteps = 2; // Offset count for MXAO bilateral blur filter. Higher means smoother but also blurrier AO.

// Distance where SSAO starts and completely fades out. 0.0 = camera, 1.0 = far plane.
static const float SSAOFadeoutStart = 0.9; 
static const float SSAOFadeoutEnd = 1.0;

cbuffer SSAOBuffer : register(b0)
{
	matrix projMatrix;
	matrix invProjMatrix;
	float invWidth;
	float invHeight;
	float aspectRatio;
	float dummy;
}

Texture2D depthTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D colorTexture : register(t2);
Texture2D noiseTexture : register(t3);

static const float3 sampleSphere[] = {
	float3(0.2024537f, 0.841204f, -0.9060141f),
	float3(-0.2200423f, 0.6282339f,-0.8275437f),
	float3(0.3677659f, 0.1086345f,-0.4466777f),
	float3(0.8775856f, 0.4617546f,-0.6427765f),
	float3(0.7867433f,-0.141479f, -0.1567597f),
	float3(0.4839356f,-0.8253108f,-0.1563844f),
	float3(0.4401554f,-0.4228428f,-0.3300118f),
	float3(0.0019193f,-0.8048455f, 0.0726584f),
	float3(-0.7578573f,-0.5583301f, 0.2347527f),
	float3(-0.4540417f,-0.252365f, 0.0694318f),
	float3(-0.0483353f,-0.2527294f, 0.5924745f),
	float3(-0.4192392f, 0.2084218f,-0.3672943f),
	float3(-0.8433938f, 0.1451271f, 0.2202872f),
	float3(-0.4037157f,-0.8263387f, 0.4698132f),
	float3(-0.6657394f, 0.6298575f, 0.6342437f),
	float3(-0.0001783f, 0.2834622f, 0.8343929f),
};


struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float4 ViewDir : TEXCOORD0;
	float2 TexCoord : TEXCOORD1;
};


VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;

	output.ViewDir = mul(output.Pos, invProjMatrix);
	return output;
}

float GetDepth(float2 coords)
{
	return depthTexture.Sample(LinearClampSampler, coords).r;
}

float3 GetPosition(in float2 coords)
{
	float4 ssPosition = float4(2.0 * coords.x - 1.0, 2.0 * (1.0 - coords.y) - 1.0, GetDepth(coords), 1.0);
	ssPosition = mul(ssPosition, invProjMatrix);
	return ssPosition.xyz / ssPosition.w;
}

float3 GetNormal(float2 coords)
{
	return normalize(normalTexture.Sample(LinearClampSampler, coords).xyz);
}

float4 GetColor(float2 coords)
{
	return colorTexture.Sample(LinearClampSampler, coords);
}

float4 PS(VS_OUTPUT input) : SV_Target
{
	float3 normal = GetNormal(input.TexCoord);
	float depth = GetDepth(input.TexCoord) * SSAODepthScale;
	float3 randNorm = noiseTexture.Sample(LinearWrapSampler, input.Pos.xy / 64.0).xyz;

	const unsigned int samples = 8;
	const float radius = abs(SSAORadius * SSAODepthScale) / depth;
	const float strength = 16 / samples; // Per step strength
	const float falloffMin = 0.0001f; // Depth falloff min value 
	const float falloffMax = 0.002f; // Depth falloff max value 
	const float MAX_DISTANCE = 0.0007f; // Depth falloff max value 

	float3 centerPos = float3(input.TexCoord, depth);

	float occlusion = 0.0;
	for (unsigned int i = 0; i < samples; ++i)
	{
		// Get sample position
		float3 offset = reflect(sampleSphere[i * 2], randNorm); // Reflect sample to random direction. 
		offset = sign(dot(offset, normal)) * offset; // Convert to hemisphere. 
		offset.y = -offset.y;

		float3 ray = centerPos + offset * radius;

		// Linear depth at ray.xy 
		float occDepth = GetDepth(ray.xy) * SSAODepthScale;

		// Viewspace normal at ray.xy.
		float3 occNormal = GetNormal(ray.xy);

		float depthDifference = (centerPos.z - occDepth);

		float normalDifference = dot(occNormal, normal);
		
		// Occlusion dependent on angle between normals, smaller angle causes more occlusion.
		float normalOcc = (1.0f - saturate(normalDifference));

		occlusion += normalOcc;

		// Occlusion dependent on depth difference, limited using falloffMin and falloffMax. 
		// helps to reduce self occlusion and halo-artifacts. Try a bit around with falloffMin/Max. 
		float depthOcc = step(falloffMin, depthDifference) * (1.0f - smoothstep(falloffMin, falloffMax, depthDifference));

		occlusion += saturate(depthOcc * normalOcc);
	}

	occlusion /= samples; // Divide by number of samples to get the average occlusion. 
	occlusion = saturate(pow(1.0f - occlusion, SSAOStrength));

	return float4(0.0, 0.0, 0.0, occlusion);
}


void GetBlurWeight(in float4 tempKey, in float4 centerKey, in float surfacealignment, inout float weight)
{
	float depthdiff = abs(tempKey.w - centerKey.w);
	float normaldiff = saturate(1.0 - dot(tempKey.xyz, centerKey.xyz));

	weight = saturate(0.15 / surfacealignment - depthdiff) * saturate(0.65 - normaldiff);
	weight = saturate(weight * 4.0) * 2.0;
}

void GetBlurKeyAndSample(in float2 texcoord, in float inputscale, inout float4 tempsample, inout float4 key)
{
	tempsample = GetColor(texcoord.xy * inputscale);
	key = float4(GetNormal(texcoord.xy), GetPosition(texcoord.xy).z);
}

#define BLUR_COMP_SWIZZLE w
float4 BlurFilter(in float2 texcoord, in float inputscale, in float radius, in int blursteps)
{
	float4 tempsample;
	float4 centerkey, tempkey;
	float  centerweight = 1.0, tempweight;
	float4 blurcoord = 0.0;

	GetBlurKeyAndSample(texcoord.xy, inputscale, tempsample, centerkey);
	float surfacealignment = saturate(-dot(centerkey.xyz, normalize(float3(texcoord.xy*2.0 - 1.0, 1.0)*centerkey.w)));

	float4 blurSum = tempsample.BLUR_COMP_SWIZZLE;
	float2 blurOffsets[8] = { float2(1.5,0.5), float2(-1.5,-0.5), float2(-0.5,1.5), float2(0.5,-1.5), float2(1.5,2.5), float2(-1.5,-2.5), float2(-2.5,1.5), float2(2.5,-1.5) };

	[loop]
	for (int iStep = 0; iStep < blursteps; iStep++)
	{
		float2 sampleCoord = texcoord.xy + blurOffsets[iStep] * float2(invWidth, invHeight) * radius / inputscale;

		GetBlurKeyAndSample(sampleCoord, inputscale, tempsample, tempkey);
		GetBlurWeight(tempkey, centerkey, surfacealignment, tempweight);

		blurSum += tempsample.BLUR_COMP_SWIZZLE * tempweight;
		centerweight += tempweight;
	}

	blurSum.BLUR_COMP_SWIZZLE /= centerweight;

	blurSum.xyz = centerkey.xyz * 0.5 + 0.5;

	return blurSum;
}


float4 PS_BlurX(in VS_OUTPUT IN) : SV_Target
{
	return BlurFilter(IN.TexCoord, 1.0, 1.0, 8);
}


float4 PS_BlurY(in VS_OUTPUT IN) : SV_Target
{
	return BlurFilter(IN.TexCoord, 1.0, 0.75, 4);
}
