struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
#if REDLINE_VERTEX
	cbuffer WorldMatrixBuffer : register(b0)
	{
		matrix World;
	}

	cbuffer ViewMatrixBuffer : register(b1)
	{
		matrix View;
	}

	cbuffer ProjMatrixBuffer : register(b2)
	{
		matrix Projection;
	}

	VS_OUTPUT VS(uint id:SV_VertexID)
	{
		VS_OUTPUT output;
		output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
		output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
		output.Pos.z = 0.0f;
		output.Pos.w = 1.0f;

		output.TexCoord.x = (float)(id / 2) * 2.0f;
		output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
		return output;
	}
#endif

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
#if REDLINE_PIXEL
	cbuffer ConstantBuffer : register(b0)
	{
		matrix viewToWorldMatrix;
		matrix invProjMatrix;
		int EnvNumMipMaps;
		float ambient_intensity;
	}

	cbuffer LightBuffer : register(b1)
	{
		float3 color;
		float intensity;
		float3 position;
		int type;
		float3 direction;
		float angle; // for spot lights
	}

	#if SHADOW_ENABLED
		cbuffer ShadowsBuffer : register(b2)
		{
			matrix lightView;
			matrix lightProjection[MAX_CASCADES];
			float4 splitTexCoord[MAX_CASCADES];
			float shadowResolution;
		}
	#endif

	Texture2D<float> depthTexture : register(t0);
	Texture2D normalTexture : register(t1);
	Texture2D colorTexture : register(t2);
	Texture2D<float> occlusionTexture : register(t3);

	#if SHADOW_ENABLED
		Texture2D<float> shadowMap : register(t4);
	#endif

	TextureCube skyboxTexture : register(t5);

	#include "lighting_common.fx"

	struct PS_OUTPUT
	{
		float Depth : SV_Target0;
		float4 Normal : SV_Target1;
		float4 Color : SV_Target2;
	};

	float4 PS(VS_OUTPUT input, float4 screenPos : SV_Position) : SV_Target
	{
		float3 vsPosition = GetPosition(input.TexCoord, invProjMatrix, depthTexture);

		float4 vsNormal = normalTexture.Load(int3(screenPos.xy, 0)); // RGB: normal A: metallic
		float4 albedo = colorTexture.Load(int3(screenPos.xy, 0)); // RGB: color A: roughness

		float occTex = occlusionTexture.Load(int3(screenPos.xy, 0));
		//return float4(occTex, occTex, occTex, 1.0);

		float roughness = albedo.a; // 0: smooth, 1: rough
		float metallic = vsNormal.a; // 0: plastic, 1: metal

		return float4(ComputeFinalPass(vsPosition, vsNormal.xyz, occTex, roughness, metallic, albedo.rgb), 1.0);
	}
#endif
