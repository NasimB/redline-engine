cbuffer WorldViewMatrixBuffer : register(b0)
{
	matrix World;
	matrix View;
}

cbuffer ProjMatrixBuffer : register(b1)
{
	matrix Projection;
}

struct VS_INPUT
{
	float4 Pos : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 Binormal : BINORMAL0;
};

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float4 Color : COLOR0;
};


VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output;
	output.Pos = mul(Projection, mul(View, mul(World, input.Pos)));
	output.Color = float4(input.Normal, 1.0f);
	return output;
}


float4 PS(VS_OUTPUT input) : SV_Target
{	
	return input.Color;
}
