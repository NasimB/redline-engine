#ifndef LIGHTING_COMMON_INCLUDE
#define LIGHTING_COMMON_INCLUDE 1

#include "common.fx"
#if SHADOW_ENABLED
#include "shadow_common.fx"
#endif

#define Directionnal 0
#define Point 1
#define Spot 2

float Specular_D(float a, float NdH)
{
	// Isotropic ggx.
	float a2 = a*a;
	float NdH2 = NdH * NdH;

	float denominator = NdH2 * (a2 - 1.0f) + 1.0f;
	denominator *= denominator;
	denominator *= PI;

	return a2 / denominator;
}

float3 Specular_F(float3 specularColor, float3 h, float3 v)
{
	return (specularColor + (1.0f - specularColor) * pow((1.0f - saturate(dot(v, h))), 5));
}

float3 Specular_F_Roughness(float3 specularColor, float a, float3 h, float3 v)
{
	// Sclick using roughness to attenuate fresnel.
	return (specularColor + (max(1.0f - a, specularColor) - specularColor) * pow((1 - saturate(dot(v, h))), 5));
}

float Specular_G(float a, float NdV, float NdL, float NdH, float VdH, float LdV)
{
	// Smith schlick-GGX.
	float k = a * 0.5f;
	float GV = NdV / (NdV * (1 - k) + k);
	float GL = NdL / (NdL * (1 - k) + k);

	return GV * GL;
}

float3 Specular(float3 specularColor, float3 h, float3 v, float3 l, float a, float NdL, float NdV, float NdH, float VdH, float LdV)
{
	return ((Specular_D(a, NdH) * Specular_G(a, NdV, NdL, NdH, VdH, LdV)) * Specular_F(specularColor, v, h)) / (4.0f * NdL * NdV + 0.0001f);
}

//--------------------------------------------------------------------------------------
// Lighting function
//--------------------------------------------------------------------------------------
float3 ComputeBRDF(float3 N, float3 V, float3 lightDirection, float intensity, float roughness, float3 lightColor, float3 realAlbedo, float3 realSpecularColor)
{
	// Compute some useful values.
	float NdL = saturate(dot(N, lightDirection));
	float NdV = saturate(dot(N, V));
	float3 h = normalize(lightDirection + V);
	float NdH = saturate(dot(N, h));
	float VdH = saturate(dot(V, h));
	float LdV = saturate(dot(lightDirection, V));
	float a = max(0.001, roughness * roughness);

	float3 cDiff = realAlbedo / PI;
	float3 cSpec = Specular(realSpecularColor, h, V, lightDirection, a, NdL, NdV, NdH, VdH, LdV);

	return lightColor * NdL * (cDiff * (1.0 - cSpec) + cSpec) * intensity;
}

struct Light 
{
	float3 color;
	float intensity;
	float3 position;
	int type;
	float3 direction;
	float angle; // for spot lights
};

float3 ComputeLight(Light light, float3 vsPosition, float3 vsNormal, float3 albedo, float roughness, float metallic)
{
	float3 V = normalize(vsPosition);
	float3 N = normalize(vsNormal);

	// Lerp with metallic value to find the good diffuse and specular.
	float3 realAlbedo = albedo - albedo * metallic;

	// 0.03 default specular value for dielectric.
	float3 realSpecularColor = lerp(0.03, albedo, metallic);

	float intensity = 0.0;
	float3 lightDirection = float3(0, 1, 0);

	if (light.type == Directionnal) // Directional
	{
		lightDirection = normalize(light.direction);
		#if SHADOW_ENABLED
			float sunShadow = GetShadowFromCascade(vsPosition);
		#else
			float sunShadow = 1.0;
		#endif
		intensity = max(light.intensity * sunShadow, 0.0);
	}
	else if (light.type == Point) // Point
	{
		float r = light.intensity;
		float3 L = light.position - vsPosition.xyz;
		float distance = length(L);
		float d = max(distance - r, 0.0);
		L /= distance;

		// calculate basic attenuation
		float denom = d / r + 1;
		float attenuation = 1 / (denom*denom);

		// scale and bias attenuation such that:
		//   attenuation == 0 at extent of max influence
		//   attenuation == 1 when d == 0
		static const float cutoff = 0.005;
		attenuation = (attenuation - cutoff) / (1.0 - cutoff);
		intensity = max(attenuation, 0.0);
		lightDirection = L;
	}
	else if (light.type == Spot) // Spot
	{
		float r = light.intensity;
		float3 L = light.position - vsPosition.xyz;
		float distance = length(L);
		float d = max(distance - r, 0.0);
		L /= distance;

		// calculate basic attenuation
		float denom = d / r + 1;
		float attenuation = 1 / (denom*denom);

		// scale and bias attenuation such that:
		//   attenuation == 0 at extent of max influence
		//   attenuation == 1 when d == 0
		static const float cutoff = 0.005;
		attenuation = (attenuation - cutoff) / (1.0 - cutoff);
		intensity = max(attenuation, 0.0);

		lightDirection = L;
		float angleAttenuation = acos(dot(normalize(-light.direction), L));
		intensity = max((light.angle - angleAttenuation), 0.0) * intensity;
	}

	return ComputeBRDF(N, -V, lightDirection, intensity, roughness, light.color, realAlbedo, realSpecularColor);
}

float3 ComputeFinalPass(float3 vsPosition, float3 vsNormal, float4 GIOcclusion, float roughness, float metallic, float3 albedo)
{
	float3 V = normalize(vsPosition);
	float3 N = normalize(vsNormal);

	float occlusion = GIOcclusion.w;

	// Lerp with metallic value to find the good diffuse and specular.
	float3 realAlbedo = albedo - albedo * metallic;

	// 0.03 default specular value for dielectric.
	float3 realSpecularColor = lerp(0.03, albedo, metallic);

	float MipmapIndex = (roughness - 0.01) * EnvNumMipMaps;
	float3 wsReflectDirection = normalize(mul(viewToWorldMatrix, float4(reflect(V, N), 0.0)).xyz);

	float3 envColor = skyboxTexture.SampleLevel(LinearWrapSampler, wsReflectDirection, MipmapIndex).xyz;
	float3 envFresnel = Specular_F_Roughness(realSpecularColor, roughness * roughness, N, -V);

	float3 wsNormal = normalize(mul(viewToWorldMatrix, float4(N, 0.0)).xyz);
	float3 indirectDiffuse = skyboxTexture.SampleLevel(LinearWrapSampler, wsNormal, EnvNumMipMaps - 2).xyz * occlusion;
	//indirectDiffuse += GIOcclusion.rgb * occlusion * 0.2;

	return envFresnel * envColor + realAlbedo * indirectDiffuse;
}
#endif
