cbuffer ConstantBuffer : register(b0)
{
	matrix viewToWorldMatrix;
	matrix invProjMatrix;
	int EnvNumMipMaps;
	float ambient_intensity;
}

cbuffer LightBuffer : register(b1)
{
	float3 color;
	float intensity;
	float3 position;
	int type;
	float3 direction;
	float angle; // for spot lights
}

#if SHADOW_ENABLED
	cbuffer ShadowsBuffer : register(b2)
	{
		matrix lightView;
		matrix lightProjection[MAX_CASCADES];
		float4 splitTexCoord[MAX_CASCADES];
		float shadowResolution;
	}
#endif

Texture2D<float> depthTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D colorTexture : register(t2);
Texture2D occlusionTexture : register(t3);

#if SHADOW_ENABLED
	Texture2D<float> shadowMap : register(t4);
#endif

TextureCube skyboxTexture : register(t5);

#include "lighting_common.fx"


struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
	return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input, float4 screenPos : SV_Position) : SV_Target
{
	float3 vsPosition = GetPosition(input.TexCoord, invProjMatrix, depthTexture);

	float4 vsNormal = normalTexture.Load(int3(screenPos.xy, 0)); // RGB: normal A: metallic
	float4 albedo = colorTexture.Load(int3(screenPos.xy, 0)); // RGB: color A: roughness

	float roughness = albedo.a; // 0: smooth, 1: rough
	float metallic = vsNormal.a; // 0: plastic, 1: metal

	Light light;
	light.color = color;
	light.intensity = intensity;
	light.position = position;
	light.type = type;
	light.direction = direction;
	light.angle = angle;
	
	return float4(ComputeLight(light, vsPosition, vsNormal.xyz, albedo.rgb, roughness, metallic), 1.0);
}
