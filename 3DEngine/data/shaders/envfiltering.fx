//
// Screen Space Ambient Occlusion shader
//
// Author: Nasim BOUGUERRA
// 

#include "common.fx"

TextureCube inputTexture : register(t0);

cbuffer EnvFilteringBuffer : register(b0)
{
	float4 normalRoughness;
	float4 upSize;
	float sampleCount;
}

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
	return output;
}

float radicalInverse_VdC(uint bits)
{
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10f; // / 0x100000000
}

float2 Hammersley(uint i, uint N)
{
	return float2(float(i) / float(N), radicalInverse_VdC(i));
}

float3 ImportanceSampleGGX(float2 vXi, float fRoughness, float3 vNormal)
{
	// Compute the local half vector
	float fA = fRoughness * fRoughness;
	float fPhi = 2.0f * PI * vXi.x;
	float fCosTheta = sqrt((1.0f - vXi.y) / (1.0f + (fA*fA - 1.0f) * vXi.y));
	float fSinTheta = sqrt(1.0f - fCosTheta * fCosTheta);
	float3 vHalf;
	vHalf.x = fSinTheta * cos(fPhi);
	vHalf.y = fSinTheta * sin(fPhi);
	vHalf.z = fCosTheta;

	return vHalf;
}

float D_GGX(float a, float NdH)
{
	float alpha2 = a * a;
	float sum = ((NdH * NdH) * (alpha2 - 1.0) + 1.0);
	float denom = PI * sum * sum;
	return max(alpha2 / denom, 0.01);
}


float3 PS(VS_OUTPUT input) : SV_Target
{
	float3 up = normalize(upSize.xyz);
	float EnvMapSize = upSize.w;

	float3 vNormal = normalize(normalRoughness.xyz);
	float3 vView = normalize(vNormal - up.xyz * (input.TexCoord.y - 0.5) * 2.0 + cross(up.xyz, vNormal) * (input.TexCoord.x - 0.5) * 2.0);
	vNormal = vView;


	float fRoughness = normalRoughness.w;

	if (fRoughness < 0.01)
	{
		return inputTexture.SampleLevel(LinearWrapSampler, vView, 0.0).rgb;
	}

	// Compute a matrix to rotate the samples
	float3 vTangentY = abs(vNormal.z) < 0.999f ? float3(0.0f, 0.0f, 1.0f) : float3(1.0f, 0.0f, 0.0f);
	float3 vTangentX = normalize(cross(vTangentY, vNormal));
	vTangentY = cross(vNormal, vTangentX);

	float3x3 mTangentToWorld = float3x3(
		vTangentX,
		vTangentY,
		vNormal);

	float3 vPrefilteredColor;
	float fTotalWeight = 0.0f;
	uint iNumSamples = sampleCount;
	
	for (uint i = 0; i < iNumSamples; i++)
	{
		float2 vXi = Hammersley(i, iNumSamples);

		float3 vHalf = mul(ImportanceSampleGGX(vXi, fRoughness, vNormal), mTangentToWorld);
		float3 vLight = 2.0f * dot(vView, vHalf) * vHalf - vView;

		float fNdotL = saturate(dot(vNormal, vLight));
		if (fNdotL > 0.0f)
		{
			// Vectors to evaluate pdf
			float fNdotH = saturate(dot(vNormal, vHalf));
			float fVdotH = saturate(dot(vView, vHalf));

			// Probability Distribution Function
			float fPdf = D_GGX(max(fRoughness, 0.01), fNdotH) * fNdotH / (4.0f * fVdotH);

			// Solid angle represented by this sample
			float fOmegaS = 1.0 / (iNumSamples * fPdf);

			// Solid angle covered by 1 pixel with 6 faces that are EnvMapSize X EnvMapSize
			float fOmegaP = 4.0 * PI / (6.0 * EnvMapSize * EnvMapSize);
			// Original paper suggest biasing the mip to improve the results
			float fMipBias = 1.0f;
			float fMipLevel = max(0.5 * log2(fOmegaS / fOmegaP) + fMipBias, 0.0f);
			vPrefilteredColor += inputTexture.SampleLevel(LinearWrapSampler, vLight, fMipLevel).rgb * fNdotL;
			fTotalWeight += fNdotL;
		}
	}
	return vPrefilteredColor / fTotalWeight;
}
