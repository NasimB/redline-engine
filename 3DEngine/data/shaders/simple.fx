#include "common.fx"

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL0;
};

#if REDLINE_VERTEX
	cbuffer WorldMatrixBuffer : register(b0)
	{
		matrix World;
	}

	cbuffer ViewMatrixBuffer : register(b1)
	{
		matrix View;
	}

	cbuffer ProjMatrixBuffer : register(b2)
	{
		matrix Projection;
	}

	struct VS_INPUT
	{
		float4 Pos : POSITION;
		float2 TexCoord : TEXCOORD0;
		float3 Normal : NORMAL0;
	};

	VS_OUTPUT VS(VS_INPUT input)
	{
		VS_OUTPUT output;
		output.Pos = mul(Projection, mul(View, mul(World, input.Pos)));
		output.TexCoord = input.TexCoord;
		output.Normal = mul(World, float4(input.Normal, 0.0)).xyz;
		return output;
	}
#endif

#if REDLINE_PIXEL
	cbuffer MaterialBuffer : register(b0)
	{
		float3 diffuseColor;
		float roughness;
		float metallic;
		bool hasDiffuseMap;
		bool hasNormalMap;
		bool hasSpecularMap;
	}

	cbuffer SkyboxPSBuffer : register(b1)
	{
		float2 g; // X: G, Y: G^2
		float3 lightDirection;
		float3 cameraPosition;
	}

	Texture2D diffuseTexture : register(t0);

	float4 PS(VS_OUTPUT input) : SV_Target
	{
		float4 diffuse = float4(diffuseColor, 1.0);
		if (hasDiffuseMap)
		{
			diffuse *= pow(diffuseTexture.Sample(LinearWrapSampler, input.TexCoord), 2.2);
			clip(diffuse.a - 0.1);
		}
		return float4(diffuse.rgb * max(dot(lightDirection, input.Normal) * saturate((0.1 + lightDirection.y) * 5.0), 0.02), 1.0);
	}
#endif
