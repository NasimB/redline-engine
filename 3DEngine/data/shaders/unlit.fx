Texture2D baseTexture : register(t0);
SamplerState baseSampler : register(s0);


struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
	return baseTexture.Sample(baseSampler, input.TexCoord);
}
