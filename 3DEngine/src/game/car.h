#pragma once

#include "../engine/gameobject.h"
#include "../engine/light.h"
#include "../engine/material.h"
#include "../engine/texture.h"
#include "../engine/math.h"
#include "../engine/Rigidbody.h"

namespace RedlineGame
{
	struct WheelInfo
	{
		btVector3 contactPoint;
		btVector3 rel_pos;
		float angularVelocity;
		float direction;
		float load;
		float slipRatio;
		float slipAngle;
		float travel;
		bool isGrounded;
	};

	enum Wheel
	{
		FRONT_LEFT,
		FRONT_RIGHT,
		REAR_LEFT,
		REAR_RIGHT
	};

	enum Transmission
	{
		FWD,
		RWD,
		AWD
	};

	class Car : public Redline::GameObject
	{
	public:
		Car();
		~Car();

		void InitCar();
		void Update(float delta_time) override;
		void OnPhysicsUpdate(float delta_time);
		void Shutdown() const;

		void Reset();

		void SetLightsState(const bool state) { m_lights_enabled = state; }

		float GetRPM() const { return m_rpm; }

		GameObject* GetBody() const { return m_body; }

	private:
		void UpdateInputs(float delta_time);
		void UpdateGearbox(float delta_time);
		void UpdateEngine(float delta_time);
		void UpdateSuspensions(float delta_time);

		static float ComputeLongitudinalForce(float slip_ratio, float load, float& Fx0);
		static float ComputeLateralForce(float slip_angle, float load);
		static float ComputeAligingMoment(float slip_angle, float load);

		void ApplyDriveTorque(int wheelIndex, float delta_time);

		GameObject* m_body;

		GameObject* m_wheels[4];

		btGeneric6DofSpringConstraint* wheelConstraints[4];

		const DirectX::SimpleMath::Vector3 positions[4] =
		{
			DirectX::SimpleMath::Vector3(1.45f, 0.0f, 2.44f),
			DirectX::SimpleMath::Vector3(-1.45f, 0.0f, 2.44f),
			DirectX::SimpleMath::Vector3(1.45f, 0.0f, -2.33f),
			DirectX::SimpleMath::Vector3(-1.45f, 0.0f, -2.33f)
		};

		const int numWheels = 4;

		float m_force_feedback_intensity = 100.0f;

		int m_transmissionType = RWD;
		float m_AWDTorqueSplitFront = 0.45f;

		const float m_wheelRadius = 0.6f;
		const float m_wheelWidth = 0.3f;
		const float m_wheelMass = 15.0f;
		const float m_wheelInertia = 0.5f * m_wheelMass * (m_wheelRadius * m_wheelRadius);

		const float m_suspensionMinLength = 0.65f;
		const float m_suspensionMaxLength = 1.15f;

		const float m_maxSuspensionForce = 50000.0;

		float m_suspensionRestLength_front = 0.805f;
		float m_suspensionStiffness_front = 68.529f;
		float m_suspensionCompression_front = 5826.243f;
		float m_suspensionDamping_front = 3484.135f;
		
		float m_suspensionRestLength_rear = 0.839f;
		float m_suspensionStiffness_rear = 62.553f;
		float m_suspensionCompression_rear = 1292.162f;
		float m_suspensionDamping_rear = 4084.675f;

		float m_antiroll_stiffness = 3000.0f;

		const float m_mass = 1164.0f;

		const float cDrag = 0.4257f;
		const float cRr = 12.8f;

		float cEngineTorque = 270.0f;
		float cBrakeTorque = 1800.0f;

		bool automaticGearbox = true;
		const int numGears = 6;
		const float gearRatios[8] = {-2.900f, 0.000f, 3.321f, 1.902f, 1.308f, 1.000f, 0.759f, 0.600f };

		const float diffRatio = 10.00f;
		const float transmissionEfficiency = 0.7f;

		unsigned short m_currentGear = 1;
		float m_automatic_shifting_cooldown = 0.0f;

		const float m_maxRPM = 6900.0f;
		float m_rpm = 0.0f;

		/*const float m_flywheel_radius = 0.1397f;
		const float m_flywheel_mass = 8.55f;
		const float m_flywheel_inertia = 0.5f * m_flywheel_mass * (m_flywheel_radius * m_flywheel_radius);*/

		float m_steering_angle = 0.0f;
		bool m_stabilized_steering = true;
		const float m_max_steering_angle = 35.0f * DEG2RAD; // The maximum steering angle of the car
		const float m_optimal_slip_angle = 10.0f * DEG2RAD; // The maximum allowed slip angle when turning, more and the car understeers, not enough and we don't turn optimally.

		float m_clutch = 0.0f;
		float m_throttle = 0.0f;
		float m_brake = 0.0f;
		float m_ebrake = 0.0f;

		float m_speed = 0.0f;

		WheelInfo m_wheelsInfo[4];

		Redline::GameObject* m_rear_light_go;
		Redline::Light* m_rear_light;

		Redline::GameObject* m_front_light_go;
		Redline::Light* m_front_light;

		DirectX::SimpleMath::Vector3 rear_light_position = DirectX::SimpleMath::Vector3(0.0f, 0.2f, -4.05f);
		DirectX::SimpleMath::Vector3 front_light_position = DirectX::SimpleMath::Vector3(0.0f, -0.1f, 4.1f);

		Redline::Material* middleRearLightMaterial = nullptr;
		Redline::Material* rearLightMaterial = nullptr;
		Redline::Texture* rearLightTextureOn = nullptr;
		Redline::Texture* rearLightTextureOff = nullptr;


		bool m_lights_enabled = false;

		int m_display_state = 0;

		DirectX::SimpleMath::Vector3 m_center_of_mass;

		float m_old_ff = 0.0f;

		Redline::Rigidbody* m_rigidbody;
	};
}
