#pragma once

#include "g_camera.h"

namespace Redline
{
	class GameObject;
	class Light;
}

namespace RedlineGame
{
	class Car;

	class Game
	{
	public:
		Game();
		~Game();

		void InitScene();

		bool Initialize();
		bool Update(float delta_time);
		bool OnPhysicsUpdate(float delta_time) const;
		void Reset();
		void Shutdown() const;

		static float RandomValue() { return static_cast<float>(rand()) / RAND_MAX; }

		static bool m_isDebugPanelEnabled;

	private:
		Car* m_car;
		Camera* m_camera;

		Redline::GameObject* m_sun_game_object;
		Redline::Light* m_sun;

		std::vector<Redline::GameObject*> m_objects;
		DirectX::SimpleMath::Vector3 m_sun_direction;
		float m_sun_intensity;

		float time_speed = 1.0f;
	};
}
