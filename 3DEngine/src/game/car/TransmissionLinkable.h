#pragma once
namespace RedlineGame
{
	class TransmissionLinkable
	{
	public:
		TransmissionLinkable();
		~TransmissionLinkable();

		virtual void ApplyUpwardTorque(float torque) = 0;
		virtual void ApplyDownwardTorque(float torque) = 0;
	};
}
