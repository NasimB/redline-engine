#pragma once
namespace RedlineGame
{
	class TransmissionLinkable;

	class TransmissionLink
	{
	public:
		TransmissionLink();
		~TransmissionLink();

		void Link(TransmissionLinkable* linkable);
		void Unlink();

		virtual void ApplyUpwardTorque(float torque) = 0;
		virtual void ApplyDownwardTorque(float torque) = 0;

	private:
		TransmissionLinkable* m_linkable;
	};
}

