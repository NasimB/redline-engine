#include "../../stdafx.h"
#include "TransmissionLink.h"
#include "TransmissionLinkable.h"


RedlineGame::TransmissionLink::TransmissionLink(): 
	m_linkable(nullptr)
{
}


RedlineGame::TransmissionLink::~TransmissionLink()
{
}

void RedlineGame::TransmissionLink::Link(TransmissionLinkable* linkable)
{
	if (linkable != nullptr)
	{
		m_linkable = linkable;
	}
}

void RedlineGame::TransmissionLink::Unlink()
{
	m_linkable = nullptr;
}
