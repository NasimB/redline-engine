#include "car.h"
#include "../engine/engine.h"

using namespace std;
using namespace Redline;
using namespace DirectX::SimpleMath;


RedlineGame::Car::Car()
{
	InitCar();
}


RedlineGame::Car::~Car()
{
}


void RedlineGame::Car::InitCar()
{
	m_center_of_mass = Vector3(0.0f, -0.3f, 0.5f);

	m_body = Engine::CreateGameObject("240SX");

	MeshRenderer* mesh_renderer = m_body->AddComponent<MeshRenderer>();
	mesh_renderer->SetMesh("car");
	//mesh_renderer->SetActive(false);

	m_rigidbody = AddComponent<Rigidbody>();

	m_body->GetTransform()->SetParent(&m_transform);
	m_body->GetTransform()->SetPosition(-m_center_of_mass);

	vector<Material>& mats = mesh_renderer->GetMaterials();
	middleRearLightMaterial = &mats[1];
	rearLightMaterial = &mats[13];
	rearLightTextureOff = ResourcesManager::GetTexture("data\\textures\\240SX\\240sx_kit00_brakelight_off.dds");
	rearLightTextureOn = ResourcesManager::GetTexture("data\\textures\\240SX\\240sx_kit00_brakelight_on.dds");

	btCompoundShape* shape = new btCompoundShape();
	btTransform transform;
	transform.setIdentity();
	transform.setOrigin(btVector3(-m_center_of_mass.x, -m_center_of_mass.y, -m_center_of_mass.z));
	shape->addChildShape(transform, new btBoxShape(btVector3(1.64f, 1.05f, 4.39f)));
	m_rigidbody->CreateCustom(m_mass, shape);

	m_rigidbody->GetRigidbody()->setActivationState(DISABLE_DEACTIVATION);

	for (int i = 0; i < numWheels; ++i)
	{
		GameObject* wheel = Engine::CreateGameObject("wheel (" + to_string(i) + ")");
		wheel->GetTransform()->SetParent(m_body->GetTransform());
		wheel->GetTransform()->SetPosition(positions[i]);

		MeshRenderer* wheel_mesh_renderer = wheel->AddComponent<MeshRenderer>();
		wheel_mesh_renderer->SetMesh("wheel");

		m_wheels[i] = wheel;
		m_wheelsInfo[i].contactPoint = btVector3(0, 0, 0);
		m_wheelsInfo[i].rel_pos = btVector3(0, 0, 0);
		m_wheelsInfo[i].angularVelocity = 0.0f;
		m_wheelsInfo[i].direction = 0.0f;
		m_wheelsInfo[i].load = 0.0f;
		m_wheelsInfo[i].slipRatio = 0.0f;
		m_wheelsInfo[i].slipAngle = 0.0f;
		m_wheelsInfo[i].isGrounded = false;
	}

	// Rear Lights
	{
		m_rear_light_go = new GameObject();
		m_rear_light_go->GetTransform()->SetParent(m_body->GetTransform());
		m_rear_light_go->GetTransform()->SetPosition(rear_light_position);

		m_rear_light = m_rear_light_go->AddComponent<Light>();
		m_rear_light->SetType(LIGHT_SPOT);
		m_rear_light->SetColor(1, 0, 0);
		m_rear_light->SetIntensity(3.0f);
		m_rear_light->SetDirection(Vector3::UnitZ * -1.0f);
		m_rear_light->SetAngle(170.0f);
	}

	// Front Lights
	{
		m_front_light_go = new GameObject();
		m_front_light_go->GetTransform()->SetParent(m_body->GetTransform());
		m_front_light_go->GetTransform()->SetPosition(front_light_position);

		m_front_light = m_front_light_go->AddComponent<Light>();
		m_front_light->SetType(LIGHT_SPOT);
		m_front_light->SetColor(1.0f, 1.0f, 0.8f);
		m_front_light->SetIntensity(10.0f);
		m_front_light->SetDirection(Vector3::UnitZ);
		m_front_light->SetAngle(125.0f);
	}


	//Engine::GetAudio().PlayEvent("{3c343ae9-635a-4c81-868e-c117e68a8f6f}");
}


void RedlineGame::Car::Update(const float delta_time)
{
	UpdateInputs(delta_time);

	//static Vector3 old_car_position(0, 0, 0);
	//Vector3 position = m_transform.GetPosition();
	//Vector3 forward = Vector3::TransformNormal(Vector3(0, 0, 1), m_transform.GetMatrix());
	//Vector3 velocity = (position - old_car_position) / delta_time;
	//old_car_position = position;

	//static float smoothed_rpm = 0.0f;
	//float old_smoothed_rpm = smoothed_rpm;
	//smoothed_rpm = Math::Lerp(smoothed_rpm, m_rpm, 1.0f - Math::Exp(-5.0f * delta_time));
	//float load = ((smoothed_rpm - old_smoothed_rpm) / m_maxRPM) * 10.0f;

	/*Debug::Log("Smoothed RPM: " + to_string(smoothed_rpm));
	Debug::Log("Load: " + to_string(load));

	Engine::GetAudio().SetEventPosition("{3c343ae9-635a-4c81-868e-c117e68a8f6f}", position, forward, Vector3(0, 1.0f, 0.0f), velocity);
	Engine::GetAudio().SetEventParameter("{3c343ae9-635a-4c81-868e-c117e68a8f6f}", "RPM", smoothed_rpm);
	Engine::GetAudio().SetEventParameter("{3c343ae9-635a-4c81-868e-c117e68a8f6f}", "load", load);*/



	if (Input::GetButtonDown(DIK_F2))
	{
		m_display_state++;
		if (m_display_state > 2)
			m_display_state = 0;
	}

	if (m_display_state < 2)
	{
		Vector2 res = Engine::GetRenderer().GetWindowResolution();
		ImGui::SetNextWindowPos(ImVec2(res.x - 160, res.y - 70));
		ImGui::SetNextWindowSize(ImVec2(170, 65));
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoTitleBar
			| ImGuiWindowFlags_NoResize
			| ImGuiWindowFlags_NoMove
			| ImGuiWindowFlags_NoScrollbar
			| ImGuiWindowFlags_NoCollapse;
		if (ImGui::Begin("Car info", nullptr, window_flags))
		{
			ImGui::Text("Speed: %.0f km/h", Math::Abs(m_speed * 3.6f));

			float percent = m_rpm / m_maxRPM;
			string bar = "RPM: ";
			for (float i = 0; i < round(percent * 15.0f); ++i)
			{
				bar += "|";
			}
			ImGui::Text(bar.c_str());

			string gear = "Gear: ";
			switch (m_currentGear)
			{
			case 0:
				gear += "R";
				break;
			case 1:
				gear += "N";
				break;
			default:
				gear += to_string(m_currentGear - 1);
				break;
			}
			ImGui::Text(gear.c_str());
		}
		ImGui::End();
	}

	// LIGHTS
	{
		bool braking = m_brake > 0.0f;

		m_rear_light->SetActive(braking || m_lights_enabled);
		m_rear_light->SetIntensity(braking ? 8.0f : (m_lights_enabled ? 4.0f : 0.0f));

		m_front_light->SetActive(m_lights_enabled);

		middleRearLightMaterial->SetDiffuseTexture(braking ? rearLightTextureOn : rearLightTextureOff);
		rearLightMaterial->SetDiffuseTexture((braking || m_lights_enabled) ? rearLightTextureOn : rearLightTextureOff);

		if (braking)
			rearLightMaterial->SetDiffuseColor(1.0f, 1.0f, 1.0f);
		else
			rearLightMaterial->SetDiffuseColor(0.7f, 0.7f, 0.7f);
	}

	if (m_display_state == 1)
	{
		Vector2 res = Engine::GetRenderer().GetWindowResolution();
		ImGui::SetNextWindowPos(ImVec2(res.x - 530, 0));
		if (ImGui::Begin("Car settings"))
		{
			ImGui::Text("Engine:");
			ImGui::SliderFloat("Engine Torque", &cEngineTorque, 0.0f, 1000.0f);
			ImGui::SliderFloat("Brake Torque", &cBrakeTorque, 0.0f, 2000.0f);

			ImGui::Text(" ");
			ImGui::Text("Transmission:");
			ImGui::RadioButton("FWD", &m_transmissionType, FWD);
			ImGui::SameLine(0.0f, 10.0f);
			ImGui::RadioButton("RWD", &m_transmissionType, RWD);
			ImGui::SameLine(0.0f, 10.0f);
			ImGui::RadioButton("AWD", &m_transmissionType, AWD);
			if (m_transmissionType == AWD)
				ImGui::SliderFloat("AWD Torque Split (front)", &m_AWDTorqueSplitFront, 0.0f, 1.0f);
			ImGui::Checkbox("Automatic shifting", &automaticGearbox);

			ImGui::Text(" ");
			ImGui::Text("Front Suspension:");
			ImGui::SliderFloat("F Suspension Height", &m_suspensionRestLength_front, m_suspensionMinLength, m_suspensionMaxLength);
			ImGui::SliderFloat("F Suspension Stiffness", &m_suspensionStiffness_front, 1, 200);
			ImGui::SliderFloat("F Suspension Compression", &m_suspensionCompression_front, 1, 10000);
			ImGui::SliderFloat("F Suspension Damping", &m_suspensionDamping_front, 1, 10000);
			ImGui::Text(" ");
			ImGui::Text("Rear Suspension:");
			ImGui::SliderFloat("R Suspension Height", &m_suspensionRestLength_rear, m_suspensionMinLength, m_suspensionMaxLength);
			ImGui::SliderFloat("R Suspension Stiffness", &m_suspensionStiffness_rear, 1, 200);
			ImGui::SliderFloat("R Suspension Compression", &m_suspensionCompression_rear, 1, 10000);
			ImGui::SliderFloat("R Suspension Damping", &m_suspensionDamping_rear, 1, 10000);

			if (Input::IsWheelConnected())
			{
				ImGui::Text(" ");
				ImGui::SliderFloat("Force Feedback Intensity", &m_force_feedback_intensity, 0.0f, 1000.0f);
			}

			ImGui::Text(" ");
			ImGui::SliderFloat("Anti-roll bar stiffness", &m_antiroll_stiffness, 0.0f, 10000.0f);
		}
		ImGui::End();
	}
}


void RedlineGame::Car::OnPhysicsUpdate(const float delta_time)
{
	UpdateSuspensions(delta_time);
	UpdateGearbox(delta_time);
	UpdateEngine(delta_time);
}


void RedlineGame::Car::UpdateInputs(const float delta_time)
{
	if (Input::IsWheelConnected())
	{
		DIJOYSTATE2 wheelState = Input::GetWheelState();
		m_steering_angle = -wheelState.lX / 1000.0f * m_max_steering_angle;
		m_throttle = 0.5f + -wheelState.lY / 2000.0f;
		if (m_throttle < 0.05f) m_throttle = 0.0f;
		m_brake = 0.5f + -wheelState.lRz / 2000.0f;
		if (m_brake < 0.05f) m_brake = 0.0f;

		if (Input::GetWheelButtonPressed(6))
		{
			m_ebrake = 1.0f;
		}
		else if (m_speed > 1.0f || m_throttle > 0.1f && m_currentGear != 1)
		{
			m_ebrake = 0.0f;
		}

		if (!automaticGearbox)
		{
			if (Input::GetWheelButtonPressed(14))
				m_currentGear = 0;
			else if (Input::GetWheelButtonPressed(8))
				m_currentGear = 2;
			else if (Input::GetWheelButtonPressed(9))
				m_currentGear = 3;
			else if (Input::GetWheelButtonPressed(10))
				m_currentGear = 4;
			else if (Input::GetWheelButtonPressed(11))
				m_currentGear = 5;
			else if (Input::GetWheelButtonPressed(12))
				m_currentGear = Math::Min(6, numGears + 1);
			else if (Input::GetWheelButtonPressed(13))
				m_currentGear = Math::Min(7, numGears + 1);
			else
				m_currentGear = 1;
		}
		else
		{
			if (Input::GetWheelButtonDown(5))
			{
				m_currentGear = Math::Max(m_currentGear - 1, 0);
			}

			if (Input::GetWheelButtonDown(4))
			{
				m_currentGear = Math::Min(m_currentGear + 1, numGears + 1);
			}
		}
	}
	else
	{
		bool stabilized = m_stabilized_steering && (m_wheelsInfo[FRONT_LEFT].isGrounded || m_wheelsInfo[FRONT_RIGHT].isGrounded) && m_speed > 10.0f;

		float desired_steering = 0.0f;

		if(stabilized)
		{
			float slip_angle = (m_wheelsInfo[FRONT_LEFT].slipAngle + m_wheelsInfo[FRONT_RIGHT].slipAngle) * 0.5f * DEG2RAD; // Get the average slip of the two front wheels

			float rest_angle = m_steering_angle - slip_angle; // Steering angle at which the tire isn't stressed laterally.

			desired_steering = rest_angle;

			if (Input::GetButtonPressed(DIK_LEFT))
			{
				desired_steering += m_optimal_slip_angle;
			}
			else if (Input::GetButtonPressed(DIK_RIGHT))
			{
				desired_steering -= m_optimal_slip_angle;
			}

			desired_steering = Math::Clamp(desired_steering, -m_max_steering_angle, m_max_steering_angle);
		}
		else
		{
			if (Input::GetButtonPressed(DIK_LEFT))
			{
				desired_steering = m_max_steering_angle;
			}
			else if (Input::GetButtonPressed(DIK_RIGHT))
			{
				desired_steering = -m_max_steering_angle;
			}
		}

		m_steering_angle = Math::Lerp(m_steering_angle, desired_steering, 1.0f - Math::Exp(-4.0f * delta_time));

		m_throttle = Input::GetButtonPressed(DIK_UP) ? 1.0f : 0.0f;
		m_brake = Input::GetButtonPressed(DIK_DOWN) ? 1.0f : 0.0f;

		if (Input::GetButtonPressed(DIK_SPACE))
		{
			m_ebrake = 1.0f;
		}
		else if (m_speed > 1.0f || m_throttle > 0.1f && m_currentGear != 1)
		{
			m_ebrake = 0.0f;
		}
	}

	if (Input::GetButtonDown(DIK_Z))
	{
		m_currentGear = Math::Max(m_currentGear - 1, 0);
	}

	if (Input::GetButtonDown(DIK_X))
	{
		m_currentGear = Math::Min(m_currentGear + 1, numGears + 1);
	}
}


void RedlineGame::Car::UpdateGearbox(const float delta_time)
{
	if (!automaticGearbox)
		return;

	m_automatic_shifting_cooldown += delta_time;

	if (m_automatic_shifting_cooldown < 0.2f)
		return;

	int index = FRONT_LEFT;
	if (m_transmissionType == RWD) index = REAR_LEFT;
	bool isGrounded = m_wheelsInfo[index].isGrounded || m_wheelsInfo[index + 1].isGrounded;

	if (!isGrounded)
		return;

	float slipRatio = Math::Max(m_wheelsInfo[index].slipRatio, m_wheelsInfo[index + 1].slipRatio);

	float speedWheel = Math::Max(m_wheelsInfo[index].angularVelocity, m_wheelsInfo[index + 1].angularVelocity);
	float rpmLowerGear = m_maxRPM;

	if (m_currentGear > 2)
		rpmLowerGear = (speedWheel * gearRatios[m_currentGear - 1] * diffRatio * 60.0f) / (2.0f * float(M_PI));

	if (rpmLowerGear < m_maxRPM * 0.80f)
	{
		m_currentGear--;
		m_automatic_shifting_cooldown = 0.0f;
	}
	else if (m_rpm > m_maxRPM * 0.90f && m_currentGear < (numGears + 1) && m_currentGear > 1 && slipRatio < 0.20f) // Don't shift up if tires don't have enough grip
	{
		m_currentGear++;
		m_automatic_shifting_cooldown = 0.0f;
	}
}


void RedlineGame::Car::UpdateEngine(const float delta_time)
{
	btRigidBody* rigidbody = m_rigidbody->GetRigidbody();
	btTransform chassis_transform = rigidbody->getWorldTransform();

	btVector3 forward = chassis_transform(btVector3(0, 0, 1)) - chassis_transform.getOrigin();
	forward.normalize();

	btVector3 velocity = rigidbody->getLinearVelocity();

	if (velocity.length2() > btScalar(0.0))
	{
		btVector3 velocityNorm = velocity.normalized();
		m_speed = forward.dot(velocity);

		btScalar fDrag = -cDrag * m_speed * m_speed; // Drag resistance
		btScalar fRr = 0.0f; // -cRr * m_speed; // Rolling resistance
		rigidbody->applyImpulse(velocityNorm * (fDrag + fRr) * delta_time, btVector3(0, 0, 0));
	}

	int index = FRONT_LEFT;
	if (m_transmissionType == RWD) index = REAR_LEFT;
	float speedWheel = Math::Max(m_wheelsInfo[index].angularVelocity, m_wheelsInfo[index + 1].angularVelocity);
	m_rpm = (speedWheel * gearRatios[m_currentGear] * diffRatio * 60.0f) / (2.0f * float(M_PI));
	m_rpm = Math::Clamp(m_rpm, 1000.0f, m_maxRPM);

	float Mz = 0.0f;

	for (int i = 0; i < numWheels; ++i)
	{
		btQuaternion bullet_chassis_rotation = chassis_transform.getRotation();
		Quaternion chassis_rotation(bullet_chassis_rotation.x(), bullet_chassis_rotation.y(), bullet_chassis_rotation.z(), bullet_chassis_rotation.w());

		if (i <= FRONT_RIGHT) // Front wheels only
		{
			chassis_rotation = Quaternion::CreateFromYawPitchRoll(m_steering_angle, 0, 0) * chassis_rotation;
		}

		//rot = Quaternion::CreateFromYawPitchRoll(0.0f, 0, -5.0f * DEG2RAD) * rot; // Camber

		Vector3 wheelForward = Vector3::Transform(Vector3(0, 0, 1), chassis_rotation);
		wheelForward.Normalize();
		btVector3 btWheelForward(wheelForward.x, wheelForward.y, wheelForward.z);

		btVector3 wheelVelocity = rigidbody->getVelocityInLocalPoint(m_wheelsInfo[i].rel_pos);
		float speedAtWheel = btWheelForward.dot(wheelVelocity);

		ApplyDriveTorque(i, delta_time);

		if (m_brake > 0.01f)
		{
			float brakeBalance = i > FRONT_RIGHT ? 0.3f : 0.7f;
			float wheelSign = Math::Sign(m_wheelsInfo[i].angularVelocity);
			float torque = wheelSign * cBrakeTorque * brakeBalance * m_brake; // Brake torque
			m_wheelsInfo[i].angularVelocity -= (torque / m_wheelInertia) * delta_time;

			if (Math::Sign(m_wheelsInfo[i].angularVelocity) != wheelSign)
			{
				m_wheelsInfo[i].angularVelocity = 0.0f;
			}
		}


		if (m_ebrake > 0.01f && i > FRONT_RIGHT)
		{
			float wheelSign = Math::Sign(m_wheelsInfo[i].angularVelocity);
			float torque = wheelSign * cBrakeTorque * 2.0f * m_ebrake; // E-Brake torque
			m_wheelsInfo[i].angularVelocity -= (torque / m_wheelInertia) * delta_time;

			if (Math::Sign(m_wheelsInfo[i].angularVelocity) != wheelSign)
			{
				m_wheelsInfo[i].angularVelocity = 0.0f;
			}
		}


		// Clamp Math::Max angularVelocity
		if (m_currentGear > 1)
		{
			float maxAngularVelocity = m_maxRPM * (2.0f * M_PI) / gearRatios[m_currentGear] / diffRatio / 60.0f;
			m_wheelsInfo[i].angularVelocity = Math::Clamp(m_wheelsInfo[i].angularVelocity, 0.0f, maxAngularVelocity);
		}
		else if (m_currentGear == 0)
		{
			float maxAngularVelocity = m_maxRPM * (2.0f * M_PI) / gearRatios[m_currentGear] / diffRatio / 60.0f;
			m_wheelsInfo[i].angularVelocity = Math::Clamp(m_wheelsInfo[i].angularVelocity, maxAngularVelocity, 0.0f);
		}


		if (m_wheelsInfo[i].isGrounded)
		{
			Vector3 wheelRight = Vector3::Transform(Vector3(1, 0, 0), chassis_rotation);
			wheelRight.Normalize();
			btVector3 btWheelRight(wheelRight.x, wheelRight.y, wheelRight.z);

			float wheelSpeed = m_wheelsInfo[i].angularVelocity * m_wheelRadius;

			float slipRatio = Math::Clamp((wheelSpeed - speedAtWheel) / Math::Abs(speedAtWheel), -2.0f, 2.0f);
			m_wheelsInfo[i].slipRatio = slipRatio;
			float Fx0 = 0.0f;
			float fLong = Math::Abs(slipRatio) > EPSILON ? ComputeLongitudinalForce(slipRatio, m_wheelsInfo[i].load, Fx0) : 0.0f;


			float vel = btWheelRight.dot(wheelVelocity);
			float velSign = vel / -Math::Abs(vel);
			float slipAngle = Math::Clamp(btWheelForward.angle(wheelVelocity) * float(RAD2DEG) * velSign, -45.0f, 45.0f);
			m_wheelsInfo[i].slipAngle = slipAngle;

			float fLat = Math::Abs(slipAngle) > EPSILON ? ComputeLateralForce(slipAngle, m_wheelsInfo[i].load) : 0.0f;

			//float weight = i <= FRONT_RIGHT ? 1.0f : 0.0f;
			if (i <= FRONT_RIGHT)
				Mz += ComputeAligingMoment(slipAngle, m_wheelsInfo[i].load);// *weight;


			// Combine forces
			//if (speedAtWheel > 10.0f) // Small old hotfix for low speed, seems fixed but commented just in case
			{
				float fXScaled = fLong / Fx0;
				float fLatMax = fLat * sqrt(1.0f - fXScaled);
				fLat = Math::Min(fLat, fLatMax);
			}

			btVector3 impulse(fLong, 0, fLat);

			float torque = impulse.x() * m_wheelRadius;
			m_wheelsInfo[i].angularVelocity -= (torque / m_wheelInertia) * delta_time;

			if (impulse.length() > btScalar(0.00001))
			{
				impulse = btWheelForward * impulse.x() + btWheelRight * impulse.z();
				rigidbody->applyImpulse(impulse * delta_time, m_wheelsInfo[i].rel_pos);
			}

			if (m_display_state == 1)
			{
				btVector3 btStart = rigidbody->getCenterOfMassPosition() + m_wheelsInfo[i].rel_pos;
				Vector3 start(btStart.x(), btStart.y(), btStart.z());
				Vector3 end = start + wheelRight * fLat * 0.001f;
				Debug::Line(start, end, Vector3(1, 0, 0));

				end = start + wheelForward * fLong * 0.001f;
				Debug::Line(start, end, Vector3(0, 0, 1));
			}
		}


		m_wheelsInfo[i].direction += m_wheelsInfo[i].angularVelocity * delta_time;

		Quaternion wheel_rotation;

		if (i == FRONT_LEFT || i == REAR_LEFT)
		{
			wheel_rotation = Quaternion::CreateFromAxisAngle(Vector3::UnitX, -m_wheelsInfo[i].direction);
			wheel_rotation *= Quaternion::CreateFromAxisAngle(Vector3::UnitY, float(M_PI));
		}
		else
		{
			wheel_rotation = Quaternion::CreateFromAxisAngle(Vector3::UnitX, m_wheelsInfo[i].direction);
		}

		if (i < REAR_LEFT)
		{
			wheel_rotation *= Quaternion::CreateFromAxisAngle(Vector3::UnitY, m_steering_angle);
		}

		m_wheels[i]->GetTransform()->SetRotation(wheel_rotation);
	}

	if (Input::IsWheelConnected())
	{
		float multiplier = Math::Lerp(0.5f, 1.0f, Math::Min(Math::Abs(m_speed) / 10.0f, 1.0f));
		m_old_ff = Math::Lerp(m_old_ff, Mz * m_force_feedback_intensity * multiplier, 1.0f - Math::Exp(delta_time * -20.0f));
		Input::ApplyForceFeedback(Vector2(m_old_ff, 0));
	}
}


void RedlineGame::Car::UpdateSuspensions(const float delta_time)
{
	btRigidBody* rigidbody = m_rigidbody->GetRigidbody();
	btTransform chassis_transform = rigidbody->getWorldTransform();

	btVector3 down = chassis_transform(btVector3(0, -1, 0)) - chassis_transform.getOrigin();
	down.normalize();

	for (int i = 0; i < numWheels; ++i)
	{
		Vector3 startPos = positions[i] - m_center_of_mass;
		btVector3 from = chassis_transform(btVector3(startPos.x, startPos.y, startPos.z));
		btVector3 to = from + down * (m_suspensionMaxLength + m_wheelRadius);

		btCollisionWorld::ClosestRayResultCallback ray(from, to);
		Engine::GetPhysics().GetWorld()->rayTest(from, to, ray);

		m_wheelsInfo[i].isGrounded = ray.hasHit();

		m_wheelsInfo[i].load = 0.0f;

		if (ray.hasHit())
		{
			btScalar distance = ray.m_closestHitFraction * (m_suspensionMaxLength + m_wheelRadius);
			btScalar length = Math::Max(Math::Min(distance - m_wheelRadius, m_suspensionMaxLength), m_suspensionMinLength);

			m_wheelsInfo[i].travel = (length - m_suspensionMinLength) / m_suspensionMaxLength;

			m_wheels[i]->GetTransform()->SetPosition(positions[i] + Vector3(0.0f, -length, 0.0f));

			btVector3 btLocalPos = btVector3(startPos.x, startPos.y - distance, startPos.z);
			m_wheelsInfo[i].contactPoint = btLocalPos;

			btScalar suspensionForce;
			btVector3 relPos = (from + down * distance) - rigidbody->getCenterOfMassPosition();
			m_wheelsInfo[i].rel_pos = relPos;

			if (length - m_suspensionMinLength < EPSILON)
			{
				suspensionForce = m_maxSuspensionForce;
			}
			else
			{
				const btScalar suspensionRestLength = i > FRONT_RIGHT ? m_suspensionRestLength_rear : m_suspensionRestLength_front;
				const btScalar suspensionStiffness = i > FRONT_RIGHT ? m_suspensionStiffness_rear : m_suspensionStiffness_front;
				const btScalar suspensionCompression = i > FRONT_RIGHT ? m_suspensionCompression_rear : m_suspensionCompression_front;
				const btScalar suspensionDamping = i > FRONT_RIGHT ? m_suspensionDamping_rear : m_suspensionDamping_front;

				// Spring
				{
					btScalar length_diff = (suspensionRestLength - length);
					suspensionForce = (suspensionStiffness * length_diff) * m_mass;
				}

				// Damping
				{
					btVector3 chassis_velocity_at_contactPoint = rigidbody->getVelocityInLocalPoint(relPos);
					btScalar verticalVelocity = -down.dot(chassis_velocity_at_contactPoint);

					if (verticalVelocity < 0.0)
					{
						suspensionForce -= verticalVelocity * suspensionCompression;
					}
					else
					{
						suspensionForce -= verticalVelocity * suspensionDamping;
					}
				}

				suspensionForce = Math::Max(Math::Min(suspensionForce, m_maxSuspensionForce), -m_maxSuspensionForce);
			}

			m_wheelsInfo[i].load = suspensionForce;

			//btVector3 impulse = down * -suspensionForce * delta_time;
			//m_rigidbody->applyImpulse(impulse, relPos);
		}
		else
		{
			m_wheels[i]->GetTransform()->SetPosition(positions[i] + Vector3(0.0f, -m_suspensionMaxLength, 0.0f));
		}
	}


	// Anti-roll bar
	for (int i = 0; i < numWheels; i += 2)
	{
		float travelL = 1.0f;
		float travelR = 1.0f;

		bool groundedL = m_wheelsInfo[i].isGrounded;
		if (groundedL)
			travelL = m_wheelsInfo[i].travel;

		bool groundedR = m_wheelsInfo[i + 1].isGrounded;
		if (groundedR)
			travelR = m_wheelsInfo[i + 1].travel;

		float antiRollForce = (travelL - travelR) * m_antiroll_stiffness;

		m_wheelsInfo[i].load += down.dot(btVector3(0, -1, 0)) * m_wheelMass * 9.81f;
		m_wheelsInfo[i + 1].load += down.dot(btVector3(0, -1, 0)) * m_wheelMass * 9.81f;

		if (groundedL)
			m_wheelsInfo[i].load -= antiRollForce;

		if (groundedR)
			m_wheelsInfo[i + 1].load += antiRollForce;

#ifdef _DEBUG
		if (m_display_state == 1)
		{
			btQuaternion bullet_chassis_rotation = chassis_transform.getRotation();
			Quaternion chassis_rotation(bullet_chassis_rotation.x(), bullet_chassis_rotation.y(), bullet_chassis_rotation.z(), bullet_chassis_rotation.w());

			Vector3 up = Vector3::Transform(Vector3::UnitY, chassis_rotation);

			btVector3 contact_point_l = m_wheelsInfo[i].rel_pos + chassis_transform.getOrigin();
			Vector3 start_l(contact_point_l.x(), contact_point_l.y(), contact_point_l.z());
			start_l += up * m_wheelRadius;

			btVector3 contact_point_r = m_wheelsInfo[i + 1].rel_pos + chassis_transform.getOrigin();
			Vector3 start_r(contact_point_r.x(), contact_point_r.y(), contact_point_r.z());
			start_r += up * m_wheelRadius;

			Debug::Line(start_l, start_l + up * m_wheelsInfo[i].load * 0.001f, Vector3(0, 1, 0));
			Debug::Line(start_r, start_r + up * m_wheelsInfo[i + 1].load * 0.001f, Vector3(0, 1, 0));
		}
#endif

		btVector3 impulse = down * -m_wheelsInfo[i].load * delta_time;
		Vector3 start_pos = positions[i];
		btVector3 rel_pos = chassis_transform(btVector3(start_pos.x, start_pos.y, start_pos.z)) - chassis_transform.getOrigin();
		rigidbody->applyImpulse(impulse, rel_pos);

		impulse = down * -m_wheelsInfo[i + 1].load * delta_time;
		start_pos = positions[i + 1];
		rel_pos = chassis_transform(btVector3(start_pos.x, start_pos.y, start_pos.z)) - chassis_transform.getOrigin();
		rigidbody->applyImpulse(impulse, rel_pos);
	}
}


float RedlineGame::Car::ComputeLongitudinalForce(const float slip_ratio, const float load, float& Fx0)
{
	/*float cTraction = 1.0f / 5.0f;
	float fLong = cTraction * slipRatio * load;
	fLong = Math::Max(Math::Min(fLong, load * 1), load * -1);
	return fLong;*/

	/// PACEJKA 96 Fx  (MF 5.2)
	float b0 = 1.36f;
	float b1 = -40.0f;
	float b2 = 1500.0f; // 1275.0f;
	float b3 = 40.0f;
	float b4 = 240.0f;
	float b5 = 0.08f;
	float b6 = -0.05f;
	float b7 = 0.05f;
	float b8 = -0.025f;
	float b9 = 0.015f;
	float b10 = 0.4f;
	float b11 = -50.0f;
	float b12 = 0.0f;

	// Calculate derived coefficients
	float slipPercentage = slip_ratio * 100.0f;
	float Fz = Math::Clamp(load, 500.0f, 10000.0f) / 1000.0f; // Load as KiloNewtons
	float FzSquared = Fz * Fz;
	float C = b0;
	float uP = b1 * Fz + b2;
	float D = uP * Fz;

	// Avoid div by 0
	float B;
	if ((C > -EPSILON && C < EPSILON) || (D > -EPSILON && D < EPSILON))
	{
		B = 99999.0f;
	}
	else
	{
		B = ((b3 * FzSquared + b4 * Fz) * expf(-b5 * Fz)) / (C * D);
	}

	float E = b6 * FzSquared + b7 * Fz + b8;
	float Sh = b9 * Fz + b10;
	float Sv = b11 * Fz + b12;

	// Notice that product BCD is the longitudinal tire stiffness
	//float longStiffness = B * C * D;

	// Remember the Math::Max longitudinal force (where Math::Sin(...)==1)
	Fx0 = D + Sv;

	// Calculate result force
	float Fx = D * Math::Sin(C * Math::Atan(B * (1.0f - E) * (slipPercentage + Sh) + E * Math::Atan(B * (slipPercentage + Sh)))) + Sv;

	return Fx;
}


float RedlineGame::Car::ComputeLateralForce(const float slip_angle, const float load)
{
	/*float cStiffness = 1.0f / 15.0f; // 6 for slick - 15 for poor road tires
	float fLat = cStiffness * slipAngle * load;
	fLat = Math::Max(Math::Min(fLat, load * 1.05f), load * -1.05f);
	return fLat;*/

	float camber = 0.00f;

	/// PACEJKA 96 Fy (MF 5.2)
	float a0 = 1.28f;
	float a1 = -28.0f;
	float a2 = 1500.0f; // 1275.0f;
	float a3 = 1900.0f;
	float a4 = 8.0f;
	float a5 = 0.015f;
	float a6 = -0.25f;
	float a7 = 0.1f;
	float a8 = -0.03f;
	float a9 = -0.001f;
	float a10 = -0.15f;
	float a111 = 0.0f;
	float a112 = -0.29f;
	float a12 = 17.8f;
	float a13 = -2.4f;

	float Fz = Math::Clamp(load, 500.0f, 10000.0f) / 1000.0f; // Load as KiloNewtons

	// Calculate derived coefficients
	float C = a0;
	float uP = a1*Fz + a2;
	float D = uP*Fz;
	float E = a6*Fz + a7;

	float B;
	// Avoid div by 0
	if ((C > -EPSILON && C < EPSILON) || (D > -EPSILON && D < EPSILON))
	{
		B = 99999.0f;
	}
	else
	{
		if (a4 > -EPSILON && a4 < EPSILON)
		{
			B = 99999.0f;
		}
		else
		{
			// Notice that product BCD is the lateral stiffness (=cornering)
			float latStiffness = a3 * Math::Sin(2 * Math::Atan(Fz / a4)) * (1.0f - a5 * fabs(camber));
			B = latStiffness / (C * D);
		}
	}

	float Sh = a8 * camber + a9 * Fz + a10;
	float Sv = (a111 * Fz + a112) * camber * Fz + a12 * Fz + a13;

	// Remember maximum lateral force
	//maxForce.y = D + Sv;

	// Calculate result force
	float Fy = D * Math::Sin(C * Math::Atan(B * (1.0f - E) * (slip_angle + Sh) + E * Math::Atan(B * (slip_angle + Sh)))) + Sv;

	return Fy;
}


float RedlineGame::Car::ComputeAligingMoment(const float slip_angle, const float load)
{
	float camber = 0.0f;
	float Fz = load / 1000.0f; // Load as KiloNewtons

	float c0 = 2.315f;
	float c1 = -4.0f;
	float c2 = -3.0f;
	float c3 = -1.6f;
	float c4 = -6.0f;
	float c5 = 0.0f;
	float c6 = 0.0f;
	float c7 = 0.02f;
	float c8 = -0.58f;
	float c9 = 0.18f;
	float c10 = 0.043f;
	float c11 = 0.048f;
	float c12 = -0.0035f;
	float c13 = -0.18f;
	float c14 = 0.14f;
	float c15 = -1.029f;
	float c16 = 0.27f;
	float c17 = -1.1f;

	float B;

	// Calculate derived coefficients
	float FzSquared = Fz*Fz;
	float C = c0;
	float D = c1*FzSquared + c2*Fz;
	float E = (c7*FzSquared + c8*Fz + c9)*(1 - c10*fabs(camber));
	if ((C > -EPSILON && C < EPSILON) ||
		(D > -EPSILON && D < EPSILON))
	{
		B = 99999.0f;
	}
	else
	{
		B = ((c3*FzSquared + c4*Fz)*(1 - c6*fabs(camber))*expf(-c5*Fz)) / (C*D);
	}
	float Sh = c11*camber + c12*Fz + c13;
	float Sv = (c14*FzSquared + c15*Fz)*camber + c16*Fz + c17;

	float Mz = D*Math::Sin(C*Math::Atan(B*(1.0f - E)*(slip_angle + Sh) +
		E*Math::Atan(B*(slip_angle + Sh)))) + Sv;

	return Mz;
}


void RedlineGame::Car::ApplyDriveTorque(const int wheel_index, const float delta_time)
{
	if (wheel_index > FRONT_RIGHT && m_transmissionType == FWD) return;
	if (wheel_index < REAR_LEFT && m_transmissionType == RWD) return;

	float scale = m_transmissionType == AWD
		? 0.25f * (wheel_index < REAR_LEFT ? m_AWDTorqueSplitFront : 1.0f - m_AWDTorqueSplitFront)
		: 0.5f;
	float torque = cEngineTorque * gearRatios[m_currentGear] * diffRatio * transmissionEfficiency * scale * m_throttle;
	// Drive torque

	float oppositeWheelAngularVelocity;
	float wheelAngularVelocity = m_wheelsInfo[wheel_index].angularVelocity;

	switch (wheel_index)
	{
	case FRONT_LEFT:
		oppositeWheelAngularVelocity = m_wheelsInfo[FRONT_RIGHT].angularVelocity;
		break;
	case FRONT_RIGHT:
		oppositeWheelAngularVelocity = m_wheelsInfo[FRONT_LEFT].angularVelocity;
		break;
	case REAR_LEFT:
		oppositeWheelAngularVelocity = m_wheelsInfo[REAR_RIGHT].angularVelocity;
		break;
	case REAR_RIGHT:
		oppositeWheelAngularVelocity = m_wheelsInfo[REAR_LEFT].angularVelocity;
		break;
	default:
		oppositeWheelAngularVelocity = wheelAngularVelocity;
	}

	float torqueDiff = Math::Abs(wheelAngularVelocity - oppositeWheelAngularVelocity) * m_wheelInertia / delta_time;

	if (wheelAngularVelocity > oppositeWheelAngularVelocity)
		m_wheelsInfo[wheel_index].angularVelocity += (torque - torqueDiff * 0.5f) / m_wheelInertia * delta_time;
	else
		m_wheelsInfo[wheel_index].angularVelocity += (torque + torqueDiff * 0.5f) / m_wheelInertia * delta_time;
}


void RedlineGame::Car::Shutdown() const
{

}


void RedlineGame::Car::Reset()
{
	m_currentGear = 1;
	m_steering_angle = 0.0f;

	m_rigidbody->GetRigidbody()->setAngularVelocity(btVector3(0, 0, 0));
	m_rigidbody->GetRigidbody()->setLinearVelocity(btVector3(0, 0, 0));

	for (int i = 0; i < numWheels; ++i)
	{
		m_wheelsInfo[i].load = 0.0f;
		m_wheelsInfo[i].direction = 0.0f;
		m_wheelsInfo[i].angularVelocity = 0.0f;
		m_wheelsInfo[i].contactPoint = btVector3(0, 0, 0);
		m_wheelsInfo[i].isGrounded = false;
	}
}
