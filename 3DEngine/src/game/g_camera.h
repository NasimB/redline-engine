#pragma once

namespace Redline
{
	class GameObject;
}

namespace RedlineGame
{
	class Camera
	{
	public:
		Camera();
		~Camera();
		void Update(float delta_time);

		// Set the target of the camera
		void SetTarget(Redline::GameObject* targ) { m_target = targ; }

		// Retrieve the target of the camera
		Redline::GameObject* GetTarget() const { return m_target; }

	private:
		DirectX::SimpleMath::Vector3 m_forward;

		const float m_camera_base_speed = 40.0f;
		float m_cameraSpeedMultiplier = 1;
		
		int m_camera_mode = 1;
		
		Redline::GameObject* m_target = nullptr;
	};
}
