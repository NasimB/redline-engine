#include "game.h"
#include "car.h"

#include "../engine/engine.h"
#include "../engine/Rigidbody.h"

using namespace Redline;
using namespace std;
using namespace DirectX::SimpleMath;

bool RedlineGame::Game::m_isDebugPanelEnabled = false;


RedlineGame::Game::Game() :
	m_car(nullptr),
	m_camera(nullptr), 
	m_sun_game_object(nullptr),
	m_sun(nullptr),
	m_sun_intensity(4.0f)
{
}


RedlineGame::Game::~Game()
{
}


bool RedlineGame::Game::Initialize()
{
	InitScene();
	m_isDebugPanelEnabled = false;
	return true;
}


void RedlineGame::Game::InitScene()
{
	/*m_ambiant_sound = Engine::AddSound("ambiant");
	m_ambiant_sound->Play(true);
	m_ambiant_sound->SetVolume(10.0f);*/

	Engine::GetAudio().LoadBank("data\\sounds\\Master Bank.bank");
	Engine::GetAudio().PlayEvent("{675846a9-0a2d-4504-b6b1-06623e971d6c}");

	m_sun_direction = Vector3(0.127f, 0.845f, -0.520f);
	m_sun_direction.Normalize();

	/*m_sun_game_object = Engine::CreateGameObject("Sun");
	m_sun = m_sun_game_object->AddComponent<Light>();
	m_sun->SetType(LIGHT_DIRECTIONAL);
	m_sun->SetDirection(m_sun_direction);
	m_sun->SetIntensity(Math::Clamp(m_sun_direction.y * 8.0f, 0.0f, 1.0f) * m_sun_intensity);*/

	Engine::GetSceneManager().LoadSceneV2("city");

	m_sun = Engine::GetLights()[0];

	m_car = new Car();
	Engine::CreateGameObject(m_car);
	m_car->GetTransform()->SetPosition(0, 3.0f, 0);
	m_car->GetTransform()->SetRotation(0.0f, float(M_PI_2), 0.0f);

	m_camera = new Camera();
	m_camera->SetTarget(m_car->GetBody());
}


bool RedlineGame::Game::Update(const float delta_time)
{
	static float time = 40.0f;
	static bool freeze_sun = true;

	if (!freeze_sun)
	{
		time += delta_time * (m_sun_direction.y < 0.0f ? 2.0f : 1.0f) * time_speed;
		m_sun_direction.x = cosf(time * 0.01f * M_PI);
		m_sun_direction.y = sinf(time * 0.01f * M_PI);
		m_sun_direction.z = 1.0f;
		m_sun_direction.Normalize();
		m_sun->SetDirection(m_sun_direction);
		m_sun->SetIntensity(Math::Clamp(m_sun_direction.y * 8.0f, 0.0f, 1.0f) * m_sun_intensity);
	}

	m_car->SetLightsState(m_sun_direction.y < 0.1f);

	vector<Light*>& lights = Engine::GetLights();

	for (size_t i = 0; i < lights.size(); ++i)
	{
		if (lights[i]->GetType() == LIGHT_DIRECTIONAL)
			continue;

		Transform* transform = lights[i]->GetGameObject()->GetTransform();
		Vector3 world_pos = Vector3::Transform(transform->GetPosition(), transform->GetMatrix());

		float distance = Vector3::Distance(world_pos, Engine::GetCamera().GetPosition());
		lights[i]->SetActive(m_sun_direction.y < 0.1f && distance < 250.0);

		float intensity = Math::Lerp(15.0f, 0.0f, Math::Clamp((distance - 200.0f) / 50.0f, 0.0f, 1.0f));
		lights[i]->SetIntensity(intensity);
	}

	/*static float roughness = 0.1f;
	static float metallic = 0.0f;
	static float color[] = { 1.0f, 0.0f, 0.0f };*/
	
	if (Input::GetButtonDown(DIK_F1))
	{
		m_isDebugPanelEnabled = !m_isDebugPanelEnabled;
	}

	m_car->Update(delta_time);
	m_camera->Update(delta_time);

	// DEBUG PANEL
	if (m_isDebugPanelEnabled)
	{
		ImGui::Text("Framerate: %.1f FPS (%.3f ms)", ImGui::GetIO().Framerate, 1000.0f / ImGui::GetIO().Framerate);
		ImGui::Text(" ");

		ImGui::Text("Scene:");

		static float time_scale = 1.0f;
		ImGui::DragFloat("Physics Time scale", &time_scale, 0.1f, 0.0f, 2.0f);
		Engine::GetPhysics().SetTimeScale(time_scale);

		ImGui::Checkbox("Freeze Sun Direction", &freeze_sun);

		if (!freeze_sun)
		{
			ImGui::DragFloat("Time speed", &time_speed);
		}
		else
		{
			ImGui::DragFloat3("Sun Direction", reinterpret_cast<float*>(&m_sun_direction), 0.01f, -1.0f, 1.0f);
			m_sun_direction.Normalize();
			m_sun->SetDirection(m_sun_direction);
		}

		ImGui::SliderFloat("Sun Intensity", &m_sun_intensity, 0.01f, 10.0f);
		m_sun->SetIntensity(min(max(m_sun_direction.y * 8.0f, 0.0f), 1.0f) * m_sun_intensity);


		ImGui::Text(" ");
		if (ImGui::Button("Reset Scene"))
		{
			Reset();
		}


		ImGui::Text(" ");
		ImGui::Text("Object instancing:");

		if (ImGui::Button("Spawn Cube") || Input::GetButtonPressed(DIK_1))
		{
			GameObject* object = Engine::CreateGameObject("cube");
			if (object != nullptr)
			{
				MeshRenderer* mesh_renderer = object->AddComponent<MeshRenderer>();
				mesh_renderer->SetMesh("cube");
				Material* mat = &mesh_renderer->GetMaterials()[0];
				mat->SetRoughness(RandomValue());
				mat->SetMetalic(RandomValue());
				mat->SetDiffuseColor(RandomValue(), RandomValue(), RandomValue());

				Rigidbody* rigidbody = object->AddComponent<Rigidbody>();
				rigidbody->CreateBox(10.0f, 2.0f, 2.0f, 2.0f);

				Vector3 pos = Engine::GetCamera().GetPosition();
				Vector3 fwd = Engine::GetCamera().GetForwardVector();
				object->GetTransform()->SetPosition(pos + fwd * 20);
				
				m_objects.emplace_back(object);
			}
		}

		if (ImGui::Button("Spawn Sphere") || Input::GetButtonPressed(DIK_2))
		{
			GameObject* object = Engine::CreateGameObject("sphere");
			if (object != nullptr)
			{
				MeshRenderer* mesh_renderer = object->AddComponent<MeshRenderer>();
				mesh_renderer->SetMesh("sphere");
				Material* mat = &mesh_renderer->GetMaterials()[0];
				mat->SetRoughness(0.0f); // RandomValue());
				mat->SetMetalic(1.0f); // RandomValue());
				mat->SetDiffuseColor(1, 1, 1); // RandomValue(), RandomValue(), RandomValue());

				Rigidbody* rigidbody = object->AddComponent<Rigidbody>();
				rigidbody->CreateSphere(10.0f, 1.0f);

				Vector3 pos = Engine::GetCamera().GetPosition();
				Vector3 fwd = Engine::GetCamera().GetForwardVector();
				object->GetTransform()->SetPosition(pos + fwd * 20);

				m_objects.emplace_back(object);
			}
		}

		if (ImGui::Button("Spawn 10 Spheres") || Input::GetButtonPressed(DIK_3))
		{
			for (float i = 0; i < 10; ++i)
			{
				GameObject* object = Engine::CreateGameObject("sphere");
				if (object != nullptr)
				{
					MeshRenderer* mesh_renderer = object->AddComponent<MeshRenderer>();
					mesh_renderer->SetMesh("sphere");
					Material* mat = &mesh_renderer->GetMaterials()[0];
					mat->SetRoughness(RandomValue());
					mat->SetMetalic(RandomValue());
					mat->SetDiffuseColor(RandomValue(), RandomValue(), RandomValue());

					Rigidbody* rigidbody = object->AddComponent<Rigidbody>();
					rigidbody->CreateSphere(10.0f, 1.0f);

					Vector3 pos = Engine::GetCamera().GetPosition();
					Vector3 fwd = Engine::GetCamera().GetForwardVector();
					object->GetTransform()->SetPosition(pos + fwd * (20.0f + i));

					m_objects.emplace_back(object);
				}
			}
		}

		if (ImGui::Button("Spawn 10 Cubes") || Input::GetButtonPressed(DIK_4))
		{
			for (float i = 0; i < 10; ++i)
			{
				GameObject* object = Engine::CreateGameObject("cube");
				if (object != nullptr)
				{
					MeshRenderer* mesh_renderer = object->AddComponent<MeshRenderer>();
					mesh_renderer->SetMesh("cube");
					Material* mat = &mesh_renderer->GetMaterials()[0];
					mat->SetRoughness(RandomValue());
					mat->SetMetalic(RandomValue());
					mat->SetDiffuseColor(RandomValue(), RandomValue(), RandomValue());

					Rigidbody* rigidbody = object->AddComponent<Rigidbody>();
					rigidbody->CreateBox(10.0f, 2.0f, 2.0f, 2.0f);

					Vector3 pos = Engine::GetCamera().GetPosition();
					Vector3 fwd = Engine::GetCamera().GetForwardVector();
					object->GetTransform()->SetPosition(pos + fwd * (20.0f + i));

					m_objects.emplace_back(object);
				}
			}
		}


		/*ImGui::Text(" ");
		ImGui::Text("Material tester sphere:");

		ImGui::SliderFloat("Roughness", &roughness, 0.01f, 0.99f);
		ImGui::SliderFloat("Metallic", &metallic, 0.01f, 0.99f);
		ImGui::ColorEdit3("Color", color);*/
	}


	if (!m_objects.empty())
	{
		// Remove objects that fell off the map
		int objectCount = static_cast<int>(m_objects.size()) - 1;
		for (int i = objectCount; i >= 0; i--)
		{
			if (m_objects[i]->GetTransform()->GetPosition().y < -100.0f)
			{
				Engine::RemoveGameObject(m_objects[i]);
				m_objects.erase(m_objects.begin() + i);
			}
		}
	}
	return true;
}


bool RedlineGame::Game::OnPhysicsUpdate(const float delta_time) const
{
	m_car->OnPhysicsUpdate(delta_time);
	return true;
}


void RedlineGame::Game::Reset()
{
	size_t objectsCount = m_objects.size();
	if (objectsCount > 0)
	{
		for (size_t i = 0; i < objectsCount; ++i)
		{
			Engine::RemoveGameObject(m_objects[i]);
		}
		m_objects.clear();
	}

	if (m_car != nullptr)
	{
		m_car->Reset();
		m_car->GetTransform()->SetPosition(0.0f, 3.0f, 0.0f);
		m_car->GetTransform()->SetRotation(0.0f, float(M_PI_2), 0.0f);
	}

	//m_sun_direction = Vector3(rand() / float(RAND_MAX), rand() / float(RAND_MAX), rand() / float(RAND_MAX));
	m_sun_direction = Vector3(0.127f, 0.845f, -0.520f);
	m_sun_direction.Normalize();
	m_sun_intensity = 4.0f;
	m_sun->SetDirection(m_sun_direction);
	m_sun->SetIntensity(min(max(m_sun_direction.y * 8.0f, 0.0f), 1.0f) * m_sun_intensity);
}


void RedlineGame::Game::Shutdown() const
{
	if (m_car != nullptr)
	{
		m_car->Shutdown();
	}

	Engine::GetSceneManager().UnloadScene();
}
