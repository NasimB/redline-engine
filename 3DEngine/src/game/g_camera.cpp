#include "g_camera.h"

#include "../engine/engine.h"
#include "../engine/gameobject.h"

using namespace Redline;
using namespace DirectX::SimpleMath;

RedlineGame::Camera::Camera() :
	m_forward(0.0f, 0.0f, 1.0f)
{
	Engine::GetCamera().SetFOV(70);
}


RedlineGame::Camera::~Camera()
{
}


void RedlineGame::Camera::Update(const float delta_time)
{
	if (Input::GetButtonDown(DIK_C))
	{
		m_camera_mode++;

		if (m_camera_mode > 2)
			m_camera_mode = 0;
	}

	// FREE CAMERA MOVEMENTS
	if (m_camera_mode == 0)
	{
		if (Input::GetButtonPressed(DIK_A))
		{
			Vector3 pos = Engine::GetCamera().GetPosition();
			Vector3 right = Engine::GetCamera().GetRightVector();

			Engine::GetCamera().SetPosition(pos + right * (m_camera_base_speed * delta_time * m_cameraSpeedMultiplier));
		}
		else if (Input::GetButtonPressed(DIK_D))
		{
			Vector3 pos = Engine::GetCamera().GetPosition();
			Vector3 right = Engine::GetCamera().GetRightVector();

			Engine::GetCamera().SetPosition(pos - right * (m_camera_base_speed * delta_time * m_cameraSpeedMultiplier));
		}

		if (Input::GetButtonPressed(DIK_W))
		{
			Vector3 pos = Engine::GetCamera().GetPosition();
			Vector3 forward = Engine::GetCamera().GetForwardVector();

			Engine::GetCamera().SetPosition(pos + forward * (m_camera_base_speed * delta_time * m_cameraSpeedMultiplier));
		}
		else if (Input::GetButtonPressed(DIK_S))
		{
			Vector3 pos = Engine::GetCamera().GetPosition();
			Vector3 forward = Engine::GetCamera().GetForwardVector();

			Engine::GetCamera().SetPosition(pos - forward * (m_camera_base_speed * delta_time * m_cameraSpeedMultiplier));
		}

		if (Input::GetMouseButtonPressed(MOUSE_RIGHT))
		{
			Vector3 rot = Engine::GetCamera().GetRotation();
			Vector2 delta = Input::GetMouseDelta() * 0.1f;

			const float maxRot = 90.0f;

			Vector3 newRot = rot + Vector3(delta.y, -delta.x, 0);
			newRot.x = min(max(newRot.x, -maxRot), maxRot);
			Engine::GetCamera().SetRotation(newRot);
		}

		if (Input::GetButtonPressed(DIK_LSHIFT))
		{
			m_cameraSpeedMultiplier = 2.0f;
		}
		else if (Input::GetButtonPressed(DIK_LALT))
		{
			m_cameraSpeedMultiplier = 0.2f;
		}
		else
		{
			m_cameraSpeedMultiplier = 1.0f;
		}

		Engine::GetCamera().SetFOV(60.0f);
	}
	else if (m_camera_mode == 1)
	{
		if (m_target != nullptr)
		{
			Matrix& world_matrix = m_target->GetTransform()->GetMatrix();

			Vector3 world_pos = Vector3::Transform(Vector3::Zero, world_matrix);

			Vector3 forward = Vector3::TransformNormal(Vector3::Forward, world_matrix);
			Vector3 right = Vector3::TransformNormal(Vector3::Right, world_matrix);
			Vector3 up = Vector3::TransformNormal(Vector3::Up, world_matrix);

			float angle = Math::Clamp(acos(forward.Dot(m_forward)), 0.0f, float(M_PI));
			m_forward = Vector3::Lerp(m_forward, forward, 1.0f - Math::Exp(-delta_time * Math::Lerp(3.0f, 16.0f, angle / float(M_PI))));
			m_forward.Normalize();

			Vector3 cameraPosition = world_pos + right * 0.0f + up * 2.0f + m_forward * 12.0f;
			Engine::GetCamera().SetPosition(cameraPosition);

			Engine::GetCamera().SetLookAt(cameraPosition + forward);

			Engine::GetCamera().SetFOV(70.0f);


			/*btVector3 velocity = m_target->GetRigidbody()->getLinearVelocity();
			float speed = velocity.length();
			Engine::GetCamera().SetFOV(Math::Lerp(70.0f, 80.0f, Math::Clamp(speed / 55.5f, 0.0f, 1.0f)));*/
		}
	}
	else if (m_camera_mode == 2) // Interior view
	{
		if (m_target != nullptr)
		{
			Matrix& world_matrix = m_target->GetTransform()->GetMatrix();

			Vector3 local_pos = Vector3(0.8f, 0.7f, -0.8f);

			Vector3 world_pos = Vector3::Transform(local_pos, world_matrix);
			Vector3 forward = Vector3::TransformNormal(Vector3::Forward, world_matrix);

			Engine::GetCamera().SetPosition(world_pos);

			Engine::GetCamera().SetLookAt(world_pos + forward);

			Engine::GetCamera().SetFOV(55.0f);
		}
	}
}
