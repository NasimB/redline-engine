﻿#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <vector>
#include <map>
#include <string>
#include <fstream>

#include <d3d11.h>
#pragma comment(lib, "d3d11.lib")

#include <d3dcompiler.h>
#pragma comment(lib,"d3dcompiler.lib")

#include <DirectXMath.h>

#include <SimpleMath.h>
#include <DDSTextureLoader.h>
#pragma comment(lib, "DirectXTK.lib")


#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <dinputd.h>
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")


#include <wrl/client.h>

#include <btBulletDynamicsCommon.h>

#include "../../../Libraries/Imgui/imgui.h"
#include "../../../Libraries/Imgui/imgui_impl_dx11.h"


#include "fmod_studio.hpp"
#include "fmod.hpp"

#ifdef _DEBUG
#pragma comment(lib, "fmodL64_vc.lib")
#pragma comment(lib, "fmodstudioL64_vc.lib")
#else
#pragma comment(lib, "fmod64_vc.lib")
#pragma comment(lib, "fmodstudio64_vc.lib")
#endif


#include "engine/math.h"
#include "engine/stringtools.h"


#define USE_IMGUI 1