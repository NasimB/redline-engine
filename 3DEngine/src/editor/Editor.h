﻿#pragma once

namespace RedlineEditor
{
	class Editor
	{
	public:
		Editor();
		~Editor();

		bool Initialize() const;
		bool Shutdown() const;

		void Update(float delta_time);
	private:
		void UpdateCamera(float delta_time);

		float m_camera_base_speed = 40.0f;

		float m_cameraSpeedMultiplier = 1;
	};
}
