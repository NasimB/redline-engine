﻿#include "../stdafx.h"
#include "Editor.h"
#include "../engine/engine.h"

using namespace Redline;
using namespace std;
using namespace DirectX::SimpleMath;

RedlineEditor::Editor::Editor()
{

}

RedlineEditor::Editor::~Editor()
{

}


bool RedlineEditor::Editor::Initialize() const
{
	Engine::GetSceneManager().LoadScene("city_old");
	return true;
}


bool RedlineEditor::Editor::Shutdown() const
{
	return true;
}


void RedlineEditor::Editor::Update(const float delta_time)
{
	UpdateCamera(delta_time);


	vector<Light*>& lights = Engine::GetLights();

	for (size_t i = 0; i < lights.size(); ++i)
	{
		if (lights[i]->GetType() == LIGHT_DIRECTIONAL)
			continue;

		Transform* transform = lights[i]->GetGameObject()->GetTransform();
		Vector3 world_pos = Vector3::Transform(transform->GetPosition(), transform->GetMatrix());

		float distance = Vector3::Distance(world_pos, Engine::GetCamera().GetPosition());
		lights[i]->SetActive(distance < 150.0);

		float intensity = Math::Lerp(15.0f, 0.0f, Math::Clamp((distance - 100.0f) / 50.0f, 0.0f, 1.0f));
		lights[i]->SetIntensity(intensity);
	}


	static GameObject* lastGo = nullptr;

	// Map editor
	ImGui::SetNextWindowPos(ImVec2(310, 0), ImGuiSetCond_FirstUseEver);
	ImGui::SetNextWindowSize(ImVec2(300, 0));
	if (ImGui::Begin("Map placement"))
	{
		if (!Engine::GetSceneManager().IsSceneLoaded())
		{
			if (ImGui::Button("Load", ImVec2(125.0f, 0)))
			{
				Engine::GetSceneManager().LoadSceneV2("city");
			}
		}
		else
		{
			if (ImGui::Button("Unload", ImVec2(125.0f, 0)))
			{
				lastGo = nullptr;
				Engine::GetSceneManager().UnloadScene();
			}

			ImGui::SameLine(0, 25.0f);

			if (ImGui::Button("Save", ImVec2(125.0f, 0)))
			{
				Engine::GetSceneManager().SaveScene("city");
			}
		}

		ImGui::Spacing();

		if (ImGui::Button("Add Building 1"))
		{
			GameObject* building = Engine::CreateGameObject("building_01");

			Rigidbody* rigidbody = building->AddComponent<Rigidbody>();
			rigidbody->CreateBox(0.0f, 50.0f, 40.0f, 30.0f);

			Engine::GetSceneManager().AddGameObject(building);

			if (lastGo != nullptr)
			{
				lastGo->GetTransform()->showGizmo = false;
				Rigidbody* lastGoRB = lastGo->GetComponent<Rigidbody>(Component::Rigidbody);
				if (lastGoRB != nullptr)
					lastGoRB->SetDebugState(false);
			}

			lastGo = building;
			lastGo->GetTransform()->showGizmo = true;
			rigidbody->SetDebugState(true);
		}

		ImGui::SameLine();

		if (ImGui::Button("Add Building 2"))
		{
			GameObject* building = Engine::CreateGameObject("building_02");
			Rigidbody* rigidbody = building->AddComponent<Rigidbody>();
			rigidbody->CreateBox(0.0f, 50.0f, 40.0f, 30.0f);
			Engine::GetSceneManager().AddGameObject(building);

			if (lastGo != nullptr)
			{
				lastGo->GetTransform()->showGizmo = false;
				Rigidbody* lastGoRB = lastGo->GetComponent<Rigidbody>(Component::Rigidbody);
				if (lastGoRB != nullptr)
					lastGoRB->SetDebugState(false);
			}

			lastGo = building;
			lastGo->GetTransform()->showGizmo = true;
			rigidbody->SetDebugState(true);
		}

		if (ImGui::Button("Add Post (Single)"))
		{
			GameObject* post = Engine::CreateGameObject("post_single");
			Rigidbody* rigidbody = post->AddComponent<Rigidbody>();
			rigidbody->CreateBox(0.0f, 0.7f, 10.5f, 0.7f);
			Engine::GetSceneManager().AddGameObject(post);

			if (lastGo != nullptr)
			{
				lastGo->GetTransform()->showGizmo = false;
				Rigidbody* lastGoRB = lastGo->GetComponent<Rigidbody>(Component::Rigidbody);
				if (lastGoRB != nullptr)
					lastGoRB->SetDebugState(false);
			}

			lastGo = post;
			lastGo->GetTransform()->showGizmo = true;
			rigidbody->SetDebugState(true);
		}

		if (ImGui::Button("Add Road Block A"))
		{
			GameObject* ground = Engine::CreateGameObject("block_A");

			Rigidbody* rigidbody = ground->AddComponent<Rigidbody>();
			rigidbody->CreateComplexMesh();

			Engine::GetSceneManager().AddGameObject(ground);

			if (lastGo != nullptr)
			{
				lastGo->GetTransform()->showGizmo = false;
				Rigidbody* lastGoRB = lastGo->GetComponent<Rigidbody>(Component::Rigidbody);
				if (lastGoRB != nullptr)
					lastGoRB->SetDebugState(false);
			}

			lastGo = ground;
			lastGo->GetTransform()->showGizmo = true;
			rigidbody->SetDebugState(true);
		}

		if (ImGui::Button("Add Road Block B"))
		{
			GameObject* ground = Engine::CreateGameObject("block_B");

			Rigidbody* rigidbody = ground->AddComponent<Rigidbody>();
			rigidbody->CreateComplexMesh();

			Engine::GetSceneManager().AddGameObject(ground);

			if (lastGo != nullptr)
			{
				lastGo->GetTransform()->showGizmo = false;
				Rigidbody* lastGoRB = lastGo->GetComponent<Rigidbody>(Component::Rigidbody);
				if (lastGoRB != nullptr)
					lastGoRB->SetDebugState(false);
			}

			lastGo = ground;
			lastGo->GetTransform()->showGizmo = true;
			rigidbody->SetDebugState(true);
		}

		if (ImGui::Button("Add Road Block C"))
		{
			GameObject* ground = Engine::CreateGameObject("block_C");

			Rigidbody* rigidbody = ground->AddComponent<Rigidbody>();
			rigidbody->CreateComplexMesh();

			Engine::GetSceneManager().AddGameObject(ground);

			if (lastGo != nullptr)
			{
				lastGo->GetTransform()->showGizmo = false;
				Rigidbody* lastGoRB = lastGo->GetComponent<Rigidbody>(Component::Rigidbody);
				if (lastGoRB != nullptr)
					lastGoRB->SetDebugState(false);
			}

			lastGo = ground;
			lastGo->GetTransform()->showGizmo = true;
			rigidbody->SetDebugState(true);
		}
	}
	ImGui::End();


	if (lastGo != nullptr)
	{
		ImGui::SetNextWindowPos(ImVec2(310, 310), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("Object Inspector"))
		{
			static char buff[256];
			ImGui::InputText("Name", buff, 256);
			if (ImGui::IsItemActive())
				lastGo->SetName(buff);
			else
				strncpy_s(buff, lastGo->GetName().c_str(), sizeof(buff));

			lastGo->GetTransform()->ShowInspector();

			for (Component* component : lastGo->GetAllComponents())
			{
				component->ShowInspector();
			}
		}
		ImGui::End();

		if (Input::GetButtonDown(DIK_DELETE))
		{
			Engine::GetSceneManager().RemoveGameObject(lastGo);
			Engine::RemoveGameObject(lastGo);
			lastGo = nullptr;
		}
	}


	ImGui::SetNextWindowPos(ImVec2(0, 0));
	if (ImGui::Begin("Scene Objects"))
	{
		vector<GameObject*> gameobjects = Engine::GetSceneManager().GetGameObjects();
		size_t objCount = gameobjects.size();

		int node_clicked = -1; // Temporary storage of what node we have clicked to process selection at the end of the loop. May be a pointer to your own node type, etc.

		for (size_t i = 0; i < objCount; ++i)
		{
			GameObject* go = gameobjects[i];
			Transform* transform = go->GetTransform();

			if (transform->GetParent() == nullptr)
			{
				transform->ShowHierarchy(node_clicked, lastGo);
			}
		}

		if (node_clicked != -1)
		{
			if (lastGo != nullptr)
			{
				lastGo->GetTransform()->showGizmo = false;
				Rigidbody* lastGoRB = lastGo->GetComponent<Rigidbody>(Component::Rigidbody);
				if (lastGoRB != nullptr)
					lastGoRB->SetDebugState(false);
			}

			lastGo = Engine::GetSceneManager().GetGameObject(static_cast<unsigned int>(node_clicked));

			Transform* transform = lastGo->GetTransform();

			transform->showGizmo = true;
			Rigidbody* rigidbody = lastGo->GetComponent<Rigidbody>(Component::Rigidbody);
			if (rigidbody)
				rigidbody->SetDebugState(true);
		}

	}
	ImGui::End();
}


void RedlineEditor::Editor::UpdateCamera(const float delta_time)
{
	if (Input::GetButtonPressed(DIK_A))
	{
		Vector3 pos = Engine::GetCamera().GetPosition();
		Vector3 right = Engine::GetCamera().GetRightVector();

		Engine::GetCamera().SetPosition(pos + right * (m_camera_base_speed * delta_time * m_cameraSpeedMultiplier));
	}
	else if (Input::GetButtonPressed(DIK_D))
	{
		Vector3 pos = Engine::GetCamera().GetPosition();
		Vector3 right = Engine::GetCamera().GetRightVector();

		Engine::GetCamera().SetPosition(pos - right * (m_camera_base_speed * delta_time * m_cameraSpeedMultiplier));
	}

	if (Input::GetButtonPressed(DIK_W))
	{
		Vector3 pos = Engine::GetCamera().GetPosition();
		Vector3 forward = Engine::GetCamera().GetForwardVector();

		Engine::GetCamera().SetPosition(pos + forward * (m_camera_base_speed * delta_time * m_cameraSpeedMultiplier));
	}
	else if (Input::GetButtonPressed(DIK_S))
	{
		Vector3 pos = Engine::GetCamera().GetPosition();
		Vector3 forward = Engine::GetCamera().GetForwardVector();

		Engine::GetCamera().SetPosition(pos - forward * (m_camera_base_speed * delta_time * m_cameraSpeedMultiplier));
	}

	if (Input::GetMouseButtonPressed(MOUSE_RIGHT))
	{
		Vector3 rot = Engine::GetCamera().GetRotation();
		Vector2 delta = Input::GetMouseDelta() * 0.1f;

		const float maxRot = 90.0f;

		Vector3 newRot = rot + Vector3(delta.y, -delta.x, 0);
		newRot.x = min(max(newRot.x, -maxRot), maxRot);
		Engine::GetCamera().SetRotation(newRot);
	}

	if (Input::GetButtonPressed(DIK_LSHIFT))
	{
		m_cameraSpeedMultiplier = 2.0f;
	}
	else if (Input::GetButtonPressed(DIK_LALT))
	{
		m_cameraSpeedMultiplier = 0.2f;
	}
	else
	{
		m_cameraSpeedMultiplier = 1.0f;
	}

	Engine::GetCamera().SetFOV(60.0f);
}
