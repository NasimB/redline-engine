#include "light.h"

using namespace std;
using namespace DirectX::SimpleMath;

Redline::Light::Light(GameObject* game_object) :
	Component(game_object, ComponentType::Light),
	m_direction(0, -1, 0),
	m_light_type(LIGHT_DIRECTIONAL),
	m_color(1, 1, 1),
	m_angle(25.0f),
	m_intensity(1.0f),
	m_enabled(true),
	m_cast_shadows(true)
{
}


Redline::Light::~Light()
{

}


void Redline::Light::Serialize(ofstream& stream)
{
	stream.write(reinterpret_cast<char*>(&m_direction), sizeof(Vector3));

	short light_type = static_cast<short>(m_light_type);
	stream.write(reinterpret_cast<char*>(&light_type), sizeof(short));

	stream.write(reinterpret_cast<char*>(&m_color), sizeof(Vector3));

	stream.write(reinterpret_cast<char*>(&m_angle), sizeof(float));
	stream.write(reinterpret_cast<char*>(&m_intensity), sizeof(float));

	stream.write(reinterpret_cast<char*>(&m_enabled), sizeof(bool));
	stream.write(reinterpret_cast<char*>(&m_cast_shadows), sizeof(bool));
}


void Redline::Light::Deserialize(ifstream& stream)
{
	stream.read(reinterpret_cast<char*>(&m_direction), sizeof(Vector3));

	short light_type = 0;
	stream.read(reinterpret_cast<char*>(&light_type), sizeof(short));
	m_light_type = static_cast<LightType>(light_type);

	stream.read(reinterpret_cast<char*>(&m_color), sizeof(Vector3));

	stream.read(reinterpret_cast<char*>(&m_angle), sizeof(float));
	stream.read(reinterpret_cast<char*>(&m_intensity), sizeof(float));

	stream.read(reinterpret_cast<char*>(&m_enabled), sizeof(bool));
	stream.read(reinterpret_cast<char*>(&m_cast_shadows), sizeof(bool));
}


#ifdef REDLINE_EDITOR
void Redline::Light::ShowInspector()
{
	if (!ImGui::TreeNodeEx("Light", ImGuiTreeNodeFlags_DefaultOpen))
		return;

	static int type = 0;
	bool changed = ImGui::Combo("Light Type", &type, "Directional\0Point\0Spot");

	if (changed)
		m_light_type = static_cast<LightType>(type);
	else
		type = static_cast<int>(m_light_type);


	switch (m_light_type)
	{
	default: break;
	case LIGHT_DIRECTIONAL:
		if (!(changed |= ImGui::DragFloat3("Direction", reinterpret_cast<float*>(&m_direction), 0.05f)))
			m_direction.Normalize();
		break;
	case LIGHT_POINT:
		break;
	case LIGHT_SPOT:
		changed |= ImGui::DragFloat("Angle", &m_angle, 1.0f, 0.0f, 360.0f);
		break;
	}

	changed |= ImGui::DragFloat("Intensity", &m_intensity, 1.0f, 0.0f, 200.0f);
	changed |= ImGui::ColorEdit3("Color", reinterpret_cast<float*>(&m_color));

	ImGui::TreePop();
}
#endif
