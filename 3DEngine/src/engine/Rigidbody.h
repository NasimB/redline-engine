﻿#pragma once

#include "Component.h"

namespace Redline
{
	class GameObject;

	class Rigidbody : public Component
	{
	public:
		enum ColliderType
		{
			None = 0,
			Box,
			Sphere,
			Cylinder,
			ConvexMesh,
			ComplexMesh,
			Custom
		};

		Rigidbody(GameObject* baseGo);
		~Rigidbody();

		Rigidbody& operator=(const Rigidbody& other);

		// Returns true if the rigidbody was initialized with a collider
		bool IsEnabled() const { return m_enabled; }

		// Returns false if the rigidbody is asleep
		bool IsActive() const { return m_enabled; }

		// Initialize the rigidbody with a box shape. 
		bool CreateBox(const float mass, const float size_X, const float size_Y, const float size_Z);

		// Initialize the rigidbody with a sphere shape. 
		bool CreateSphere(const float mass, const float radius);

		// Initialize the rigidbody with a cylinder shape. 
		// Axis 1 is X, 2 is Y and 3 is Z.
		bool CreateCylinder(const float mass, const float radius, const float height, const int axis = 1);

		// Initialize the rigidbody with a concave mesh shape. Only for static objects.
		bool CreateComplexMesh();

		// Initialize the rigidbody with a custom shape. 
		bool CreateCustom(const float mass, btCollisionShape* shape);

		void Shutdown();
		

		void SetDebugState(bool state) const;

		float GetMass() const { return m_mass; }
		void SetMass(const float mass);

		DirectX::SimpleMath::Vector3 GetPosition() const;
		void SetPosition(DirectX::SimpleMath::Vector3 pos) const;

		DirectX::SimpleMath::Quaternion GetRotation() const;
		void SetRotation(DirectX::SimpleMath::Quaternion rot) const;

		DirectX::SimpleMath::Vector3 GetCenter() const { return m_center; }
		void SetCenter(const DirectX::SimpleMath::Vector3 center) { m_center = center; }

		btRigidBody* GetRigidbody() const { return m_rigidbody; }
		ColliderType GetColliderType() const { return m_collider_type; }
		
		float* GetParameters() { return m_parameters; }

		void CreateFromData(const ColliderType type, const float params[4], const DirectX::SimpleMath::Vector3 center);

		void RefreshCollider();

		static ComponentType GetComponentType() { return Component::Rigidbody; }

		void Serialize(std::ofstream& stream) override;
		void Deserialize(std::ifstream& stream) override;

#ifdef REDLINE_EDITOR
		void ShowInspector() override;
#endif

	private:
		void DestroyShape();

		btRigidBody* m_rigidbody;
		btMotionState* m_motion_state;
		GameObject* m_game_object;

		DirectX::SimpleMath::Vector3 m_center;

		bool m_enabled;

		ColliderType m_collider_type;

		float m_mass;

		float m_parameters[4];
	};
}
