#pragma once

#include "transform.h"
#include "Component.h"
#include "engine.h"

namespace Redline
{
	class Rigidbody;
	class MeshRenderer;
	class Light;

	class GameObject
	{
	public:
		// WARNING: Don't use this constructor, use Engine::CreateGameObject(name) instead.
		GameObject(const std::string& name = "");
		virtual ~GameObject();

		virtual void Update(float deltaTime) { }

		void PrepareDraw();

		Transform*		GetTransform()		{ return &m_transform; }

		std::string GetName() const { return m_name; }
		void SetName(const std::string& value) { m_name = value; }
	
		size_t GetID() const { return m_id; }
		void SetID(const size_t value) { m_id = value; }

		template<typename T> T* AddComponent()
		{
			T* component = Engine::AddComponent<T>(this);

			if (component != nullptr)
				m_components.emplace_back(component);

			return component;
		}

		Component* AddComponent(Component::ComponentType type);

		template<typename T> T* GetComponent(const Component::ComponentType type)
		{
			for (auto component : m_components)
			{
				if (component->GetComponentType() == type)
					return reinterpret_cast<T*>(component);
			}
			return nullptr;
		}

		void RemoveComponent(Component* component);

		std::vector<Component*>& GetAllComponents() { return m_components; }

	protected:
		std::vector<Component*> m_components;

		Transform m_transform;

		size_t m_id;
		std::string m_name;
	};
}
