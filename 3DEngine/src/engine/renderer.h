#pragma once

#if defined(DEBUG) || defined(_DEBUG)
	#ifndef D3D_DEBUG_INFO
		#define D3D_DEBUG_INFO
	#endif
#endif

#include "texture.h"
#include "shader.h"
#include "vertexbuffer.h"
#include "indexbuffer.h"
#include "light.h"
#include "skybox.h"
#include "rendertarget.h"
#include "SSAORenderer.h"
#include "BloomRenderer.h"
#include "FXAARenderer.h"
#include "SMAARenderer.h"
#include "meshrenderer.h"
#include "shadowrenderer.h"

namespace Redline
{
	enum GBufferRenderTarget : int
	{
		DEPTH_BUFFER = 0,
		NORMAL_BUFFER,
		COLOR_BUFFER,
		BUFFER_COUNT
	};

	struct ConstantBuffer
	{
		DirectX::XMMATRIX viewToWorldMatrix;
		DirectX::XMMATRIX invProjMatrix;
		int EnvNumMipMaps;
		float ambient_intensity;
	};

	struct WorldViewMatrixBuffer
	{
		DirectX::XMMATRIX mWorld;
		DirectX::XMMATRIX mView;
	};

	struct MatrixBuffer
	{
		DirectX::XMMATRIX mMatrix;
	};

	struct LightBuffer
	{
		DirectX::SimpleMath::Vector3 color;
		float intensity;
		DirectX::SimpleMath::Vector3 position;
		int type;
		DirectX::SimpleMath::Vector3 direction;
		float angle; // for spot lights
	};

	struct EnvFilteringBuffer
	{
		DirectX::XMVECTOR vNormalAndRoughness;
		DirectX::XMVECTOR vUp;
		float sample_count;
	};

	struct RenderObject
	{
		DirectX::SimpleMath::Matrix world_matrix;
		MaterialBuffer material_buffer;
		ID3D11ShaderResourceView* diffuse_texture;
		ID3D11ShaderResourceView* normal_texture;
		ID3D11Buffer* vertex_buffer;
		ID3D11Buffer* index_buffer;
		unsigned int indices_count;
		byte offset[120];
	};

	class Renderer
	{
	public:
		Renderer();
		~Renderer();

		bool Initialize(HWND hwnd, bool vSync);
		bool InitializeResources(HWND hwnd);
		bool InitializeSkybox();

		bool Render(float delta_time);

		void ClearState() const { m_deviceContext->ClearState(); }

		// Shader
		void SetShader(Shader* shader) const;

		// Z-test
		void DisableZTest() const;
		void EnableZTest() const;

		// Matrices
		void SetWorldMatrix(const DirectX::SimpleMath::Matrix& matrix) const;
		void SetViewMatrix(const DirectX::SimpleMath::Matrix& matrix) const;
		void SetProjectionMatrix(const DirectX::SimpleMath::Matrix& matrix) const;
		void SetViewProjectionMatrices(const DirectX::SimpleMath::Matrix& view_matrix, const DirectX::SimpleMath::Matrix& projection_matrix) const;

		// Culling
		std::vector<RenderObject> CullOffscreenObjects(const std::vector<MeshRenderer*>& mesh_renderers, const DirectX::SimpleMath::Matrix& view_matrix, const DirectX::SimpleMath::Matrix& projection_matrix);
		void CullAndDrawObjects(const std::vector<MeshRenderer*>& mesh_renderers, const DirectX::SimpleMath::Matrix& view_matrix, const DirectX::SimpleMath::Matrix& projection_matrix);

		Texture* CreateTextureFromFile(const std::string& textureName) const;
		Shader* CreateShaderFromFile(const std::string& shaderName) const;
		VertexBuffer* CreateVertexBuffer(const std::vector<Vertex>& vertices) const;
		IndexBuffer* CreateIndexBuffer(const std::vector<unsigned int>& indices) const;

		DirectX::SimpleMath::Vector2 GetWindowResolution() const { return DirectX::SimpleMath::Vector2(static_cast<float>(m_window_width), static_cast<float>(m_window_height)); }
		DirectX::SimpleMath::Vector2 GetInternalResolution() const { return DirectX::SimpleMath::Vector2(static_cast<float>(m_width), static_cast<float>(m_height)); }

		

	private:
		static void BuildTransform(Transform* transform);

		void DrawSkybox(const DirectX::SimpleMath::Vector3& cameraPosition, float farPlane);

		void DrawDebug() const;

		bool InitializeSamplers();
		void ShutdownSamplers();
		Microsoft::WRL::ComPtr<ID3D11SamplerState> m_linear_clamp_sampler;
		Microsoft::WRL::ComPtr<ID3D11SamplerState> m_point_clamp_sampler;
		Microsoft::WRL::ComPtr<ID3D11SamplerState> m_linear_wrap_sampler;
		Microsoft::WRL::ComPtr<ID3D11SamplerState> m_point_wrap_sampler;
		Microsoft::WRL::ComPtr<ID3D11SamplerState> m_shadow_comparaison_sampler;

		bool InitializeGBuffers();

		bool InitializeShadowMap();
		void ShutdownShadowMap();
		
		bool InitializePostProcess();

		bool InitializeSSAO();
		void ShutdownSSAO();

		bool InitializeBloom();
		void ShutdownBloom();

		bool InitializeVolumetricFog();
		void ShutdownVolumetricFog();

		bool InitializeAA();
		void ShutdownAA();

		bool InitializeEnvironmentCubemap();
		void ShutdownEnvironmentCubemap();

		void RenderToGBuffers(const std::vector<MeshRenderer*>& mesh_renderers);
		void RenderToEnvironmentMap(const std::vector<MeshRenderer*>& mesh_renderers);

		void SwitchToGBuffers() const;

		void ClearRTInputsAndOutputs() const;

		bool IsLightVisible(const Light& light) const;

		void RenderToScreen(RenderTarget* input_RT);
		void RenderDeferredImage(const std::vector<MeshRenderer*>& shadow_mesh_renderers);
		void RenderFogPass();

		void BuildViewFrustum(const DirectX::SimpleMath::Matrix& view, const DirectX::SimpleMath::Matrix& projection);
		
		bool IsVisibleOnScreen(const DirectX::SimpleMath::Vector3& position, float radius) const;

		float GetOnScreenSize(const DirectX::SimpleMath::Vector3& position, float radius, const DirectX::SimpleMath::Vector3& cameraPosition, float fov) const;


		bool m_vsync_enabled;
		int m_window_width;
		int m_window_height;
		int m_width;
		int m_height;

		int m_sampleCount = 1;
		int m_sampleQuality = 0;

		D3D_DRIVER_TYPE m_driverType = D3D_DRIVER_TYPE_NULL;
		D3D_FEATURE_LEVEL m_featureLevel = D3D_FEATURE_LEVEL_11_0;
		Microsoft::WRL::ComPtr<ID3D11Device> m_device;
		Microsoft::WRL::ComPtr<ID3D11DeviceContext> m_deviceContext;
		Microsoft::WRL::ComPtr<IDXGISwapChain> m_swapChain;
		Microsoft::WRL::ComPtr<ID3D11RenderTargetView> m_renderTargetView;
#ifdef _DEBUG
		Microsoft::WRL::ComPtr<ID3D11Debug> m_d3dDebug;
#endif // _DEBUG

		Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_rasterizerState;
		
		Microsoft::WRL::ComPtr<ID3D11BlendState> m_blend_state_opaque;
		Microsoft::WRL::ComPtr<ID3D11BlendState> m_blend_state_additive;

		ID3D11Buffer* m_constants_buffer;
		ID3D11Buffer* m_lightCB;
		ID3D11Buffer* m_world_matrix_buffer;
		ID3D11Buffer* m_view_matrix_buffer;
		ID3D11Buffer* m_projection_matrix_buffer;

		ID3D11Buffer* m_bufferMaterial;
		ID3D11Buffer* m_bufferVSSkybox;
		ID3D11Buffer* m_bufferPSSkybox;

		Skybox* m_skybox;
		Shader* m_wireframeShader;
		
		Shader* m_unlit_shader;


		// Deferred rendering variables and constants
		RenderTarget* m_renderTargets[BUFFER_COUNT]{};

		float m_renderScale = 1.0f;

		ID3D11Texture2D* m_depthStencil;
		ID3D11DepthStencilView* m_depthStencilView;
		ID3D11DepthStencilState* m_depthStencilState;
		ID3D11DepthStencilState* m_depthDisabledStencilState;

		D3D11_VIEWPORT m_viewport{};

		Shader* m_GBufferShader;
		Shader* m_deferredShader;

		// Real-time environnement cubemap
		ID3D11Texture2D* m_environmentDepthStencil;
		ID3D11DepthStencilView* m_environmentDepthStencilView;
		D3D11_VIEWPORT m_environmentViewport{};
		RenderTarget* m_environment_cubemap;
		RenderTarget* m_environment_cubemap_filtered;
		Shader* m_environement_filtering_shader;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_env_filtering_buffer;
		Shader* m_simple_shader;
		
		// Frustrum culling
		DirectX::SimpleMath::Plane m_frustum[6];

		// Shadow Mapping
		bool m_shadowEnabled = true;
		ID3D11Buffer* m_shadow_constant_buffer;
		RenderTarget* m_shadowmap_render_target;
		ShadowRenderer m_shadow_renderer;


		// Post Process
		RenderTarget* m_postProcessRT_A;
		RenderTarget* m_postProcessRT_B;
		

		// Atmospheric fog
		Shader* m_fog_shader;

		// SSAO
		SSAORenderer m_ssao_renderer;

		// Bloom & tonemapping
		BloomRenderer m_bloom_renderer;

		// FXAA
		FXAARenderer m_fxaa_renderer;
		bool m_fxaa_enabled;

		// SMAA
		SMAARenderer m_smaa_renderer;
		bool m_smaa_enabled;

		// Debug stats
		size_t m_draw_call_count;
	};
}
