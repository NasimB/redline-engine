#include "shadowrenderer.h"
#include "engine.h"

using namespace Microsoft::WRL;
using namespace std;
using namespace DirectX::SimpleMath;


Redline::ShadowRenderer::ShadowRenderer() :
	m_shadowShader(nullptr),
	m_shadowMapDepthBuffer(nullptr),
	m_shadowMapDepthView(nullptr)
{
}


Redline::ShadowRenderer::~ShadowRenderer()
{
	ShutdownCascadedShadowMaps();

	m_shadow_rasterizer_state.Reset();
}


bool Redline::ShadowRenderer::Initialize(const ComPtr<ID3D11Device>& device)
{
	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.CullMode = D3D11_CULL_NONE;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.SlopeScaledDepthBias = 2.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.ScissorEnable = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.AntialiasedLineEnable = false;

	if (FAILED(device->CreateRasterizerState(&rasterDesc, m_shadow_rasterizer_state.ReleaseAndGetAddressOf())))
	{
		Debug::Err("Failed to create the shadow rasterizer state!");
		return false;
	}

	m_shadowShader = ResourcesManager::GetShader("data\\shaders\\shadows.fx");
	if (m_shadowShader == nullptr)
	{
		Debug::Err("Failed to compile the shadow shader!");
		return false;
	}

	if (!InitializeCascadedShadowMaps(device))
	{
		return false;
	}
	return true;
}


bool Redline::ShadowRenderer::InitializeCascadedShadowMaps(const Microsoft::WRL::ComPtr<ID3D11Device>& device)
{
	shadow_map_resolution = Engine::GetConfig().shadow_res;
	m_num_cascades = Math::Clamp(Engine::GetConfig().shadow_cascades, 1, MAX_CASCADES);

	// Set up the description of the depth buffer
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = shadow_map_resolution;
	depthBufferDesc.Height = shadow_map_resolution;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	// Create the texture for the depth buffer using the filled out description
	if (FAILED(device->CreateTexture2D(&depthBufferDesc, NULL, &m_shadowMapDepthBuffer)))
	{
		Debug::Err("Failed to create the shadow map depth buffer!");
		return false;
	}

	// Set up the depth stencil view description
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = depthBufferDesc.Format;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	// Create the depth stencil view
	if (FAILED(device->CreateDepthStencilView(m_shadowMapDepthBuffer, &depthStencilViewDesc, &m_shadowMapDepthView)))
	{
		Debug::Err("Failed to create the shadow map depth stencil view!");
		return false;
	}
	return true;
}


void Redline::ShadowRenderer::ShutdownCascadedShadowMaps() const
{
	m_shadowMapDepthView->Release();
	m_shadowMapDepthBuffer->Release();
}


void Redline::ShadowRenderer::GenerateCascadedShadowMap(const ComPtr<ID3D11DeviceContext>& device_context,
	const std::vector<MeshRenderer*>& mesh_renderers,
	const Light& directional_light,
	ShadowsBuffer& shadow_buffer,
	RenderTarget* render_target,
	unsigned int cascadeCount)
{
	if (cascadeCount == 0)
	{
		cascadeCount = m_num_cascades;
	}

	float resolution = static_cast<float>(shadow_map_resolution);
	shadow_buffer.shadowResolution = resolution;

	ID3D11RasterizerState* oldRSState = nullptr;
	device_context->RSGetState(&oldRSState);

	device_context->RSSetState(m_shadow_rasterizer_state.Get());

	// View matrix calculation
	const Vector3 at(0, 0, 0);
	const Vector3 pos = Vector3::TransformNormal(directional_light.GetDirection(), directional_light.GetGameObject()->GetTransform()->GetMatrix());
	const Vector3 up(0, 1, 0);

	Matrix light_view_matrix = Matrix::CreateLookAt(pos, at, up);
	shadow_buffer.mView = light_view_matrix;

	// Frustrum splits calculation
	Vector2 clip_planes = Engine::GetCamera().GetClipPlanes();
	UpdateSplitDistances(clip_planes.x, Math::Min(clip_planes.y, max_shadow_distance));

	// Clear the depth buffer
	render_target->Clear(device_context.Get(), 1.0f, 1.0f, 1.0f, 1.0f);
	device_context->ClearDepthStencilView(m_shadowMapDepthView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	// Bind the render target
	device_context->OMSetRenderTargets(1, render_target->GetRenderTargetView().GetAddressOf(), m_shadowMapDepthView);

	// Cascades generation
	for (unsigned int cascade_index = 0; cascade_index < cascadeCount; ++cascade_index)
	{
		Vector2 cascade_clip_planes = m_frustrumSplits[cascade_index];

		// Compute and set viewport for this cascade
		Vector4 scaleOffset = GetViewportForCascade(cascade_index, cascadeCount);
		shadow_buffer.mTileOffsets[cascade_index] = scaleOffset;

		D3D11_VIEWPORT viewport;
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;
		viewport.TopLeftX = resolution * scaleOffset.x;
		viewport.TopLeftY = resolution * scaleOffset.y;
		viewport.Width = resolution * scaleOffset.z;
		viewport.Height = resolution * scaleOffset.w;
		device_context->RSSetViewports(1, &viewport);

		Matrix proj_matrix;
		GenerateCascade(device_context, mesh_renderers, light_view_matrix, proj_matrix, cascade_clip_planes.x, cascade_clip_planes.y);
		shadow_buffer.mProjections[cascade_index] = proj_matrix;
	}

	device_context->RSSetState(oldRSState);
}


void Redline::ShadowRenderer::GenerateCascade(const ComPtr<ID3D11DeviceContext>& device_context,
	const std::vector<MeshRenderer*>& mesh_renderers,
	Matrix& light_view_matrix,
	Matrix& light_proj_matrix,
	float near_plane, float far_plane) const
{
	light_proj_matrix = CreateCascadeProjectionMatrix(near_plane, far_plane, light_view_matrix);

	// Make sure that previous shadow maps are unbinded
	ID3D11ShaderResourceView* nullSRV[4] = { nullptr, nullptr, nullptr, nullptr };
	device_context->PSSetShaderResources(4, 4, nullSRV);

	// Set the shadow shader
	Engine::GetRenderer().SetShader(m_shadowShader);

	Engine::GetRenderer().CullAndDrawObjects(mesh_renderers, light_view_matrix, light_proj_matrix);
}


Matrix Redline::ShadowRenderer::CreateCascadeProjectionMatrix(float nearPlane, float farPlane, const Matrix& light_view_matrix) const
{
	Vector3 corners[8];
	// Get frustrum corners in world space
	GetCorners(corners, LinearToExponentialDepth(nearPlane), LinearToExponentialDepth(farPlane));

	float minX, maxX, minY, maxY, minZ, maxZ;

	minX = minY = minZ = FLT_MAX;
	maxX = maxY = maxZ = -FLT_MAX;

	for (const auto& corner : corners)
	{
		// Convert from world space to light space
		const Vector3 cornerViewSpace = Vector3::Transform(corner, light_view_matrix);

		minX = min(cornerViewSpace.x, minX);
		maxX = max(cornerViewSpace.x, maxX);
		minY = min(cornerViewSpace.y, minY);
		maxY = max(cornerViewSpace.y, maxY);
		minZ = min(cornerViewSpace.z, minZ);
		maxZ = max(cornerViewSpace.z, maxZ);
	}

	float averageZ = minZ + (maxZ - minZ) * 0.5f;

	return Matrix::CreateOrthographicOffCenter(minX, maxX, maxY, minY, -averageZ - 500.0f, -averageZ + 500.0f);
}


void Redline::ShadowRenderer::GetCorners(Vector3* corners, const float& nearClip, const float& farClip) const
{
	const Matrix invProjMatrix = Engine::GetCamera().GetProjectionMatrix().Invert();
	const Matrix invViewMatrix = Engine::GetCamera().GetViewMatrix().Invert();

	// Homogenous points for source cube in clip-space
	// With -1 to 1 in x/y and 0 to 1 in z (D3D)
	Vector4 v[8] =
	{
		{ -1, -1, nearClip, 1 },
		{ -1, 1, nearClip, 1 },
		{ 1, 1, nearClip, 1 },
		{ 1, -1, nearClip, 1 },
		{ -1, -1, farClip, 1 },
		{ -1, 1, farClip, 1 },
		{ 1, 1, farClip, 1 },
		{ 1, -1, farClip, 1 }
	};

	for (int i = 0; i < 8; ++i)
	{
		// 4x4 * 4x1 matrix/vector multiplication 
		v[i] = Vector4::Transform(v[i], invProjMatrix);
		v[i] = Vector4::Transform(v[i], invViewMatrix);

		// Homogenous to cartesian conversion
		corners[i] = Vector3(v[i] / v[i].w);
	}
}


float Redline::ShadowRenderer::LinearToExponentialDepth(const float linear_depth)
{
	const float zNear = Redline::Engine::GetCamera().GetClipPlanes().x;
	const float zFar = Redline::Engine::GetCamera().GetClipPlanes().y;

	float nonLinearDepth = (zFar + zNear - 2.0f * zNear * zFar / linear_depth) / (zFar - zNear);
	nonLinearDepth = (nonLinearDepth + 1.0f) / 2.0f;
	return nonLinearDepth;
}

/**
* Computes the near and far distances for every frustum slice
* in camera eye space - that is, at what distance does a slice start and end
*/
void Redline::ShadowRenderer::UpdateSplitDistances(float camera_near_plane, float camera_far_plane)
{
	float nd = camera_near_plane;
	float fd = camera_far_plane;

	static float lambda = 0.9f;
	float ratio = fd / nd;
	m_frustrumSplits[0].x = nd;

	for (int i = 1; i < m_num_cascades; i++)
	{
		float si = i / static_cast<float>(m_num_cascades);

		float t_near = lambda * (nd * powf(ratio, si)) + (1 - lambda) * (nd + (fd - nd) * si);
		float t_far = t_near * 1.005f;
		m_frustrumSplits[i].x = t_near;
		m_frustrumSplits[i - 1].y = t_far;
	}

	m_frustrumSplits[m_num_cascades - 1].y = fd;
}


Vector4 Redline::ShadowRenderer::GetViewportForCascade(int cascadeIndex, int cascadeCount)
{
	Vector4 viewport;
	if (cascadeCount == 1)
	{
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.z = 1.0f;
		viewport.w = 1.0f;
	}
	else
	{
		viewport.x = static_cast<float>(cascadeIndex % 2) * 0.5f;
		viewport.y = static_cast<float>(cascadeIndex / 2) * 0.5f;
		viewport.z = 0.5f;
		viewport.w = 0.5f;
	}
	return viewport;
}
