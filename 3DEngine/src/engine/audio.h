#pragma once

#include "debug.h"

namespace Redline
{
	class Audio
	{
	public:
		Audio();
		~Audio();

		bool Initialize();

		bool Update();

		static bool ErrorCheck(const FMOD_RESULT result) 
		{
			if (result != FMOD_OK) 
			{
				Debug::Log("FMOD ERROR: " + result);
				return false;
			}
			return true;
		}

		static FMOD_VECTOR VectorToFmod(const DirectX::SimpleMath::Vector3& vector)
		{
			FMOD_VECTOR fmod_vector;
			fmod_vector.x = vector.x;
			fmod_vector.y = vector.y;
			fmod_vector.z = vector.z;
			return fmod_vector;
		}

		static float dbToVolume(const float dB) { return powf(10.0f, 0.05f * dB); }
		static float VolumeTodB(const float volume) { return 20.0f * log10f(volume); }

		void LoadBank(const std::string& strBankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags = 0);
		void LoadEvent(const std::string& strEventName);
		void LoadSound(const std::string &strSoundName, bool b3d = true, bool bLooping = false, bool bStream = false);
		void UnloadSound(const std::string &strSoundName);
		void Set3DListenerAndOrientation(const DirectX::SimpleMath::Vector3& position, const DirectX::SimpleMath::Vector3& forward, const DirectX::SimpleMath::Vector3& up, const DirectX::SimpleMath::Vector3& velocity = DirectX::SimpleMath::Vector3(0, 0, 0)) const;
		int PlaySounds(const std::string &strSoundName, const DirectX::SimpleMath::Vector3& vPos = DirectX::SimpleMath::Vector3{ 0, 0, 0 }, float fVolumedB = 0.0f);
		void PlayEvent(const std::string &strEventName);
		void SetEventPosition(const std::string &strEventName, const DirectX::SimpleMath::Vector3& position, const DirectX::SimpleMath::Vector3& forward, const DirectX::SimpleMath::Vector3& up, const DirectX::SimpleMath::Vector3& velocity = DirectX::SimpleMath::Vector3(0, 0, 0));
		void StopChannel(int nChannelId);
		void StopEvent(const std::string &strEventName, bool bImmediate = false);
		void GetEventParameter(const std::string &strEventName, const std::string &strEventParameter, float* parameter);
		void SetEventParameter(const std::string &strEventName, const std::string &strParameterName, float fValue);
		void StopAllChannels();
		void SetChannel3dPosition(int nChannelId, const DirectX::SimpleMath::Vector3& vPosition);
		void SetChannelVolume(int nChannelId, float fVolumedB);
		bool IsPlaying(int nChannelId) const;
		bool IsEventPlaying(const std::string &strEventName) const;

	private:
		FMOD::Studio::System* m_studio_system;
		FMOD::System* m_system;

		int mnNextChannelId;

		typedef std::map<std::string, FMOD::Sound*> SoundMap;
		typedef std::map<int, FMOD::Channel*> ChannelMap;
		typedef std::map<std::string, FMOD::Studio::EventInstance*> EventMap;
		typedef std::map<std::string, FMOD::Studio::Bank*> BankMap;
		BankMap mBanks;
		EventMap mEvents;
		SoundMap mSounds;
		ChannelMap mChannels;
	};
}
