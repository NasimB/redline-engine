#pragma once

#include "../stdafx.h"

namespace Redline
{
	class Shader
	{
	public:
		Shader() = default;
		~Shader();

		bool CreateFromFile(const std::string& filePath, const Microsoft::WRL::ComPtr<ID3D11Device>& device, const std::string& vertexShader = "VS", const std::string& pixelShader = "PS");

		bool Recompile(const Microsoft::WRL::ComPtr<ID3D11Device>& device, bool force_recompile = false);

		static void SetGlobalDefine(std::string keyword, std::string value = "1");

		void SetDefine(std::string keyword, std::string value = "1");
		void RemoveDefine(const std::string& keyword);

		ID3D11PixelShader* GetPixelShader() const { return m_pixel_shader.Get(); }
		ID3D11VertexShader* GetVertexShader() const { return m_vertex_shader.Get(); }
		ID3D11InputLayout* GetInputLayout() const { return m_vertex_layout.Get(); }

	private:
		static HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut, bool isPixelShader);

		static std::map<std::string, std::string> s_global_defines;

		bool m_needs_recompile = false;

		std::map<std::string, std::string> m_shader_defines;

		std::string m_shader_path;
		std::string m_vertex_shader_entry_point;
		std::string m_pixel_shader_entry_point;

		Microsoft::WRL::ComPtr<ID3D11VertexShader> m_vertex_shader;
		Microsoft::WRL::ComPtr<ID3D11PixelShader> m_pixel_shader;
		Microsoft::WRL::ComPtr<ID3D11InputLayout> m_vertex_layout;
	};
}
