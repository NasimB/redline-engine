#pragma once

#include "renderer.h"
#include "window.h"
#include "light.h"
#include "camera.h"
#include "timer.h"
#include "input.h"
#include "ResourcesManager.h"
#include "config.h"
#include "audio.h"
#include "debug.h"
#include "physics.h"
#include "gameobject.h"
#include "Rigidbody.h"
#include "SceneManager.h"

#ifdef REDLINE_EDITOR
#include "../editor/Editor.h"
#else
#include "../game/game.h"
#endif

#define BUILDTYPE "Alpha-DEV"

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if (p) { (p)->Release(); (p) = nullptr; } }
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(p)      { if (p) { delete (p); (p) = nullptr; } }
#endif

namespace Redline
{
	class GameObject;

	class Engine
	{
	public:
		Engine();
		~Engine();

		bool Initialize();
		bool Shutdown();
		int Run();
		bool Update();
		bool OnPhysicsUpdate(float delta_time) const;

		// Creates a gameobject and returns a pointer to it
		static GameObject* CreateGameObject(const std::string& name);
		static void CreateGameObject(GameObject* gameObject);
		static void RemoveGameObject(GameObject* gameObject);

		// Returns the renderer system
		static Renderer& GetRenderer() { return m_engine->m_renderer; }
		// Returns the camera system
		static Camera& GetCamera() { return m_engine->m_camera; }
		// Returns the input system
		static Input& GetInput() { return m_engine->m_input; }
		// Returns the config system
		static Config& GetConfig() { return m_engine->m_config; }
		// Returns the physics system
		static Physics& GetPhysics() { return m_engine->m_physics; }
		// Returns the timer system
		static Timer& GetTimer() { return m_engine->m_timer; }
		// Returns the audio system
		static Audio& GetAudio() { return m_engine->m_audio; }
		// Returns the scene manager
		static SceneManager& GetSceneManager() { return m_engine->m_scene_manager; }

		// Get a light by its index. Returns nullptr if index is out of range.
		static Light* GetLight(const size_t index)
		{
			if (index < 0 || index >= m_engine->m_lights.size())
				return nullptr;

			return m_engine->m_lights[index];
		}

		// Returns lights count.
		static size_t GetLightsCount() { return m_engine->m_lights.size(); }

		// Returns a reference to the lights array.
		static std::vector<Light*>& GetLights() { return m_engine->m_lights; }

		static std::vector<MeshRenderer*>& GetMeshRenderers() { return m_engine->m_mesh_renderers; }

		template<typename T> static T* AddComponent(GameObject* game_object)
		{
			T* component = new T(game_object);
			switch (T::GetComponentType())
			{
			default:
			case Component::Unknown:
			{
				Debug::Warn("Unknown component type, you may have forgotten something while adding it.");
				delete component;
				return nullptr;
			}
			case Component::Light:
			case Component::MeshRenderer:
			case Component::Rigidbody:
				//return m_engine->InsertComponent(T(game_object));
				m_engine->InsertComponent(component);
			}

			return component;
		}

		static void RemoveComponent(Component* component);

	private:
		static void GetDesktopResolution(int& horizontal, int& vertical);

		//Light* InsertComponent(Light&& component) { m_lights.emplace_back(component);  return &m_lights.back(); }
		void InsertComponent(Light* component) { m_lights.emplace_back(component); }
		//MeshRenderer* InsertComponent(MeshRenderer&& component) { m_mesh_renderers.emplace_back(component);  return &m_mesh_renderers.back(); }
		void InsertComponent(MeshRenderer* component) { m_mesh_renderers.emplace_back(component); }
		//Rigidbody* InsertComponent(Rigidbody&& component) { m_rigidbodies.emplace_back(component); return &m_rigidbodies.back(); }
		void InsertComponent(Rigidbody* component) { m_rigidbodies.emplace_back(component); }

		void RemoveLight(Light* light);
		void RemoveMeshRenderer(MeshRenderer* renderer);
		void RemoveRigidBody(Rigidbody* rigidBody);

#ifdef REDLINE_EDITOR
		RedlineEditor::Editor m_editor;
#else
		RedlineGame::Game m_game;
#endif
		Window m_window;
		Timer m_timer;
		Camera m_camera;
		Physics m_physics;
		Input m_input;
		Audio m_audio;
		ResourcesManager m_resources;
		Debug m_debug;
		Config m_config;
		Renderer m_renderer;
		SceneManager m_scene_manager;

		bool m_input_pressed;

		std::vector<GameObject*> m_gameobjects;
		//std::vector<Light> m_lights;
		std::vector<Light*> m_lights;
		//std::vector<MeshRenderer> m_mesh_renderers;
		std::vector<MeshRenderer*> m_mesh_renderers;
		//std::vector<Rigidbody> m_rigidbodies;
		std::vector<Rigidbody*> m_rigidbodies;

		static Engine* m_engine;

		bool complete = false;
	};
}
