﻿#include "BloomRenderer.h"
#include "debug.h"
#include "ResourcesManager.h"
#include "engine.h"


Redline::BloomRenderer::BloomRenderer():
	m_bloomBlur_A_RT(nullptr),
	m_bloomBlur_B_RT(nullptr),
	m_tonemapped_RT(nullptr),
	m_exposure_A_RT(nullptr),
	m_exposure_B_RT(nullptr),
	m_combine_shader(nullptr),
	m_adaptation_shader(nullptr),
	m_blurHShader(nullptr), 
	m_blurVShader(nullptr),
	m_luminance_buffer(nullptr),
	m_use_A_buffer(false),
	m_bloomWidth(0),
	m_bloomHeight(0), 
	m_render_width(0), 
	m_render_height(0)
{
}


Redline::BloomRenderer::~BloomRenderer()
{

}


bool Redline::BloomRenderer::Initialize(const Microsoft::WRL::ComPtr<ID3D11Device>& device, const int render_width, const int render_height)
{
	m_render_width = static_cast<float>(render_width);
	m_render_height = static_cast<float>(render_height);

	m_bloomWidth = m_render_width * m_bloomRenderScale;
	m_bloomHeight = m_render_height * m_bloomRenderScale;

	const unsigned int bloomWidth = static_cast<unsigned int>(m_bloomWidth);
	const unsigned int bloomHeight = static_cast<unsigned int>(m_bloomHeight);

	m_bloomBlur_A_RT = RenderTarget::Create(device.Get(), bloomWidth, bloomHeight, DXGI_FORMAT_R16G16B16A16_UNORM);
	if (m_bloomBlur_A_RT == nullptr)
	{
		Debug::Err("Failed to create the bloom blur render target!");
		return false;
	}

	m_bloomBlur_B_RT = RenderTarget::Create(device.Get(), bloomWidth, bloomHeight, DXGI_FORMAT_R16G16B16A16_UNORM);
	if (m_bloomBlur_B_RT == nullptr)
	{
		Debug::Err("Failed to create the bloom blur render target!");
		return false;
	}

	m_combine_shader = ResourcesManager::GetShader("data\\shaders\\tonemapping.fx");
	if (m_combine_shader == nullptr)
	{
		Debug::Err("Failed to compile the bloom combine shader!");
		return false;
	}

	m_adaptation_shader = new Shader();
	if (!m_adaptation_shader->CreateFromFile("data\\shaders\\tonemapping.fx", device, "VS", "PS_LuminanceAdaptation"))
	{
		Debug::Err("Failed to compile the exposure adaptation shader!");
		return false;
	}

	// Create luminance buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(LuminanceBuffer);
	if (FAILED(device->CreateBuffer(&bd, nullptr, &m_luminance_buffer)))
	{
		Debug::Err("Failed to create the tonemapping buffer!");
		return false;
	}

	m_tonemapped_RT = RenderTarget::Create(device.Get(), render_width, render_height, DXGI_FORMAT_R16G16B16A16_UNORM, true);
	if (m_tonemapped_RT == nullptr)
	{
		Debug::Err("Failed to create the tonemapped render target!");
		return false;
	}

	m_exposure_A_RT = RenderTarget::Create(device.Get(), 1, 1, DXGI_FORMAT_R16_FLOAT);
	if (m_exposure_A_RT == nullptr)
	{
		Debug::Err("Failed to create the exposure render target!");
		return false;
	}

	m_exposure_B_RT = RenderTarget::Create(device.Get(), 1, 1, DXGI_FORMAT_R16_FLOAT);
	if (m_exposure_B_RT == nullptr)
	{
		Debug::Err("Failed to create the exposure render target!");
		return false;
	}

	m_blurHShader = ResourcesManager::GetShader("data\\shaders\\blurH.fx");
	if (m_blurHShader == nullptr)
	{
		Debug::Err("Failed to compile the horizontal blur shader!");
		return false;
	}

	m_blurVShader = ResourcesManager::GetShader("data\\shaders\\blurV.fx");
	if (m_blurVShader == nullptr)
	{
		Debug::Err("Failed to compile the vertical blur shader!");
		return false;
	}

	return true;
}


void Redline::BloomRenderer::Shutdown()
{
	if (m_bloomBlur_B_RT)
	{
		delete m_bloomBlur_B_RT;
		m_bloomBlur_B_RT = nullptr;
	}

	if (m_bloomBlur_A_RT)
	{
		delete m_bloomBlur_A_RT;
		m_bloomBlur_A_RT = nullptr;
	}

	SAFE_RELEASE(m_luminance_buffer);

	if (m_tonemapped_RT)
	{
		delete m_tonemapped_RT;
		m_tonemapped_RT = nullptr;
	}

	if (m_exposure_A_RT)
	{
		delete m_exposure_A_RT;
		m_exposure_A_RT = nullptr;
	}

	if (m_exposure_B_RT)
	{
		delete m_exposure_B_RT;
		m_exposure_B_RT = nullptr;
	}

	if (m_adaptation_shader)
	{
		delete m_adaptation_shader;
		m_adaptation_shader = nullptr;
	}
}


bool Redline::BloomRenderer::Render(const float delta_time, const Microsoft::WRL::ComPtr<ID3D11DeviceContext>& device_context, RenderTarget* base_RT, RenderTarget* output_RT)
{
	m_use_A_buffer = !m_use_A_buffer;

	device_context->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	device_context->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	// It's the same full screen triangle vertex shader for all theses passes, so set it once now
	device_context->VSSetShader(m_combine_shader->GetVertexShader(), nullptr, 0);

	// Generate mips on the last buffer used, to compute luminance on the highest mip (1x1)
	base_RT->GenerateMips(device_context.Get());

	D3D11_VIEWPORT viewport;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;

	// Exposure adaptation
	{
		ID3D11RenderTargetView* renderTarget = (m_use_A_buffer ? m_exposure_A_RT : m_exposure_B_RT)->GetRenderTargetView().Get();
		device_context->OMSetRenderTargets(1, &renderTarget, nullptr);

		viewport.Width = 1.0f;
		viewport.Height = 1.0f;
		device_context->RSSetViewports(1, &viewport);

		device_context->PSSetShader(m_adaptation_shader->GetPixelShader(), nullptr, 0);

		LuminanceBuffer buffer =
		{
			delta_time,
			m_minimum_exposure,
			m_maximum_exposure,
			m_adaptation_speed,
			bloom_ammount
		};
		device_context->UpdateSubresource(m_luminance_buffer, 0, nullptr, &buffer, 0, 0);

		device_context->PSSetConstantBuffers(0, 1, &m_luminance_buffer);

		ID3D11ShaderResourceView* srv[] = { base_RT->GetShaderResourceView().Get(), (m_use_A_buffer ? m_exposure_B_RT : m_exposure_A_RT)->GetShaderResourceView().Get() };
		device_context->PSSetShaderResources(0, 2, srv);

		device_context->Draw(3, 0);
	}

	viewport.Width = m_bloomWidth;
	viewport.Height = m_bloomHeight;
	device_context->RSSetViewports(1, &viewport);

	for (int i = 0; i < 2; ++i)
	{
		// BLUR X
		{
			ID3D11ShaderResourceView* emptySRV[] = { nullptr };
			device_context->PSSetShaderResources(0, 1, emptySRV);

			device_context->OMSetRenderTargets(1, m_bloomBlur_B_RT->GetRenderTargetView().GetAddressOf(), nullptr);
			device_context->PSSetShader(m_blurHShader->GetPixelShader(), nullptr, 0);

			device_context->PSSetShaderResources(0, 1, (i == 0 ? base_RT : m_bloomBlur_A_RT)->GetShaderResourceView().GetAddressOf());

			device_context->Draw(3, 0);
		}

		// BLUR Y
		{
			ID3D11ShaderResourceView* emptySRV[] = { nullptr };
			device_context->PSSetShaderResources(0, 1, emptySRV);

			device_context->OMSetRenderTargets(1, m_bloomBlur_A_RT->GetRenderTargetView().GetAddressOf(), nullptr);
			device_context->PSSetShader(m_blurVShader->GetPixelShader(), nullptr, 0);

			device_context->PSSetShaderResources(0, 1, m_bloomBlur_B_RT->GetShaderResourceView().GetAddressOf());

			device_context->Draw(3, 0);
		}
	}


	// Tonemapping + bloom
	{
		viewport.Width = m_render_width;
		viewport.Height = m_render_height;
		device_context->RSSetViewports(1, &viewport);

		ID3D11ShaderResourceView* emptySRV[] = { nullptr };
		device_context->PSSetShaderResources(0, 1, emptySRV);

		device_context->OMSetRenderTargets(1, output_RT->GetRenderTargetView().GetAddressOf(), nullptr);
		device_context->PSSetShader(m_combine_shader->GetPixelShader(), nullptr, 0);

		ID3D11ShaderResourceView* srv[] = { base_RT->GetShaderResourceView().Get(), m_bloomBlur_A_RT->GetShaderResourceView().Get(), (m_use_A_buffer ? m_exposure_A_RT : m_exposure_B_RT)->GetShaderResourceView().Get() };
		device_context->PSSetShaderResources(0, 3, srv);

		device_context->Draw(3, 0);
	}


	ID3D11ShaderResourceView* srv[] = { nullptr, nullptr, nullptr };
	device_context->PSSetShaderResources(0, 3, srv);
	return true;
}
