#pragma once

#include <math.h>

namespace Redline
{
#define EPSILON 1e-6

#define M_PI 3.14159265358979323846f
#define M_PI_2 M_PI / 2.0f

#define DEG2RAD M_PI / 180.0f
#define RAD2DEG 180.0f / M_PI

	class Math
	{
	public:
		Math() {}
		~Math() {}

		// Linear Interpolation from A to B
		template<typename T> static T Lerp(T a, T b, T t) { return a + t * (b - a); }

		// Returns the absolute value
		template<typename T> static T Abs(T value) { return abs(value); }

		// Returns the absolute value
		template<typename T> static T Sign(T value) { return value / Abs(value); }

		// Returns the rounded value
		template<typename T> static T Round(T value) { return round(value); }

		// Returns the floored value
		template<typename T> static T Floor(T value) { return floor(value); }

		// Returns the ceiled value
		template<typename T> static T Ceil(T value) { return ceil(value); }

		// Returns the power of the value
		template<typename T> static T Pow(T value, T power) { return pow(value, power); }

		// Returns the square root of the value
		template<typename T> static T Sqrt(T value) { return sqrt(value); }

		// Returns the exponent of the value
		template<typename T> static T Exp(T value) { return exp(value); }

		// Returns the sinus of the value
		template<typename T> static T Sin(T value) { return sin(value); }

		// Returns the cosinus of the value
		template<typename T> static T Cos(T value) { return cos(value); }

		// Returns the tangent of the value
		template<typename T> static T Tan(T value) { return tan(value); }

		// Returns the arc tangent of the value
		template<typename T> static T Atan(T value) { return atan(value); }

		// Returns the 2D arc tangent of the value
		template<typename T> static T Atan2(T y, T x) { return atan2(y, x); }

		// Returns the minimum value between the two
		template<typename T> static T Min(T a, T b) { return a <= b ? a : b; }

		// Returns the maximum value between the two
		template<typename T> static T Max(T a, T b) { return a >= b ? a : b; }

		// Clamps the given value between the two others
		template<typename T> static T Clamp(T value, T min, T max) { return Max(Min(value, max), min); }


		static void QuaternionToEuler(const DirectX::SimpleMath::Quaternion& quaternion, DirectX::SimpleMath::Vector3 &euler)
		{
			float w = quaternion.w;
			float x = quaternion.x;
			float y = quaternion.y;
			float z = quaternion.z;

			float sqw = w*w;
			float sqx = x*x;
			float sqy = y*y;
			float sqz = z*z;

			euler.z = atan2(2.0f * (x*y + z*w), (sqx - sqy - sqz + sqw)) * (180.0f / M_PI);
			euler.x = atan2(2.0f * (y*z + x*w), (-sqx - sqy + sqz + sqw)) * (180.0f / M_PI);
			euler.y = asin(-2.0f * (x*z - y*w)) * (180.0f / M_PI);
		}
	};
}
