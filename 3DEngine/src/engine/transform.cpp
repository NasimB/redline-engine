#include "transform.h"
#include "debug.h"
#include "gameobject.h"
#include "Rigidbody.h"

using namespace std;
using namespace DirectX::SimpleMath;


Redline::Transform::Transform(GameObject* go) :
	m_position(),
	m_rotation(),
	m_scale(1.0f),
	m_pos_dirty(false),
	m_rot_dirty(false),
	m_scale_dirty(false),
	isWorldDirty(false),
	m_rigidbody(nullptr),
	m_parent(nullptr)
{
	m_game_object = go;
}


Redline::Transform::~Transform()
{
	for (auto child : m_childrens)
	{
		child->SetParent(nullptr);
	}

	if (m_parent != nullptr)
		m_parent->RemoveChild(this);
}


void Redline::Transform::SetParent(Transform* parent)
{
#ifdef _DEBUG
	if (parent == this)
	{
		Debug::Warn("Transform::SetParent: Trying to set transform as its own parent, this will cause a crash on Release build.");
		parent = nullptr;
	}
#endif

	if (m_parent != nullptr)
		m_parent->RemoveChild(this);

	if (parent != nullptr)
		parent->AddChild(this);

	m_parent = parent;
}


Matrix& Redline::Transform::GetMatrix()
{
	RebuildMatrix();

	bool hasRigidbody = m_rigidbody != nullptr;

	if (m_parent != nullptr && !hasRigidbody)
	{
		m_world_matrix = m_local_matrix * m_parent->GetMatrix();
		return m_world_matrix;
	}

	return m_local_matrix;
}


bool Redline::Transform::RebuildMatrix()
{
	bool hasRigidbody = m_rigidbody != nullptr;
	if (hasRigidbody)
	{
		bool isActive = m_rigidbody->IsActive();

		if (m_pos_dirty)
		{
			m_rigidbody->SetPosition(m_position);
		}
		else if (isActive)
		{
			m_position = m_rigidbody->GetPosition();
			m_pos_dirty = true;
		}

		if (m_rot_dirty)
		{
			m_rigidbody->SetRotation(m_rotation);
		}
		else if (isActive)
		{
			m_rotation = m_rigidbody->GetRotation();
			m_rot_dirty = true;
		}
	}

	if (m_pos_dirty)
		m_positionMatrix = Matrix::CreateTranslation(m_position);

	if (m_rot_dirty)
		m_rotationMatrix = Matrix::CreateFromQuaternion(m_rotation);

	if (m_scale_dirty)
		m_scaleMatrix = Matrix::CreateScale(m_scale);

	bool has_changed = m_pos_dirty || m_rot_dirty || m_scale_dirty;

	if (has_changed)
	{
		m_local_matrix = (m_scaleMatrix * m_rotationMatrix * m_positionMatrix);

		m_pos_dirty = false;
		m_rot_dirty = false;
		m_scale_dirty = false;
	}

#ifndef REDLINE_EDITOR
#ifndef _DEBUG
	showGizmo = false;
#endif
#endif

	if (showGizmo)
	{
		bool asleep = m_rigidbody != nullptr && !m_rigidbody->GetRigidbody()->isActive();

		Matrix world_matrix = m_parent != nullptr ? m_local_matrix * m_parent->GetMatrix() : m_local_matrix;

		Vector3 world_position = Vector3::Transform(Vector3::Zero, world_matrix);
		Vector3 up = Vector3::TransformNormal(Vector3::Up, world_matrix);
		Vector3 right = Vector3::TransformNormal(Vector3::Right, world_matrix);
		Vector3 forward = Vector3::TransformNormal(-Vector3::Forward, world_matrix);

		Debug::Line(world_position, world_position + right, Vector3(asleep ? 0.5f : 1.0f, 0.0f, 0.0f));
		Debug::Line(world_position, world_position + up, Vector3(0.0f, asleep ? 0.5f : 1.0f, 0.0f));
		Debug::Line(world_position, world_position + forward, Vector3(0.0f, 0.0f, asleep ? 0.5f : 1.0f));
	}

	return has_changed;
}


void Redline::Transform::AddChild(Transform* child)
{
	m_childrens.emplace_back(child);
}


void Redline::Transform::RemoveChild(Transform* child)
{
	if (m_childrens.empty())
		return;

	vector<Transform*>::iterator it = find(m_childrens.begin(), m_childrens.end(), child);

	if (it != m_childrens.end())
		m_childrens.erase(it);
}


#ifdef REDLINE_EDITOR
void Redline::Transform::ShowInspector()
{
	if (!ImGui::TreeNodeEx("Transform", ImGuiTreeNodeFlags_DefaultOpen))
		return;

	bool changed;

	if ((changed = ImGui::DragFloat3("Position", reinterpret_cast<float*>(&m_position), 0.1f)))
		m_pos_dirty = true;

	static Vector3 euler;
	if (changed |= ImGui::DragFloat3("Rotation", reinterpret_cast<float*>(&euler), 0.1f))
		SetRotation(euler * float(DEG2RAD));
	else
		Math::QuaternionToEuler(m_rotation, euler);

	if (changed |= ImGui::DragFloat3("Scale", reinterpret_cast<float*>(&m_scale), 0.1f))
		m_scale_dirty = true;

	if (changed)
	{
		RebuildMatrix();

		if (m_rigidbody != nullptr)
			m_rigidbody->RefreshCollider();
	}

	ImGui::TreePop();
}


void Redline::Transform::ShowHierarchy(int& id_clicked, GameObject* selected) const
{
	if (!m_game_object)
		return;

	unsigned int go_id = static_cast<unsigned int>(m_game_object->GetID());

	size_t num_childrens = m_childrens.size();

	ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;

	if(m_game_object == selected)
		node_flags |= ImGuiTreeNodeFlags_Selected;

	if(num_childrens == 0)
		node_flags |= ImGuiTreeNodeFlags_Leaf;

	// Node
	bool node_open = ImGui::TreeNodeEx(reinterpret_cast<void*>(m_game_object), node_flags, "(%d) %s", go_id, m_game_object->GetName().c_str());

	if (ImGui::IsItemClicked())
		id_clicked = static_cast<int>(m_game_object->GetID());

	if (node_open)
	{
		for (size_t i = 0; i < num_childrens; ++i)
		{
			m_childrens[i]->ShowHierarchy(id_clicked, selected);
		}
		ImGui::TreePop();
	}
}
#endif
