#include "shader.h"
#include "debug.h"

#define D3D_COMPILE_STANDARD_FILE_INCLUDE ((ID3DInclude*)(UINT_PTR)1)

using namespace Microsoft::WRL;

std::map<std::string, std::string> Redline::Shader::s_global_defines;

Redline::Shader::~Shader()
{
	m_vertex_layout.Reset();
	m_vertex_shader.Reset();
	m_pixel_shader.Reset();
}


void Redline::Shader::SetGlobalDefine(std::string keyword, std::string value)
{
	s_global_defines.emplace(keyword, value);
}

void Redline::Shader::SetDefine(std::string keyword, std::string value)
{
	auto it = m_shader_defines.find(keyword);
	if (it == m_shader_defines.end())
	{
		m_shader_defines.emplace(keyword, value);
		m_needs_recompile = true;
	}
	else
	{
		Debug::Warn("Shader: Trying to redefine an already existing define! Try removing the previous one before.");
	}
}

void Redline::Shader::RemoveDefine(const std::string& keyword)
{
	auto it = m_shader_defines.find(keyword);
	if (it == m_shader_defines.end())
	{
		m_shader_defines.erase(it);
		m_needs_recompile = true;
	}
}


bool Redline::Shader::CreateFromFile(const std::string& filePath, const ComPtr<ID3D11Device>& device, const std::string& vertexShader, const std::string& pixelShader)
{
	// Store these in case we need to recompile
	m_shader_path = filePath;
	m_vertex_shader_entry_point = vertexShader;
	m_pixel_shader_entry_point = pixelShader;
	m_needs_recompile = true;

	return Recompile(device);
}


bool Redline::Shader::Recompile(const Microsoft::WRL::ComPtr<ID3D11Device>& device, bool force_recompile)
{
	if(!force_recompile && !m_needs_recompile)
	{
		return true;
	}

	std::wstring wpath = std::wstring(m_shader_path.begin(), m_shader_path.end());

	bool use_hlsl_5 = device->GetFeatureLevel() >= D3D_FEATURE_LEVEL_11_0;

	HRESULT hr;

	// Compile the vertex shader
	ID3DBlob* pVSBlob = nullptr;
	hr = CompileShaderFromFile(wpath.c_str(), m_vertex_shader_entry_point.c_str(), use_hlsl_5 ? "vs_5_0" : "vs_4_0", &pVSBlob, false);
	if (FAILED(hr))
	{
		Debug::Err("Failed to compile the vertex shader!");
		return false;
	}

	// Create the vertex shader
	hr = device->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, m_vertex_shader.ReleaseAndGetAddressOf());
	if (FAILED(hr))
	{
		pVSBlob->Release();
		Debug::Err("Failed to create the vertex shader!");
		return false;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = device->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), m_vertex_layout.ReleaseAndGetAddressOf());
	pVSBlob->Release();
	if (FAILED(hr))
	{
		Debug::Err("Failed to create the input layout!");
		return false;
	}

	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
	hr = CompileShaderFromFile(wpath.c_str(), m_pixel_shader_entry_point.c_str(), use_hlsl_5 ? "ps_5_0" : "ps_4_0", &pPSBlob, true);
	// TODO: Use this to load compiled shader, do a proper shader cache system
	//hr = D3DReadFileToBlob(wpath.c_str(), &pPSBlob);
	if (FAILED(hr))
	{
		Debug::Err("Failed to compile the pixel shader!");
		return false;
	}

	// Create the pixel shader
	hr = device->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, m_pixel_shader.ReleaseAndGetAddressOf());
	pPSBlob->Release();
	if (FAILED(hr))
	{
		Debug::Err("Failed to create the pixel shader!");
		return false;
	}

	m_needs_recompile = false;

	return true;
}


//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DCompile
//
// With VS 11, we could load up prebuilt .cso files instead...
//--------------------------------------------------------------------------------------
HRESULT Redline::Shader::CompileShaderFromFile(const LPCWSTR szFileName, const LPCSTR szEntryPoint, const LPCSTR szShaderModel, ID3DBlob** ppBlobOut, const bool isPixelShader)
{
	HRESULT hr;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	// Disable optimizations to further improve shader debugging
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	// TODO: Do a proper shader variants system
	D3D_SHADER_MACRO* macros = new D3D_SHADER_MACRO[s_global_defines.size() + 2];
	ZeroMemory(macros, sizeof(macros));

	int i = 0;
	for (const auto& kv : s_global_defines)
	{
		macros[i].Name = kv.first.c_str();
		macros[i].Definition = kv.second.c_str();
		i++;
	}

	// Set keyword to indicate we are compiling pixel or vertex shader
	macros[i].Name = isPixelShader ? "REDLINE_PIXEL" : "REDLINE_VERTEX";
	macros[i].Definition = "1";
	i++;

	// Last macro should always be null
	macros[i].Name = NULL;
	macros[i].Definition = NULL;

	ID3DBlob* pErrorBlob = nullptr;
	hr = D3DCompileFromFile(szFileName, macros, D3D_COMPILE_STANDARD_FILE_INCLUDE, szEntryPoint, szShaderModel, dwShaderFlags, 0, ppBlobOut, &pErrorBlob);

	delete[] macros;

	if (FAILED(hr))
	{
		if (pErrorBlob)
		{
			std::string error = reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer());
			Debug::Err("Shader compilation result:\n" + error);
			pErrorBlob->Release();
		}
		return hr;
	}

	if (pErrorBlob)
	{
		pErrorBlob->Release();
	}

	return S_OK;
}
