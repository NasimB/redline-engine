#include "texture.h"

Redline::Texture::Texture() :
	m_texture(nullptr),
	m_shader_ressource_view(nullptr)
{
}

Redline::Texture::~Texture()
{
	m_shader_ressource_view.Reset();
	m_texture.Reset();
}


bool Redline::Texture::CreateFromFile(const std::string& path, const Microsoft::WRL::ComPtr<ID3D11Device>& device)
{
	std::wstring wpath(path.begin(), path.end());

	if (FAILED(DirectX::CreateDDSTextureFromFile(device.Get(), wpath.c_str(), m_texture.ReleaseAndGetAddressOf(), m_shader_ressource_view.ReleaseAndGetAddressOf())))
	{
		return false;
	}
	return true;
}
