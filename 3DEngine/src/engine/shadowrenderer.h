#pragma once

#include "../stdafx.h"
#include "shader.h"
#include "meshrenderer.h"

#define MAX_CASCADES 4

namespace Redline 
{
	class RenderTarget;
	class Light;

	struct ShadowsBuffer
	{
		DirectX::XMMATRIX mView;
		DirectX::XMMATRIX mProjections[MAX_CASCADES];
		DirectX::XMVECTOR mTileOffsets[MAX_CASCADES];
		float shadowResolution;
	};

	class ShadowRenderer
	{
	public:
		ShadowRenderer();
		~ShadowRenderer();

		bool Initialize(const Microsoft::WRL::ComPtr<ID3D11Device>& device);

		void GenerateCascadedShadowMap(const Microsoft::WRL::ComPtr<ID3D11DeviceContext>& device_context,
										const std::vector<MeshRenderer*>& mesh_renderers,
										const Light& directional_light,
										ShadowsBuffer& shadow_buffer,
										RenderTarget* render_target,
										unsigned int cascadeCount = 0);

		int GetShadowResolution() const { return shadow_map_resolution; }

	private:
		// Common
		Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_shadow_rasterizer_state;
		Shader* m_shadowShader;


		// Cascaded Shadow Mapping
		bool InitializeCascadedShadowMaps(const Microsoft::WRL::ComPtr<ID3D11Device>& device);
		void ShutdownCascadedShadowMaps() const;
		void GenerateCascade(const Microsoft::WRL::ComPtr<ID3D11DeviceContext>& device_context, const std::vector<MeshRenderer*>& mesh_renderers, DirectX::SimpleMath::Matrix& light_view_matrix, 
			DirectX::SimpleMath::Matrix& light_proj_matrix, float near_plane, float far_plane) const;
		DirectX::SimpleMath::Matrix CreateCascadeProjectionMatrix(float nearPlane, float farPlane, const DirectX::SimpleMath::Matrix& light_view_matrix) const;
		void GetCorners(DirectX::SimpleMath::Vector3* corners, const float& nearClip, const float& farClip) const;
		static float LinearToExponentialDepth(float linear_depth);
		void UpdateSplitDistances(float camera_near_plane, float camera_far_plane);
		static DirectX::SimpleMath::Vector4 GetViewportForCascade(int cascadeIndex, int cascadeCount);

		ID3D11Texture2D* m_shadowMapDepthBuffer;
		ID3D11DepthStencilView* m_shadowMapDepthView;

		int shadow_map_resolution = 512;
		int m_num_cascades = MAX_CASCADES;
		float max_shadow_distance = 1500.0f;

		DirectX::SimpleMath::Vector2 m_frustrumSplits[MAX_CASCADES];
		
	};
}
