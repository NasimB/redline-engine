#pragma once

#include "mesh.h"
#include "texture.h"
#include "shader.h"

namespace Redline
{
	/*class Resource
	{
	public:
		Resource();
		~Resource();

		bool LoadResource(const std::string& path);
		void UnloadResource();


	};*/

	class ResourcesManager
	{
	public:
		ResourcesManager();
		~ResourcesManager();

		bool Initialize(bool useMultiThreading);
		static bool Shutdown();

		static Texture* GetTexture(const std::string& path);
		static Shader* GetShader(const std::string& path);
		static Mesh* GetMesh(const std::string& path);

		static ResourcesManager* m_resources;

	private:
		bool LoadTextures(const std::vector<std::string>& paths);
		Texture* LoadTexture(const std::string& path);

		bool LoadShaders(const std::vector<std::string>& paths);
		bool LoadMeshes(const std::vector<std::string>& paths);

		/*std::map<int, Texture*> m_textures;
		std::map<int, Shader*> m_shaders;
		std::map<int, Mesh*> m_meshes;*/

		std::map<std::string, Texture*> m_textures_map;
		std::map<std::string, Shader*> m_shaders_map;
		std::map<std::string, Mesh*> m_meshes_map;

		/*std::vector<std::string> m_textures_needed;
		std::vector<std::string> m_meshes_needed;
		std::vector<std::string> m_shaders_needed;*/

		std::string m_start_directory;
	};
}
