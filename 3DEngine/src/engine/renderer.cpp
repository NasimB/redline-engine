#include "renderer.h"
#include "engine.h"
#include "debug.h"
#include "mesh.h"

using namespace Microsoft::WRL;
using namespace std;
using namespace DirectX::SimpleMath;


Redline::Renderer::Renderer() :
	m_vsync_enabled(false),
	m_window_width(1280),
	m_window_height(720),
	m_width(1280),
	m_height(720),
	m_device(nullptr),
	m_deviceContext(nullptr),
	m_swapChain(nullptr),
	m_renderTargetView(nullptr),
	m_rasterizerState(nullptr),
	m_constants_buffer(nullptr),
	m_lightCB(nullptr),
	m_world_matrix_buffer(nullptr),
	m_view_matrix_buffer(nullptr),
	m_projection_matrix_buffer(nullptr),
	m_bufferMaterial(nullptr),
	m_bufferVSSkybox(nullptr),
	m_bufferPSSkybox(nullptr),
	m_skybox(nullptr),
	m_wireframeShader(nullptr),
	m_unlit_shader(nullptr),
	m_depthStencil(nullptr),
	m_depthStencilView(nullptr),
	m_depthStencilState(nullptr),
	m_depthDisabledStencilState(nullptr),
	m_GBufferShader(nullptr),
	m_deferredShader(nullptr),
	m_environmentDepthStencil(nullptr),
	m_environmentDepthStencilView(nullptr),
	m_environment_cubemap(nullptr),
	m_environment_cubemap_filtered(nullptr),
	m_environement_filtering_shader(nullptr),
	m_env_filtering_buffer(nullptr),
	m_simple_shader(nullptr),
	m_shadow_constant_buffer(nullptr),
	m_shadowmap_render_target(nullptr),
	m_postProcessRT_A(nullptr),
	m_postProcessRT_B(nullptr),
	m_fog_shader(nullptr),
	m_fxaa_enabled(false),
	m_smaa_enabled(false),
	m_draw_call_count(0)
{
}


Redline::Renderer::~Renderer()
{
	m_renderTargetView.Reset();

#ifdef USE_IMGUI
	ImGui_ImplDX11_Shutdown();
	ImGui::DestroyContext();
#endif

	ShutdownEnvironmentCubemap();

	ShutdownBloom();

	ShutdownSSAO();

	ShutdownVolumetricFog();

	ShutdownAA();

	ShutdownSamplers();

	ShutdownShadowMap();

	if (m_postProcessRT_A)
	{
		delete m_postProcessRT_A;
		m_postProcessRT_A = nullptr;
	}

	if (m_postProcessRT_B)
	{
		delete m_postProcessRT_B;
		m_postProcessRT_B = nullptr;
	}

	SAFE_RELEASE(m_depthDisabledStencilState);
	SAFE_RELEASE(m_depthStencilState);
	SAFE_RELEASE(m_depthStencilView);
	SAFE_RELEASE(m_depthStencil);

	for (unsigned int i = 0; i < BUFFER_COUNT; i++)
	{
		delete m_renderTargets[i];
		m_renderTargets[i] = nullptr;
	}

	m_rasterizerState.Reset();
	SAFE_RELEASE(m_constants_buffer);

	SAFE_RELEASE(m_world_matrix_buffer);
	SAFE_RELEASE(m_view_matrix_buffer);
	SAFE_RELEASE(m_projection_matrix_buffer);

	SAFE_RELEASE(m_bufferVSSkybox);
	SAFE_RELEASE(m_bufferPSSkybox);
	SAFE_RELEASE(m_bufferMaterial);
	SAFE_RELEASE(m_lightCB);
	m_blend_state_opaque.Reset();
	m_blend_state_additive.Reset();

	if (m_skybox)
	{
		delete m_skybox;
		m_skybox = nullptr;
	}

	m_deviceContext.Reset();

	m_swapChain.Reset();

#ifdef _DEBUG
	m_d3dDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
	m_d3dDebug.Reset();
#endif // _DEBUG

	m_device.Reset();
}


bool Redline::Renderer::Initialize(const HWND hwnd, const bool vSync)
{
	m_vsync_enabled = vSync;

	RECT rect;
	GetClientRect(hwnd, &rect);

	m_window_width = m_width = rect.right - rect.left;
	m_window_height = m_height = rect.bottom - rect.top;

	m_renderScale = Engine::GetConfig().render_scale;

	HRESULT hr = S_OK;

	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		m_driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDevice(nullptr, m_driverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, m_device.ReleaseAndGetAddressOf(), &m_featureLevel, m_deviceContext.ReleaseAndGetAddressOf());

		if (hr == E_INVALIDARG)
		{
			// DirectX 11.0 platforms will not recognize D3D_FEATURE_LEVEL_11_1 so we need to retry without it
			hr = D3D11CreateDevice(nullptr, m_driverType, nullptr, createDeviceFlags, &featureLevels[1], numFeatureLevels - 1,
				D3D11_SDK_VERSION, m_device.ReleaseAndGetAddressOf(), &m_featureLevel, m_deviceContext.ReleaseAndGetAddressOf());
		}

		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
	{
		Debug::Err("Failed to create rendering device!");
		return false;
	}

	// Obtain DXGI factory from device (since we used nullptr for pAdapter above)
	IDXGIFactory1* dxgiFactory = nullptr;
	{
		ComPtr<IDXGIDevice> dxgiDevice = nullptr;
		hr = m_device.As(&dxgiDevice);
		if (SUCCEEDED(hr))
		{
			IDXGIAdapter* adapter = nullptr;
			hr = dxgiDevice->GetAdapter(&adapter);
			if (SUCCEEDED(hr))
			{
				adapter->GetParent(__uuidof(IDXGIFactory1), reinterpret_cast<void**>(&dxgiFactory));
				adapter->Release();
			}
			dxgiDevice.Reset();
		}
	}

	if (dxgiFactory == nullptr)
	{
		Debug::Err("Failed to obtain the DXGI factory!");
		return false;
	}

	UINT numerator = 0;
	UINT denominator = 0;
	DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM;
	{
		IDXGIAdapter* adapter;
		// Use the factory to create an adapter for the primary graphics interface (video card).
		hr = dxgiFactory->EnumAdapters(0, &adapter);
		if (FAILED(hr))
		{
			return false;
		}

		IDXGIOutput* adapterOutput;
		// Enumerate the primary adapter output (monitor).
		hr = adapter->EnumOutputs(0, &adapterOutput);
		if (FAILED(hr))
		{
			return false;
		}

		UINT numModes;
		// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
		hr = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
		if (FAILED(hr))
		{
			return false;
		}

		// Create a list to hold all the possible display modes for this monitor/video card combination.
		DXGI_MODE_DESC * displayModeList = new DXGI_MODE_DESC[numModes];

		// Now fill the display mode list structures.
		hr = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
		if (FAILED(hr))
		{
			return false;
		}

		// Now go through all the display modes and find the one that matches the screen width and height.
		// When a match is found store the numerator and denominator of the refresh rate for that monitor.
		const UINT width = static_cast<UINT>(m_window_width);
		const UINT height = static_cast<UINT>(m_window_height);
		for (UINT i = 0; i < numModes; i++)
		{
			if (displayModeList[i].Width == width && displayModeList[i].Height == height)
			{
				if (displayModeList[i].RefreshRate.Numerator > numerator)
				{
					numerator = displayModeList[i].RefreshRate.Numerator;
					denominator = displayModeList[i].RefreshRate.Denominator;
					format = displayModeList[i].Format;
				}
			}
		}
	}

#ifdef _DEBUG
	m_device.As(&m_d3dDebug);
#endif // _DEBUG

	// DirectX 11.0 systems
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = m_width;
	sd.BufferDesc.Height = m_height;
	sd.BufferDesc.Format = format;
	sd.BufferDesc.RefreshRate.Numerator = numerator;
	sd.BufferDesc.RefreshRate.Denominator = denominator;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = hwnd;
	sd.SampleDesc.Count = m_sampleCount;
	sd.SampleDesc.Quality = m_sampleQuality;
	sd.Windowed = true;

	hr = dxgiFactory->CreateSwapChain(m_device.Get(), &sd, m_swapChain.ReleaseAndGetAddressOf());

	dxgiFactory->MakeWindowAssociation(hwnd, 0/*DXGI_MWA_NO_ALT_ENTER*/);

	dxgiFactory->Release();

	if (FAILED(hr))
	{
		Debug::Err("Failed to create the swap chain!");
		return false;
	}

	m_deviceContext->OMSetRenderTargets(0, nullptr, nullptr);

	ID3D11Texture2D* pBackBuffer = nullptr;

	if (FAILED(m_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer))))
		return false;

	hr = m_device->CreateRenderTargetView(pBackBuffer, nullptr, m_renderTargetView.GetAddressOf());
	pBackBuffer->Release();

	if (FAILED(hr))
	{
		Debug::Err("Failed to create the render target view!");
		return false;
	}

	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.SlopeScaledDepthBias = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.ScissorEnable = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.AntialiasedLineEnable = false;

	hr = m_device->CreateRasterizerState(&rasterDesc, m_rasterizerState.ReleaseAndGetAddressOf());
	if (FAILED(hr))
	{
		Debug::Err("Failed to create the rasterizer state!");
		return false;
	}

	m_deviceContext->RSSetState(m_rasterizerState.Get());

	// Set the input assembler primitive topology, for now it doesn't change at runtime, as we only have triangle lists
	m_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Create buffers
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	// Create constant buffer
	bd.ByteWidth = sizeof(ConstantBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_constants_buffer)))
	{
		Debug::Err("Failed to create the constant buffer!");
		return false;
	}

	// Create matrix buffer 
	bd.ByteWidth = sizeof(MatrixBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_world_matrix_buffer)))
	{
		Debug::Err("Failed to create the world matrix buffer!");
		return false;
	}

	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_view_matrix_buffer)))
	{
		Debug::Err("Failed to create the view matrix buffer!");
		return false;
	}

	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_projection_matrix_buffer)))
	{
		Debug::Err("Failed to create the projection matrix buffer!");
		return false;
	}

	// Create material buffer 
	bd.ByteWidth = sizeof(MaterialBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_bufferMaterial)))
	{
		Debug::Err("Failed to create the material buffer!");
		return false;
	}

	// Create material buffer 
	bd.ByteWidth = sizeof(LightBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_lightCB)))
	{
		Debug::Err("Failed to create light buffer!");
		return false;
	}

	m_width = max(static_cast<int>(m_width * m_renderScale), 1);
	m_height = max(static_cast<int>(m_height * m_renderScale), 1);

	D3D11_BLEND_DESC blendDesc = { 0 };
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.IndependentBlendEnable = false;
	blendDesc.RenderTarget[0].BlendEnable = false;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	if (FAILED(m_device->CreateBlendState(&blendDesc, m_blend_state_opaque.ReleaseAndGetAddressOf())))
	{
		Debug::Err("Failed to create blend state!");
		return false;
	}

	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	if (FAILED(m_device->CreateBlendState(&blendDesc, m_blend_state_additive.ReleaseAndGetAddressOf())))
	{
		Debug::Err("Failed to create blend state!");
		return false;
	}

	// TODO: Move this somewhere more relevant
	// Set shader global define if shadow are enabled
	m_shadowEnabled = Engine::GetConfig().shadow_res > 0;
	if (m_shadowEnabled)
	{
		Shader::SetGlobalDefine("SHADOW_ENABLED");
		Shader::SetGlobalDefine("MAX_CASCADES", std::to_string(MAX_CASCADES));

		int num_cascades = Math::Clamp(Engine::GetConfig().shadow_cascades, 1, MAX_CASCADES);
		Shader::SetGlobalDefine("NUM_CASCADES", std::to_string(num_cascades));
	}

	if (!InitializeSamplers())
	{
		return false;
	}
	return true;
}


bool Redline::Renderer::InitializeResources(const HWND hwnd)
{
	m_deferredShader = ResourcesManager::GetShader("data\\shaders\\default.fx");
	if (m_deferredShader == nullptr)
	{
		Debug::Err("Failed to compile the deferred shader!");
		return false;
	}

	m_wireframeShader = ResourcesManager::GetShader("data\\shaders\\wireframe.fx");
	if (m_wireframeShader == nullptr)
	{
		Debug::Err("Failed to compile the wireframe shader!");
		return false;
	}

	m_unlit_shader = ResourcesManager::GetShader("data\\shaders\\unlit.fx");
	if (m_unlit_shader == nullptr)
	{
		Debug::Err("Failed to compile the unlit shader!");
		return false;
	}

	if (!InitializeGBuffers())
	{
		Debug::Err("Failed to initialize the GBuffers!");
		return false;
	}



	m_shadowEnabled = Engine::GetConfig().shadow_res > 0;
	if (m_shadowEnabled)
	{
		if (!InitializeShadowMap())
		{
			Debug::Err("Failed to initialize the shadow maps!");
			return false;
		}
	}


	if (!InitializePostProcess())
	{
		Debug::Err("Failed to initialize post process!");
		return false;
	}

	if (!InitializeEnvironmentCubemap())
	{
		Debug::Err("Failed to initialize environment reflections!");
		return false;
	}

#ifdef USE_IMGUI
	ImGui::CreateContext();
	if (!ImGui_ImplDX11_Init(hwnd, m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to initialize Dear Imgui!");
		return false;
	}
#endif

	m_deviceContext->IASetInputLayout(m_GBufferShader->GetInputLayout());
	return true;
}


bool Redline::Renderer::InitializeSkybox()
{
	m_skybox = new Skybox();
	if (!m_skybox->Initialize())
	{
		delete m_skybox;
		m_skybox = nullptr;
		return false;
	}

	// Create constant buffer 
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	bd.ByteWidth = sizeof(SkyboxVSBuffer);
	// Create vertex shader buffer 
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_bufferVSSkybox)))
		return false;

	bd.ByteWidth = sizeof(SkyboxPSBuffer);
	// Create pixel shader buffer 
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_bufferPSSkybox)))
		return false;

	return true;
}


bool Redline::Renderer::InitializeGBuffers()
{
	// DEPTH BUFFER
	m_renderTargets[DEPTH_BUFFER] = RenderTarget::Create(m_device.Get(), m_width, m_height, DXGI_FORMAT_R32_FLOAT, true);
	if (m_renderTargets[DEPTH_BUFFER] == nullptr)
	{
		return false;
	}

	// NORMAL BUFFER (and metallic in alpha channel)
	m_renderTargets[NORMAL_BUFFER] = RenderTarget::Create(m_device.Get(), m_width, m_height, DXGI_FORMAT_R16G16B16A16_FLOAT, true);
	if (m_renderTargets[NORMAL_BUFFER] == nullptr)
	{
		return false;
	}

	// COLOR BUFFER (and roughness in alpha channel)
	m_renderTargets[COLOR_BUFFER] = RenderTarget::Create(m_device.Get(), m_width, m_height, DXGI_FORMAT_B8G8R8A8_UNORM_SRGB, true);
	if (m_renderTargets[COLOR_BUFFER] == nullptr)
	{
		return false;
	}

	// Set up the description of the depth buffer
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = m_width;
	depthBufferDesc.Height = m_height;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	// Create the texture for the depth buffer using the filled out description
	if (FAILED(m_device->CreateTexture2D(&depthBufferDesc, NULL, &m_depthStencil)))
	{
		return false;
	}

	// Set up the depth stencil view description
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	// Create the depth stencil view
	if (FAILED(m_device->CreateDepthStencilView(m_depthStencil, &depthStencilViewDesc, &m_depthStencilView)))
	{
		return false;
	}

	// Setup the viewport for rendering.
	m_viewport.Width = static_cast<float>(m_width);
	m_viewport.Height = static_cast<float>(m_height);
	m_viewport.MinDepth = 0.0f;
	m_viewport.MaxDepth = 1.0f;
	m_viewport.TopLeftX = 0.0f;
	m_viewport.TopLeftY = 0.0f;

	m_GBufferShader = ResourcesManager::GetShader("data\\shaders\\deferred.fx");
	if (m_GBufferShader == nullptr)
		return false;


	// Set up the description of the stencil state.
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create the depth stencil state
	if (FAILED(m_device->CreateDepthStencilState(&depthStencilDesc, &m_depthStencilState)))
	{
		return false;
	}

	// Set the depth stencil state.
	m_deviceContext->OMSetDepthStencilState(m_depthStencilState, 1);

	depthStencilDesc.DepthEnable = false;
	depthStencilDesc.StencilEnable = false;
	// Create the depth stencil state
	if (FAILED(m_device->CreateDepthStencilState(&depthStencilDesc, &m_depthDisabledStencilState)))
	{
		return false;
	}
	return true;
}


bool Redline::Renderer::InitializeShadowMap()
{
	const int resolution = Engine::GetConfig().shadow_res;

	m_shadowmap_render_target = RenderTarget::Create(m_device.Get(), resolution, resolution, DXGI_FORMAT_R32_FLOAT, true);
	if (m_shadowmap_render_target == nullptr)
	{
		Debug::Err("Failed to create the shadow map render target!");
		return false;
	}


	// Create shadow constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(ShadowsBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_shadow_constant_buffer)))
	{
		Debug::Err("Failed to create the shadow constant buffer!");
		return false;
	}

	if (!m_shadow_renderer.Initialize(m_device))
	{
		return false;
	}
	return true;
}


void Redline::Renderer::ShutdownShadowMap()
{
	if (m_shadowmap_render_target)
	{
		delete m_shadowmap_render_target;
		m_shadowmap_render_target = nullptr;
	}
}


bool Redline::Renderer::InitializePostProcess()
{
	m_postProcessRT_A = RenderTarget::Create(m_device.Get(), m_width, m_height, DXGI_FORMAT_R16G16B16A16_UNORM, true);
	if (m_postProcessRT_A == nullptr)
	{
		Debug::Err("Failed to create a post process render target!");
		return false;
	}

	m_postProcessRT_B = RenderTarget::Create(m_device.Get(), m_width, m_height, DXGI_FORMAT_R16G16B16A16_UNORM, true);
	if (m_postProcessRT_B == nullptr)
	{
		Debug::Err("Failed to create a post process render target!");
		return false;
	}


	if (!InitializeSSAO())
		return false;

	if (!InitializeBloom())
		return false;

	if (!InitializeVolumetricFog())
		return false;

	if (!InitializeAA())
		return false;

	return true;
}


bool Redline::Renderer::InitializeAA()
{
	int aa_technique = Engine::GetConfig().aa_technique;

	switch (aa_technique)
	{
	default:
	case 0:
		return true;
	case 1:
		m_fxaa_enabled = true;
		return m_fxaa_renderer.Initialize(m_device, m_width, m_height);
	case 2:
		m_smaa_enabled = true;
		return m_smaa_renderer.Initialize(m_device, m_width, m_height);
	}
}


void Redline::Renderer::ShutdownAA()
{
	int aa_technique = Engine::GetConfig().aa_technique;

	switch (aa_technique)
	{
	default:
	case 0:
		return;
	case 1:
		m_fxaa_renderer.Shutdown();
		break;
	case 2:
		m_smaa_renderer.Shutdown();
		break;
	}
}


bool Redline::Renderer::InitializeSSAO()
{
	const int ssao_technique = Engine::GetConfig().ssao_technique;
	return m_ssao_renderer.Initialize(m_device, ssao_technique, m_width, m_height);
}


void Redline::Renderer::ShutdownSSAO()
{
	m_ssao_renderer.Shutdown();
}


bool Redline::Renderer::InitializeBloom()
{
	return m_bloom_renderer.Initialize(m_device, m_width, m_height);
}


void Redline::Renderer::ShutdownBloom()
{
	m_bloom_renderer.Shutdown();
}


bool Redline::Renderer::InitializeVolumetricFog()
{
	m_fog_shader = ResourcesManager::GetShader("data\\shaders\\fog.fx");
	if (m_fog_shader == nullptr)
	{
		Debug::Err("Failed to compile the fog shader!");
		return false;
	}

	return true;
}


void Redline::Renderer::ShutdownVolumetricFog()
{

}


bool Redline::Renderer::InitializeEnvironmentCubemap()
{
	const int cubemap_res = max(Engine::GetConfig().cubemap_res, 16);
	m_environment_cubemap = RenderTarget::Create(m_device.Get(), cubemap_res, cubemap_res, DXGI_FORMAT_R10G10B10A2_UNORM, true, 6);
	if (m_environment_cubemap == nullptr)
	{
		Debug::Err("Failed to create the environment cubemap!");
		return false;
	}

	m_environment_cubemap_filtered = RenderTarget::Create(m_device.Get(), cubemap_res, cubemap_res, DXGI_FORMAT_R10G10B10A2_UNORM, true, 6);
	if (m_environment_cubemap_filtered == nullptr)
	{
		Debug::Err("Failed to create the filtered environment cubemap!");
		return false;
	}

	// Set up the description of the depth buffer
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = cubemap_res;
	depthBufferDesc.Height = cubemap_res;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	// Create the texture for the depth buffer using the filled out description
	if (FAILED(m_device->CreateTexture2D(&depthBufferDesc, NULL, &m_environmentDepthStencil)))
	{
		Debug::Err("Failed to create the environment depth buffer!");
		return false;
	}

	// Set up the depth stencil view description
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = depthBufferDesc.Format;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	// Create the depth stencil view
	if (FAILED(m_device->CreateDepthStencilView(m_environmentDepthStencil, &depthStencilViewDesc, &m_environmentDepthStencilView)))
	{
		Debug::Err("Failed to create the environment depth stencil view!");
		return false;
	}

	m_environmentViewport.Width = static_cast<float>(cubemap_res);
	m_environmentViewport.Height = static_cast<float>(cubemap_res);
	m_environmentViewport.MinDepth = 0.0f;
	m_environmentViewport.MaxDepth = 1.0f;
	m_environmentViewport.TopLeftX = 0.0f;
	m_environmentViewport.TopLeftY = 0.0f;


	m_environement_filtering_shader = ResourcesManager::GetShader("data\\shaders\\envfiltering.fx");
	if (m_environement_filtering_shader == nullptr)
	{
		Debug::Err("Failed to compile the environment filtering shader!");
		return false;
	}

	m_simple_shader = ResourcesManager::GetShader("data\\shaders\\simple.fx");
	if (m_simple_shader == nullptr)
	{
		Debug::Err("Failed to compile the environment simple shader!");
		return false;
	}


	// Create buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(EnvFilteringBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, m_env_filtering_buffer.ReleaseAndGetAddressOf())))
	{
		Debug::Err("Failed to create the environment filtering shader buffer!");
		return false;
	}
	return true;
}


void Redline::Renderer::ShutdownEnvironmentCubemap()
{
	SAFE_RELEASE(m_environmentDepthStencilView);
	SAFE_RELEASE(m_environmentDepthStencil);

	if (m_environment_cubemap)
	{
		delete m_environment_cubemap;
		m_environment_cubemap = nullptr;
	}

	if (m_environment_cubemap_filtered)
	{
		delete m_environment_cubemap_filtered;
		m_environment_cubemap_filtered = nullptr;
	}

	m_env_filtering_buffer.Reset();
}


void Redline::Renderer::RenderToGBuffers(const vector<MeshRenderer*>& mesh_renderers)
{
	// Frustrum culling
	vector<RenderObject> objects = CullOffscreenObjects(mesh_renderers, Engine::GetCamera().GetViewMatrix(), Engine::GetCamera().GetProjectionMatrix());

	m_deviceContext->RSSetViewports(1, &m_viewport);

	// Clear the render targets and depth buffer
	m_renderTargets[0]->Clear(m_deviceContext.Get(), 1, 1, 1, 1);
	for (unsigned int i = 1; i < BUFFER_COUNT; ++i)
	{
		m_renderTargets[i]->Clear(m_deviceContext.Get());
	}
	m_deviceContext->ClearDepthStencilView(m_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	SetShader(m_GBufferShader);

	// Set camera matrices
	SetViewProjectionMatrices(Engine::GetCamera().GetViewMatrix(), Engine::GetCamera().GetProjectionMatrix());

	Matrix previous_world_matrix;
	ID3D11Buffer* previous_vertex_buffer = nullptr;

	const size_t objectsCount = objects.size();
	for (size_t i = 0; i < objectsCount; ++i)
	{
		const RenderObject& object = objects[i];

		if (i == 0 || object.world_matrix != previous_world_matrix)
		{
			// Update variables
			SetWorldMatrix(object.world_matrix);
			previous_world_matrix = object.world_matrix;
		}

		m_deviceContext->UpdateSubresource(m_bufferMaterial, 0, nullptr, &object.material_buffer, 0, 0);
		m_deviceContext->PSSetConstantBuffers(0, 1, &m_bufferMaterial);

		// Set texture and sampler and texture for the material to render
		if (object.diffuse_texture != nullptr)
		{
			m_deviceContext->PSSetShaderResources(0, 1, &object.diffuse_texture);
		}

		if (object.normal_texture != nullptr)
		{
			m_deviceContext->PSSetShaderResources(1, 1, &object.normal_texture);
		}

		if (object.vertex_buffer != previous_vertex_buffer)
		{
			// Set vertex buffer
			UINT stride = sizeof(Vertex);
			UINT offset = 0;
			m_deviceContext->IASetVertexBuffers(0, 1, &object.vertex_buffer, &stride, &offset);
			previous_vertex_buffer = object.vertex_buffer;

			// Set index buffer
			m_deviceContext->IASetIndexBuffer(object.index_buffer, DXGI_FORMAT_R32_UINT, 0);
		}

		// Render the sub-mesh
		m_deviceContext->DrawIndexed(object.indices_count, 0, 0);
		m_draw_call_count++;
	}


	// Generate Mip-Maps
	for (unsigned int i = 0; i < BUFFER_COUNT; ++i)
	{
		m_renderTargets[i]->GenerateMips(m_deviceContext.Get());
	}
}


void Redline::Renderer::RenderToEnvironmentMap(const vector<MeshRenderer*>& mesh_renderers)
{
	const float far_clip = 250.0f;
	Matrix projection_matrix = Matrix::CreatePerspectiveFieldOfView(float(M_PI) / 2.0f, 1.0f, 0.1f, far_clip);

	// Set projection matrix
	SetProjectionMatrix(projection_matrix);

	Vector3 pos = Engine::GetCamera().GetPosition() + Engine::GetCamera().GetForwardVector();

	m_deviceContext->RSSetViewports(1, &m_environmentViewport);

	for (int face_index = 0; face_index < 6; ++face_index)
	{
		Vector3 at(0, 0, 1);
		Vector3 up(0, 1, 0);

		switch (face_index)
		{
		case 0://FACE_POSITIVE_X:
			at = Vector3(1.0f, 0.0f, 0.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 1://FACE_NEGATIVE_X:
			at = Vector3(-1.0f, 0.0f, 0.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 2://FACE_POSITIVE_Y:
			at = Vector3(0.0f, 1.0f, 0.0f);
			up = Vector3(0.0f, 0.0f, 1.0f);
			break;
		case 3://FACE_NEGATIVE_Y:
			at = Vector3(0.0f, -1.0f, 0.0f);
			up = Vector3(0.0f, 0.0f, -1.0f);
			break;
		case 4://FACE_POSITIVE_Z:
			at = Vector3(0.0f, 0.0f, -1.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 5://FACE_NEGATIVE_Z:
			at = Vector3(0.0f, 0.0f, 1.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		}

		// Set view matrix
		Matrix view_matrix = Matrix::CreateLookAt(pos, pos + at, up);
		SetViewMatrix(view_matrix);

		// Frustrum culling
		vector<RenderObject> objects = CullOffscreenObjects(mesh_renderers, view_matrix, projection_matrix);

		// Clear the depth buffer
		m_environment_cubemap->Clear(m_deviceContext.Get(), 0, 0, 0, 0, face_index, 0);
		m_deviceContext->ClearDepthStencilView(m_environmentDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

		m_deviceContext->OMSetRenderTargets(1, m_environment_cubemap->GetRenderTargetView(face_index).GetAddressOf(), m_environmentDepthStencilView);

		DrawSkybox(pos, 100.0f);

		if (objects.empty())
			continue;

		SetShader(m_simple_shader);

		// Update variables
		//SkyboxPSBuffer skybox_ps_buffer = m_skybox->GetPSBuffer();
		//m_deviceContext->UpdateSubresource(m_bufferPSSkybox, 0, nullptr, &skybox_ps_buffer, 0, 0);
		m_deviceContext->PSSetConstantBuffers(1, 1, &m_bufferPSSkybox);

		Matrix previous_world_matrix;
		ID3D11Buffer* previous_vertex_buffer = nullptr;

		const size_t objectsCount = objects.size();
		for (size_t k = 0; k < objectsCount; ++k)
		{
			const RenderObject& object = objects[k];

			if (k == 0 || object.world_matrix != previous_world_matrix)
			{
				SetWorldMatrix(object.world_matrix);
				previous_world_matrix = object.world_matrix;
			}

			m_deviceContext->UpdateSubresource(m_bufferMaterial, 0, nullptr, &object.material_buffer, 0, 0);
			m_deviceContext->PSSetConstantBuffers(0, 1, &m_bufferMaterial);

			// Set texture and sampler and texture for the material to render
			if (object.diffuse_texture != nullptr)
			{
				m_deviceContext->PSSetShaderResources(0, 1, &object.diffuse_texture);
			}

			if (object.vertex_buffer != previous_vertex_buffer)
			{
				// Set vertex buffer
				UINT stride = sizeof(Vertex);
				UINT offset = 0;
				m_deviceContext->IASetVertexBuffers(0, 1, &object.vertex_buffer, &stride, &offset);
				previous_vertex_buffer = object.vertex_buffer;

				// Set index buffer
				m_deviceContext->IASetIndexBuffer(object.index_buffer, DXGI_FORMAT_R32_UINT, 0);
			}

			// Render the sub-mesh
			m_deviceContext->DrawIndexed(object.indices_count, 0, 0);
			m_draw_call_count++;
		}
	}


	m_environment_cubemap->GenerateMips(m_deviceContext.Get());

	m_deviceContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	SetShader(m_environement_filtering_shader);

	DisableZTest();

	int cubemap_res = Engine::GetConfig().cubemap_res;

	D3D11_VIEWPORT viewport;
	viewport.Width = static_cast<FLOAT>(cubemap_res);
	viewport.Height = static_cast<FLOAT>(cubemap_res);
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;

	const float sample_counts[] = { 1, 8, 32, 64, 128, 128, 256, 256, 256 };

	// TODO: Faces in inner loop, mips in outer
	const int num_mips = m_environment_cubemap->GetNumberOfMips();
	for (int face_index = 0; face_index < 6; ++face_index)
	{
		Vector3 at(0, 0, 1);
		Vector3 up(0, 1, 0);

		switch (face_index)
		{
		case 0://FACE_POSITIVE_X:
			at = Vector3(1.0f, 0.0f, 0.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 1://FACE_NEGATIVE_X:
			at = Vector3(-1.0f, 0.0f, 0.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 2://FACE_POSITIVE_Y:
			at = Vector3(0.0f, 1.0f, 0.0f);
			up = Vector3(0.0f, 0.0f, -1.0f);
			break;
		case 3://FACE_NEGATIVE_Y:
			at = Vector3(0.0f, -1.0f, 0.0f);
			up = Vector3(0.0f, 0.0f, 1.0f);
			break;
		case 4://FACE_POSITIVE_Z:
			at = Vector3(0.0f, 0.0f, 1.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 5://FACE_NEGATIVE_Z:
			at = Vector3(0.0f, 0.0f, -1.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		}

		for (int mip_level = 0; mip_level < num_mips - 2; ++mip_level)
		{
			// Get mip resolution
			float res = static_cast<float>(cubemap_res);
			for (int div = 0; div < mip_level; ++div)
				res /= 2;

			float roughness = mip_level / static_cast<float>(num_mips - 2);
			EnvFilteringBuffer buffer = {
				Vector4(at.x, at.y, at.z, roughness),
				Vector4(up.x, up.y, up.z, res),
				sample_counts[mip_level]
			};

			viewport.Height = viewport.Width = res;
			m_deviceContext->RSSetViewports(1, &viewport);

			m_deviceContext->UpdateSubresource(m_env_filtering_buffer.Get(), 0, nullptr, &buffer, 0, 0);
			m_deviceContext->PSSetConstantBuffers(0, 1, m_env_filtering_buffer.GetAddressOf());

			m_deviceContext->OMSetRenderTargets(1, m_environment_cubemap_filtered->GetRenderTargetView(face_index, mip_level).GetAddressOf(), nullptr);
			m_deviceContext->PSSetShaderResources(0, 1, m_environment_cubemap->GetShaderResourceView().GetAddressOf());

			m_deviceContext->Draw(3, 0);
			m_draw_call_count++;
		}
	}

	EnableZTest();
}


void Redline::Renderer::SwitchToGBuffers() const
{
	ID3D11RenderTargetView* rtViews[BUFFER_COUNT];
	for (unsigned int i = 0; i < BUFFER_COUNT; ++i)
	{
		rtViews[i] = m_renderTargets[i]->GetRenderTargetView().Get();
	}
	m_deviceContext->OMSetRenderTargets(BUFFER_COUNT, rtViews, m_depthStencilView);
}


void Redline::Renderer::ClearRTInputsAndOutputs() const
{
	ID3D11RenderTargetView* rtViews[] = {
		nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr
	};

	ID3D11ShaderResourceView* srViews[] = {
		nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr
	};

	m_deviceContext->OMSetRenderTargets(8, rtViews, nullptr);
	m_deviceContext->PSSetShaderResources(0, 8, srViews);
}


bool Redline::Renderer::IsLightVisible(const Light& light) const
{
	if (!light.IsActive())
	{
		return false;
	}

	if (light.GetIntensity() < EPSILON)
	{
		return false;
	}

	const Matrix& world_matrix = light.GetGameObject()->GetTransform()->GetMatrix();

	Vector3 world_position = Vector3::Transform(Vector3::Zero, world_matrix);

	LightType type = light.GetType();

	if (type == LIGHT_DIRECTIONAL)
	{
		return true;
	}
	else if (type == LIGHT_SPOT)
	{
		Vector3 center;
		float radius;

		Vector3 direction = Vector3::TransformNormal(-light.GetDirection(), world_matrix);
		direction.Normalize();

		float angle = light.GetAngle() * DEG2RAD * 0.5f;
		float size = light.GetIntensity() * 10.0f;

		if (angle > M_PI / 4.0f)
		{
			center = world_position + cos(angle) * size * direction;
			radius = sin(angle) * size;
		}
		else
		{
			center = world_position + size / (2.0f * cos(angle)) * direction;
			radius = size / (2.0f * cos(angle));
		}

		return IsVisibleOnScreen(center, radius);
	}
	else if (type == LIGHT_POINT)
	{
		return IsVisibleOnScreen(world_position, light.GetIntensity() * 10.0f);
	}
	return false;
}


void Redline::Renderer::RenderToScreen(RenderTarget* input_RT)
{
	m_deviceContext->OMSetRenderTargets(1, m_renderTargetView.GetAddressOf(), nullptr);

	D3D11_VIEWPORT screenViewport;
	screenViewport.TopLeftX = 0.0f;
	screenViewport.TopLeftY = 0.0f;
	screenViewport.MinDepth = 0.0f;
	screenViewport.MaxDepth = 1.0f;
	screenViewport.Width = static_cast<FLOAT>(m_window_width);
	screenViewport.Height = static_cast<FLOAT>(m_window_height);
	m_deviceContext->RSSetViewports(1, &screenViewport);

	m_deviceContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	SetShader(m_unlit_shader);

	input_RT->GenerateMips(m_deviceContext.Get());

	m_deviceContext->PSSetShaderResources(0, 1, input_RT->GetShaderResourceView().GetAddressOf());

	m_deviceContext->Draw(3, 0);
	m_draw_call_count++;
}


void Redline::Renderer::RenderDeferredImage(const vector<MeshRenderer*>& shadow_mesh_renderers)
{
	m_deviceContext->RSSetViewports(1, &m_viewport);

	m_deviceContext->OMSetRenderTargets(1, m_postProcessRT_A->GetRenderTargetView().GetAddressOf(), m_depthStencilView);

	m_postProcessRT_A->Clear(m_deviceContext.Get(), 0, 0, 0, 0);

	m_deviceContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	ID3D11ShaderResourceView* gBufferSRV[BUFFER_COUNT + 1];
	for (unsigned int i = 0; i < BUFFER_COUNT; ++i)
	{
		gBufferSRV[i] = m_renderTargets[i]->GetShaderResourceView().Get();
	}

	gBufferSRV[BUFFER_COUNT] = m_ssao_renderer.IsEnabled() ? m_ssao_renderer.GetRenderTarget()->GetShaderResourceView().Get() : nullptr;

	m_deviceContext->PSSetShaderResources(0, BUFFER_COUNT + 1, gBufferSRV);


	ConstantBuffer constant_buffer;
	constant_buffer.viewToWorldMatrix = Engine::GetCamera().GetViewMatrix().Invert();
	constant_buffer.invProjMatrix = Engine::GetCamera().GetProjectionMatrix().Invert();
	constant_buffer.EnvNumMipMaps = m_environment_cubemap->GetNumberOfMips() - 2;
	constant_buffer.ambient_intensity = 1.0f; // Math::Clamp(Engine::GetLight(0)->GetDirection().y * 10.0f, 0.5f, 4.0f);
	m_deviceContext->UpdateSubresource(m_constants_buffer, 0, nullptr, &constant_buffer, 0, 0);
	m_deviceContext->PSSetConstantBuffers(0, 1, &m_constants_buffer);

	Shader* accumulationShader = ResourcesManager::GetShader("data\\shaders\\accumulation.fx");
	SetShader(accumulationShader);

	std::vector<Light*> lights = Engine::GetLights();
	for (auto& i : lights)
	{
		const Light& light = *i;
		if (!IsLightVisible(light))
		{
			continue;
		}

		if (light.IsCastingShadows())
		{
			if (light.GetType() == LIGHT_DIRECTIONAL)
			{
				ShadowsBuffer sb{};
				m_shadow_renderer.GenerateCascadedShadowMap(m_deviceContext, shadow_mesh_renderers, light, sb, m_shadowmap_render_target);

				m_deviceContext->UpdateSubresource(m_shadow_constant_buffer, 0, nullptr, &sb, 0, 0);
				m_deviceContext->PSSetConstantBuffers(2, 1, &m_shadow_constant_buffer);

				m_deviceContext->OMSetRenderTargets(1, m_postProcessRT_A->GetRenderTargetView().GetAddressOf(), m_depthStencilView);

				m_deviceContext->PSSetShaderResources(BUFFER_COUNT + 1, 1, m_shadowmap_render_target->GetShaderResourceView().GetAddressOf());
			}

			// Restore render target after shadow map generation
			//m_deviceContext->OMSetRenderTargets(1, m_postProcessRT_A->GetRenderTargetView().GetAddressOf(), m_depthStencilView);
			m_deviceContext->RSSetViewports(1, &m_viewport);
			m_deviceContext->PSSetConstantBuffers(0, 1, &m_constants_buffer);
			m_deviceContext->PSSetShaderResources(0, BUFFER_COUNT + 1, gBufferSRV);
			SetShader(accumulationShader);
		}

		const Matrix& view_matrix = Engine::GetCamera().GetViewMatrix();
		const Matrix& world_matrix = light.GetGameObject()->GetTransform()->GetMatrix();
		Vector3 vsPosition = Vector3::Transform(Vector3::Zero, world_matrix);
		vsPosition = Vector3::Transform(vsPosition, view_matrix);

		Vector3 vsDirection = Vector3::TransformNormal(light.GetDirection(), world_matrix);
		vsDirection.Normalize();
		vsDirection = Vector3::TransformNormal(vsDirection, view_matrix);

		LightBuffer lb = {
			light.GetColor(),
			light.GetIntensity(),
			vsPosition,
			light.GetType(),
			vsDirection,
			light.GetAngle() * float(DEG2RAD) * 0.5f
		};
		m_deviceContext->UpdateSubresource(m_lightCB, 0, nullptr, &lb, 0, 0);

		m_deviceContext->PSSetConstantBuffers(1, 1, &m_lightCB);

		float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
		UINT sampleMask = 0xffffffff;
		m_deviceContext->OMSetBlendState(m_blend_state_additive.Get(), blendFactor, sampleMask);

		// TODO: Compute light shadow (if needed) before accumulating sample
		// TODO: Draw bounding mesh for point and spot lights
		m_deviceContext->Draw(3, 0);
		m_draw_call_count++;

		m_deviceContext->OMSetBlendState(m_blend_state_opaque.Get(), blendFactor, sampleMask);
	}



	float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	UINT sampleMask = 0xffffffff;
	m_deviceContext->OMSetBlendState(m_blend_state_additive.Get(), blendFactor, sampleMask);

	SetShader(m_deferredShader);
	m_deviceContext->PSSetShaderResources(BUFFER_COUNT + 2, 1, m_environment_cubemap_filtered->GetShaderResourceView().GetAddressOf());

	m_deviceContext->Draw(3, 0);
	m_draw_call_count++;

	m_deviceContext->OMSetBlendState(m_blend_state_opaque.Get(), blendFactor, sampleMask);
}


void Redline::Renderer::RenderFogPass()
{
	m_deviceContext->OMSetRenderTargets(1, m_postProcessRT_B->GetRenderTargetView().GetAddressOf(), m_depthStencilView);

	m_deviceContext->RSSetViewports(1, &m_viewport);

	m_deviceContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	SkyboxVSBuffer svb = m_skybox->GetVSBuffer();
	m_deviceContext->UpdateSubresource(m_bufferVSSkybox, 0, nullptr, &svb, 0, 0);

	SkyboxPSBuffer spb = m_skybox->GetPSBuffer();
	m_deviceContext->UpdateSubresource(m_bufferPSSkybox, 0, nullptr, &spb, 0, 0);

	SetShader(m_fog_shader);

	ID3D11Buffer* buffers[4] = { m_constants_buffer, m_bufferVSSkybox, m_bufferPSSkybox, nullptr };

	// TODO: Implement fog shadow again ?
	/*if (m_shadowEnabled)
	{
		ID3D11ShaderResourceView* srViews[ShadowRenderer::m_num_cascades];
		for (unsigned int i = 0; i < ShadowRenderer::m_num_cascades; ++i)
		{
			srViews[i] = m_shadowMapsRenderTargets[i]->GetShaderResourceView().Get();
		}
		m_deviceContext->PSSetShaderResources(2, ShadowRenderer::m_num_cascades, srViews);
	}*/

	m_deviceContext->PSSetConstantBuffers(0, 4, buffers);

	ID3D11ShaderResourceView* srViews[] = { m_postProcessRT_A->GetShaderResourceView().Get(), m_renderTargets[0]->GetShaderResourceView().Get() };
	m_deviceContext->PSSetShaderResources(0, 2, srViews);

	m_deviceContext->Draw(3, 0);
	m_draw_call_count++;
}


void Redline::Renderer::DisableZTest() const
{
	m_deviceContext->OMSetDepthStencilState(m_depthDisabledStencilState, 1);
}


void Redline::Renderer::EnableZTest() const
{
	m_deviceContext->OMSetDepthStencilState(m_depthStencilState, 1);
}


void Redline::Renderer::SetWorldMatrix(const Matrix& matrix) const
{
	MatrixBuffer buffer = {
		matrix
	};
	m_deviceContext->UpdateSubresource(m_world_matrix_buffer, 0, nullptr, &buffer, 0, 0);

	m_deviceContext->VSSetConstantBuffers(0, 1, &m_world_matrix_buffer);
}


void Redline::Renderer::SetViewMatrix(const Matrix& matrix) const
{
	MatrixBuffer buffer = {
		matrix
	};
	m_deviceContext->UpdateSubresource(m_view_matrix_buffer, 0, nullptr, &buffer, 0, 0);

	m_deviceContext->VSSetConstantBuffers(1, 1, &m_view_matrix_buffer);
}


void Redline::Renderer::SetProjectionMatrix(const Matrix& matrix) const
{
	MatrixBuffer buffer = {
		matrix
	};
	m_deviceContext->UpdateSubresource(m_projection_matrix_buffer, 0, nullptr, &buffer, 0, 0);

	m_deviceContext->VSSetConstantBuffers(2, 1, &m_projection_matrix_buffer);
}


void Redline::Renderer::SetViewProjectionMatrices(const Matrix& view_matrix,
	const Matrix& projection_matrix) const
{
	MatrixBuffer buffer = {
		view_matrix
	};
	m_deviceContext->UpdateSubresource(m_view_matrix_buffer, 0, nullptr, &buffer, 0, 0);

	buffer.mMatrix = projection_matrix;
	m_deviceContext->UpdateSubresource(m_projection_matrix_buffer, 0, nullptr, &buffer, 0, 0);

	ID3D11Buffer* constant_buffers[] = { m_view_matrix_buffer, m_projection_matrix_buffer };
	m_deviceContext->VSSetConstantBuffers(1, 2, constant_buffers);
}


void Redline::Renderer::BuildViewFrustum(const Matrix& view, const Matrix& projection)
{
	Matrix viewProjection = view * projection;

	// Left plane
	m_frustum[0].x = viewProjection._14 + viewProjection._11;
	m_frustum[0].y = viewProjection._24 + viewProjection._21;
	m_frustum[0].z = viewProjection._34 + viewProjection._31;
	m_frustum[0].w = viewProjection._44 + viewProjection._41;

	// Right plane
	m_frustum[1].x = viewProjection._14 - viewProjection._11;
	m_frustum[1].y = viewProjection._24 - viewProjection._21;
	m_frustum[1].z = viewProjection._34 - viewProjection._31;
	m_frustum[1].w = viewProjection._44 - viewProjection._41;

	// Top plane
	m_frustum[2].x = viewProjection._14 - viewProjection._12;
	m_frustum[2].y = viewProjection._24 - viewProjection._22;
	m_frustum[2].z = viewProjection._34 - viewProjection._32;
	m_frustum[2].w = viewProjection._44 - viewProjection._42;

	// Bottom plane
	m_frustum[3].x = viewProjection._14 + viewProjection._12;
	m_frustum[3].y = viewProjection._24 + viewProjection._22;
	m_frustum[3].z = viewProjection._34 + viewProjection._32;
	m_frustum[3].w = viewProjection._44 + viewProjection._42;

	// Near plane
	m_frustum[4].x = viewProjection._13;
	m_frustum[4].y = viewProjection._23;
	m_frustum[4].z = viewProjection._33;
	m_frustum[4].w = viewProjection._43;

	// Far plane
	m_frustum[5].x = viewProjection._14 - viewProjection._13;
	m_frustum[5].y = viewProjection._24 - viewProjection._23;
	m_frustum[5].z = viewProjection._34 - viewProjection._33;
	m_frustum[5].w = viewProjection._44 - viewProjection._43;

	// Normalize planes
	for (size_t i = 0; i < 6; ++i)
	{
		m_frustum[i].Normalize();
	}
}


vector<Redline::RenderObject> Redline::Renderer::CullOffscreenObjects(const vector<MeshRenderer*>& mesh_renderers, const Matrix& view_matrix, const Matrix& projection_matrix)
{
	BuildViewFrustum(view_matrix, projection_matrix);

	// Camera Field of View in radians
	//const float cameraFOV = Engine::GetCamera().GetFOV() * float(DEG2RAD);

	// Camera Position
	//const Vector3& cameraPosition = Engine::GetCamera().GetPosition();

	vector<RenderObject> objects;

	const size_t count = mesh_renderers.size();
	for (size_t i = 0; i < count; ++i)
	{
		MeshRenderer* mesh_renderer = mesh_renderers[i];

		GameObject* go = mesh_renderer->GetGameObject();

		if (!mesh_renderer->IsActive())
			continue;

		Mesh* mesh = mesh_renderer->GetMesh();

		if (mesh == nullptr)
			continue;

		Transform* transform = go->GetTransform();
		const Matrix& world_matrix = transform->GetMatrix();

		Vector3 world_position = Vector3::Transform(Vector3::Zero, world_matrix);

		Vector3 forward = Vector3::TransformNormal(Vector3::Forward, world_matrix);
		Vector3 up = Vector3::TransformNormal(Vector3::Up, world_matrix);
		Vector3 right = Vector3::TransformNormal(Vector3::Right, world_matrix);

		const float maxScale = max(max(forward.Length(), up.Length()), right.Length());
		const float& radius = mesh->GetBoundingSphereRadius();

		if (!IsVisibleOnScreen(world_position, radius * maxScale))
			continue;

		const vector<SubMesh>& submeshes = mesh->GetSubMeshes();
		const vector<Material>& materials = mesh_renderer->GetMaterials();

		const size_t submeshesCount = submeshes.size();
		for (size_t j = 0; j < submeshesCount; ++j)
		{
			const SubMesh& submesh = submeshes[j];

			Vector3 submesh_world_position = Vector3::Transform(submesh.boundingSphereCenter, world_matrix);

			float submesh_radius = submesh.boundingSphereRadius;

			if (!IsVisibleOnScreen(submesh_world_position, submesh_radius * maxScale))
				continue;

			/*const float pixelSize = GetOnScreenSize(world_position, radius * maxScale, cameraPosition, cameraFOV) * m_height;

			if (pixelSize < 1.0f)
				continue;*/

			const Material& material = materials[submesh.materialID];

			RenderObject object;
			object.world_matrix = world_matrix;
			object.material_buffer = material.GetBuffer();
			object.diffuse_texture = material.GetDiffuseTexture() != nullptr ? material.GetDiffuseTexture()->GetTexture().Get() : nullptr;
			object.normal_texture = material.GetNormalTexture() != nullptr ? material.GetNormalTexture()->GetTexture().Get() : nullptr;
			object.vertex_buffer = submesh.vBuffer->GetVertexBuffer();
			object.index_buffer = submesh.iBuffer->GetIndexBuffer();
			object.indices_count = submesh.iBuffer->GetIndicesCount();

			objects.emplace_back(object);
		}
	}

	return objects;
}


void Redline::Renderer::CullAndDrawObjects(const std::vector<MeshRenderer*>& mesh_renderers,
	const DirectX::SimpleMath::Matrix& view_matrix, const DirectX::SimpleMath::Matrix& projection_matrix)
{
	// Frustrum culling
	vector<RenderObject> objects = CullOffscreenObjects(mesh_renderers, view_matrix, projection_matrix);

	if (objects.empty())
	{
		return;
	}

	SetViewProjectionMatrices(view_matrix, projection_matrix);

	Matrix previous_world_matrix;
	ID3D11Buffer* previous_vertex_buffer = nullptr;

	const size_t objectsCount = objects.size();
	for (size_t i = 0; i < objectsCount; ++i)
	{
		const RenderObject& object = objects[i];

		if (i == 0 || object.world_matrix != previous_world_matrix)
		{
			// Update variables
			SetWorldMatrix(object.world_matrix);
			previous_world_matrix = object.world_matrix;
		}

		m_deviceContext->UpdateSubresource(m_bufferMaterial, 0, nullptr, &object.material_buffer, 0, 0);
		m_deviceContext->PSSetConstantBuffers(3, 1, &m_bufferMaterial);

		// Set texture and sampler and texture for the material to render
		if (object.diffuse_texture != nullptr)
		{
			m_deviceContext->PSSetShaderResources(0, 1, &object.diffuse_texture);
		}

		if (object.normal_texture != nullptr)
		{
			m_deviceContext->PSSetShaderResources(1, 1, &object.normal_texture);
		}

		if (object.vertex_buffer != previous_vertex_buffer)
		{
			// Set vertex buffer
			UINT stride = sizeof(Vertex);
			UINT offset = 0;
			m_deviceContext->IASetVertexBuffers(0, 1, &object.vertex_buffer, &stride, &offset);
			previous_vertex_buffer = object.vertex_buffer;

			// Set index buffer
			m_deviceContext->IASetIndexBuffer(object.index_buffer, DXGI_FORMAT_R32_UINT, 0);
		}

		// Render the sub-mesh
		m_deviceContext->DrawIndexed(object.indices_count, 0, 0);
		m_draw_call_count++;
	}
}


bool Redline::Renderer::IsVisibleOnScreen(const Vector3& position, const float radius) const
{
	for (size_t i = 0; i < 6; ++i)
	{
		if (m_frustum[i].DotCoordinate(position) < -radius)
		{
			return false;
		}
	}

	return true;
}


float Redline::Renderer::GetOnScreenSize(const Vector3 & position, const float radius, const Vector3 & cameraPosition, const float fov) const
{
	float distanceSquared = Vector3::DistanceSquared(position, cameraPosition);
	float radiusSquared = radius * radius;
	if (radiusSquared > distanceSquared)
		return 1.0f;
	float pixelRadius = cosf(fov * 0.5f) * radius / sqrt(distanceSquared - radiusSquared);
	return pixelRadius;
}


void Redline::Renderer::BuildTransform(Transform* transform)
{
	transform->RebuildMatrix();

	size_t childCount = transform->GetChildsCount();
	for (size_t i = 0; i < childCount; ++i)
	{
		Transform* child = transform->GetChild(i);
		BuildTransform(child);
	}
}


bool Redline::Renderer::Render(const float delta_time)
{
	vector<MeshRenderer*>& all_mesh_renderers = Engine::GetMeshRenderers();
	vector<MeshRenderer*> mesh_renderers;
	vector<MeshRenderer*> shadow_mesh_renderers;

	for (auto mesh_renderer : all_mesh_renderers)
	{
		if (mesh_renderer->IsActive())
		{
			mesh_renderers.emplace_back(mesh_renderer);

			if (mesh_renderer->IsCastingShadow())
				shadow_mesh_renderers.emplace_back(mesh_renderer);
		}
	}




	Engine::GetTimer().StartCounter();
	m_draw_call_count = 0;

	ClearRTInputsAndOutputs();

	EnableZTest();
	RenderToEnvironmentMap(mesh_renderers);

	// Fill GBuffers pass
	SwitchToGBuffers();
	RenderToGBuffers(mesh_renderers);

	// Deferred shading
	{
		DisableZTest();

		// SSAO
		m_ssao_renderer.Render(m_deviceContext, m_renderTargets[0], m_renderTargets[1], m_renderTargets[2]);

		RenderDeferredImage(shadow_mesh_renderers);

		EnableZTest();
		SetViewProjectionMatrices(Engine::GetCamera().GetViewMatrix(), Engine::GetCamera().GetProjectionMatrix());
		DrawSkybox(Engine::GetCamera().GetPosition(), Engine::GetCamera().GetClipPlanes().y);
		DisableZTest();

		RenderFogPass();

		// Tonemapping and bloom
		m_bloom_renderer.Render(delta_time, m_deviceContext, m_postProcessRT_B, m_postProcessRT_A);
	}

	if (m_fxaa_enabled)
		m_fxaa_renderer.Render(m_deviceContext, m_postProcessRT_A, m_postProcessRT_B);
	else if (m_smaa_enabled)
		m_smaa_renderer.Render(m_deviceContext, m_postProcessRT_A, m_postProcessRT_B);

	bool aa_enabled = m_fxaa_enabled || m_smaa_enabled;

	RenderToScreen(aa_enabled ? m_postProcessRT_B : m_postProcessRT_A);

#ifdef REDLINE_EDITOR
	DisableZTest();
	DrawDebug();
#else
#ifdef _DEBUG  
	ImGui::SetNextWindowPos(ImVec2(5, 5), ImGuiSetCond_FirstUseEver);
	if (ImGui::Begin("Render stats"))
	{
		double time = Engine::GetTimer().GetCounter();
		ImGui::Text("Render Time: %.2f ms", time);
		const float targetFrameRate = 60.0f;
		const float targetFrameTime = (1.0f / targetFrameRate) * 1000.0f;
		float fraction = static_cast<float>(time) / targetFrameTime;
		ImGui::Text("Budget utilization (%.2f ms allowed):", targetFrameTime);
		ImGui::ProgressBar(fraction);
		ImGui::Value("Draw calls", static_cast<unsigned int>(m_draw_call_count));
	}
	ImGui::End();

	DisableZTest();
	DrawDebug();
#endif
#endif

#ifdef USE_IMGUI
	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
#endif

	// switch the back buffer and the front buffer
	m_swapChain->Present(m_vsync_enabled ? 1 : 0, 0);
	return true;
}


void Redline::Renderer::DrawSkybox(const Vector3& cameraPosition, const float farPlane)
{
	// Generate and set the world matrix
	m_skybox->UpdateCamera(cameraPosition, farPlane);
	SetWorldMatrix(m_skybox->GetMatrix());

	SubMesh submesh = m_skybox->GetMesh()->GetSubMeshes()[0];

	SkyboxVSBuffer svb = m_skybox->GetVSBuffer();
	m_deviceContext->UpdateSubresource(m_bufferVSSkybox, 0, nullptr, &svb, 0, 0);

	SkyboxPSBuffer spb = m_skybox->GetPSBuffer();
	m_deviceContext->UpdateSubresource(m_bufferPSSkybox, 0, nullptr, &spb, 0, 0);

	Shader* skyboxShader = m_skybox->GetShader();
	SetShader(skyboxShader);

	// Set constant buffers to GPU
	m_deviceContext->VSSetConstantBuffers(3, 1, &m_bufferVSSkybox);
	m_deviceContext->PSSetConstantBuffers(0, 1, &m_bufferPSSkybox);

	// Set vertex buffer
	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	ID3D11Buffer* const buffer = submesh.vBuffer->GetVertexBuffer();
	m_deviceContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

	// Set index buffer
	m_deviceContext->IASetIndexBuffer(submesh.iBuffer->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

	// Render a triangle
	m_deviceContext->DrawIndexed(submesh.iBuffer->GetIndicesCount(), 0, 0);

	m_draw_call_count++;
}


void Redline::Renderer::DrawDebug() const
{
	vector<DebugLine> debugLines = Debug::GetDebugLines();

	const size_t linesCount = debugLines.size();

	if (linesCount > 0)
	{
		vector<Vertex> vertices;

		for (size_t i = 0; i < linesCount; ++i)
		{
			Vertex vtx;
			vtx.position = debugLines[i].startPos;
			vtx.normal = debugLines[i].color;
			vertices.emplace_back(vtx);

			vtx.position = debugLines[i].endPos;
			vertices.emplace_back(vtx);
		}

		VertexBuffer* buff = CreateVertexBuffer(vertices);

		UINT stride = sizeof(Vertex);
		UINT offset = 0;
		ID3D11Buffer* const buffer = buff->GetVertexBuffer();
		m_deviceContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);
		m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

		WorldViewMatrixBuffer mb = {
			Matrix::Identity,
			Engine::GetCamera().GetViewMatrix()
		};
		m_deviceContext->UpdateSubresource(m_view_matrix_buffer, 0, nullptr, &mb, 0, 0);

		MatrixBuffer projBuff = {
			Engine::GetCamera().GetProjectionMatrix()
		};

		m_deviceContext->UpdateSubresource(m_projection_matrix_buffer, 0, nullptr, &projBuff, 0, 0);

		ID3D11Buffer* buffers[] = { m_view_matrix_buffer, m_projection_matrix_buffer };
		m_deviceContext->VSSetConstantBuffers(0, 2, buffers);

		SetShader(m_wireframeShader);

		m_deviceContext->Draw(static_cast<UINT>(vertices.size()), 0);

		delete buff;
		Debug::ClearDebugLines();
	}

	Debug::DrawConsole();
}


bool Redline::Renderer::InitializeSamplers()
{
	HRESULT hr;

	const int filterMode = Engine::GetConfig().texture_filtering;
	const int minLOD = Engine::GetConfig().min_tex_lod;
	const int maxLOD = Engine::GetConfig().max_tex_lod;


	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));

	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;

	if (filterMode == 2 || filterMode == 4 || filterMode == 8 || filterMode == 16) // Enable ansiotropic filtering
	{
		sampDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		sampDesc.MaxAnisotropy = filterMode;
	}

	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = static_cast<FLOAT>(minLOD);
	sampDesc.MaxLOD = static_cast<FLOAT>(maxLOD);
	hr = m_device->CreateSamplerState(&sampDesc, m_linear_wrap_sampler.GetAddressOf());
	if (FAILED(hr))
	{
		Debug::Err("Failed to create the linear wrap sampler!");
		return false;
	}

	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	hr = m_device->CreateSamplerState(&sampDesc, m_linear_clamp_sampler.GetAddressOf());
	if (FAILED(hr))
	{
		Debug::Err("Failed to create the linear clamp sampler!");
		return false;
	}

	sampDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
	hr = m_device->CreateSamplerState(&sampDesc, m_point_clamp_sampler.GetAddressOf());
	if (FAILED(hr))
	{
		Debug::Err("Failed to create the point clamp sampler!");
		return false;
	}

	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	hr = m_device->CreateSamplerState(&sampDesc, m_point_wrap_sampler.GetAddressOf());
	if (FAILED(hr))
	{
		Debug::Err("Failed to create the point wrap sampler!");
		return false;
	}




	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.MinLOD = 0.0f;
	sampDesc.MaxLOD = 1.0f;
	sampDesc.MaxLOD = 1.0f;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_LESS;
	hr = m_device->CreateSamplerState(&sampDesc, m_shadow_comparaison_sampler.GetAddressOf());
	if (FAILED(hr))
	{
		Debug::Err("Failed to create the shadow comparaison sampler!");
		return false;
	}

	ID3D11SamplerState* samplers[] =
	{
		m_linear_clamp_sampler.Get(),
		m_point_clamp_sampler.Get(),
		m_linear_wrap_sampler.Get(),
		m_point_wrap_sampler.Get(),
		m_shadow_comparaison_sampler.Get()
	};
	m_deviceContext->PSSetSamplers(0, 5, samplers);
	return true;
}


void Redline::Renderer::ShutdownSamplers()
{
	m_linear_clamp_sampler.Reset();
	m_point_clamp_sampler.Reset();
	m_linear_wrap_sampler.Reset();
	m_point_wrap_sampler.Reset();
	m_shadow_comparaison_sampler.Reset();
}


void Redline::Renderer::SetShader(Shader* shader) const
{
	shader->Recompile(m_device);

#if _DEBUG
	if (shader->GetVertexShader() == nullptr || shader->GetPixelShader() == nullptr)
	{
		Debug::Err("Renderer: Can't bind a null shader!");
		return;
	}
#endif

	m_deviceContext->VSSetShader(shader->GetVertexShader(), nullptr, 0);
	m_deviceContext->PSSetShader(shader->GetPixelShader(), nullptr, 0);
}


Redline::Texture* Redline::Renderer::CreateTextureFromFile(const string& textureName) const
{
	Texture* texture = new Texture();
	if (!texture->CreateFromFile(textureName, m_device))
	{
		delete texture;
		return nullptr;
	}
	return texture;
}


Redline::Shader* Redline::Renderer::CreateShaderFromFile(const string& shaderName) const
{
	Shader* shader = new Shader();
	if (!shader->CreateFromFile(shaderName, m_device))
	{
		delete shader;
		return nullptr;
	}
	return shader;
}


Redline::VertexBuffer* Redline::Renderer::CreateVertexBuffer(const vector<Vertex>& vertices) const
{
	VertexBuffer* buffer = new VertexBuffer();
	if (!buffer->CreateFromVector(vertices, m_device))
	{
		delete buffer;
		return nullptr;
	}
	return buffer;
}


Redline::IndexBuffer* Redline::Renderer::CreateIndexBuffer(const vector<unsigned int>& indices) const
{
	IndexBuffer* buffer = new IndexBuffer();
	if (!buffer->CreateFromVector(indices, m_device))
	{
		delete buffer;
		return nullptr;
	}
	return buffer;
}
