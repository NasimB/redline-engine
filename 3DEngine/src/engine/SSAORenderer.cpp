﻿#include "SSAORenderer.h"

#include "debug.h"
#include "ResourcesManager.h"
#include "renderer.h"
#include "engine.h"

using namespace std;
using namespace Microsoft::WRL;

Redline::SSAORenderer::SSAORenderer() :
	m_ssaoRT(nullptr),
	m_ssaoBlurRT(nullptr),
	m_ssaoShader(nullptr),
	m_ssaoBlurXShader(nullptr),
	m_ssaoBlurYShader(nullptr),
	m_bufferSSAO(nullptr), 
	m_noise_texture(nullptr),
	m_ssaoWidth(0),
	m_ssaoHeight(0)
{
}


Redline::SSAORenderer::~SSAORenderer()
{

}


bool Redline::SSAORenderer::Initialize(const ComPtr<ID3D11Device>& device, const int ssao_technique, const int render_width, const int render_height)
{
	m_ssaoEnabled = ssao_technique > 0;

	if (!m_ssaoEnabled)
		return true;


	m_ssaoWidth = static_cast<float>(render_width) * m_ssaoRenderScale;
	m_ssaoHeight = static_cast<float>(render_height) * m_ssaoRenderScale;
	const unsigned int ssaoWidth = static_cast<unsigned int>(m_ssaoWidth);
	const unsigned int ssaoHeight = static_cast<unsigned int>(m_ssaoHeight);

	m_ssaoRT = RenderTarget::Create(device.Get(), ssaoWidth, ssaoHeight, DXGI_FORMAT_R8_UNORM, true);
	if (m_ssaoRT == nullptr)
	{
		Debug::Err("Failed to create the SSAO render target!");
		return false;
	}

	
	m_ssaoBlurRT = RenderTarget::Create(device.Get(), ssaoWidth, ssaoHeight, DXGI_FORMAT_R8_UNORM, true);
	if (m_ssaoBlurRT == nullptr)
	{
		Debug::Err("Failed to create the SSAO blur render target!");
		return false;
	}


	string path;
	switch (ssao_technique)
	{
	default:
	case 1:
		path = "data\\shaders\\ssao.fx";
		break;
	case 2:
		path = "data\\shaders\\mxao.fx";
		break;
	case 3:
		path = "data\\shaders\\ssrtgi.fx";
		break;
	}


	m_noise_texture = ResourcesManager::GetTexture("data\\textures\\blue_noise.dds");
	if (m_noise_texture == nullptr)
	{
		Debug::Err("Blue noise texture is missing!");
		return false;
	}

	m_ssaoShader = ResourcesManager::GetShader(path);
	if (m_ssaoShader == nullptr)
	{
		Debug::Err("SSAO shader is missing!");
		return false;
	}

	m_ssaoBlurXShader = new Shader();
	if (!m_ssaoBlurXShader->CreateFromFile(path, device, "VS", "PS_BlurX"))
	{
		Debug::Err("Failed to compile the SSAO Blur X shader!");
		return false;
	}

	m_ssaoBlurYShader = new Shader();
	if (!m_ssaoBlurYShader->CreateFromFile(path, device, "VS", "PS_BlurY"))
	{
		Debug::Err("Failed to compile the SSAO Blur Y shader!");
		return false;
	}

	// Create SSAO buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(SSAOBuffer);

	SSAOBuffer buffer = {
		Engine::GetCamera().GetProjectionMatrix(),
		Engine::GetCamera().GetProjectionMatrix().Invert(),
		1.0f / m_ssaoWidth,
		1.0f / m_ssaoHeight,
		m_ssaoWidth / m_ssaoHeight,
		0.0f
	};

	D3D11_SUBRESOURCE_DATA subresource_data;
	subresource_data.pSysMem = &buffer;

	if (FAILED(device->CreateBuffer(&bd, &subresource_data, &m_bufferSSAO)))
	{
		Debug::Err("Failed to create the SSAO buffer!");
		return false;
	}

	return true;
}


void Redline::SSAORenderer::Shutdown()
{
	if (m_bufferSSAO)
	{
		m_bufferSSAO->Release();
		m_bufferSSAO = nullptr;
	}

	if (m_ssaoBlurXShader)
	{
		delete m_ssaoBlurXShader;
		m_ssaoBlurXShader = nullptr;
	}

	if (m_ssaoBlurYShader)
	{
		delete m_ssaoBlurYShader;
		m_ssaoBlurYShader = nullptr;
	}

	if (m_ssaoBlurRT)
	{
		delete m_ssaoBlurRT;
		m_ssaoBlurRT = nullptr;
	}

	if (m_ssaoRT)
	{
		delete m_ssaoRT;
		m_ssaoRT = nullptr;
	}
}


bool Redline::SSAORenderer::Render(const ComPtr<ID3D11DeviceContext>& device_context, RenderTarget* position_RT, RenderTarget* normal_RT, RenderTarget* color_RT) const
{
	if (!m_ssaoEnabled)
		return true;

	m_ssaoRT->Clear(device_context.Get(), 1, 1, 1, 1);

	device_context->OMSetRenderTargets(1, m_ssaoRT->GetRenderTargetView().GetAddressOf(), nullptr);

	D3D11_VIEWPORT viewport;
	viewport.Width = m_ssaoWidth;
	viewport.Height = m_ssaoHeight;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	device_context->RSSetViewports(1, &viewport);

	device_context->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	device_context->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	device_context->VSSetShader(m_ssaoShader->GetVertexShader(), nullptr, 0);
	device_context->PSSetShader(m_ssaoShader->GetPixelShader(), nullptr, 0);

	SSAOBuffer buffer = {
		Engine::GetCamera().GetProjectionMatrix(),
		Engine::GetCamera().GetProjectionMatrix().Invert(),
		1.0f / m_ssaoWidth,
		1.0f / m_ssaoHeight,
		m_ssaoWidth / m_ssaoHeight,
		0.0f
	};
	device_context->UpdateSubresource(m_bufferSSAO, 0, nullptr, &buffer, 0, 0);

	device_context->VSSetConstantBuffers(0, 1, &m_bufferSSAO);
	device_context->PSSetConstantBuffers(0, 1, &m_bufferSSAO);

	ID3D11ShaderResourceView* srViews[] = { position_RT->GetShaderResourceView().Get(), normal_RT->GetShaderResourceView().Get(), color_RT->GetShaderResourceView().Get(), m_noise_texture->GetTexture().Get() };
	device_context->PSSetShaderResources(0, 4, srViews);

	device_context->Draw(3, 0);

	// BLUR X
	{
		device_context->OMSetRenderTargets(1, m_ssaoBlurRT->GetRenderTargetView().GetAddressOf(), nullptr);
		device_context->PSSetShader(m_ssaoBlurXShader->GetPixelShader(), nullptr, 0);

		device_context->PSSetShaderResources(2, 1, m_ssaoRT->GetShaderResourceView().GetAddressOf());

		device_context->Draw(3, 0);
	}

	// BLUR Y
	{
		ID3D11ShaderResourceView* emptySRV[] = { nullptr };
		device_context->PSSetShaderResources(2, 1, emptySRV);

		device_context->OMSetRenderTargets(1, m_ssaoRT->GetRenderTargetView().GetAddressOf(), nullptr);
		device_context->PSSetShader(m_ssaoBlurYShader->GetPixelShader(), nullptr, 0);

		device_context->PSSetShaderResources(2, 1, m_ssaoBlurRT->GetShaderResourceView().GetAddressOf());

		device_context->Draw(3, 0);
	}

	return true;
}
