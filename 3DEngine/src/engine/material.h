#pragma once

#include "math.h"

namespace Redline
{
	struct MaterialBuffer
	{
		float mDiffuseColor[3];
		float roughness;
		float metalic;
		int hasDiffuseMap;
		int hasNormalMap;
		int hasSpecularMap;
	};

	class Texture;

	class Material
	{
	public:
		Material();
		~Material();

		bool Initialize(Texture* diffuseTexture = nullptr, Texture* normalTexture = nullptr);

		float* GetDiffuseColor() { return m_material_buffer.mDiffuseColor; }

		void SetDiffuseColor(const float& r, const float& g, const float& b, const bool is_linear = false)
		{
			m_material_buffer.mDiffuseColor[0] = is_linear ? r : Math::Pow(r, 2.2f);
			m_material_buffer.mDiffuseColor[1] = is_linear ? g : Math::Pow(g, 2.2f);
			m_material_buffer.mDiffuseColor[2] = is_linear ? b : Math::Pow(b, 2.2f);
		}

		float GetRoughness() const { return m_material_buffer.roughness; }
		void SetRoughness(const float& value) { m_material_buffer.roughness = Math::Clamp(value, 0.01f, 1.0f); }

		float GetMetalic() const { return m_material_buffer.metalic; }
		void SetMetalic(const float& value) { m_material_buffer.metalic = Math::Clamp(value, 0.01f, 1.0f); }

		const MaterialBuffer& GetBuffer() const { return m_material_buffer; }

		Texture* GetDiffuseTexture() const { return m_diffuse_texture; }

		void SetDiffuseTexture(Texture* diffuseTexture)
		{
			m_diffuse_texture = diffuseTexture;
			m_material_buffer.hasDiffuseMap = m_diffuse_texture != nullptr ? 1 : 0;
		}

		Texture* GetNormalTexture() const { return m_normal_texture; }

		void SetNormalTexture(Texture* normalTexture)
		{
			m_normal_texture = normalTexture;
			m_material_buffer.hasNormalMap = m_normal_texture != nullptr ? 1 : 0;
		}


		void SetName(const std::string& name) { m_name = name; }
		std::string GetName() const { return m_name; }

#ifdef REDLINE_EDITOR
		void ShowInspector();
#endif

	private:
		Texture* m_diffuse_texture;
		Texture* m_normal_texture;
		MaterialBuffer m_material_buffer;

		std::string m_name;
	};
}
