#include "debug.h"

Redline::Debug* Redline::Debug::m_debug = nullptr;


Redline::Debug::Debug()
{
	m_debug = this;

	m_debugArray.reserve(static_cast<size_t>(maxSize));

	myfile = std::ofstream("debug.log", std::ios::trunc);
}


Redline::Debug::~Debug()
{
	myfile.close();
}


void Redline::Debug::AddDebugLog(const std::string& text, const int type, const bool showOnConsole)
{
	std::string prefix = type == DEBUG_LOG ? "[Log] " : type == DEBUG_ERROR ? "[Error] " : "[Warn] ";
	std::string suffix = "\n";
	std::string log = prefix + text + suffix;

	OutputDebugStringA(log.c_str());

	while (static_cast<int>(m_debugArray.size()) > maxSize)
	{
		m_debugArray.erase(m_debugArray.begin());
	}

	if (showOnConsole)
	{
		m_debugArray.emplace_back(log);
	}

	if (myfile.is_open())
	{
		std::string s(log.begin(), log.end());
		SYSTEMTIME time;
		GetLocalTime(&time);
		myfile << "[" << static_cast<int>(time.wDay) << "/" << static_cast<int>(time.wMonth) << "/" << static_cast<int>(time.wYear) << " - " << static_cast<int>(time.wHour) << ":" << (std::to_string(time.wMinute).size() == 1 ? "0" + std::to_string(time.wMinute) : std::to_string(time.wMinute)) << ":" << (std::to_string(time.wSecond).size() == 1 ? "0" + std::to_string(time.wSecond) : std::to_string(time.wSecond)) << "] " << s;
	}
}


void Redline::Debug::Log(const std::string& text, const bool showOnConsole)
{
	m_debug->AddDebugLog(text, DEBUG_LOG, showOnConsole);
}


void Redline::Debug::Err(const std::string& text, const bool showOnConsole)
{
	m_debug->AddDebugLog(text, DEBUG_ERROR, showOnConsole);
}


void Redline::Debug::Warn(const std::string& text, const bool showOnConsole)
{
	m_debug->AddDebugLog(text, DEBUG_WARN, showOnConsole);
}


void Redline::Debug::DrawConsole()
{
	size_t arraySize = m_debug->m_debugArray.size();

	if (arraySize == 0)
		return;

	ImGui::SetNextWindowPos(ImVec2(5, 5), ImGuiSetCond_FirstUseEver);
	if(ImGui::Begin("Debug console"))
	{
		for (size_t i = 0; i < arraySize; ++i)
			ImGui::Text(m_debug->m_debugArray[i].c_str());
	}
	ImGui::End();
}


void Redline::Debug::Line(const DirectX::SimpleMath::Vector3& startPos, const DirectX::SimpleMath::Vector3& endPos, const DirectX::SimpleMath::Vector3& color)
{
	DebugLine debugLine;
	debugLine.startPos = static_cast<DirectX::XMFLOAT3>(startPos);
	debugLine.endPos = static_cast<DirectX::XMFLOAT3>(endPos);
	debugLine.color = static_cast<DirectX::XMFLOAT3>(color);
	m_debug->linesArray.emplace_back(debugLine);
}
