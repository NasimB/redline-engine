#pragma once

#include "transform.h"
#include "math.h"
#include "Component.h"

namespace Redline
{
	enum LightType
	{
		LIGHT_DIRECTIONAL = 0,
		LIGHT_POINT,
		LIGHT_SPOT
	};

	class Light : public Component
	{
	public:
		Light(GameObject* game_object);
		~Light();

		void SetColor(const float red, const float green, const float blue)
		{
			m_color.x = Math::Pow(red, 2.2f);
			m_color.y = Math::Pow(green, 2.2f);
			m_color.z = Math::Pow(blue, 2.2f);
		}

		void SetColor(const DirectX::SimpleMath::Vector3& color) { m_color = color; }
		DirectX::SimpleMath::Vector3 GetColor() const { return m_color; }

		void SetDirection(const float x, const float y, const float z)
		{
			m_direction = DirectX::SimpleMath::Vector3(x, y, z);
			m_direction.Normalize();
		}

		void SetDirection(const DirectX::SimpleMath::Vector3& direction)
		{
			m_direction = direction;
			m_direction.Normalize();
		}
		DirectX::SimpleMath::Vector3 GetDirection() const { return m_direction; }

		void SetIntensity(const float value) { m_intensity = Math::Max(value, 0.0f); }
		float GetIntensity() const { return m_intensity; }

		// Set the angle (for spot lights only) (in degrees)
		void SetAngle(const float value) { m_angle = Math::Clamp(value, 0.0f, 360.0f); }
		// Returns the angle for the light (for spot lights only)
		float GetAngle() const { return m_angle; }

		void SetType(const LightType type) { m_light_type = type; }
		LightType GetType() const { return m_light_type; }

		void SetActive(const bool value) { m_enabled = value; }
		bool IsActive() const { return m_enabled; }

		void SetCastShadows(const bool value) { m_cast_shadows = value; }
		bool IsCastingShadows() const { return m_cast_shadows; }

		static ComponentType GetComponentType() { return Component::Light; }

		void Serialize(std::ofstream& stream) override;
		void Deserialize(std::ifstream& stream) override;

#ifdef REDLINE_EDITOR
		void ShowInspector() override;
#endif

	protected:
		DirectX::SimpleMath::Vector3 m_direction;
		LightType m_light_type;
		DirectX::SimpleMath::Vector3 m_color;

		float m_angle;
		float m_intensity;

		bool m_enabled;
		bool m_cast_shadows;
	};
}
