#include "engine.h"

//extern "C" _declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
extern "C" __declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;
extern "C" __declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;

Redline::Engine* engine = nullptr;

void myTickCallback(btDynamicsWorld* world, btScalar timeStep)
{
	engine->OnPhysicsUpdate(timeStep);
}


int CALLBACK WinMain(HINSTANCE instance, HINSTANCE previous_instance, PSTR command_line, INT show_flag)
{
	engine = new Redline::Engine();

	if (!engine->Initialize())
	{
		// Engine failed to initialize
		MessageBoxA(nullptr, "Engine initialization failed !", "Fatal Error", MB_OK | MB_ICONERROR);
		return 0;
	}

	engine->GetPhysics().GetWorld()->setInternalTickCallback(myTickCallback);

	int return_code = engine->Run();

	delete engine;

	return return_code;
}
