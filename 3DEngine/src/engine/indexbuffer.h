#pragma once

namespace Redline
{
	class IndexBuffer
	{
	public:
		IndexBuffer();
		~IndexBuffer();

		bool CreateFromVector(const std::vector<unsigned int>& indices, const Microsoft::WRL::ComPtr<ID3D11Device>& device);

		ID3D11Buffer* GetIndexBuffer() const { return m_indexBuffer; }

		std::vector<unsigned int>& GetIndices() { return m_indices; }
		unsigned int GetIndicesCount() const { return m_indicesCount; }

	private:
		std::vector<unsigned int> m_indices;
		ID3D11Buffer* m_indexBuffer = nullptr;
		unsigned int m_indicesCount = 0;
	};
}
