#include "mesh.h"
#include "engine.h"

#include <fstream>
#include "stringtools.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

Redline::Mesh::Mesh() :
	m_name("")
{
}


Redline::Mesh::~Mesh()
{
	for (SubMesh sub : m_submeshes)
	{
		delete sub.vBuffer;
		delete sub.iBuffer;
	}
}


bool Redline::Mesh::Initialize(const std::vector<SubMesh>& submeshes, const std::vector<Material>& materials)
{
	m_submeshes = submeshes;
	m_materials = materials;
	return true;
}


bool Redline::Mesh::Initialize(const string& filename)
{
	vector<string> pathSplited = StringTools::Split(filename, '\\');
	m_name = pathSplited[pathSplited.size()-1];
	m_name = m_name.substr(0, m_name.size() - 4);

	if (StringTools::HasEnding(filename, ".rmf"))
		return LoadRedlineMesh(filename);

	return false;
}


bool Redline::Mesh::LoadRedlineMesh(const string& filename)
{
	ifstream fileIn(filename, ios_base::binary);

	if (!fileIn.is_open())
		return false;

	MeshHeader header;
	fileIn.read(reinterpret_cast<char*>(&header), sizeof(MeshHeader));

	if (header.fileType[0] != 'R' || header.fileType[1] != 'M' || header.fileType[2] != 'F')
	{
		MessageBoxA(nullptr, "Cannot read mesh file : Filetype is invalid", "Fatal Error", MB_OK | MB_ICONERROR);
		return false;
	}

	if (header.fileType[3] == '3')
	{
		const size_t materialsCount = static_cast<size_t>(header.materialsCount);
		m_materials.reserve(materialsCount);
		for (size_t i = 0; i < materialsCount; ++i)
		{
			MaterialStruct3 mat;
			fileIn.read(reinterpret_cast<char*>(&mat), sizeof(MaterialStruct3));

			string str = "data\\textures\\";
			str += mat.diffuseTextureName;

			if (!StringTools::HasEnding(mat.diffuseTextureName, ".dds") && !StringTools::HasEnding(mat.diffuseTextureName, ".DDS"))
				str += ".dds";

			Texture* diffuseTexture = ResourcesManager::GetTexture(str);

			str = "data\\textures\\";
			str += mat.normalTextureName;

			if (!StringTools::HasEnding(mat.normalTextureName, ".dds") && !StringTools::HasEnding(mat.normalTextureName, ".DDS"))
				str += ".dds";

			Texture* normalTexture = ResourcesManager::GetTexture(str);

			Material material;
			material.SetName(mat.name);
			material.Initialize(diffuseTexture, normalTexture);
			material.SetDiffuseColor(mat.diffuseColor[0], mat.diffuseColor[1], mat.diffuseColor[2]);
			m_materials.emplace_back(material);
		}

		const size_t objectsCount = static_cast<size_t>(header.objectsCount);
		m_submeshes.reserve(objectsCount);
		for (size_t i = 0; i < objectsCount; ++i)
		{
			SubMesh subMesh;

			unsigned int value = 0;

			fileIn.read(reinterpret_cast<char*>(&value), sizeof(unsigned int));
			const size_t vertexCount = static_cast<size_t>(value);

			fileIn.read(reinterpret_cast<char*>(&value), sizeof(unsigned int));
			const size_t indexCount = static_cast<size_t>(value);

			fileIn.read(reinterpret_cast<char*>(&subMesh.materialID), sizeof(unsigned int));

			const float weight = 1 / static_cast<float>(vertexCount);
			Vector3 avgPos(0, 0, 0);

			vector<Vertex> vertices;
			vertices.reserve(vertexCount);

			struct VertexFile
			{
				float position[3];
				float normal[3];
				float texture[2];
			};

			for (size_t j = 0; j < vertexCount; ++j)
			{
				VertexFile vf;
				fileIn.read(reinterpret_cast<char*>(&vf), sizeof(VertexFile));

				Vertex v;
				const Vector3 position = Vector3(vf.position[0], vf.position[1], -vf.position[2]);
				v.position = static_cast<DirectX::XMFLOAT3>(position);
				v.normal = static_cast<DirectX::XMFLOAT3>(Vector3(vf.normal[0], vf.normal[1], -vf.normal[2]));
				v.texture = static_cast<DirectX::XMFLOAT2>(Vector2(vf.texture[0], vf.texture[1]));
				vertices.emplace_back(v);

				avgPos += position * weight;
				m_boundingSphereRadius = max(m_boundingSphereRadius, position.LengthSquared());
			}


			VertexBuffer* vBuffer = Engine::GetRenderer().CreateVertexBuffer(vertices);
			subMesh.vBuffer = vBuffer;

			vector<unsigned int> indices(indexCount);
			for (size_t j = 0; j < indexCount; ++j)
			{
				indices[j] = static_cast<unsigned int>(j);
			}

			IndexBuffer* iBuffer = Engine::GetRenderer().CreateIndexBuffer(indices);
			subMesh.iBuffer = iBuffer;

			subMesh.boundingSphereCenter = avgPos;
			subMesh.boundingSphereRadius = ComputeBoundingSphere(avgPos, vertices);

			m_submeshes.emplace_back(subMesh);
		}

		m_boundingSphereRadius = sqrt(m_boundingSphereRadius);
	}
	else if(header.fileType[3] == '4')
	{
		const size_t materialsCount = static_cast<size_t>(header.materialsCount);
		m_materials.reserve(materialsCount);
		for (size_t i = 0; i < materialsCount; ++i)
		{
			MaterialStruct mat;
			fileIn.read(reinterpret_cast<char*>(&mat), sizeof(MaterialStruct));

			Texture* diffuseTexture = nullptr;
			if (strcmp(mat.albedo_name, "") != 0)
			{
				string str = "data\\textures\\";
				str += mat.albedo_name;

				if (!StringTools::HasEnding(mat.albedo_name, ".dds") && !StringTools::HasEnding(mat.albedo_name, ".DDS"))
					str += ".dds";

				diffuseTexture = ResourcesManager::GetTexture(str);
			}

			Texture* normalTexture = nullptr;
			if (strcmp(mat.normal_name, "") != 0)
			{
				string str = "data\\textures\\";
				str += mat.normal_name;

				if (!StringTools::HasEnding(mat.normal_name, ".dds") && !StringTools::HasEnding(mat.normal_name, ".DDS"))
					str += ".dds";

				normalTexture = ResourcesManager::GetTexture(str);
			}

			Material material;
			material.Initialize(diffuseTexture, normalTexture);
			material.SetName(mat.material_name);
			material.SetDiffuseColor(mat.albedo_color[0], mat.albedo_color[1], mat.albedo_color[2], true);
			material.SetRoughness(mat.roughness);
			material.SetMetalic(mat.metallic);

			m_materials.emplace_back(material);
		}

		const size_t objectsCount = static_cast<size_t>(header.objectsCount);
		m_submeshes.reserve(objectsCount);
		for (size_t i = 0; i < objectsCount; ++i)
		{
			SubMesh subMesh;

			unsigned int value = 0;

			fileIn.read(reinterpret_cast<char*>(&value), sizeof(unsigned int));
			const size_t vertexCount = static_cast<size_t>(value);

			fileIn.read(reinterpret_cast<char*>(&value), sizeof(unsigned int));
			const size_t indexCount = static_cast<size_t>(value);

			fileIn.read(reinterpret_cast<char*>(&subMesh.materialID), sizeof(unsigned int));

			const float weight = 1 / static_cast<float>(vertexCount);
			Vector3 avgPos(0, 0, 0);

			vector<Vertex> vertices;
			vertices.reserve(vertexCount);

			for (size_t j = 0; j < vertexCount; ++j)
			{
				Vertex vertex;
				fileIn.read(reinterpret_cast<char*>(&vertex), sizeof(Vertex));
				vertices.emplace_back(vertex);

				Vector3 position = vertex.position;
				avgPos += position * weight;
				m_boundingSphereRadius = max(m_boundingSphereRadius, position.LengthSquared());
			}


			VertexBuffer* vBuffer = Engine::GetRenderer().CreateVertexBuffer(vertices);
			subMesh.vBuffer = vBuffer;

			unsigned int* indices_array = new unsigned int[indexCount];
			fileIn.read(reinterpret_cast<char*>(indices_array), indexCount * sizeof(unsigned int));

			vector<unsigned int> indices;
			indices.insert(indices.end(), &indices_array[0], &indices_array[indexCount]);

			IndexBuffer* iBuffer = Engine::GetRenderer().CreateIndexBuffer(indices);
			subMesh.iBuffer = iBuffer;

			subMesh.boundingSphereCenter = avgPos;
			subMesh.boundingSphereRadius = ComputeBoundingSphere(avgPos, vertices);

			m_submeshes.emplace_back(subMesh);
		}

		m_boundingSphereRadius = sqrt(m_boundingSphereRadius);
	}

	fileIn.close();

	return true;
}


float Redline::Mesh::ComputeBoundingSphere(const Vector3& avgPos, const vector<Vertex>& vertices) const
{
	float radius = 0.0f;

	const size_t verticesCount = vertices.size();
	for (size_t i = 0; i < verticesCount; ++i)
	{
		radius = max(radius, Vector3::DistanceSquared(vertices[i].position, avgPos));
	}

	return sqrt(radius);
}
