#include "meshrenderer.h"
#include "ResourcesManager.h"

Redline::MeshRenderer::MeshRenderer(GameObject* game_object) :
	Component(game_object, ComponentType::MeshRenderer),
	m_mesh(nullptr),
	m_casting_shadows(true),
	m_active(true)
{
}


Redline::MeshRenderer::~MeshRenderer()
{
	m_mesh = nullptr;
}


void Redline::MeshRenderer::SetMesh(const std::string meshName)
{
	std::string path = "data\\meshes\\" + meshName + ".rmf";
	Mesh* mesh = ResourcesManager::GetMesh(path);

	m_mesh = mesh;

	if (mesh != nullptr)
	{
		m_materials = mesh->GetMaterials();
	}
	else if(!m_materials.empty())
	{
		m_materials.clear();
	}
}


void Redline::MeshRenderer::Serialize(std::ofstream& stream)
{
	char mesh_name[256];
	strncpy_s(mesh_name, m_mesh->GetName().c_str(), sizeof(mesh_name));
	stream.write(reinterpret_cast<char*>(&mesh_name), sizeof(mesh_name));
	stream.write(reinterpret_cast<char*>(&m_casting_shadows), sizeof(bool));
	stream.write(reinterpret_cast<char*>(&m_active), sizeof(bool));
}


void Redline::MeshRenderer::Deserialize(std::ifstream& stream)
{
	char mesh_name[256];
	stream.read(reinterpret_cast<char*>(&mesh_name), sizeof(mesh_name));
	stream.read(reinterpret_cast<char*>(&m_casting_shadows), sizeof(bool));
	stream.read(reinterpret_cast<char*>(&m_active), sizeof(bool));
	SetMesh(mesh_name);
}


#ifdef REDLINE_EDITOR
void Redline::MeshRenderer::ShowInspector()
{
	if (!ImGui::TreeNodeEx("Mesh Renderer", ImGuiTreeNodeFlags_DefaultOpen))
		return;

	ImGui::Checkbox("Active", &m_active);

	ImGui::Spacing();

	std::string text = "Mesh: ";
	if (m_mesh != nullptr)
		text += m_mesh->GetName() + ".rmf";
	else
		text += "None";

	ImGui::Text(text.c_str());

	ImGui::Checkbox("Casts shadow", &m_casting_shadows);

	ImGui::Spacing();

	if (ImGui::TreeNode("Materials"))
	{
		for (auto material : m_materials)
		{
			material.ShowInspector();
		}

		ImGui::TreePop();
	}

	ImGui::TreePop();
}
#endif
