#pragma once

namespace Redline
{
	class Window
	{
	public:
		Window();
		~Window();
		bool Initialize(bool fullscreen, int width, int height);
		static LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);

		HWND GetHWND() const { return m_hwnd; }
		HINSTANCE GetHInstance() const { return m_hinstance; }

	private:
		HWND m_hwnd;
		HINSTANCE m_hinstance;
		std::wstring m_name;
	};
}
