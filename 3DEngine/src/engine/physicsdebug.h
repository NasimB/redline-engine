#pragma once

namespace Redline
{
	class PhysicsDebug : public btIDebugDraw
	{
	public:
		PhysicsDebug();

		void drawLine(const btVector3& from, const btVector3& to, const btVector3& color) override;

		void draw3dText(const btVector3& location, const char* textString) override {}

		void setDebugMode(int debugMode) override { m_debug_draw_mode = debugMode; }

		int	getDebugMode() const override { return m_debug_draw_mode; }

		void reportErrorWarning(const char* warningString) override;

		void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) override {}

	private:
		int m_debug_draw_mode;
	};
}
