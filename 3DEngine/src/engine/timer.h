#pragma once

namespace Redline
{
	class Timer
	{
	public:
		Timer();
		~Timer();

		bool Initialize();
		void Update();
		float GetDeltaTime();
		unsigned int GetFPS() const;
		void StartCounter();
		double GetCounter() const;

		double GetTime() const;
	private:
		INT64 m_start_time;
		float m_ticks_per_ms;
		int m_frame_tick;
		INT64 m_last_check;
		int m_fps;

		double PCFreq = 0.0;
		__int64 CounterStart = 0;
	};
}
