#include "physics.h"
#include "physicsdebug.h"


Redline::Physics::Physics() :
	m_world(nullptr),
	m_broadphase(nullptr),
	m_dispatcher(nullptr),
	m_collision_configuration(nullptr),
	m_sequential_impulse_constraint_solver(nullptr),
	m_physics_debug(nullptr)
{
}


Redline::Physics::~Physics()
{
}


bool Redline::Physics::Initialize()
{
	m_collision_configuration = new btDefaultCollisionConfiguration();
	m_dispatcher = new btCollisionDispatcher(m_collision_configuration);
	m_broadphase = new btDbvtBroadphase();
	m_sequential_impulse_constraint_solver = new btSequentialImpulseConstraintSolver;
	m_world = new btDiscreteDynamicsWorld(m_dispatcher, m_broadphase, m_sequential_impulse_constraint_solver, m_collision_configuration);
	m_world->setGravity(btVector3(0, -9.81f, 0));

	m_physics_debug = new PhysicsDebug();
#ifdef REDLINE_EDITOR
	m_physics_debug->setDebugMode(btIDebugDraw::DebugDrawModes::DBG_DrawWireframe);
#else
	m_physics_debug->setDebugMode(btIDebugDraw::DebugDrawModes::DBG_NoDebug);
#endif
	m_world->setDebugDrawer(m_physics_debug);
	return true;
}


bool Redline::Physics::Update(const float delta_time) const
{
	const btScalar deltaTime = delta_time * m_time_scale;
	const btScalar refreshTime = (1.0f / 120.0f) * m_time_scale;
	m_world->stepSimulation(deltaTime, 6, refreshTime);
	return true;
}


void Redline::Physics::DrawDebug() const
{
	m_world->debugDrawWorld();
}


void Redline::Physics::Shutdown()
{
	delete m_world;
	m_world = nullptr;

	delete m_sequential_impulse_constraint_solver;
	m_sequential_impulse_constraint_solver = nullptr;

	delete m_broadphase;
	m_broadphase = nullptr;

	delete m_dispatcher;
	m_dispatcher = nullptr;

	delete m_collision_configuration;
	m_collision_configuration = nullptr;

	delete m_physics_debug;
	m_physics_debug = nullptr;
}


void Redline::Physics::AddRigidBody(btRigidBody* rigidBody) const
{
	m_world->addRigidBody(rigidBody);
}


void Redline::Physics::RemoveRigidBody(btRigidBody* rigidBody) const
{
	m_world->removeRigidBody(rigidBody);
}
