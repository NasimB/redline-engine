﻿#pragma once

#include "shader.h"
#include "rendertarget.h"
#include "texture.h"

namespace Redline
{
	class SMAARenderer
	{
	public:
		SMAARenderer();
		~SMAARenderer();

		bool Initialize(const Microsoft::WRL::ComPtr<ID3D11Device>& device, const int width, const int height);

		void Shutdown();

		void Render(Microsoft::WRL::ComPtr<ID3D11DeviceContext> device_context, RenderTarget* input_RT, RenderTarget* output_RT) const;

		struct SMAABuffer
		{
			float RCPFrame[4];
		};
	private:
		ID3D11Buffer* m_smaa_buffer;
		Shader* m_smaa_edge_shader;
		RenderTarget* m_edges_RT;

		Shader* m_smaa_blend_shader;
		RenderTarget* m_blend_RT;

		Shader* m_smaa_neighbor_blend_shader;

		Texture* m_area_texture;
		Texture* m_search_texture;

		float m_width;
		float m_height;
	};
}
