﻿#pragma once

#include "shader.h"
#include "rendertarget.h"

namespace Redline
{
	class FXAARenderer
	{
	public:
		FXAARenderer();
		~FXAARenderer();

		bool Initialize(const Microsoft::WRL::ComPtr<ID3D11Device>& device, const int width, const int height);

		void Shutdown();

		void Render(Microsoft::WRL::ComPtr<ID3D11DeviceContext> device_context, RenderTarget* input_RT, RenderTarget* output_RT) const;

		struct FXAABuffer
		{
			float RCPFrame[4];
		};
	private:
		ID3D11Buffer* m_fxaa_buffer;
		Shader* m_fxaa_shader;

		float m_width;
		float m_height;
	};
}
