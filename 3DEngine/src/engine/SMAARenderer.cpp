﻿#include "SMAARenderer.h"
#include "debug.h"
#include "ResourcesManager.h"


Redline::SMAARenderer::SMAARenderer():
	m_smaa_buffer(nullptr),
	m_smaa_edge_shader(nullptr),
	m_edges_RT(nullptr),
	m_smaa_blend_shader(nullptr),
	m_blend_RT(nullptr), 
	m_smaa_neighbor_blend_shader(nullptr),
	m_area_texture(nullptr),
	m_search_texture(nullptr),
	m_width(0),
	m_height(0)
{
}


Redline::SMAARenderer::~SMAARenderer()
{

}


bool Redline::SMAARenderer::Initialize(const Microsoft::WRL::ComPtr<ID3D11Device>& device, const int width, const int height)
{
	m_smaa_edge_shader = new Shader();
	if (!m_smaa_edge_shader->CreateFromFile("data\\shaders\\smaa.fx", device, "VS_EdgeDetection", "PS_EdgeDetection"))
	{
		Debug::Err("Failed to compile the SMAA Edge Detection shader!");
		return false;
	}

	m_smaa_blend_shader = new Shader();
	if (!m_smaa_blend_shader->CreateFromFile("data\\shaders\\smaa.fx", device, "VS_BlendingWeight", "PS_BlendingWeight"))
	{
		Debug::Err("Failed to compile the SMAA Blend Weights shader!");
		return false;
	}

	m_smaa_neighbor_blend_shader = new Shader();
	if (!m_smaa_neighbor_blend_shader->CreateFromFile("data\\shaders\\smaa.fx", device, "VS_NeighborhoodBlend", "PS_NeighborhoodBlend"))
	{
		Debug::Err("Failed to compile the SMAA Neighborhood Blending shader!");
		return false;
	}

	m_width = static_cast<float>(width);
	m_height = static_cast<float>(height);

	// Create FXAA buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(SMAABuffer);

	SMAABuffer buffer = {
		1.0f / m_width,
		1.0f / m_height,
		m_width,
		m_height
	};

	D3D11_SUBRESOURCE_DATA subresource_data;
	subresource_data.pSysMem = &buffer;

	if (FAILED(device->CreateBuffer(&bd, &subresource_data, &m_smaa_buffer)))
	{
		Debug::Err("Failed to create the SMAA buffer!");
		return false;
	}

	m_edges_RT = RenderTarget::Create(device.Get(), width, height, DXGI_FORMAT_R8G8B8A8_UNORM, true);
	if (m_edges_RT == nullptr)
	{
		Debug::Err("Failed to create the edge detection render target!");
		return false;
	}

	m_blend_RT = RenderTarget::Create(device.Get(), width, height, DXGI_FORMAT_R8G8B8A8_UNORM, true);
	if (m_blend_RT == nullptr)
	{
		Debug::Err("Failed to create the blend weights render target!");
		return false;
	}

	m_area_texture = ResourcesManager::GetTexture("data\\textures\\SMAA_AreaTex.dds");
	m_search_texture = ResourcesManager::GetTexture("data\\textures\\SMAA_SearchTex.dds");

	if(m_area_texture == nullptr || m_search_texture == nullptr)
	{
		Debug::Err("SMAA textures are missing!");
		return false;
	}
	return true;
}


void Redline::SMAARenderer::Shutdown()
{
	if (m_blend_RT)
	{
		delete m_blend_RT;
		m_blend_RT = nullptr;
	}

	if (m_smaa_blend_shader)
	{
		delete m_smaa_blend_shader;
		m_smaa_blend_shader = nullptr;
	}

	if (m_edges_RT)
	{
		delete m_edges_RT;
		m_edges_RT = nullptr;
	}

	if(m_smaa_edge_shader)
	{
		delete m_smaa_edge_shader;
		m_smaa_edge_shader = nullptr;
	}

	if(m_smaa_neighbor_blend_shader)
	{
		delete m_smaa_neighbor_blend_shader;
		m_smaa_neighbor_blend_shader = nullptr;
	}

	if (m_smaa_buffer)
	{
		m_smaa_buffer->Release();
		m_smaa_buffer = nullptr;
	}
}


void Redline::SMAARenderer::Render(Microsoft::WRL::ComPtr<ID3D11DeviceContext> device_context, RenderTarget* input_RT, RenderTarget* output_RT) const
{
	device_context->OMSetRenderTargets(1, m_edges_RT->GetRenderTargetView().GetAddressOf(), nullptr);

	const float clear_color[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	device_context->ClearRenderTargetView(m_edges_RT->GetRenderTargetView().Get(), clear_color);

	D3D11_VIEWPORT viewport;
	viewport.Width = m_width;
	viewport.Height = m_height;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	device_context->RSSetViewports(1, &viewport);

	device_context->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	device_context->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	device_context->VSSetShader(m_smaa_edge_shader->GetVertexShader(), nullptr, 0);
	device_context->PSSetShader(m_smaa_edge_shader->GetPixelShader(), nullptr, 0);

	device_context->VSSetConstantBuffers(0, 1, &m_smaa_buffer);
	device_context->PSSetConstantBuffers(0, 1, &m_smaa_buffer);

	device_context->PSSetShaderResources(0, 1, input_RT->GetShaderResourceView().GetAddressOf());

	device_context->Draw(3, 0);


	device_context->OMSetRenderTargets(1, m_blend_RT->GetRenderTargetView().GetAddressOf(), nullptr);

	device_context->ClearRenderTargetView(m_blend_RT->GetRenderTargetView().Get(), clear_color);

	device_context->VSSetShader(m_smaa_blend_shader->GetVertexShader(), nullptr, 0);
	device_context->PSSetShader(m_smaa_blend_shader->GetPixelShader(), nullptr, 0);

	ID3D11ShaderResourceView* srv[] = { m_edges_RT->GetShaderResourceView().Get(), m_area_texture->GetTexture().Get(), m_search_texture->GetTexture().Get()};
	device_context->PSSetShaderResources(0, 3, srv);

	device_context->Draw(3, 0);



	device_context->OMSetRenderTargets(1, output_RT->GetRenderTargetView().GetAddressOf(), nullptr);

	device_context->ClearRenderTargetView(output_RT->GetRenderTargetView().Get(), clear_color);

	device_context->VSSetShader(m_smaa_neighbor_blend_shader->GetVertexShader(), nullptr, 0);
	device_context->PSSetShader(m_smaa_neighbor_blend_shader->GetPixelShader(), nullptr, 0);

	ID3D11ShaderResourceView* srv_nblend[] = { input_RT->GetShaderResourceView().Get(), m_blend_RT->GetShaderResourceView().Get() };
	device_context->PSSetShaderResources(0, 2, srv_nblend);

	device_context->Draw(3, 0);
}
