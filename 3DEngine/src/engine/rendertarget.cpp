#include "rendertarget.h"
#include "debug.h"


Redline::RenderTarget::RenderTarget() :
	m_texture(nullptr),
	m_render_target_view(nullptr),
	m_shader_resource_view(nullptr),
	m_num_faces(0),
	m_mip_levels(0)
{
}


Redline::RenderTarget::~RenderTarget()
{
	m_shader_resource_view.Reset();

	for (int i = 0; i < m_num_faces; ++i)
	{
		for (int j = 0; j < m_mip_levels; ++j)
		{
			m_render_target_view[i][j].Reset();
		}
	}
	m_texture.Reset();
}

Redline::RenderTarget* Redline::RenderTarget::Create(ID3D11Device* device, unsigned width, unsigned height, DXGI_FORMAT format, bool allowMips, int numFaces)
{
	RenderTarget* render_target = new RenderTarget();
	if(render_target->Initialize(device, width,  height, format, allowMips, numFaces))
	{
		return render_target;
	}
	return nullptr;
}


UINT GetNumMipLevels(UINT width, UINT height)
{
	UINT numLevels = 1;
	while (width > 1 && height > 1)
	{
		width = max(width / 2, 1);
		height = max(height / 2, 1);
		++numLevels;
	}

	return numLevels;
}


bool Redline::RenderTarget::Initialize(ID3D11Device* device, const unsigned int width, const unsigned int height, const DXGI_FORMAT format, const bool allowMips, const int numFaces)
{
	m_mip_levels = allowMips ? GetNumMipLevels(width, height) : 1;
	m_num_faces = numFaces;

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));
	textureDesc.Width = width;
	textureDesc.Height = height;
	textureDesc.MipLevels = m_mip_levels;
	textureDesc.ArraySize = m_num_faces;
	textureDesc.Format = format;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = allowMips ? D3D11_RESOURCE_MISC_GENERATE_MIPS : 0;
	if (m_num_faces == 6) textureDesc.MiscFlags |= D3D11_RESOURCE_MISC_TEXTURECUBE;

	if (FAILED(device->CreateTexture2D(&textureDesc, NULL, m_texture.ReleaseAndGetAddressOf())))
	{
		Debug::Err("Failed to create render target texture!");
		return false;
	}

	D3D11_RENDER_TARGET_VIEW_DESC viewDesc;
	viewDesc.Format = textureDesc.Format;
	if (m_num_faces == 1)
	{
		viewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		viewDesc.Texture2D.MipSlice = 0;
	}
	else
	{
		viewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
		viewDesc.Texture2DArray.MipSlice = 0;
		viewDesc.Texture2DArray.ArraySize = 1;
	}

	m_render_target_view = new Microsoft::WRL::ComPtr<ID3D11RenderTargetView>*[m_num_faces];
	for (int i = 0; i < m_num_faces; ++i)
	{
		m_render_target_view[i] = new Microsoft::WRL::ComPtr<ID3D11RenderTargetView>[m_mip_levels];
		viewDesc.Texture2DArray.FirstArraySlice = i;
		for (int j = 0; j < m_mip_levels; ++j)
		{
			viewDesc.Texture2DArray.MipSlice = j;
			if (FAILED(device->CreateRenderTargetView(m_texture.Get(), &viewDesc, m_render_target_view[i][j].ReleaseAndGetAddressOf())))
			{
				Debug::Err("Failed to create render target view!");
				return false;
			}
		}
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = textureDesc.Format;

	if (m_num_faces == 6)
	{
		shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
		shaderResourceViewDesc.TextureCube.MipLevels = m_mip_levels;
		shaderResourceViewDesc.TextureCube.MostDetailedMip = 0;
	}
	else if (m_num_faces == 1)
	{
		shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		shaderResourceViewDesc.Texture2D.MipLevels = m_mip_levels;
		shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	}
	else
	{
		shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
		shaderResourceViewDesc.Texture2DArray.MipLevels = m_mip_levels;
		shaderResourceViewDesc.Texture2DArray.MostDetailedMip = 0;
	}

	if (FAILED(device->CreateShaderResourceView(m_texture.Get(), &shaderResourceViewDesc, m_shader_resource_view.ReleaseAndGetAddressOf())))
	{
		Debug::Err("Failed to create render target shader resource view!");
		return false;
	}
	return true;
}


void Redline::RenderTarget::GenerateMips(ID3D11DeviceContext* deviceContext) const
{
	deviceContext->GenerateMips(m_shader_resource_view.Get());
}


void Redline::RenderTarget::Clear(ID3D11DeviceContext* deviceContext, float r, float g, float b, float a, int faceIndex, int mipLevel) const
{
	const float clearColor[] = { r, g, b, a };
	deviceContext->ClearRenderTargetView(m_render_target_view[min(max(faceIndex, 0), m_num_faces - 1)][min(max(mipLevel, 0), m_mip_levels - 1)].Get(), clearColor);
}
