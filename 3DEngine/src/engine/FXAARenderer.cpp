﻿#include "FXAARenderer.h"
#include "debug.h"
#include "ResourcesManager.h"


Redline::FXAARenderer::FXAARenderer():
	m_fxaa_buffer(nullptr),
	m_fxaa_shader(nullptr), 
	m_width(0), 
	m_height(0)
{
}


Redline::FXAARenderer::~FXAARenderer()
{

}


bool Redline::FXAARenderer::Initialize(const Microsoft::WRL::ComPtr<ID3D11Device>& device, const int width, const int height)
{
	m_fxaa_shader = ResourcesManager::GetShader("data\\shaders\\fxaa.fx");
	if (m_fxaa_shader == nullptr)
	{
		Debug::Err("FXAA shader is missing!");
		return false;
	}

	m_width = static_cast<float>(width);
	m_height = static_cast<float>(height);

	// Create FXAA buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(FXAABuffer);

	FXAABuffer buffer = {
		1.0f / m_width,
		1.0f / m_height,
		0.0f,
		0.0f
	};

	D3D11_SUBRESOURCE_DATA subresource_data;
	subresource_data.pSysMem = &buffer;

	if (FAILED(device->CreateBuffer(&bd, &subresource_data, &m_fxaa_buffer)))
	{
		Debug::Err("Failed to create the FXAA buffer!");
		return false;
	}
	return true;
}


void Redline::FXAARenderer::Shutdown()
{
	m_fxaa_shader = nullptr;

	if (m_fxaa_buffer)
	{
		m_fxaa_buffer->Release();
		m_fxaa_buffer = nullptr;
	}
}


void Redline::FXAARenderer::Render(Microsoft::WRL::ComPtr<ID3D11DeviceContext> device_context, RenderTarget* input_RT, RenderTarget* output_RT) const
{
	device_context->OMSetRenderTargets(1, output_RT->GetRenderTargetView().GetAddressOf(), nullptr);

	D3D11_VIEWPORT viewport;
	viewport.Width = m_width;
	viewport.Height = m_height;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	device_context->RSSetViewports(1, &viewport);

	device_context->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	device_context->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	device_context->VSSetShader(m_fxaa_shader->GetVertexShader(), nullptr, 0);
	device_context->PSSetShader(m_fxaa_shader->GetPixelShader(), nullptr, 0);

	device_context->PSSetConstantBuffers(0, 1, &m_fxaa_buffer);

	device_context->PSSetShaderResources(0, 1, input_RT->GetShaderResourceView().GetAddressOf());

	device_context->Draw(3, 0);
}
