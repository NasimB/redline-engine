﻿#pragma once

namespace Redline
{
	class GameObject;
	class Light;

	struct MapObject
	{
		char name[128];
		char mesh_name[128];
		DirectX::SimpleMath::Vector3 position;
		DirectX::SimpleMath::Quaternion rotation;
		DirectX::SimpleMath::Vector3 scale;
	};

	struct MapObjectV2
	{
		unsigned int id;
		char name[128];
		DirectX::SimpleMath::Vector3 position;
		DirectX::SimpleMath::Quaternion rotation;
		DirectX::SimpleMath::Vector3 scale;
		unsigned int parent_id;
	};

	class SceneManager
	{
	public:
		SceneManager();
		~SceneManager();

		bool LoadScene(std::string scene_name);
		bool LoadSceneV2(std::string scene_name);
		void UnloadScene();

		bool IsSceneLoaded() const { return m_is_scene_loaded; }

		size_t AddGameObject(GameObject* go);
		void RemoveGameObject(GameObject* go);

		GameObject* GetGameObject(const unsigned int id) { return m_game_objects[id]; }
		std::vector<GameObject*>& GetGameObjects();

#ifdef REDLINE_EDITOR
		bool SaveScene(std::string scene_name);
#endif

	private:
		bool m_is_scene_loaded;

		bool m_is_vector_dirty;

		std::map<unsigned int, GameObject*> m_game_objects;
		std::vector<GameObject*> m_game_objects_vector;
	};
}
