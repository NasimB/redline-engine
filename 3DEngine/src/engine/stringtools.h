#pragma once

namespace Redline
{
	class StringTools
	{
	public:
		static bool HasEnding(std::string const& fullString, std::string const& ending);
		static bool StartsWith(std::string const& fullString, std::string const& ending);
		static std::vector<std::string> Split(const std::string& str, char delimiter);
		static std::string ToLower(const std::string& str);
		static bool StringToBool(const std::string& str);
		static int IndexOf(const std::vector<std::string>& input, const std::string& e, const int low, const int high, const bool doSorting = true);
	};
}
