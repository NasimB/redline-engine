#include "input.h"
#include "debug.h"

Redline::Input* Redline::Input::m_input = nullptr;


Redline::Input::Input()
	: m_directInput(nullptr),
	m_keyboard(nullptr),
	m_mouse(nullptr),
	m_wheel(nullptr),
	m_previousKeyboardState(),
	m_keyboardState(),
	m_mouseState(),
	m_wheelState(),
	m_wheelPreviousState(),
	m_force_feedback(),
	m_force_feedback_axis(0),
	m_screenWidth(0),
	m_screenHeight(0),
	m_mouseX(0),
	m_mouseY(0),
	m_isKeyboardAquired(false),
	m_isMouseAquired(false),
	m_wheelConnected(false),
	m_isWheelAquired(false)
{
	m_input = this;
}


Redline::Input::~Input()
{
	// Release the wheel.
	if (m_wheel)
	{
		m_wheel->Unacquire();
		m_wheel->Release();
		m_wheel = nullptr;
	}

	// Release the mouse.
	if (m_mouse)
	{
		m_mouse->Unacquire();
		m_mouse->Release();
		m_mouse = nullptr;
	}

	// Release the keyboard.
	if (m_keyboard)
	{
		m_keyboard->Unacquire();
		m_keyboard->Release();
		m_keyboard = nullptr;
	}

	// Release the main interface to direct input.
	if (m_directInput)
	{
		m_directInput->Release();
		m_directInput = nullptr;
	}
}


bool Redline::Input::InitializeWheel(HWND hwnd)
{
	m_wheel->SetDataFormat(&c_dfDIJoystick2);

	if (FAILED(m_wheel->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE)))
	{
		return false;
	}

	DIPROPDWORD dipdw;
	dipdw.diph.dwSize = sizeof(DIPROPDWORD);
	dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	dipdw.diph.dwObj = 0;
	dipdw.diph.dwHow = DIPH_DEVICE;
	dipdw.dwData = FALSE;

	if (FAILED(m_wheel->SetProperty(DIPROP_AUTOCENTER, &dipdw.diph)))
	{
		return false;
	}

	m_wheel->EnumObjects(EnumJoystickObjectsCallback, this, DIDFT_ALL);

	// This simple sample only supports one or two axis joysticks
	if (m_force_feedback_axis > 2)
		m_force_feedback_axis = 2;

	// This application needs only one effect: Applying raw forces.
	DWORD rgdwAxes[2] = { DIJOFS_X, DIJOFS_Y };
	LONG rglDirection[2] = { 0, 0 };
	DICONSTANTFORCE cf = { 0 };

	DIEFFECT eff;
	ZeroMemory(&eff, sizeof(eff));
	eff.dwSize = sizeof(DIEFFECT);
	eff.dwFlags = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
	eff.dwDuration = INFINITE;
	eff.dwSamplePeriod = 0;
	eff.dwGain = DI_FFNOMINALMAX;
	eff.dwTriggerButton = DIEB_NOTRIGGER;
	eff.dwTriggerRepeatInterval = 0;
	eff.cAxes = m_force_feedback_axis;
	eff.rgdwAxes = rgdwAxes;
	eff.rglDirection = rglDirection;
	eff.lpEnvelope = 0;
	eff.cbTypeSpecificParams = sizeof(DICONSTANTFORCE);
	eff.lpvTypeSpecificParams = &cf;
	eff.dwStartDelay = 0;

	// Create the prepared effect
	if (FAILED(m_wheel->CreateEffect(GUID_ConstantForce, &eff, &m_force_feedback, nullptr)))
		return false;

	return true;
}


bool Redline::Input::Initialize(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight)
{
	// Store the screen size which will be used for positioning the mouse cursor.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	POINT mousePos;

	GetCursorPos(&mousePos);

	// Initialize the location of the mouse on the screen.
	m_mouseX = mousePos.x;
	m_mouseY = mousePos.y;

	// Initialize the main direct input interface.
	if (FAILED(DirectInput8Create(hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8, reinterpret_cast<void**>(&m_directInput), NULL)))
	{
		Debug::Err("Failed to create direct input instance", false);
		return false;
	}

	// Initialize the direct input interface for the keyboard.
	if (FAILED(m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyboard, NULL)))
	{
		Debug::Err("Failed to create direct input keyboard device", false);
		return false;
	}

	// Look for a force feedback device we can use
	if (FAILED(m_directInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumFFDevicesCallback, nullptr, DIEDFL_ATTACHEDONLY | DIEDFL_FORCEFEEDBACK)))
	{
		Debug::Err("Failed to enum DirectInput devices", false);
		return false;
	}

	if (m_wheel != nullptr)
		m_wheelConnected = InitializeWheel(hwnd);

	// Set the data format. In this case since it is a keyboard we can use the predefined data format.
	if (FAILED(m_keyboard->SetDataFormat(&c_dfDIKeyboard)))
	{
		Debug::Err("Failed set keyboard data format", false);
		return false;
	}

	// Set the cooperative level of the keyboard to not share with other programs.
	if (FAILED(m_keyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
	{
		Debug::Err("Failed set keyboard cooperative level", false);
		return false;
	}

	// Now acquire the keyboard.
	if (!FAILED(m_keyboard->Acquire()))
	{
		m_isKeyboardAquired = true;
	}

	// Initialize the direct input interface for the mouse.
	if (FAILED(m_directInput->CreateDevice(GUID_SysMouse, &m_mouse, NULL)))
	{
		Debug::Err("Failed to create direct input mouse device", false);
		return false;
	}

	// Set the data format for the mouse using the pre-defined mouse data format.
	if (FAILED(m_mouse->SetDataFormat(&c_dfDIMouse)))
	{
		Debug::Err("Failed set mouse data format", false);
		return false;
	}

	// Set the cooperative level of the mouse to share with other programs.
	if (FAILED(m_mouse->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
	{
		Debug::Err("Failed set mouse cooperative level", false);
		return false;
	}

	// Acquire the mouse.
	if (!FAILED(m_mouse->Acquire()))
	{
		m_isMouseAquired = true;
	}

	return true;
}


bool Redline::Input::Update()
{
	// Read the current state of the keyboard.
	if (!ReadKeyboard())
		return false;

	// Read the current state of the mouse.
	if (!ReadMouse())
		return false;

	// Read the current state of the wheel.
	if (!ReadWheel())
		return false;

	// Process the changes in the mouse and keyboard.
	ProcessInput();

	return true;
}


void Redline::Input::ApplyForceFeedback(DirectX::SimpleMath::Vector2 force)
{

	float intensity = force.Length();
	if (intensity > DI_FFNOMINALMAX)
	{
		force.Normalize();
		force *= DI_FFNOMINALMAX;
	}

	// Modifying an effect is basically the same as creating a new one, except
	// you need only specify the parameters you are modifying
	LONG rglDirection[2] = { 0, 0 };

	DICONSTANTFORCE cf;

	if (m_input->m_force_feedback_axis == 1)
	{
		// If only one force feedback axis, then apply only one direction and 
		// keep the direction at zero
		cf.lMagnitude = static_cast<LONG>(force.x);
		rglDirection[0] = 0;
	}
	else
	{
		// If two force feedback axis, then apply magnitude from both directions 
		rglDirection[0] = static_cast<LONG>(force.x);
		rglDirection[1] = static_cast<LONG>(force.y);
		cf.lMagnitude = static_cast<LONG>(intensity);
	}

	DIEFFECT eff;
	ZeroMemory(&eff, sizeof(eff));
	eff.dwSize = sizeof(DIEFFECT);
	eff.dwFlags = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
	eff.cAxes = m_input->m_force_feedback_axis;
	eff.rglDirection = rglDirection;
	eff.lpEnvelope = 0;
	eff.cbTypeSpecificParams = sizeof(DICONSTANTFORCE);
	eff.lpvTypeSpecificParams = &cf;
	eff.dwStartDelay = 0;

	// Now set the new parameters and start the effect immediately.
	m_input->m_force_feedback->SetParameters(&eff, DIEP_DIRECTION | DIEP_TYPESPECIFICPARAMS | DIEP_START);
}


bool Redline::Input::ReadKeyboard()
{
	memcpy(m_previousKeyboardState, m_keyboardState, sizeof(m_previousKeyboardState));

	if (!m_isKeyboardAquired)
	{
		if (FAILED(m_keyboard->Acquire()))
		{
			return true;
		}

		m_isKeyboardAquired = true;
	}

	if (FAILED(m_keyboard->GetDeviceState(sizeof(m_keyboardState), static_cast<LPVOID>(&m_keyboardState))))
	{
		m_isKeyboardAquired = false;
	}
	return true;
}


bool Redline::Input::ReadMouse()
{
	if (!m_isMouseAquired)
	{
		if (FAILED(m_mouse->Acquire()))
		{
			return true;
		}

		m_isMouseAquired = true;
	}

	if (FAILED(m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), &m_mouseState)))
	{
		m_isMouseAquired = false;
	}
	return true;
}


bool Redline::Input::ReadWheel()
{
	if (!m_wheelConnected)
		return true;

	memcpy(&m_wheelPreviousState, &m_wheelState, sizeof(m_wheelPreviousState));

	if (FAILED(m_wheel->Poll()))
	{
		m_isWheelAquired = false;
	}

	if (!m_isWheelAquired)
	{
		if (FAILED(m_wheel->Acquire()))
		{
			return true;
		}

		m_isWheelAquired = true;
	}

	if (FAILED(m_wheel->GetDeviceState(sizeof(DIJOYSTATE2), &m_wheelState)))
	{
		m_isWheelAquired = false;
	}
	return true;
}


void Redline::Input::ProcessInput()
{
	// Update the location of the mouse cursor based on the change of the mouse location during the frame
	m_mouseX += m_mouseState.lX;
	m_mouseY += m_mouseState.lY;

	mouseDelta.x = static_cast<float>(m_mouseState.lX);
	mouseDelta.y = static_cast<float>(m_mouseState.lY);

	// Ensure the mouse location doesn't exceed the screen width or height
	m_mouseX = min(max(m_mouseX, 0), m_screenWidth);
	m_mouseY = min(max(m_mouseY, 0), m_screenHeight);
}


BOOL CALLBACK Redline::Input::EnumJoystickObjectsCallback(const DIDEVICEOBJECTINSTANCE* pdidInstance, VOID* pvRef)
{
	Input* context = static_cast<Input*>(pvRef);
	//static int nSliderCount = 0;  // Number of returned slider controls
	//static int nPOVCount = 0;     // Number of returned POV controls

	// For axes that are returned, set the DIPROP_RANGE property for the
	// enumerated axis in order to scale min/max values.
	if (pdidInstance->dwType & DIDFT_AXIS)
	{
		if ((pdidInstance->dwFlags & DIDOI_FFACTUATOR) != 0)
			context->m_force_feedback_axis++;

		DIPROPRANGE diprg;
		diprg.diph.dwSize = sizeof(DIPROPRANGE);
		diprg.diph.dwHeaderSize = sizeof(DIPROPHEADER);
		diprg.diph.dwHow = DIPH_BYID;
		diprg.diph.dwObj = pdidInstance->dwType; // Specify the enumerated axis
		diprg.lMin = -1000;
		diprg.lMax = +1000;

		// Set the range for the axis
		if (FAILED(context->m_wheel->SetProperty(DIPROP_RANGE, &diprg.diph)))
			return DIENUM_STOP;
	}
	return DIENUM_CONTINUE;
}


BOOL Redline::Input::EnumFFDevicesCallback(const DIDEVICEINSTANCE* pInst, void* pContext)
{
	LPDIRECTINPUTDEVICE8 pDevice;
	HRESULT hr;

	// Obtain an interface to the enumerated force feedback device.
	hr = m_input->m_directInput->CreateDevice(pInst->guidInstance, &pDevice, nullptr);

	// If it failed, then we can't use this device for some
	// bizarre reason.  (Maybe the user unplugged it while we
	// were in the middle of enumerating it.)  So continue enumerating
	if (FAILED(hr))
		return DIENUM_CONTINUE;

	// We successfully created an IDirectInputDevice8.  So stop looking 
	// for another one.
	m_input->m_wheel = pDevice;

	return DIENUM_STOP;
}


bool Redline::Input::IsEscapePressed()
{
	// Do a bitwise and on the keyboard state to check if the escape key is currently being pressed
	return m_keyboardState[DIK_ESCAPE] & 0x80;
}


POINT Redline::Input::GetMouseLocation(bool absolute)
{
	POINT point;
	if (absolute)
	{
		GetCursorPos(&point);
	}
	else
	{
		point.x = m_input->m_mouseX;
		point.y = m_input->m_mouseY;
	}
	return point;
}


bool Redline::Input::GetMouseButtonPressed(int keyCode)
{
	return m_input->m_mouseState.rgbButtons[keyCode] & 0x80;
}


bool Redline::Input::GetWheelButtonPressed(int keyCode)
{
	return m_input->m_wheelState.rgbButtons[keyCode] & 0x80;
}


bool Redline::Input::GetWheelButtonDown(int keyCode)
{
	return !(m_input->m_wheelPreviousState.rgbButtons[keyCode] & 0x80) && m_input->m_wheelState.rgbButtons[keyCode] & 0x80;
}


bool Redline::Input::GetWheelButtonUp(int keyCode)
{
	return m_input->m_wheelPreviousState.rgbButtons[keyCode] & 0x80 && !(m_input->m_wheelState.rgbButtons[keyCode] & 0x80);
}


bool Redline::Input::GetButtonPressed(int keyCode)
{
	return m_input->m_keyboardState[keyCode] & 0x80;
}


bool Redline::Input::GetButtonDown(int keyCode)
{
	return !(m_input->m_previousKeyboardState[keyCode] & 0x80) && m_input->m_keyboardState[keyCode] & 0x80;
}


bool Redline::Input::GetButtonUp(int keyCode)
{
	return m_input->m_previousKeyboardState[keyCode] & 0x80 && !(m_input->m_keyboardState[keyCode] & 0x80);
}
