#include "config.h"
#include "stringtools.h"
#include <fstream>
#include <algorithm>

Redline::Config::Config()
{
}


Redline::Config::~Config()
{
}


bool Redline::Config::Initialize()
{
	std::fstream myfile("engineconfig.ini");

	if (myfile.is_open())
	{
		for (std::string s; getline(myfile, s);)
		{
			if (s.size())
			{
				if (StringTools::StartsWith(s, "[") || StringTools::StartsWith(s, ";"))
					continue;

				const std::string name = StringTools::ToLower(StringTools::Split(s, '=')[0]);
				const std::string value = StringTools::ToLower(StringTools::Split(s, '=')[1]);

				if (name == "fullscreen")
				{
					fullscreen = StringTools::StringToBool(value);
				}
				if (name == "multithreading")
				{
					multithreading = StringTools::StringToBool(value);
				}
				else if (name == "vsync")
				{
					vsync = StringTools::StringToBool(value);
				}
				else if (name == "width")
				{
					width = max(stoi(value), 640);
				}
				else if (name == "height")
				{
					height = max(stoi(value), 480);
				}
				else if (name == "shadows_res")
				{
					shadow_res = max(stoi(value), 0);
				}
				else if (name == "shadow_cascades")
				{
					shadow_cascades = max(stoi(value), 1);
				}
				else if (name == "cubemap_res")
				{
					cubemap_res = max(stoi(value), 0);
				}
				else if (name == "tex_filtering")
				{
					texture_filtering = min(max(stoi(value), 0), 16);
				}
				else if (name == "min_tex_lod")
				{
					min_tex_lod = max(stoi(value), 0);
				}
				else if (name == "max_tex_lod")
				{
					max_tex_lod = max(stoi(value), 0);
				}
				else if(name == "ssao_technique")
				{
					ssao_technique = min(max(stoi(value), 0), 3);
				}
				else if (name == "aa_technique")
				{
					aa_technique = min(max(stoi(value), 0), 2);
				}
				else if (name == "render_scale")
				{
					render_scale = min(max(stof(value), 0.1f), 8.0f);
				}
			}
		}
		myfile.close();
	}
	return true;
}
