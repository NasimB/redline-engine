#pragma once

#include "mesh.h"
#include "Component.h"

namespace Redline
{
	class MeshRenderer : public Component
	{
	public:
		MeshRenderer(GameObject* game_object);
		~MeshRenderer();

		bool IsActive() const { return m_active; }
		void SetActive(const bool value) { m_active = value; }

		bool IsCastingShadow() const { return m_casting_shadows; }
		void SetCastingShadow(const bool value) { m_casting_shadows = value; }

		std::vector<Material>& GetMaterials() { return m_materials; }

		Mesh* GetMesh() const { return m_mesh; }
		void SetMesh(std::string meshName);
		void SetMesh(Mesh* mesh) { m_mesh = mesh; }

		static ComponentType GetComponentType() { return Component::MeshRenderer; }

		void Serialize(std::ofstream& stream) override;
		void Deserialize(std::ifstream& stream) override;

#ifdef REDLINE_EDITOR
		void ShowInspector() override;
#endif
	private:
		Mesh* m_mesh;
		std::vector<Material> m_materials;

		bool m_casting_shadows;
		bool m_active;
	};
}
