﻿#include "Rigidbody.h"

#include "debug.h"
#include "mesh.h"
#include "gameobject.h"
#include "math.h"
#include "engine.h"

using namespace DirectX::SimpleMath;
using namespace std;

Redline::Rigidbody::Rigidbody(GameObject* baseGo) :
	Component(baseGo, Component::Rigidbody),
	m_rigidbody(nullptr),
	m_motion_state(nullptr),
	m_game_object(baseGo),
	m_enabled(false),
	m_collider_type(None),
	m_mass(0)
{
}


Redline::Rigidbody::~Rigidbody()
{

}


Redline::Rigidbody& Redline::Rigidbody::operator=(const Rigidbody& other)
{
	if (this != &other) // protect against invalid self-assignment
	{
		m_rigidbody = other.m_rigidbody;
		m_motion_state = other.m_motion_state;
		m_game_object = other.m_game_object;
		m_center = other.m_center;
		m_enabled = other.m_enabled;
		m_collider_type = other.m_collider_type;
		m_mass = other.m_mass;
		m_parameters[0] = other.m_parameters[0];
		m_parameters[1] = other.m_parameters[1];
		m_parameters[2] = other.m_parameters[2];
		m_parameters[3] = other.m_parameters[3];
	}
	// by convention, always return *this
	return *this;
}


bool Redline::Rigidbody::CreateBox(const float mass, const float size_X, const float size_Y, const float size_Z)
{
	if (m_enabled)
	{
		Debug::Warn("Rigidbody::CreateBox: Rigidbody is already initialized!");
		return false;
	}

	btTransform bttransform;
	bttransform.setIdentity();

	btVector3 localInertia(1, 1, 1);

	m_mass = Math::Max(mass, 0.0f);


	Vector3 scale = m_game_object->GetTransform()->GetScale();
	btBoxShape* shape = new btBoxShape(btVector3(size_X * 0.5f * scale.x, size_Y * 0.5f * scale.y, size_Z * 0.5f * scale.z));

	if (m_mass > EPSILON)
		shape->calculateLocalInertia(mass, localInertia);

	m_motion_state = new btDefaultMotionState(bttransform);

	btRigidBody::btRigidBodyConstructionInfo myBoxRigidBodyConstructionInfo(m_mass, m_motion_state, shape, localInertia);

	m_rigidbody = new btRigidBody(myBoxRigidBodyConstructionInfo);

	Engine::GetPhysics().AddRigidBody(m_rigidbody);
	m_collider_type = Box;

	m_parameters[0] = m_mass;
	m_parameters[1] = size_X;
	m_parameters[2] = size_Y;
	m_parameters[3] = size_Z;

	m_enabled = true;
	m_game_object->GetTransform()->SetRigidbody(this);

	m_rigidbody->setCollisionFlags(m_rigidbody->getCollisionFlags() | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);   // Disable debug drawing
	return true;
}


bool Redline::Rigidbody::CreateSphere(const float mass, const float radius)
{
	if (m_enabled)
	{
		Debug::Warn("Rigidbody::CreateSphere: Rigidbody is already initialized!");
		return false;
	}

	btTransform bttransform;
	bttransform.setIdentity();

	btVector3 localInertia(1, 1, 1);

	m_mass = Math::Max(mass, 0.0f);

	Vector3 scale = m_game_object->GetTransform()->GetScale();
	float maxScale = Math::Max(Math::Max(scale.x, scale.y), scale.z);

	btSphereShape* shape = new btSphereShape(radius * maxScale);

	if (m_mass > EPSILON)
		shape->calculateLocalInertia(mass, localInertia);

	m_motion_state = new btDefaultMotionState(bttransform);

	btRigidBody::btRigidBodyConstructionInfo myBoxRigidBodyConstructionInfo(m_mass, m_motion_state, shape, localInertia);

	m_rigidbody = new btRigidBody(myBoxRigidBodyConstructionInfo);

	Engine::GetPhysics().AddRigidBody(m_rigidbody);
	m_collider_type = Sphere;

	m_parameters[0] = m_mass;
	m_parameters[1] = radius;

	m_enabled = true;
	m_game_object->GetTransform()->SetRigidbody(this);

	m_rigidbody->setCollisionFlags(m_rigidbody->getCollisionFlags() | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);   // Disable debug drawing
	return true;
}


bool Redline::Rigidbody::CreateCylinder(const float mass, const float radius, const float height, const int axis)
{
	if (m_enabled)
	{
		Debug::Warn("Rigidbody::CreateCylinder: Rigidbody is already initialized!");
		return false;
	}

	btTransform bttransform;
	bttransform.setIdentity();

	btVector3 localInertia(1, 1, 1);

	m_mass = Math::Max(mass, 0.0f);

	Vector3 scale = m_game_object->GetTransform()->GetScale();

	btCylinderShape* shape;
	switch (axis)
	{
	case 0:
		shape = new btCylinderShapeX(btVector3(height * 0.5f * scale.x, radius * scale.y, radius * scale.z));
		break;
	default:
	case 1:
		shape = new btCylinderShape(btVector3(radius * scale.x, height * 0.5f * scale.y, radius * scale.z));
		break;
	case 2:
		shape = new btCylinderShapeZ(btVector3(radius * scale.x, radius * scale.y, height * 0.5f * scale.z));
		break;
	}

	if (m_mass > EPSILON)
		shape->calculateLocalInertia(mass, localInertia);

	m_motion_state = new btDefaultMotionState(bttransform);

	btRigidBody::btRigidBodyConstructionInfo myBoxRigidBodyConstructionInfo(m_mass, m_motion_state, shape, localInertia);

	m_rigidbody = new btRigidBody(myBoxRigidBodyConstructionInfo);

	Engine::GetPhysics().AddRigidBody(m_rigidbody);
	m_collider_type = Cylinder;

	m_parameters[0] = m_mass;
	m_parameters[1] = radius;
	m_parameters[2] = height;
	m_parameters[3] = static_cast<float>(axis);

	m_enabled = true;
	m_game_object->GetTransform()->SetRigidbody(this);

	m_rigidbody->setCollisionFlags(m_rigidbody->getCollisionFlags() | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);   // Disable debug drawing
	return true;
}


bool Redline::Rigidbody::CreateComplexMesh()
{
	if (m_enabled)
	{
		Debug::Warn("Rigidbody::CreateComplexMesh: Rigidbody is already initialized!");
		return false;
	}

	btTransform bttransform;
	bttransform.setIdentity();

	Vector3 scale = m_game_object->GetTransform()->GetScale();

	btTriangleMesh* bullet_mesh = new btTriangleMesh();

	Redline::MeshRenderer* mesh_renderer = m_game_object->GetComponent<Redline::MeshRenderer>(MeshRenderer);

	if (mesh_renderer == nullptr)
	{
		Debug::Err("Rigidbody::CreateComplexMesh: Gameobject has no mesh renderer!");
		return false;
	}

	Mesh* mesh = mesh_renderer->GetMesh();

	if (mesh == nullptr)
	{
		Debug::Err("Rigidbody::CreateComplexMesh: Gameobject has no mesh!");
		return false;
	}

	vector<SubMesh>& submeshes = mesh->GetSubMeshes();
	size_t submeshesCount = submeshes.size();
	for (size_t i = 0; i < submeshesCount; ++i)
	{
		vector<Vertex>& vertices = submeshes[i].vBuffer->GetVertices();
		vector<unsigned int>& indices = submeshes[i].iBuffer->GetIndices();

		size_t indicesCount = indices.size();
		for (size_t j = 0; j < indicesCount; j += 3)
		{
			Vector3 vertex0 = vertices[indices[j]].position * scale;
			Vector3 vertex1 = vertices[indices[j + 1]].position * scale;
			Vector3 vertex2 = vertices[indices[j + 2]].position * scale;
			bullet_mesh->addTriangle(btVector3(vertex0.x, vertex0.y, vertex0.z), btVector3(vertex1.x, vertex1.y, vertex1.z), btVector3(vertex2.x, vertex2.y, vertex2.z));
		}
	}


	btBvhTriangleMeshShape* shape = new btBvhTriangleMeshShape(bullet_mesh, false, true);

	m_motion_state = new btDefaultMotionState(bttransform);

	btRigidBody::btRigidBodyConstructionInfo myBoxRigidBodyConstructionInfo(m_mass, m_motion_state, shape, btVector3(1, 1, 1));

	m_rigidbody = new btRigidBody(myBoxRigidBodyConstructionInfo);

	Engine::GetPhysics().AddRigidBody(m_rigidbody);
	m_collider_type = ComplexMesh;

	m_enabled = true;
	m_game_object->GetTransform()->SetRigidbody(this);

	m_rigidbody->setCollisionFlags(m_rigidbody->getCollisionFlags() | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);   // Disable debug drawing
	return true;
}


bool Redline::Rigidbody::CreateCustom(const float mass, btCollisionShape* shape)
{
	if (m_enabled)
	{
		Debug::Warn("Rigidbody::CreateCustom: Rigidbody is already initialized!");
		return false;
	}

	btTransform bttransform;
	bttransform.setIdentity();

	btVector3 localInertia(1, 1, 1);

	m_mass = Math::Max(mass, 0.0f);

	if (m_mass > EPSILON)
		shape->calculateLocalInertia(mass, localInertia);

	m_motion_state = new btDefaultMotionState(bttransform);

	btRigidBody::btRigidBodyConstructionInfo myBoxRigidBodyConstructionInfo(m_mass, m_motion_state, shape, localInertia);

	m_rigidbody = new btRigidBody(myBoxRigidBodyConstructionInfo);

	Engine::GetPhysics().AddRigidBody(m_rigidbody);
	m_collider_type = Custom;

	m_enabled = true;
	m_game_object->GetTransform()->SetRigidbody(this);

	m_rigidbody->setCollisionFlags(m_rigidbody->getCollisionFlags() | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);   // Disable debug drawing
	return true;
}


void Redline::Rigidbody::Shutdown()
{
	DestroyShape();

	m_game_object = nullptr;
}


void Redline::Rigidbody::DestroyShape()
{
	if (m_rigidbody != nullptr)
	{
		Engine::GetPhysics().RemoveRigidBody(m_rigidbody);
		delete m_rigidbody;
		m_rigidbody = nullptr;
	}

	if (m_motion_state != nullptr)
	{
		delete m_motion_state;
		m_motion_state = nullptr;
	}

	m_enabled = false;
	m_game_object->GetTransform()->SetRigidbody(nullptr);
}


void Redline::Rigidbody::SetDebugState(const bool state) const
{
	if (!m_rigidbody)
		return;

	int flags = m_rigidbody->getCollisionFlags();

	if (state)
		flags &= ~btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT;
	else
		flags |= btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT;

	m_rigidbody->setCollisionFlags(flags);
}


void Redline::Rigidbody::SetMass(const float mass)
{
	if (!m_enabled)
	{
		Debug::Err("Rigidbody::SetMass: Rigidbody isn't initialized!");
		return;
	}

	Engine::GetPhysics().RemoveRigidBody(m_rigidbody);

	m_mass = Math::Max(mass, 0.0f);

	btVector3 inertia = btVector3(0, 0, 0);
	m_rigidbody->getCollisionShape()->calculateLocalInertia(mass, inertia);
	m_rigidbody->setMassProps(mass, inertia);
	m_rigidbody->updateInertiaTensor();

	Engine::GetPhysics().AddRigidBody(m_rigidbody);
}


Vector3 Redline::Rigidbody::GetPosition() const
{
	btVector3 pos = m_rigidbody->getCenterOfMassTransform().getOrigin();
	return Vector3(pos.x(), pos.y(), pos.z()) + m_center;
}


void Redline::Rigidbody::SetPosition(const Vector3 pos) const
{
	if (!m_enabled)
	{
		Debug::Err("Rigidbody::SetPosition: Rigidbody isn't initialized!");
		return;
	}

	Vector3 bulletPos = pos - m_center;

	btTransform transform = m_rigidbody->getCenterOfMassTransform();
	transform.setOrigin(btVector3(bulletPos.x, bulletPos.y, bulletPos.z));
	m_rigidbody->setCenterOfMassTransform(transform);
}


Quaternion Redline::Rigidbody::GetRotation() const
{
	btQuaternion rot = m_rigidbody->getCenterOfMassTransform().getRotation();
	return Quaternion(rot.x(), rot.y(), rot.z(), rot.w());
}


void Redline::Rigidbody::SetRotation(const Quaternion rot) const
{
	if (!m_enabled)
	{
		Debug::Err("Rigidbody::SetRotation: Rigidbody isn't initialized!");
		return;
	}

	btTransform transform = m_rigidbody->getCenterOfMassTransform();
	transform.setRotation(btQuaternion(rot.x, rot.y, rot.z, rot.w));
	m_rigidbody->setCenterOfMassTransform(transform);
}


void Redline::Rigidbody::CreateFromData(const ColliderType type, const float params[4], const Vector3 center)
{
	switch (type)
	{
	default:
	case None:
		return;
	case Box:
		CreateBox(params[0], params[1], params[2], params[3]);
		break;
	case Sphere:
		CreateSphere(params[0], params[1]);
		break;
	case Cylinder:
		CreateCylinder(params[0], params[1], params[2], static_cast<int>(params[3]));
		break;
	case ComplexMesh:
		CreateComplexMesh();
		break;
	}

	m_center = center;

	m_game_object->GetTransform()->SetRigidbody(m_enabled ? this : nullptr);
}


void Redline::Rigidbody::RefreshCollider()
{
	if (!m_enabled)
	{
		CreateFromData(m_collider_type, m_parameters, m_center);
		return;
	}

	int flags = m_rigidbody->getCollisionFlags();
	btTransform transform = m_rigidbody->getCenterOfMassTransform();

	DestroyShape();

	CreateFromData(m_collider_type, m_parameters, m_center);

	m_game_object->GetTransform()->SetRigidbody(m_enabled ? this : nullptr);

	if (!m_enabled)
		return;

	m_rigidbody->setCollisionFlags(flags);
	m_rigidbody->setCenterOfMassTransform(transform);
}


void Redline::Rigidbody::Serialize(ofstream& stream)
{
	unsigned short type = static_cast<unsigned short>(m_collider_type);
	stream.write(reinterpret_cast<char*>(&type), sizeof(unsigned short));

	stream.write(reinterpret_cast<char*>(&m_parameters), sizeof(float) * 4);

	stream.write(reinterpret_cast<char*>(&m_center), sizeof(Vector3));
}


void Redline::Rigidbody::Deserialize(ifstream& stream)
{
	unsigned short type = 0;
	stream.read(reinterpret_cast<char*>(&type), sizeof(unsigned short));

	stream.read(reinterpret_cast<char*>(&m_parameters), sizeof(float) * 4);

	stream.read(reinterpret_cast<char*>(&m_center), sizeof(Vector3));

	CreateFromData(static_cast<ColliderType>(type), m_parameters, m_center);
}


#ifdef REDLINE_EDITOR
void Redline::Rigidbody::ShowInspector()
{
	if (!ImGui::TreeNodeEx("Rigidbody", ImGuiTreeNodeFlags_DefaultOpen))
		return;

	static int type = 0;
	bool changed = ImGui::Combo("Collider Type", &type, "None\0Box\0Sphere\0Cylinder\0Convex Mesh\0Complex Mesh");

	if (changed)
		m_collider_type = static_cast<ColliderType>(type);
	else
		type = static_cast<int>(m_collider_type);


	switch (m_collider_type)
	{
	default: break;
	case Box:
		changed |= ImGui::DragFloat("Mass", &m_parameters[0]);
		changed |= ImGui::DragFloat3("Size", &m_parameters[1]);
		break;
	case Sphere:
		changed |= ImGui::DragFloat("Mass", &m_parameters[0]);
		changed |= ImGui::DragFloat("Radius", &m_parameters[1]);
		break;
	case Cylinder:
		changed |= ImGui::DragFloat("Mass", &m_parameters[0]);
		changed |= ImGui::DragFloat("Radius", &m_parameters[1]);
		changed |= ImGui::DragFloat("Height", &m_parameters[2]);
		static int axis = static_cast<int>(m_parameters[3]);
		if (changed |= ImGui::Combo("Axis", &axis, "X (right)\0Y (up)\0Z (forward)"))
			m_parameters[3] = static_cast<float>(axis);
		break;
	}

	ImGui::DragFloat3("Center", reinterpret_cast<float*>(&m_center));


	if (changed)
	{
		RefreshCollider();
	}

	ImGui::TreePop();
}
#endif
