#pragma once

namespace Redline
{
	class GameObject;
	class Rigidbody;

	class Transform
	{
	public:
		Transform(GameObject* go);
		~Transform();

		// Position
		void SetPosition(const DirectX::SimpleMath::Vector3& pos)
		{
			m_position = pos;
			m_pos_dirty = true;
		}

		void SetPosition(DirectX::SimpleMath::Vector3* pos)
		{
			m_position = *pos;
			m_pos_dirty = true;
		}

		void SetPosition(const float x, const float y, const float z)
		{
			m_position.x = x;
			m_position.y = y;
			m_position.z = z;
			m_pos_dirty = true;
		}

		void Translate(const DirectX::SimpleMath::Vector3& pos)
		{
			m_position += pos;
			m_pos_dirty = true;
		}

		void Translate(DirectX::SimpleMath::Vector3* pos)
		{
			m_position += *pos;
			m_pos_dirty = true;
		}

		void Translate(float x, float y, float z)
		{
			m_position.x += x;
			m_position.y += y;
			m_position.z += z;
			m_pos_dirty = true;
		}

		DirectX::SimpleMath::Vector3 GetPosition() const { return m_position; }

		// Scale
		void SetScale(const DirectX::SimpleMath::Vector3& scale)
		{
			m_scale = scale;
			m_scale_dirty = true;
		}

		void SetScale(DirectX::SimpleMath::Vector3* scale)
		{
			m_scale = *scale;
			m_scale_dirty = true;
		}

		void SetScale(float x, float y, float z)
		{
			m_scale.x = x;
			m_scale.y = y;
			m_scale.z = z;
			m_scale_dirty = true;
		}

		void SetScale(const float scale)
		{
			m_scale.x = m_scale.y = m_scale.z = scale;
			m_scale_dirty = true;
		}

		void ScaleBy(const float scale)
		{
			m_scale.x *= scale;
			m_scale.y *= scale;
			m_scale.z *= scale;
			m_scale_dirty = true;
		}

		DirectX::SimpleMath::Vector3 GetScale() const { return m_scale; }

		// Rotation
		void SetRotation(const DirectX::SimpleMath::Quaternion& rotation)
		{
			m_rotation = rotation;
			m_rot_dirty = true;
		}

		// Sets rotation (in radians)
		void SetRotation(const DirectX::SimpleMath::Vector3& rotation)
		{
			m_rotation = DirectX::SimpleMath::Quaternion::CreateFromYawPitchRoll(rotation.y, rotation.x, rotation.z);
			m_rot_dirty = true;
		}

		// Sets rotation (in radians)
		void SetRotation(DirectX::SimpleMath::Vector3* rotation)
		{
			m_rotation = DirectX::SimpleMath::Quaternion::CreateFromYawPitchRoll(rotation->y, rotation->x, rotation->z);
			m_rot_dirty = true;
		}

		// Sets rotation (in radians)
		void SetRotation(float x, float y, float z)
		{
			m_rotation = DirectX::SimpleMath::Quaternion::CreateFromYawPitchRoll(y, x, z);
			m_rot_dirty = true;
		}

		void SetRotation(float w, float x, float y, float z)
		{
			m_rotation.w = w;
			m_rotation.x = x;
			m_rotation.y = y;
			m_rotation.z = z;
			m_rot_dirty = true;
		}

		DirectX::SimpleMath::Quaternion GetRotation() const { return m_rotation; }


		Transform* GetParent() const { return m_parent; }
		void SetParent(Transform* parent);

		size_t GetChildsCount() const { return m_childrens.size(); }
		Transform* GetChild(const size_t index) const { return m_childrens[index]; }

		GameObject* GetGameObject() const { return m_game_object; }

		// Matrix
		DirectX::SimpleMath::Matrix& GetMatrix();

		// Returns true if the local matrix has been modified
		bool RebuildMatrix();

		void SetDirty() { isWorldDirty = true; }

		void SetRigidbody(Rigidbody* rb) { m_rigidbody = rb; }

		bool showGizmo = false;

#ifdef REDLINE_EDITOR
		void ShowInspector();
		void ShowHierarchy(int& id_clicked, GameObject* selected) const;
#endif

	protected:
		void AddChild(Transform* child);
		void RemoveChild(Transform* child);

	private:
		DirectX::SimpleMath::Vector3 m_position;
		DirectX::SimpleMath::Quaternion m_rotation;
		DirectX::SimpleMath::Vector3 m_scale;

		DirectX::SimpleMath::Matrix m_positionMatrix;
		DirectX::SimpleMath::Matrix m_rotationMatrix;
		DirectX::SimpleMath::Matrix m_scaleMatrix;
		DirectX::SimpleMath::Matrix m_local_matrix;
		DirectX::SimpleMath::Matrix m_world_matrix;

		bool m_pos_dirty;
		bool m_rot_dirty;
		bool m_scale_dirty;

		bool isWorldDirty;

		GameObject* m_game_object;
		Rigidbody* m_rigidbody;

		Transform* m_parent;

		std::vector<Transform*> m_childrens;

	};
}
