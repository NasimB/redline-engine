#include "physicsdebug.h"
#include "debug.h"


Redline::PhysicsDebug::PhysicsDebug()
	: m_debug_draw_mode(0)
{
}


void Redline::PhysicsDebug::drawLine(const btVector3& from, const btVector3& to, const btVector3& color)
{
	Debug::Line(DirectX::SimpleMath::Vector3(from.x(), from.y(), from.z()),
		DirectX::SimpleMath::Vector3(to.x(), to.y(), to.z()),
		DirectX::SimpleMath::Vector3(color.x(), color.y(), color.z()));
}


void Redline::PhysicsDebug::reportErrorWarning(const char* warningString)
{
	Debug::Warn(warningString);
}