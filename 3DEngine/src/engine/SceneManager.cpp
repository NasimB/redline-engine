﻿#include "SceneManager.h"

#include "../engine/engine.h"
#include "../engine/gameobject.h"
#include "../engine/Rigidbody.h"

using namespace Redline;
using namespace std;
using namespace DirectX::SimpleMath;


SceneManager::SceneManager() :
	m_is_scene_loaded(false), 
	m_is_vector_dirty(false)
{
}


SceneManager::~SceneManager()
{

}


bool SceneManager::LoadScene(const string scene_name)
{
	ifstream file;
	file.open("data//scenes//" + scene_name + ".scene", ios::binary);

	if (!file.is_open())
		return false;

	float sun_intensity = 4.0f;
	Vector3 sun_direction = Vector3(0.127f, 0.845f, -0.520f);

	GameObject* sun_game_object = Engine::CreateGameObject("Sun");
	Light* sun = sun_game_object->AddComponent<Light>();
	sun->SetType(LIGHT_DIRECTIONAL);
	sun->SetDirection(sun_direction);
	sun->SetIntensity(Math::Clamp(sun_direction.y * 8.0f, 0.0f, 1.0f) * sun_intensity);

	AddGameObject(sun_game_object);

	unsigned int object_count = 0;
	file.read(reinterpret_cast<char*>(&object_count), sizeof(unsigned int));

	MapObject* objects = new MapObject[object_count];

	file.read(reinterpret_cast<char*>(objects), static_cast<size_t>(object_count) * sizeof(MapObject));

	for (unsigned int i = 0; i < object_count; ++i)
	{
		string go_name = objects[i].name;
		GameObject* object = Engine::CreateGameObject(go_name);

		if (object == nullptr)
			continue;

		string mesh_name = objects[i].mesh_name;
		if (!mesh_name.empty())
		{
			MeshRenderer* mesh_renderer = object->AddComponent<MeshRenderer>();
			mesh_renderer->SetMesh(mesh_name);
		}


		if (object == nullptr)
			continue;

		if (mesh_name == "building_01")
		{
			Rigidbody* rigidbody = object->AddComponent<Rigidbody>();
			rigidbody->CreateBox(0.0f, 50.0f, 40.0f, 30.0f);
			rigidbody->SetCenter(Vector3(0.0f, -20.0f, 0.0f));

			Transform* transform = object->GetTransform();
			transform->SetPosition(objects[i].position);
			transform->SetRotation(objects[i].rotation);
			transform->SetScale(objects[i].scale);
		}
		else if (mesh_name == "post_single")
		{
			Rigidbody* rigidbody = object->AddComponent<Rigidbody>();
			rigidbody->CreateBox(0.0f, 0.7f, 10.5f, 0.7f);
			rigidbody->SetCenter(Vector3(0.0f, -5.25f, 0.0f));

			object->GetTransform()->SetPosition(objects[i].position);
			object->GetTransform()->SetRotation(objects[i].rotation);
			object->GetTransform()->SetScale(objects[i].scale);

			GameObject* go = Engine::CreateGameObject("Spot Light");
			AddGameObject(go);

			go->GetTransform()->SetParent(object->GetTransform());
			go->GetTransform()->SetPosition(0.0f, 11.0f, -3.0f);
			go->GetTransform()->SetRotation(5.0f * DEG2RAD, 0.0f, 0.0f);

			Light* spot = go->AddComponent<Light>();
			spot->SetType(LIGHT_SPOT);
			spot->SetDirection(Vector3(0.0f, -1.0f, 0.0f));
			spot->SetColor(1.0f, 1.0f, 0.5f);
			spot->SetIntensity(15.0f);
			spot->SetAngle(110.0f);
		}
		else
		{
			Rigidbody* rigidbody = object->AddComponent<Rigidbody>();
			rigidbody->CreateComplexMesh();

			Transform* transform = object->GetTransform();
			transform->SetPosition(objects[i].position);
			transform->SetRotation(objects[i].rotation);
			transform->SetScale(objects[i].scale);
		}

		AddGameObject(object);
	}

	delete[] objects;

	file.close();

	m_is_scene_loaded = true;
	return true;
}


bool SceneManager::LoadSceneV2(const string scene_name)
{
	ifstream file;
	file.open("data//scenes//" + scene_name + ".scene", ios::binary);

	if (!file.is_open())
		return false;

	unsigned int object_count = 0;
	file.read(reinterpret_cast<char*>(&object_count), sizeof(unsigned int));

	MapObjectV2* objects = new MapObjectV2[object_count];

	for (unsigned int i = 0; i < object_count; ++i)
	{
		file.read(reinterpret_cast<char*>(&objects[i]), sizeof(MapObjectV2));

		string go_name = objects[i].name;
		GameObject* object = Engine::CreateGameObject(go_name);

		
		unsigned short component_count = 0;
		file.read(reinterpret_cast<char*>(&component_count), sizeof(unsigned short));

		for (unsigned short j = 0; j < component_count; ++j)
		{
			unsigned short component_type = 0;
			file.read(reinterpret_cast<char*>(&component_type), sizeof(unsigned short));
			
			Component* component = object->AddComponent(static_cast<Component::ComponentType>(component_type));
			component->Deserialize(file);
		}

		Transform* transform = object->GetTransform();
		transform->SetPosition(objects[i].position);
		transform->SetRotation(objects[i].rotation);
		transform->SetScale(objects[i].scale);

		AddGameObject(object);
	}

	file.close();

	vector<GameObject*>& gos = GetGameObjects();
	size_t go_count = gos.size();
	for (unsigned int i = 0; i < go_count; ++i)
	{
		unsigned int parent_id = objects[i].parent_id;

		if (parent_id != 0)
		{
			GameObject* parentGo = GetGameObject(parent_id);
			gos[i]->GetTransform()->SetParent(parentGo->GetTransform());
		}
	}

	delete[] objects;

	m_is_scene_loaded = true;
	return true;
}


void SceneManager::UnloadScene()
{
	for (auto const& imap : m_game_objects)
	{
		GameObject* go = imap.second;

		if (go == nullptr)
			continue;

		Engine::RemoveGameObject(go);
	}
	m_game_objects.clear();
	
	m_is_vector_dirty = true;
	m_is_scene_loaded = false;
}


size_t SceneManager::AddGameObject(GameObject* go)
{
	unsigned int id = static_cast<unsigned int>(m_game_objects.size()) + 1;

	m_game_objects.insert({ id, go });

	go->SetID(id);

	m_is_vector_dirty = true;

	return id;
}


void SceneManager::RemoveGameObject(GameObject* go)
{
	map<unsigned int, GameObject*>::iterator iter = m_game_objects.find(static_cast<unsigned int>(go->GetID()));

	if (iter != m_game_objects.end())
	{
		m_game_objects.erase(iter);
	}

	m_is_vector_dirty = true;
}


vector<GameObject*>& SceneManager::GetGameObjects()
{
	if (m_is_vector_dirty)
	{
		m_game_objects_vector.clear();

		for (auto const& imap : m_game_objects)
		{
			GameObject* go = imap.second;

			if(go != nullptr)
				m_game_objects_vector.push_back(go);
		}

		m_is_vector_dirty = false;
	}

	return m_game_objects_vector;
}


#ifdef REDLINE_EDITOR
bool SceneManager::SaveScene(const string scene_name)
{
	ofstream file;
	file.open("data//scenes//" + scene_name + ".scene", ios::binary);

	if (file.is_open())
	{
		vector<GameObject*>& game_objects = GetGameObjects();
		unsigned int object_count = static_cast<unsigned int>(game_objects.size());

		file.write(reinterpret_cast<char*>(&object_count), sizeof(unsigned int));

		for (unsigned int i = 0; i < object_count; ++i)
		{
			GameObject* game_object = game_objects[i];

			MapObjectV2 object;
			memset(&object, 0, sizeof MapObjectV2);

			// Copy name
			strncpy_s(object.name, game_object->GetName().c_str(), sizeof(object.name));

			// Copy transform
			Transform* transform = game_object->GetTransform();
			object.position = transform->GetPosition();
			object.rotation = transform->GetRotation();
			object.scale = transform->GetScale();

			Transform* parent_transform = transform->GetParent();
			object.parent_id = parent_transform != nullptr ? static_cast<unsigned int>(parent_transform->GetGameObject()->GetID()) : 0;

			file.write(reinterpret_cast<char*>(&object), sizeof(MapObjectV2));


			vector<Component*>& components = game_object->GetAllComponents();
			unsigned short component_count = static_cast<unsigned short>(components.size());
			file.write(reinterpret_cast<char*>(&component_count), sizeof(unsigned short));

			for(Component* component : game_object->GetAllComponents())
			{
				unsigned short component_type = static_cast<unsigned short>(component->GetComponentType());
				file.write(reinterpret_cast<char*>(&component_type), sizeof(unsigned short));
				component->Serialize(file);
			}
		}

		file.close();
	}
	return true;
}
#endif
