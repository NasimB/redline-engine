#pragma once

namespace Redline
{
	class RenderTarget
	{
	public:
		RenderTarget();
		~RenderTarget();

		static RenderTarget* Create(ID3D11Device* device, unsigned int width, unsigned int height, DXGI_FORMAT format, bool allowMips = false, int numFaces = 1);

		bool Initialize(ID3D11Device* device, unsigned int width, unsigned int height, DXGI_FORMAT format, bool allowMips = false, int numFaces = 1);

		void GenerateMips(ID3D11DeviceContext* deviceContext) const;
		void Clear(ID3D11DeviceContext* deviceContext, float r = 0.0f, float g = 0.0f, float b = 0.0f, float a = 0.0f, int faceIndex = 0, int mipLevel = 0) const;

		int GetNumberOfFaces() const { return m_num_faces; }
		int GetNumberOfMips() const { return m_mip_levels; }
		const Microsoft::WRL::ComPtr<ID3D11Texture2D>& GetTexture() const { return m_texture; }
		const Microsoft::WRL::ComPtr<ID3D11RenderTargetView>& GetRenderTargetView(int faceIndex = 0, int mipLevel = 0) const { return m_render_target_view[min(max(faceIndex, 0), m_num_faces - 1)][min(max(mipLevel, 0), m_mip_levels - 1)]; }
		const Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>& GetShaderResourceView() const { return m_shader_resource_view; }

	private:
		Microsoft::WRL::ComPtr<ID3D11Texture2D> m_texture;
		Microsoft::WRL::ComPtr<ID3D11RenderTargetView>** m_render_target_view;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shader_resource_view;

		int m_num_faces;
		int m_mip_levels;
	};
}
