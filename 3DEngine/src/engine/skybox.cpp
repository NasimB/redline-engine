#include "skybox.h"
#include "ResourcesManager.h"
#include "engine.h"

using namespace std;
using namespace DirectX::SimpleMath;

Redline::Skybox::Skybox() :
	m_shader(nullptr),
	m_mesh(nullptr),
	m_vsBuffer(),
	m_psBuffer()
{
}


Redline::Skybox::~Skybox()
{
}


bool Redline::Skybox::Initialize()
{
	m_shader = ResourcesManager::GetShader("data\\shaders\\skybox.fx");
	if (m_shader == nullptr)
	{
		return false;
	}

	m_mesh = ResourcesManager::GetMesh("data\\meshes\\skydome.rmf");
	if (m_mesh == nullptr)
	{
		return false;
	}

	return true;
}


void Redline::Skybox::UpdateCamera(const Vector3& cameraPosition, const float farPlane)
{
	m_translationMatrix = Matrix::CreateTranslation(cameraPosition);

	const float radius = farPlane * 0.95f;

	m_scaleMatrix = Matrix::CreateScale(radius);

	m_worldMatrix = m_scaleMatrix * m_translationMatrix;

	SetSkyboxParameters(cameraPosition);
}


void Redline::Skybox::SetSkyboxParameters(const Vector3& cameraPosition)
{
	const Vector3 v3CameraPos = Vector3(0, max(min(m_fInnerRadius + cameraPosition.y / 1200000.0f, m_fOuterRadius), m_fInnerRadius), 0);
	const Vector3 v3InvWavelength = Vector3(1 / powf(m_fWavelength[0], 4.0f), 1 / powf(m_fWavelength[1], 4.0f), 1 / powf(m_fWavelength[2], 4.0f));

	Light* sun = Engine::GetLight(0);
	Vector3 v3LightDirection = Vector3::Down;
	if (sun != nullptr)
	{
		v3LightDirection = Vector3::TransformNormal(sun->GetDirection(), sun->GetGameObject()->GetTransform()->GetMatrix());
	}

	// Fill vertex constant buffer
	m_vsBuffer.v3CameraPos = v3CameraPos;
	m_vsBuffer.v3LightDirection = v3LightDirection;
	m_vsBuffer.v3InvWavelength = v3InvWavelength;
	m_vsBuffer.fCameraHeight = v3CameraPos.Length();
	m_vsBuffer.fCameraHeight2 = v3CameraPos.LengthSquared();
	m_vsBuffer.fOuterRadius = m_fOuterRadius;
	m_vsBuffer.fOuterRadius2 = m_fOuterRadius * m_fOuterRadius;
	m_vsBuffer.fInnerRadius = m_fInnerRadius;
	m_vsBuffer.fInnerRadius2 = m_fInnerRadius * m_fInnerRadius;
	m_vsBuffer.fKrESun = m_Kr * m_ESun;
	m_vsBuffer.fKmESun = m_Km * m_ESun;
	m_vsBuffer.fKr4PI = m_Kr * 4.0f * float(M_PI);
	m_vsBuffer.fKm4PI = m_Km * 4.0f * float(M_PI);
	m_vsBuffer.fScale = 1.0f / (m_fOuterRadius - m_fInnerRadius);
	m_vsBuffer.fScaleDepth = m_fScaleDepth;
	m_vsBuffer.fScaleOverScaleDepth = m_fScale / m_fScaleDepth;

	// Fill pixel constant buffer
	m_psBuffer.g = Vector2(m_g, m_g * m_g);
	m_psBuffer.cameraPosition = cameraPosition;
	m_psBuffer.lightDirection = v3LightDirection;
}
