﻿#pragma once

#include "shader.h"
#include "rendertarget.h"
#include "texture.h"

namespace Redline
{
	// Class in charge of rendering the Screen Space Ambient Occlusion buffer
	class SSAORenderer
	{
	public:
		SSAORenderer();
		~SSAORenderer();

		bool Initialize(const Microsoft::WRL::ComPtr<ID3D11Device>& device, const int ssao_technique, const int render_width, const int render_height);

		void Shutdown();

		bool Render(const Microsoft::WRL::ComPtr<ID3D11DeviceContext>& device_context, RenderTarget* position_RT, RenderTarget* normal_RT, RenderTarget* color_RT) const;

		RenderTarget* GetRenderTarget() const { return m_ssaoRT; }
		bool IsEnabled() const { return m_ssaoEnabled; }

		struct SSAOBuffer
		{
			DirectX::XMMATRIX projMatrix;
			DirectX::XMMATRIX invProjMatrix;
			float halfWidth;
			float halfHeight;
			float aspectRatio;
			float dummy;
		};

	private:
		RenderTarget* m_ssaoRT;
		RenderTarget* m_ssaoBlurRT;
		Shader* m_ssaoShader;
		Shader* m_ssaoBlurXShader;
		Shader* m_ssaoBlurYShader;
		ID3D11Buffer* m_bufferSSAO;
		Texture* m_noise_texture;
		const float m_ssaoRenderScale = 1.0f;
		float m_ssaoWidth;
		float m_ssaoHeight;
		bool m_ssaoEnabled = true;
	};
}
