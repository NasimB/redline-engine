#include "audio.h"

using namespace std;
using namespace DirectX::SimpleMath;

Redline::Audio::Audio() :
	m_studio_system(nullptr),
	m_system(nullptr),
	mnNextChannelId(0)
{
}


Redline::Audio::~Audio()
{
	ErrorCheck(m_studio_system->unloadAll());
	ErrorCheck(m_studio_system->release());
}


bool Redline::Audio::Initialize()
{
	ErrorCheck(FMOD::Studio::System::create(&m_studio_system));
	ErrorCheck(m_studio_system->initialize(32, 
#ifdef _DEBUG
		FMOD_STUDIO_INIT_LIVEUPDATE, FMOD_INIT_PROFILE_ENABLE,
#else
		0, 0,
#endif
		nullptr));

	ErrorCheck(m_studio_system->getLowLevelSystem(&m_system));

	ErrorCheck(m_studio_system->setNumListeners(1));
	return true;
}


bool Redline::Audio::Update()
{
	vector<ChannelMap::iterator> stopped_channels;
	for (auto it = mChannels.begin(), itEnd = mChannels.end(); it != itEnd; ++it)
	{
		bool is_playing = false;
		it->second->isPlaying(&is_playing);
		if (!is_playing)
		{
			stopped_channels.push_back(it);
		}
	}

	for (auto& it : stopped_channels)
	{
		mChannels.erase(it);
	}

	return ErrorCheck(m_studio_system->update());
}


void Redline::Audio::LoadBank(const string& strBankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags)
{
	auto tFoundIt = mBanks.find(strBankName);

	// Bank is already loaded
	if (tFoundIt != mBanks.end())
		return;

	FMOD::Studio::Bank* pBank;
	ErrorCheck(m_studio_system->loadBankFile(strBankName.c_str(), flags, &pBank));

	if (pBank)
	{
		mBanks[strBankName] = pBank;
	}
}


void Redline::Audio::LoadEvent(const string& strEventName)
{
	auto tFoundit = mEvents.find(strEventName);

	// Event is already loaded
	if (tFoundit != mEvents.end())
		return;

	FMOD::Studio::EventDescription* pEventDescription = nullptr;
	ErrorCheck(m_studio_system->getEvent(strEventName.c_str(), &pEventDescription));
	if (pEventDescription)
	{
		FMOD::Studio::EventInstance* pEventInstance = nullptr;
		ErrorCheck(pEventDescription->createInstance(&pEventInstance));

		if (pEventInstance)
		{
			mEvents[strEventName] = pEventInstance;
		}
	}
}


void Redline::Audio::LoadSound(const string& strSoundName, bool b3d, bool bLooping, bool bStream)
{
	auto tFoundIt = mSounds.find(strSoundName);

	// Sound is already loaded
	if (tFoundIt != mSounds.end())
		return;

	FMOD_MODE eMode = FMOD_DEFAULT;
	eMode |= b3d ? FMOD_3D : FMOD_2D;
	eMode |= bLooping ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF;
	eMode |= bStream ? FMOD_CREATESTREAM : FMOD_CREATECOMPRESSEDSAMPLE;

	FMOD::Sound* pSound = nullptr;
	ErrorCheck(m_system->createSound(strSoundName.c_str(), eMode, nullptr, &pSound));
	if (pSound)
	{
		mSounds[strSoundName] = pSound;
	}
}


void Redline::Audio::UnloadSound(const string& strSoundName)
{
	auto tFoundIt = mSounds.find(strSoundName);

	// Sound isn't loaded
	if (tFoundIt == mSounds.end())
		return;

	ErrorCheck(tFoundIt->second->release());
	mSounds.erase(tFoundIt);
}


void Redline::Audio::Set3DListenerAndOrientation(const Vector3& position, const Vector3& forward, const Vector3& up, const Vector3& velocity) const
{
	FMOD_3D_ATTRIBUTES attributes;
	attributes.position = VectorToFmod(position);
	attributes.forward = VectorToFmod(forward);
	attributes.up = VectorToFmod(up);
	attributes.velocity = VectorToFmod(velocity);
	ErrorCheck(m_studio_system->setListenerAttributes(0, &attributes));
}


int Redline::Audio::PlaySounds(const string& strSoundName, const Vector3& vPos, float fVolumedB)
{
	int nChannelId = mnNextChannelId++;
	auto tFoundIt = mSounds.find(strSoundName);

	// Sound isn't loaded
	if (tFoundIt == mSounds.end())
	{
		LoadSound(strSoundName);
		tFoundIt = mSounds.find(strSoundName);

		// Sound failed to load
		if (tFoundIt == mSounds.end())
		{
			return nChannelId;
		}
	}

	FMOD::Channel* pChannel = nullptr;
	ErrorCheck(m_system->playSound(tFoundIt->second, nullptr, true, &pChannel)); // Start the sound paused, set parameters and then unpause
	if (pChannel)
	{
		FMOD_MODE currMode;
		tFoundIt->second->getMode(&currMode);
		if (currMode & FMOD_3D)
		{
			FMOD_VECTOR position = VectorToFmod(vPos);
			ErrorCheck(pChannel->set3DAttributes(&position, nullptr));
		}
		ErrorCheck(pChannel->setVolume(dbToVolume(fVolumedB)));
		ErrorCheck(pChannel->setPaused(false));
		mChannels[nChannelId] = pChannel;
	}
	return nChannelId;
}


void Redline::Audio::PlayEvent(const string& strEventName)
{
	auto tFoundit = mEvents.find(strEventName);

	// Event isn't loaded
	if (tFoundit == mEvents.end())
	{
		LoadEvent(strEventName);
		tFoundit = mEvents.find(strEventName);

		// Event failed to load
		if (tFoundit == mEvents.end())
			return;
	}

	tFoundit->second->start();
}


void Redline::Audio::SetEventPosition(const std::string& strEventName, const DirectX::SimpleMath::Vector3& position, const DirectX::SimpleMath::Vector3& forward, const DirectX::SimpleMath::Vector3& up, const DirectX::SimpleMath::Vector3& velocity)
{
	auto tFoundit = mEvents.find(strEventName);

	// Event isn't loaded
	if (tFoundit == mEvents.end())
		return;

	FMOD_3D_ATTRIBUTES attributes;
	attributes.position = VectorToFmod(position);
	attributes.forward = VectorToFmod(forward);
	attributes.up = VectorToFmod(up);
	attributes.velocity = VectorToFmod(velocity);
	tFoundit->second->set3DAttributes(&attributes);
}


void Redline::Audio::StopEvent(const string& strEventName, bool bImmediate)
{
	auto tFoundIt = mEvents.find(strEventName);

	// Event isn't loaded
	if (tFoundIt == mEvents.end())
		return;

	FMOD_STUDIO_STOP_MODE eMode = bImmediate ? FMOD_STUDIO_STOP_IMMEDIATE : FMOD_STUDIO_STOP_ALLOWFADEOUT;
	ErrorCheck(tFoundIt->second->stop(eMode));
}


void Redline::Audio::GetEventParameter(const string& strEventName, const string& strEventParameter, float* parameter)
{
	auto tFoundIt = mEvents.find(strEventName);

	// Event isn't loaded
	if (tFoundIt == mEvents.end())
		return;

	ErrorCheck(tFoundIt->second->getParameterValue(strEventParameter.c_str(), parameter));
}


void Redline::Audio::SetEventParameter(const string& strEventName, const string& strParameterName, float fValue)
{
	auto tFoundIt = mEvents.find(strEventName);

	// Event isn't loaded
	if (tFoundIt == mEvents.end())
		return;

	ErrorCheck(tFoundIt->second->setParameterValue(strParameterName.c_str(), fValue));
}


bool Redline::Audio::IsEventPlaying(const string& strEventName) const
{
	auto tFoundIt = mEvents.find(strEventName);

	// Event isn't loaded
	if (tFoundIt == mEvents.end())
		return false;

	FMOD_STUDIO_PLAYBACK_STATE state;
	ErrorCheck(tFoundIt->second->getPlaybackState(&state));
	return state == FMOD_STUDIO_PLAYBACK_PLAYING;
}
