﻿#pragma once

#include "rendertarget.h"
#include "shader.h"

namespace Redline 
{
	// Class in charge of tonemapping the image and rendering HDR bloom
	class BloomRenderer
	{
	public:
		BloomRenderer();
		~BloomRenderer();

		bool Initialize(const Microsoft::WRL::ComPtr<ID3D11Device>& device, const int render_width, const int render_height);

		void Shutdown();

		bool Render(const float delta_time, const Microsoft::WRL::ComPtr<ID3D11DeviceContext>& device_context, RenderTarget* base_RT, RenderTarget* output_RT);

		struct LuminanceBuffer
		{
			float deltaTime;
			float minExposure;
			float maxExposure;
			float adaptationSpeed;
			float bloom_ammount;
			float dummy[3];
		};

	private:
		RenderTarget* m_bloomBlur_A_RT;
		RenderTarget* m_bloomBlur_B_RT;

		RenderTarget* m_tonemapped_RT;

		RenderTarget* m_exposure_A_RT;
		RenderTarget* m_exposure_B_RT;

		Shader* m_combine_shader;
		Shader* m_adaptation_shader;

		Shader* m_blurHShader;
		Shader* m_blurVShader;

		ID3D11Buffer* m_luminance_buffer;

		bool m_use_A_buffer;
		
		const float m_bloomRenderScale = 0.25f;
		const float m_minimum_exposure = 0.5f;
		const float m_maximum_exposure = 2.5f;
		const float m_adaptation_speed = 4.0f;
		const float bloom_ammount = 0.4f;

		float m_bloomWidth;
		float m_bloomHeight;

		float m_render_width;
		float m_render_height;
	};
}
