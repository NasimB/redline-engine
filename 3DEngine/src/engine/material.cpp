#include "material.h"

Redline::Material::Material() :
	m_diffuse_texture(nullptr),
	m_normal_texture(nullptr),
	m_material_buffer()
{
}


Redline::Material::~Material()
{
	m_diffuse_texture = nullptr;
	m_normal_texture = nullptr;
}


bool Redline::Material::Initialize(Texture* diffuseTexture, Texture* normalTexture)
{
	m_diffuse_texture = diffuseTexture;
	m_normal_texture = normalTexture;

	m_material_buffer.mDiffuseColor[0] = 0.5f;
	m_material_buffer.mDiffuseColor[1] = 0.5f;
	m_material_buffer.mDiffuseColor[2] = 0.5f;

	m_material_buffer.roughness = 0.5f;
	m_material_buffer.metalic = 0.5f;

	m_material_buffer.hasDiffuseMap = m_diffuse_texture != nullptr ? 1 : 0;
	m_material_buffer.hasNormalMap = m_normal_texture != nullptr ? 1 : 0;
	m_material_buffer.hasSpecularMap = 0;
	return true;
}


#ifdef REDLINE_EDITOR
void Redline::Material::ShowInspector()
{

}
#endif
