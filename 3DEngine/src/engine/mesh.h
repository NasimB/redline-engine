#pragma once

#include "material.h"
#include "indexbuffer.h"
#include "vertexbuffer.h"

namespace Redline
{
	struct SubMesh
	{
		unsigned int materialID = 0;
		VertexBuffer* vBuffer = nullptr;
		IndexBuffer* iBuffer = nullptr;
		float boundingSphereRadius = 0.0f;
		DirectX::SimpleMath::Vector3 boundingSphereCenter;
	};

	class Mesh
	{
	public:
		Mesh();
		~Mesh();

		bool Initialize(const std::string& filename);
		bool Initialize(const std::vector<SubMesh>& submeshes, const std::vector<Material>& materials);

		std::vector<Material>& GetMaterials() { return m_materials; }
		Material& GetMaterials(unsigned int index) { return m_materials[index]; }
		std::vector<SubMesh>& GetSubMeshes() { return m_submeshes; }

		float GetBoundingSphereRadius() const { return m_boundingSphereRadius; }

		void SetCastShadow(bool value) { m_cast_shadow = value; }
		bool GetCastShadow() const { return m_cast_shadow; }

		std::string GetName() const { return m_name; }

		struct MeshHeader
		{
			char fileType[4];
			int objectsCount;
			int materialsCount;
		};

		struct MaterialStruct
		{
			char material_name[128];
			char albedo_name[128];
			char normal_name[128];
			char roughness_name[128];
			char metallic_name[128];
			float albedo_color[3];
			float roughness;
			float metallic;
		};

		struct MaterialStruct3
		{
			char name[128];
			float diffuseColor[3];
			float roughness;
			char diffuseTextureName[256];
			char normalTextureName[256];
			char specularTextureName[256];
		};

	protected:
		bool LoadRedlineMesh(const std::string& filename);
		float ComputeBoundingSphere(const DirectX::SimpleMath::Vector3& avgPos, const std::vector<Vertex>& vertices) const;

		float m_boundingSphereRadius = 0.0f;

		std::vector<Material> m_materials;
		std::vector<SubMesh> m_submeshes;

		bool m_cast_shadow = true;

		std::string m_name;
	};
}
