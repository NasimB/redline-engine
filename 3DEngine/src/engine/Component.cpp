﻿#include "../stdafx.h"
#include "Component.h"

Redline::Component::Component(GameObject* game_object, const ComponentType type) :
	m_game_object(game_object),
	m_type(type)
{

}

Redline::Component::~Component()
{
	m_game_object = nullptr;
}
