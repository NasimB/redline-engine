﻿#pragma once

namespace Redline
{
	class GameObject;

	class Component
	{
	public:
		enum ComponentType
		{
			Unknown = 0,
			Light,
			MeshRenderer,
			Rigidbody
		};

		Component(GameObject* game_object, ComponentType type);
		virtual ~Component();

		GameObject* GetGameObject() const { return m_game_object; }
		ComponentType GetComponentType() const { return m_type; }

		bool operator== (const Component& b) const { return this == &b; }

		virtual void Serialize(std::ofstream& stream) = 0;
		virtual void Deserialize(std::ifstream& stream) = 0;

#ifdef REDLINE_EDITOR
		virtual void ShowInspector() = 0;
#endif

	private:
		GameObject* m_game_object;
		ComponentType m_type;
	};
}
