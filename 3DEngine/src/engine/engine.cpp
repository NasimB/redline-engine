#include "engine.h"

#include <time.h>


Redline::Engine* Redline::Engine::m_engine = nullptr;

using namespace std;
using namespace DirectX::SimpleMath;

Redline::Engine::Engine()
{
	srand(static_cast<int>(time(nullptr)));
	m_input_pressed = false;
}


Redline::Engine::~Engine()
{
}


bool Redline::Engine::Initialize()
{
	// Assign the singleton instance
	if (!m_engine) m_engine = this;
	else return false;

	// Config initialization
	if (!m_config.Initialize())
	{
		Debug::Log("Failed loading config class !", false);
		return false;
	}

	bool fullscreen = m_config.fullscreen;
	int width = m_config.width;
	int height = m_config.height;
	if (fullscreen) GetDesktopResolution(width, height);

	// Window creation
	if (!m_window.Initialize(fullscreen, width, height))
	{
		Debug::Log("Failed loading window class !", false);
		return false;
	}

	// DirectInput initialization
	if (!m_input.Initialize(m_window.GetHInstance(), m_window.GetHWND(), width, height))
	{
		Debug::Log("Failed loading input class !", false);
		return false;
	}

	// Timer initialization
	if (!m_timer.Initialize())
	{
		Debug::Log("Failed loading timer class !", false);
		return false;
	}

	// Camera initialization 
	if (!m_camera.Initialize(width, height))
	{
		Debug::Log("Failed loading camera class !", false);
		return false;
	}

	// Renderer initialization
	if (!m_renderer.Initialize(m_window.GetHWND(), m_config.vsync))
	{
		Debug::Log("Failed initializing renderer device !", false);
		return false;
	}

	// Resources Manager initialization
	if (!m_resources.Initialize(m_config.multithreading))
	{
		Debug::Log("Failed loading ressources class !", false);
		return false;
	}

	// Renderer ressources initialization
	if (!m_renderer.InitializeResources(m_window.GetHWND()))
	{
		Debug::Log("Failed initializing renderer ressources!", false);
		return false;
	}

	// Physics initialization
	if (!m_physics.Initialize())
	{
		Debug::Log("Failed loading physics class !", false);
		return false;
	}

	// Audio initialization
	if (!m_audio.Initialize())
	{
		Debug::Log("Failed loading audio class !", false);
		return false;
	}

	// Skybox initialization
	if (!m_renderer.InitializeSkybox())
	{
		Debug::Log("Failed initializing skybox !", false);
		return false;
	}

#ifdef REDLINE_EDITOR
	// Editor initialization
	if (!m_editor.Initialize())
	{
		Debug::Log("Failed loading editor class !", false);
		return false;
	}
#else
	// Game initialization
	if (!m_game.Initialize())
	{
		Debug::Log("Failed loading game class !", false);
		return false;
	}
#endif
	return true;
}


bool Redline::Engine::Shutdown()
{
#ifdef REDLINE_EDITOR
	m_editor.Shutdown();
#else
	m_game.Shutdown();
#endif

	for (GameObject* gameobject : m_gameobjects)
		delete gameobject;

	for (Light* light : m_lights)
		delete light;

	m_physics.Shutdown();
	m_resources.Shutdown();
	return true;
}


int Redline::Engine::Run()
{
	MSG message;

	int return_code = 0;

	while (!complete)
	{
#ifdef USE_IMGUI
		ImGui_ImplDX11_NewFrame();
#endif

		m_input.Update();
		m_timer.Update();

		// Win32 message manager
		while (PeekMessage(&message, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&message);
			DispatchMessage(&message);
		}

		// We have to exit the program, we break the loop
		if (message.message == WM_QUIT || m_input.IsEscapePressed())
		{
			complete = true;
			return_code = static_cast<int>(message.wParam);
		}
		else
		{
			complete = !Update();
		}
	}

	m_renderer.ClearState();

	Shutdown();

	return return_code;
}


bool Redline::Engine::Update()
{
	string name = "Redline Engine - ";
	name += BUILDTYPE;
	name += " - FPS : " + to_string(m_timer.GetFPS());
	SetWindowTextA(m_window.GetHWND(), name.c_str());

	// Timer update
	float delta_time = Math::Min(m_timer.GetDeltaTime(), 1.0f / 20.0f);

	// Physics update
	if (!m_physics.Update(delta_time))
	{
		Debug::Log("Physics update failed !");
		return false;
	}

	m_physics.DrawDebug();


#ifdef REDLINE_EDITOR
	// Editor Update
	m_editor.Update(delta_time);
#else
	// Game Update
	m_game.Update(delta_time);
#endif

	// Camera Update
	m_camera.Update();

	static Vector3 old_camera_position(0, 0, 0);
	Vector3 position = m_camera.GetPosition();
	Vector3 velocity = (position - old_camera_position) / delta_time;
	m_audio.Set3DListenerAndOrientation(position, -m_camera.GetForwardVector(), m_camera.GetUpVector(), velocity);
	old_camera_position = position;

	// Audio Update
	if (!m_audio.Update())
	{
		Debug::Log("Audio update failed !");
		return false;
	}

	if (!m_renderer.Render(delta_time))
	{
		Debug::Log("Render failed !");
		return false;
	}
	return true;
}


bool Redline::Engine::OnPhysicsUpdate(const float delta_time) const
{
#ifndef REDLINE_EDITOR
	m_game.OnPhysicsUpdate(delta_time);
#endif
	return true;
}


Redline::GameObject* Redline::Engine::CreateGameObject(const string& name)
{
	GameObject* gameObject = new GameObject(name);
	m_engine->m_gameobjects.emplace_back(gameObject);
	return gameObject;
}


void Redline::Engine::CreateGameObject(GameObject* gameObject)
{
	m_engine->m_gameobjects.emplace_back(gameObject);
}


void Redline::Engine::RemoveGameObject(GameObject* gameObject)
{
	vector<GameObject*>::iterator it = find(m_engine->m_gameobjects.begin(), m_engine->m_gameobjects.end(), gameObject);
	if (it != m_engine->m_gameobjects.end())
	{
		m_engine->m_gameobjects.erase(it, it + 1);
		delete gameObject;
	}
}


void Redline::Engine::RemoveLight(Light* light)
{
	//vector<Light>::iterator it = find(m_lights.begin(), m_lights.end(), *light);
	vector<Light*>::iterator it = find(m_lights.begin(), m_lights.end(), light);
	if (it != m_lights.end())
	{
		m_lights.erase(it, it + 1);
		delete light;
	}
}


void Redline::Engine::RemoveMeshRenderer(MeshRenderer* renderer)
{
	//vector<MeshRenderer>::iterator it = find(m_mesh_renderers.begin(), m_mesh_renderers.end(), *renderer);
	vector<MeshRenderer*>::iterator it = find(m_mesh_renderers.begin(), m_mesh_renderers.end(), renderer);
	if (it != m_mesh_renderers.end())
	{
		m_mesh_renderers.erase(it, it + 1);
		delete renderer;
	}
}


void Redline::Engine::RemoveRigidBody(Rigidbody* rigidbody)
{
	//vector<Rigidbody>::iterator it = find(m_rigidbodies.begin(), m_rigidbodies.end(), *rigidbody);
	vector<Rigidbody*>::iterator it = find(m_rigidbodies.begin(), m_rigidbodies.end(), rigidbody);
	if (it != m_rigidbodies.end())
	{
		rigidbody->Shutdown();
		m_rigidbodies.erase(it, it + 1);
		delete rigidbody;
	}
}


void Redline::Engine::RemoveComponent(Component* component)
{
	switch (component->GetComponentType())
	{
	default:
		return;
	case Component::Light:
		m_engine->RemoveLight(static_cast<Light*>(component));
		break;
	case Component::MeshRenderer:
		m_engine->RemoveMeshRenderer(static_cast<MeshRenderer*>(component));
		break;
	case Component::Rigidbody:
		m_engine->RemoveRigidBody(static_cast<Rigidbody*>(component));
		break;
	}
}


void Redline::Engine::GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	// Get a handle to the desktop window
	const HWND hDesktop = GetDesktopWindow();
	// Get the size of screen to the variable desktop
	GetWindowRect(hDesktop, &desktop);
	// The top left corner will have coordinates (0,0)
	// and the bottom right corner will have coordinates
	// (horizontal, vertical)
	horizontal = desktop.right;
	vertical = desktop.bottom;
}
