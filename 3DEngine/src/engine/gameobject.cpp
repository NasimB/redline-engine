#include "gameobject.h"
#include "engine.h"

using namespace std;

Redline::GameObject::GameObject(const string& name) :
	m_transform(this),
    m_id(0),
	m_name(name)
{
}

Redline::GameObject::~GameObject()
{
	for(Component* component : m_components)
	{
		Engine::RemoveComponent(component);
	}
}


void Redline::GameObject::PrepareDraw()
{
	m_transform.RebuildMatrix();
}


Redline::Component* Redline::GameObject::AddComponent(const Component::ComponentType type)
{
	Component* component = nullptr;
	switch(type)
	{
	case Component::ComponentType::Light:
		component = Engine::AddComponent<Light>(this);
		break;
	case Component::ComponentType::MeshRenderer:
		component = Engine::AddComponent<MeshRenderer>(this);
		break;
	case Component::ComponentType::Rigidbody:
		component = Engine::AddComponent<Rigidbody>(this);
		break;
	default: break;
	}

	if (component != nullptr)
		m_components.emplace_back(component);

	return component;
}


void Redline::GameObject::RemoveComponent(Component* component)
{
	vector<Component*>::iterator it = find(m_components.begin(), m_components.end(), component);
	if (it != m_components.end())
	{
		m_components.erase(it, it + 1);
		Engine::RemoveComponent(component);
	}
}
