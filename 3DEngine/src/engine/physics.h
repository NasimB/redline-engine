#pragma once

namespace Redline
{
	class PhysicsDebug;

	class Physics
	{
	public:
		Physics();
		~Physics();

		bool Initialize();
		bool Update(float delta_time) const;
		void DrawDebug() const;
		void Shutdown();

		void AddRigidBody(btRigidBody* rigidBody) const;
		void RemoveRigidBody(btRigidBody* rigidBody) const;

		float GetTimeScale() const { return m_time_scale; }
		void SetTimeScale(const float value) { m_time_scale = value; }

		btDiscreteDynamicsWorld* GetWorld() const { return m_world; }

	private:
		btDiscreteDynamicsWorld* m_world;
		btBroadphaseInterface* m_broadphase;
		btCollisionDispatcher* m_dispatcher;
		btDefaultCollisionConfiguration* m_collision_configuration;
		btSequentialImpulseConstraintSolver* m_sequential_impulse_constraint_solver;
		PhysicsDebug* m_physics_debug;

		float m_time_scale = 1.0f;
	};
}

