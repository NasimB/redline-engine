#pragma once

namespace Redline
{
	class Texture
	{
	public:
		Texture();
		~Texture();

		bool CreateFromFile(const std::string& path, const Microsoft::WRL::ComPtr<ID3D11Device>& device);

		const Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>& GetTexture() const { return m_shader_ressource_view; }

	private:
		Microsoft::WRL::ComPtr<ID3D11Resource> m_texture;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shader_ressource_view;
	};
}
