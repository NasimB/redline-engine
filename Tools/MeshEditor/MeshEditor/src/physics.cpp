#include "physics.h"
#include "physicsdebug.h"


Redline::Physics::Physics() :
	myWorld(nullptr),
	myBroadphase(nullptr),
	myDispatcher(nullptr),
	myCollisionConfiguration(nullptr),
	mySequentialImpulseConstraintSolver(nullptr),
	m_physics_debug(nullptr)
{
}


Redline::Physics::~Physics()
{
}


bool Redline::Physics::Initialize()
{
	myCollisionConfiguration = new btDefaultCollisionConfiguration();
	myDispatcher = new btCollisionDispatcher(myCollisionConfiguration);
	myBroadphase = new btDbvtBroadphase();
	mySequentialImpulseConstraintSolver = new btSequentialImpulseConstraintSolver;
	myWorld = new btDiscreteDynamicsWorld(myDispatcher, myBroadphase, mySequentialImpulseConstraintSolver, myCollisionConfiguration);
	myWorld->setGravity(btVector3(0, -9.81f, 0));

	m_physics_debug = new PhysicsDebug();
	m_physics_debug->setDebugMode(btIDebugDraw::DebugDrawModes::DBG_NoDebug);
	myWorld->setDebugDrawer(m_physics_debug);
	return true;
}


bool Redline::Physics::Update(float delta_time) const
{
	const btScalar refreshTime = btScalar(1.0 / 120.0);
	myWorld->stepSimulation(btScalar(delta_time), 4, refreshTime);
	return true;
}


void Redline::Physics::DrawDebug() const
{
	myWorld->debugDrawWorld();
}


void Redline::Physics::Shutdown()
{
	delete myWorld;
	myWorld = nullptr;

	delete mySequentialImpulseConstraintSolver;
	mySequentialImpulseConstraintSolver = nullptr;

	delete myBroadphase;
	myBroadphase = nullptr;

	delete myDispatcher;
	myDispatcher = nullptr;

	delete myCollisionConfiguration;
	myCollisionConfiguration = nullptr;

	delete m_physics_debug;
	m_physics_debug = nullptr;
}


void Redline::Physics::AddRigidBody(btRigidBody* rigidBody) const
{
	myWorld->addRigidBody(rigidBody);
}


void Redline::Physics::RemoveRigidBody(btRigidBody* rigidBody) const
{
	myWorld->removeRigidBody(rigidBody);
}


void Redline::Physics::AddVehicle(btRaycastVehicle* vehicle) const
{
	myWorld->addVehicle(vehicle);
}


void Redline::Physics::RemoveVehicle(btRaycastVehicle* vehicle) const
{
	myWorld->removeVehicle(vehicle);
}
