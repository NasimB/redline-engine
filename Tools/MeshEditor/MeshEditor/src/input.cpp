#include "input.h"
#include "debug.h"

Redline::Input* Redline::Input::m_input = nullptr;


Redline::Input::Input()
	: m_directInput(nullptr),
	m_keyboard(nullptr),
	m_mouse(nullptr),
	m_previousKeyboardState(),
	m_keyboardState(),
	m_mouseState(),
	m_screenWidth(0),
	m_screenHeight(0),
	m_mouseX(0),
	m_mouseY(0),
	m_isKeyboardAquired(false),
	m_isMouseAquired(false),
	m_wheelConnected(false),
	m_isWheelAquired(false)
{
	m_input = this;
}


Redline::Input::~Input()
{
	// Release the wheel.
	if (m_wheel)
	{
		m_wheel->Unacquire();
		m_wheel->Release();
		m_wheel = nullptr;
	}

	// Release the mouse.
	if (m_mouse)
	{
		m_mouse->Unacquire();
		m_mouse->Release();
		m_mouse = nullptr;
	}

	// Release the keyboard.
	if (m_keyboard)
	{
		m_keyboard->Unacquire();
		m_keyboard->Release();
		m_keyboard = nullptr;
	}

	// Release the main interface to direct input.
	if (m_directInput)
	{
		m_directInput->Release();
		m_directInput = nullptr;
	}
}


bool Redline::Input::Initialize(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight)
{
	// Store the screen size which will be used for positioning the mouse cursor.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	POINT mousePos;

	GetCursorPos(&mousePos);

	// Initialize the location of the mouse on the screen.
	m_mouseX = mousePos.x;
	m_mouseY = mousePos.y;

	// Initialize the main direct input interface.
	if (FAILED(DirectInput8Create(hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8, reinterpret_cast<void**>(&m_directInput), NULL)))
	{
		Debug::Err("Failed to create direct input instance", false);
		return false;
	}

	// Initialize the direct input interface for the keyboard.
	if (FAILED(m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyboard, NULL)))
	{
		Debug::Err("Failed to create direct input keyboard device", false);
		return false;
	}

	if (SUCCEEDED(m_directInput->CreateDevice(GUID_Joystick, &m_wheel, NULL)))
	{
		m_wheel->SetDataFormat(&c_dfDIJoystick2);

		if (SUCCEEDED(m_wheel->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE)))
		{
			m_wheel->EnumObjects(EnumJoystickObjectsCallback, this, DIDFT_ALL);
			m_wheelConnected = true;
		}
	}

	// Set the data format. In this case since it is a keyboard we can use the predefined data format.
	if (FAILED(m_keyboard->SetDataFormat(&c_dfDIKeyboard)))
	{
		Debug::Err("Failed set keyboard data format", false);
		return false;
	}

	// Set the cooperative level of the keyboard to not share with other programs.
	if (FAILED(m_keyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
	{
		Debug::Err("Failed set keyboard cooperative level", false);
		return false;
	}

	// Now acquire the keyboard.
	if (!FAILED(m_keyboard->Acquire()))
	{
		m_isKeyboardAquired = true;
	}

	// Initialize the direct input interface for the mouse.
	if (FAILED(m_directInput->CreateDevice(GUID_SysMouse, &m_mouse, NULL)))
	{
		Debug::Err("Failed to create direct input mouse device", false);
		return false;
	}

	// Set the data format for the mouse using the pre-defined mouse data format.
	if (FAILED(m_mouse->SetDataFormat(&c_dfDIMouse)))
	{
		Debug::Err("Failed set mouse data format", false);
		return false;
	}

	// Set the cooperative level of the mouse to share with other programs.
	if (FAILED(m_mouse->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
	{
		Debug::Err("Failed set mouse cooperative level", false);
		return false;
	}

	// Acquire the mouse.
	if (!FAILED(m_mouse->Acquire()))
	{
		m_isMouseAquired = true;
	}

	return true;
}


bool Redline::Input::Update()
{
	// Read the current state of the keyboard.
	if (!ReadKeyboard())
		return false;

	// Read the current state of the mouse.
	if (!ReadMouse())
		return false;

	// Read the current state of the wheel.
	if (!ReadWheel())
		return false;

	// Process the changes in the mouse and keyboard.
	ProcessInput();

	return true;
}


bool Redline::Input::ReadKeyboard()
{
	memcpy(m_previousKeyboardState, m_keyboardState, sizeof(m_previousKeyboardState));

	if (!m_isKeyboardAquired)
	{
		if (FAILED(m_keyboard->Acquire()))
		{
			return true;
		}

		m_isKeyboardAquired = true;
	}

	if (FAILED(m_keyboard->GetDeviceState(sizeof(m_keyboardState), static_cast<LPVOID>(&m_keyboardState))))
	{
		m_isKeyboardAquired = false;
	}
	return true;
}


bool Redline::Input::ReadMouse()
{
	if (!m_isMouseAquired)
	{
		if (FAILED(m_mouse->Acquire()))
		{
			return true;
		}

		m_isMouseAquired = true;
	}

	if (FAILED(m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), &m_mouseState)))
	{
		m_isMouseAquired = false;
	}
	return true;
}


bool Redline::Input::ReadWheel()
{
	if (!m_wheelConnected)
		return true;

	if (FAILED(m_wheel->Poll()))
	{
		m_isWheelAquired = false;
	}

	if (!m_isWheelAquired)
	{
		if (FAILED(m_wheel->Acquire()))
		{
			return true;
		}

		m_isWheelAquired = true;
	}

	if (FAILED(m_wheel->GetDeviceState(sizeof(DIJOYSTATE2), &m_wheelState)))
	{
		m_isWheelAquired = false;
	}
	return true;
}



void Redline::Input::ProcessInput()
{
	// Update the location of the mouse cursor based on the change of the mouse location during the frame
	m_mouseX += m_mouseState.lX;
	m_mouseY += m_mouseState.lY;

	mouseDelta.x = static_cast<float>(m_mouseState.lX);
	mouseDelta.y = static_cast<float>(m_mouseState.lY);

	// Ensure the mouse location doesn't exceed the screen width or height
	m_mouseX = min(max(m_mouseX, 0), m_screenWidth);
	m_mouseY = min(max(m_mouseY, 0), m_screenHeight);
}


BOOL CALLBACK Redline::Input::EnumJoystickObjectsCallback(const DIDEVICEOBJECTINSTANCE* pdidInstance, VOID* pvRef)
{
	Input* context = static_cast<Input*>(pvRef);
	static int nSliderCount = 0;  // Number of returned slider controls
	static int nPOVCount = 0;     // Number of returned POV controls

	// For axes that are returned, set the DIPROP_RANGE property for the
	// enumerated axis in order to scale min/max values.
	if (pdidInstance->dwType & DIDFT_AXIS)
	{
		DIPROPRANGE diprg;
		diprg.diph.dwSize = sizeof(DIPROPRANGE);
		diprg.diph.dwHeaderSize = sizeof(DIPROPHEADER);
		diprg.diph.dwHow = DIPH_BYID;
		diprg.diph.dwObj = pdidInstance->dwType; // Specify the enumerated axis
		diprg.lMin = -1000;
		diprg.lMax = +1000;

		// Set the range for the axis
		if (FAILED(context->m_wheel->SetProperty(DIPROP_RANGE, &diprg.diph)))
			return DIENUM_STOP;
	}
	return DIENUM_CONTINUE;
}


bool Redline::Input::IsEscapePressed()
{
	// Do a bitwise and on the keyboard state to check if the escape key is currently being pressed
	if (m_keyboardState[DIK_ESCAPE] & 0x80) return true;
	return false;
}


POINT Redline::Input::GetMouseLocation(bool absolute)
{
	POINT point;
	if (absolute)
	{
		GetCursorPos(&point);
	}
	else
	{
		point.x = m_input->m_mouseX;
		point.y = m_input->m_mouseY;
	}
	return point;
}


bool Redline::Input::GetMouseButtonPressed(int keyCode)
{
	if (m_input->m_mouseState.rgbButtons[keyCode] & 0x80) return true;
	return false;
}


bool Redline::Input::GetButtonPressed(int keyCode)
{
	return m_input->m_keyboardState[keyCode] & 0x80;
}


bool Redline::Input::GetButtonDown(int keyCode)
{
	return !(m_input->m_previousKeyboardState[keyCode] & 0x80) && m_input->m_keyboardState[keyCode] & 0x80;
}


bool Redline::Input::GetButtonUp(int keyCode)
{
	return m_input->m_previousKeyboardState[keyCode] & 0x80 && !(m_input->m_keyboardState[keyCode] & 0x80);
}
