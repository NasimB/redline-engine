#include "stringtools.h"
#include <sstream>
#include <algorithm>


bool Redline::StringTools::HasEnding(std::string const& fullString, std::string const& ending)
{
	if (fullString.length() >= ending.length())
	{
		return (fullString.compare(fullString.length() - ending.length(), ending.length(), ending) == 0);
	}
	return false;
}


bool Redline::StringTools::HasBegin(std::string const& fullString, std::string const& ending)
{
	if (fullString.length() >= ending.length())
	{
		return (fullString.compare(0, ending.length(), ending) == 0);
	}
	return false;
}


std::vector<std::string> Redline::StringTools::Split(const std::string& str, char delimiter)
{
	std::vector<std::string> internal;
	std::stringstream ss(str);
	std::string tok;

	while (getline(ss, tok, delimiter))
	{
		internal.emplace_back(tok);
	}

	return internal;
}


std::string Redline::StringTools::ToLower(const std::string& str)
{
	std::string result = str;
	transform(str.begin(), str.end(), result.begin(), static_cast<int(*)(int)>(tolower));
	return result;
}


bool Redline::StringTools::StringToBool(const std::string& str)
{
	return ToLower(str).compare("true") == 0;
}


int Redline::StringTools::IndexOf(const std::vector<std::string>& input, const std::string& e, const int low, const int high, const bool doSorting)
{
	std::vector<std::string> inArray(input);

	if (doSorting)
	{
		sort(inArray.begin(), inArray.end());
	}

	if (low > high)
	{
		return -1;
	}

	int mid = (low + high) / 2;

	if (inArray[mid].compare(e) == 0)
	{
		return mid;
	}

	if (e < inArray[mid])
	{
		return IndexOf(inArray, e, low, mid - 1, false);
	}
	return IndexOf(inArray, e, mid + 1, high, false);
}
