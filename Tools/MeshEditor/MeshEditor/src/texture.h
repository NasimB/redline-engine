#pragma once

#include <string>
#include <wrl.h>
#include <d3d11.h>

#pragma comment(lib, "DirectXTK.lib")

namespace Redline
{
	class Texture
	{
	public:
		Texture();
		~Texture();

		bool CreateFromFile(const std::string& path, ID3D11Device* device);

		std::string GetName() const { return m_name; }

		const Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>& GetTexture() const { return m_shader_ressource_view; }

	private:
		Microsoft::WRL::ComPtr<ID3D11Resource> m_texture;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shader_ressource_view;

		std::string m_name;
	};
}
