#include "resources.h"
#include "engine.h"
#include <fstream>
#include "stringtools.h"
#include <thread>

using namespace std;
using namespace DirectX::SimpleMath;

Redline::Resources* Redline::Resources::m_resources = nullptr;


Redline::Resources::Resources()
{
}


Redline::Resources::~Resources()
{
}


bool Redline::Resources::Initialize(bool useMultiThreading)
{
	// Assign the singleton instance
	if (!m_resources) m_resources = this;
	else return false;

	char buffer[MAX_PATH];
	GetCurrentDirectoryA(MAX_PATH, buffer);
	m_start_directory = buffer;
	m_start_directory += "\\";

	vector<string> texturesToLoad;
	vector<string> meshesToLoad;
	vector<string> shadersToLoad;

	// Default texture
	texturesToLoad.emplace_back("data\\textures\\default.dds");

	// Default shaders
	shadersToLoad.emplace_back("data\\shaders\\default.fx");
	shadersToLoad.emplace_back("data\\shaders\\wireframe.fx");
	shadersToLoad.emplace_back("data\\shaders\\skybox.fx");

	// Default meshes
	meshesToLoad.emplace_back("data\\meshes\\cube.rmf");
	meshesToLoad.emplace_back("data\\meshes\\sphere.rmf");
	meshesToLoad.emplace_back("data\\meshes\\skydome.rmf");

	WIN32_FIND_DATAA search_data;
	memset(&search_data, 0, sizeof(WIN32_FIND_DATAA));

	HANDLE handle = FindFirstFileA("data\\objects\\*.config", &search_data);
	while (handle != INVALID_HANDLE_VALUE)
	{
		string path = "data\\objects\\";
		path += search_data.cFileName;
		fstream myfile(path);

		for (string s; getline(myfile, s);)
		{
			if (StringTools::HasEnding(s, ".dds") || StringTools::HasEnding(s, ".DDS"))
			{
				texturesToLoad.emplace_back(s);
			}
			else if (StringTools::HasEnding(s, ".fx"))
			{
				shadersToLoad.emplace_back(s);
			}
			else if (StringTools::HasEnding(s, ".rmf"))
			{
				meshesToLoad.emplace_back(s);
			}
		}

		myfile.close();

		if (!FindNextFileA(handle, &search_data))
			break;
	}


	if (useMultiThreading)
	{
		thread textureThread(&Resources::LoadTextures, this, texturesToLoad);
		thread shaderThread(&Resources::LoadShaders, this, shadersToLoad);

		textureThread.join();

		LoadMeshes(meshesToLoad);

		shaderThread.join();
	}
	else
	{
		LoadTextures(texturesToLoad);
		LoadShaders(shadersToLoad);
		LoadMeshes(meshesToLoad);
	}

	return true;
}


bool Redline::Resources::Shutdown()
{
	for (auto iterator = m_resources->m_textures.begin(); iterator != m_resources->m_textures.end(); ++iterator)
	{
		delete (*iterator).second;
		(*iterator).second = nullptr;
	}

	for (auto iterator = m_resources->m_shaders.begin(); iterator != m_resources->m_shaders.end(); ++iterator)
	{
		delete (*iterator).second;
		(*iterator).second = nullptr;
	}

	for (auto iterator = m_resources->m_meshes.begin(); iterator != m_resources->m_meshes.end(); ++iterator)
	{
		delete (*iterator).second;
		(*iterator).second = nullptr;
	}
	return true;
}


Redline::Texture* Redline::Resources::GetTexture(const string& path)
{
	map<string, Texture*>::iterator iter = m_resources->m_textures.find(path);

	if (iter != m_resources->m_textures.end())
	{
		return (*iter).second;
	}

	return m_resources->LoadTexture(path);
}


Redline::Shader* Redline::Resources::GetShader(const string& path)
{
	map<string, Shader*>::iterator iter = m_resources->m_shaders.find(path);

	if (iter != m_resources->m_shaders.end())
	{
		return (*iter).second;
	}
	return nullptr;
}


Redline::Mesh* Redline::Resources::GetMesh(const string& path)
{
	map<string, Mesh*>::iterator iter = m_resources->m_meshes.find(path);

	if (iter != m_resources->m_meshes.end())
	{
		return (*iter).second;
	}
	return nullptr;
}


bool Redline::Resources::LoadTextures(const vector<string>& paths)
{
	for (size_t i = 0; i < paths.size(); ++i)
	{
		LoadTexture(paths[i]);
	}
	return true;
}


Redline::Texture* Redline::Resources::LoadTexture(const std::string& path)
{
	map<string, Texture*>::iterator iter = m_textures.find(path);

	if (iter != m_resources->m_textures.end())
	{
		Debug::Log("Texture already loaded: " + path, false);
		return (*iter).second;
	}

	Texture* texture = Engine::GetRenderer()->CreateTextureFromFile(path);
	if (!texture)
	{
		Debug::Log("Failed to load texture: " + path, false);
		
		string full_path = m_start_directory + path;
		texture = Engine::GetRenderer()->CreateTextureFromFile(full_path);
		if (!texture)
		{
			Debug::Log("Failed to load texture: " + full_path, false);
			return nullptr;
		}
	}

	m_textures.insert({ path, texture });
	return texture;
}


bool Redline::Resources::LoadShaders(const vector<string>& paths)
{
	for (size_t i = 0; i < paths.size(); ++i)
	{
		map<string, Shader*>::iterator iter = m_shaders.find(paths[i]);

		if (iter != m_resources->m_shaders.end())
		{
			Debug::Log("Shader already loaded: " + paths[i], false);
			continue;
		}

		Shader* shader = Engine::GetRenderer()->CreateShaderFromFile(paths[i]);
		if (!shader)
		{
			Debug::Log("Failed to load shader: " + paths[i], false);
			continue;
		}

		m_shaders.insert({ paths[i], shader });
	}
	return true;
}


bool Redline::Resources::LoadMeshes(const vector<string>& paths)
{
	for (size_t i = 0; i < paths.size(); ++i)
	{
		map<string, Mesh*>::iterator iter = m_meshes.find(paths[i]);

		if (iter != m_resources->m_meshes.end())
		{
			Debug::Log("Mesh already loaded: " + paths[i], false);
			continue;
		}

		Mesh* mesh = new Mesh();
		if (!mesh->Initialize(paths[i]))
		{
			Debug::Log("Failed to load mesh: " + paths[i], false);
			delete mesh;
			continue;
		}

		m_meshes.insert({ paths[i], mesh });
	}
	return true;
}
