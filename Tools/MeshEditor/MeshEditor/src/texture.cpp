#include "texture.h"

#include <DDSTextureLoader.h>
#include "stringtools.h"
#include "debug.h"

using namespace std;

Redline::Texture::Texture() :
	m_texture(nullptr),
	m_shader_ressource_view(nullptr),
	m_name("")
{
}

Redline::Texture::~Texture()
{
	m_shader_ressource_view.Reset();
	m_texture.Reset();
}


bool Redline::Texture::CreateFromFile(const std::string& path, ID3D11Device* device)
{
	vector<string> pathSplitted = StringTools::Split(path, '\\');
	m_name = StringTools::Split(pathSplitted[pathSplitted.size() - 1], '.')[0];

	if (m_name.empty())
		return false;

	wstring wpath(path.begin(), path.end());

	if (FAILED(DirectX::CreateDDSTextureFromFile(device, wpath.c_str(), m_texture.ReleaseAndGetAddressOf(), m_shader_ressource_view.ReleaseAndGetAddressOf())))
	{
		return false;
	}
	return true;
}
