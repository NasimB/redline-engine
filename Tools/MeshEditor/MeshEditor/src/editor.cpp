#include "editor.h"

#include <string>
#include <iterator>
#include "texture.h"
#include "renderer.h"
#include "../../../../Libraries/Imgui/imgui.h"
#include "engine.h"

#include <SimpleMath.h>
#include <fstream>
#include "stringtools.h"

using namespace std;
using namespace DirectX;
using namespace SimpleMath;

Redline::Editor::Editor(): 
	m_mesh(nullptr), 
	m_gameobject(nullptr)
{

}


Redline::Editor::~Editor()
{
	if (m_mesh)
	{
		delete m_mesh;
		m_mesh = nullptr;
	}
}


bool Redline::Editor::Initialize()
{
	Light* sun = Engine::AddLight(LIGHT_DIRECTIONAL);
	sun->SetDirection(-1.0f, 1.0f, -1.0f);
	sun->SetIntensity(4.0f);
	return true;
}


void Redline::Editor::UpdateFreeCam(float delta_time)
{
	if (Input::GetButtonPressed(DIK_A))
	{
		Vector3 pos = Engine::GetCamera()->GetPosition();
		Vector3 right = Engine::GetCamera()->GetRightVector();

		Engine::GetCamera()->SetPosition(pos + right * (50.0f * delta_time * m_cameraSpeedMultiplier));
	}
	else if (Input::GetButtonPressed(DIK_D))
	{
		Vector3 pos = Engine::GetCamera()->GetPosition();
		Vector3 right = Engine::GetCamera()->GetRightVector();

		Engine::GetCamera()->SetPosition(pos - right * (50.0f * delta_time * m_cameraSpeedMultiplier));
	}

	if (Input::GetButtonPressed(DIK_W))
	{
		Vector3 pos = Engine::GetCamera()->GetPosition();
		Vector3 forward = Engine::GetCamera()->GetForwardVector();

		Engine::GetCamera()->SetPosition(pos + forward * (50.0f * delta_time * m_cameraSpeedMultiplier));
	}
	else if (Input::GetButtonPressed(DIK_S))
	{
		Vector3 pos = Engine::GetCamera()->GetPosition();
		Vector3 forward = Engine::GetCamera()->GetForwardVector();

		Engine::GetCamera()->SetPosition(pos - forward * (50.0f * delta_time * m_cameraSpeedMultiplier));
	}

	if (Input::GetMouseButtonPressed(MOUSE_RIGHT))
	{
		Vector3 rot = Engine::GetCamera()->GetRotation();
		Vector2 delta = Input::GetMouseDelta() * 0.1f;

		const float maxRot = 90.0f;

		Vector3 newRot = rot + Vector3(delta.y, -delta.x, 0);
		newRot.x = min(max(newRot.x, -maxRot), maxRot);
		Engine::GetCamera()->SetRotation(newRot);
	}

	if (Input::GetButtonPressed(DIK_LSHIFT))
	{
		m_cameraSpeedMultiplier = 2.0f;
	}
	else if (Input::GetButtonPressed(DIK_LALT))
	{
		m_cameraSpeedMultiplier = 0.3f;
	}
	else
	{
		m_cameraSpeedMultiplier = 1.0f;
	}

	Engine::GetCamera()->SetFOV(60.0f);
}


void Redline::Editor::UpdateRotateAroundCam(float delta_time)
{
	if (Input::GetButtonPressed(DIK_LEFT))
	{
		theta += 1.0f * delta_time;
	}
	else if (Input::GetButtonPressed(DIK_RIGHT))
	{
		theta -= 1.0f * delta_time;
	}

	if (Input::GetButtonPressed(DIK_UP))
	{
		phi += 1.0f * delta_time;
	}
	else if (Input::GetButtonPressed(DIK_DOWN))
	{
		phi -= 1.0f * delta_time;
	}

	const float PI = 3.14159265359f;
	phi = min(max(phi, 0.01f), PI - 0.01f);

	if (Input::GetButtonPressed(DIK_PRIOR)) // Page Up
	{
		height += 1.0f * delta_time;
	}
	else if (Input::GetButtonPressed(DIK_NEXT)) // Page Down
	{
		height -= 1.0f * delta_time;
	}

	if (Input::GetButtonPressed(DIK_ADD)) // Numpad +
	{
		distance -= 1.0f * delta_time;
	}
	else if (Input::GetButtonPressed(DIK_SUBTRACT)) // Numpad -
	{
		distance += 1.0f * delta_time;
	}
	distance = max(distance, 0.01f);

	const float x = distance * sin(phi) * cos(theta);
	const float y = distance * cos(phi);
	const float z = distance * sin(phi) * sin(theta);

	Camera* camera = Engine::GetCamera();
	Vector3 at = Vector3(0.0f, height, 0.0f);
	Vector3 position = Vector3(x, y, z);
	camera->SetPosition(at + position);
	camera->SetLookAt(at);

	ImGui::Value("Theta", theta);
	ImGui::Value("Phi", phi);
	ImGui::Value("Distance", distance);
	ImGui::Value("Height", height);
}


bool Redline::Editor::LoadMeshFile(const string& filePath)
{
	m_mesh = new Mesh();

	if (!m_mesh->Initialize(filePath))
	{
		delete m_mesh;
		m_mesh = nullptr;
		return false;
	}

	m_gameobject = new GameObject();
	m_gameobject->Initialize(m_mesh);
	Engine::AddGameObject(m_gameobject);
	return true;
}


void Redline::Editor::Update(float delta_time)
{
	UpdateCamera(delta_time);

	UpdateUI(delta_time);
}


void Redline::Editor::UpdateCamera(float delta_time)
{
	if (Input::GetButtonDown(DIK_C))
		m_camera_mode = m_camera_mode == 0 ? 1 : 0;

	switch (m_camera_mode)
	{
	case 0:
		UpdateRotateAroundCam(delta_time);
		break;
	case 1:
		UpdateFreeCam(delta_time);
		break;
	}
}


void Redline::Editor::UpdateUI(float delta_time)
{
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			if (ImGui::MenuItem("Open", "Ctrl+O"))
			{
				OpenFileDialog(Engine::GetWindow()->GetHWND(), "Mesh File (*.*)\0*.*\0");
			}

			if (ImGui::MenuItem("Save", "Ctrl+S"))
			{
				SaveFileDialog(Engine::GetWindow()->GetHWND(), "Redline Mesh File (*.rmf)\0*.rmf\0");
			}
			ImGui::EndMenu();
		}
		ImGui::EndMainMenuBar();
	}

	if (m_gameobject == nullptr)
		return;


	if (ImGui::Begin("Object properties"))
	{
		if (ImGui::TreeNode("Transform"))
		{
			ImGui::Indent();


			Vector3 position = m_gameobject->GetTransform().GetPosition();
			ImGui::DragFloat3("Position", reinterpret_cast<float*>(&position));
			m_gameobject->GetTransform().SetPosition(position);

			ImGui::DragFloat3("Rotation", reinterpret_cast<float*>(&m_rotation_offset));
			Vector3 to_radians = m_rotation_offset * float(DEG2RAD);
			m_gameobject->GetTransform().SetRotation(to_radians);

			float scale = m_gameobject->GetTransform().GetScale().x;
			ImGui::DragFloat("Scale", &scale, 0.01f);
			m_gameobject->GetTransform().SetScale(scale);


			ImGui::Unindent();
			ImGui::TreePop();
		}

		ImGui::Text("");
		ImGui::InputText("Texture folder", m_texture_folder, 64);

		ImGui::Text("");
		ImGui::Text("Materials:");

		for (int j = 0; j < m_gameobject->GetMeshRenderer().materials.size(); ++j)
		{
			Material* mat = &m_gameobject->GetMeshRenderer().materials[j];

			string name = mat->GetName();
			if (name.empty())
			{
				name = "material_" + to_string(j);
				mat->SetName(name);
			}

			if (ImGui::TreeNode(name.c_str()))
			{
				ImGui::Indent();

				ImGui::ColorEdit3("Diffuse Color", mat->GetDiffuseColor());

				float roughness = mat->GetRoughness();
				ImGui::SliderFloat("Roughness", &roughness, 0.01f, 0.99f);
				mat->SetRoughness(roughness);

				float metallic = mat->GetMetalic();
				ImGui::SliderFloat("Metallic", &metallic, 0.01f, 0.99f);
				mat->SetMetalic(metallic);

				if (mat->GetDiffuseTexture() != nullptr)
				{
					string str = "Albedo texture: " + mat->GetDiffuseTexture()->GetName() + ".dds";
					ImGui::Text(str.c_str());
				}

				if (mat->GetNormalTexture() != nullptr)
				{
					string str = "Normal texture: " + mat->GetNormalTexture()->GetName() + ".dds";
					ImGui::Text(str.c_str());
				}

				ImGui::Unindent();

				ImGui::TreePop();
			}
		}

		ImGui::End();
	}
}


void Redline::Editor::OpenFileDialog(HWND hwnd, LPCSTR filter)
{
	OPENFILENAMEA ofn;
	CHAR szFile[MAX_PATH];

	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.hwndOwner = hwnd;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = filter;
	ofn.nFilterIndex = 1;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrFileTitle = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	if (GetOpenFileNameA(&ofn))
	{
		string filePath = ofn.lpstrFile;

		if (m_mesh)
		{
			delete m_mesh;
			m_mesh = nullptr;
		}
		
		if (m_gameobject)
		{
			Engine::RemoveGameObject(m_gameobject);
			m_gameobject = nullptr;
		}

		LoadMeshFile(filePath);
	}
}


void Redline::Editor::SaveFileDialog(HWND hwnd, LPCSTR filter) const
{
	OPENFILENAMEA ofn;
	CHAR szFile[MAX_PATH];

	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.hwndOwner = hwnd;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = filter;
	ofn.nFilterIndex = 1;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrFileTitle = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT;

	if (GetSaveFileNameA(&ofn))
	{
		string filePath = ofn.lpstrFile;

		// Ensure that the filepath ends with .rmf
		if (!StringTools::HasEnding(filePath, ".rmf"))
			filePath += ".rmf";

		SaveMeshFile(filePath);
	}
}


bool Redline::Editor::SaveMeshFile(const string& filePath) const
{
	vector<Material> materials = m_gameobject->GetMeshRenderer().materials;
	size_t materials_count = materials.size();

	vector<SubMesh> submeshes = m_mesh->GetSubMeshes();
	size_t submeshes_count = submeshes.size();

	ofstream fileOut(filePath, ios::out | ios::binary | ios::trunc);
	Mesh::MeshHeader header;
	header.fileType[0] = 'R';
	header.fileType[1] = 'M';
	header.fileType[2] = 'F';
	header.fileType[3] = '4';
	header.materialsCount = static_cast<int>(materials_count);
	header.objectsCount = static_cast<int>(submeshes_count);
	fileOut.write(reinterpret_cast<const char*>(&header), sizeof(header));

	// Save materials
	for (int i = 0; i < materials_count; ++i)
	{
		Material* mat = &materials[i];
		Mesh::MaterialStruct mat_struct;
		memset(&mat_struct, 0, sizeof(mat_struct)); // Zero values

		if (mat->GetName() != "")
		{
			// Copy material name
			strncpy_s(mat_struct.material_name, mat->GetName().c_str(), sizeof(mat_struct.material_name));
		}

		if (mat->GetDiffuseTexture() != nullptr)
		{
			string texture_path = m_texture_folder;
			texture_path += "\\" + mat->GetDiffuseTexture()->GetName();

			// Copy albedo name
			strncpy_s(mat_struct.albedo_name, texture_path.c_str(), sizeof(mat_struct.albedo_name));
		}

		if (mat->GetNormalTexture() != nullptr)
		{
			string texture_path = m_texture_folder;
			texture_path += "\\" + mat->GetNormalTexture()->GetName();

			// Copy normal name
			strncpy_s(mat_struct.normal_name, texture_path.c_str(), sizeof(mat_struct.normal_name));
		}

		// Copy albedo color
		mat_struct.albedo_color[0] = mat->GetDiffuseColor()[0];
		mat_struct.albedo_color[1] = mat->GetDiffuseColor()[1];
		mat_struct.albedo_color[2] = mat->GetDiffuseColor()[2];

		// Copy material roughness
		mat_struct.roughness = mat->GetRoughness();

		// Copy material metallic
		mat_struct.metallic = mat->GetMetalic();

		fileOut.write(reinterpret_cast<const char*>(&mat_struct), sizeof(Mesh::MaterialStruct));
	}

	m_gameobject->GetTransform().RebuildMatrix();
	Matrix world_matrix = m_gameobject->GetTransform().GetMatrix();
	Matrix rotation_matrix = Matrix::CreateFromQuaternion(m_gameobject->GetTransform().GetRotation());

	// Save sub-meshes
	for(int i = 0; i < submeshes_count; ++i)
	{
		SubMesh* subMesh = &submeshes[i];

		unsigned int vertex_count = subMesh->vBuffer->GetVertexCount();
		fileOut.write(reinterpret_cast<char*>(&vertex_count), sizeof(unsigned int));

		unsigned int indices_count = subMesh->iBuffer->GetIndicesCount();
		fileOut.write(reinterpret_cast<char*>(&indices_count), sizeof(unsigned int));

		unsigned int material_id = subMesh->materialID;
		fileOut.write(reinterpret_cast<char*>(&material_id), sizeof(unsigned int));

		vector<Vertex> vertices = subMesh->vBuffer->GetVertices();
		for(unsigned int j = 0; j < vertex_count; ++j)
		{
			Vertex vertex = vertices[j];
			vertex.position = static_cast<XMFLOAT3>(Vector3::Transform(vertex.position, world_matrix));
			vertex.normal = static_cast<XMFLOAT3>(Vector3::TransformNormal(vertex.normal, rotation_matrix));
			vertex.tangent = static_cast<XMFLOAT3>(Vector3::TransformNormal(vertex.tangent, rotation_matrix));
			vertex.binormal = static_cast<XMFLOAT3>(Vector3::TransformNormal(vertex.binormal, rotation_matrix));
			fileOut.write(reinterpret_cast<char*>(&vertex), sizeof(Vertex));
		}

		unsigned int* indices = &subMesh->iBuffer->GetIndices()[0];
		fileOut.write(reinterpret_cast<char*>(indices), indices_count * sizeof(unsigned int));
	}
	return true;
}
