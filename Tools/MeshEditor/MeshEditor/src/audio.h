#pragma once

#include <XAudio2.h>

#pragma comment(lib, "xaudio2.lib")

namespace Redline
{
	class Audio
	{
	public:
		Audio();
		~Audio();

		bool Initialize();

		IXAudio2* GetXAudio() const { return m_xaudio2; }
		IXAudio2MasteringVoice* GetMasteringVoice() const { return m_mastering_voice; }

	private:
		IXAudio2* m_xaudio2;
		IXAudio2MasteringVoice* m_mastering_voice;
	};
}
