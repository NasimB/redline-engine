#include "vertexbuffer.h"

Redline::VertexBuffer::VertexBuffer()
{
}

Redline::VertexBuffer::~VertexBuffer()
{
	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = nullptr;
	}
}


bool Redline::VertexBuffer::CreateFromVector(const std::vector<Vertex>& vertices, ID3D11Device* device)
{
	if (vertices.empty())
		return false;

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = static_cast<UINT>(sizeof(Vertex) * vertices.size());
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices.data();

	if (FAILED(device->CreateBuffer(&bd, &InitData, &m_vertexBuffer)))
		return false;

	m_vertexCount = static_cast<unsigned int>(vertices.size());
	m_vertices = vertices;

	return true;
}
