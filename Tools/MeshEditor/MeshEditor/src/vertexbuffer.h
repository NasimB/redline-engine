#pragma once

#include <vector>
#include <d3d11.h>
#include <DirectXMath.h>

#pragma comment(lib, "d3d11.lib")

namespace Redline
{
	struct Vertex
	{
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT2 texture;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT3 tangent;
		DirectX::XMFLOAT3 binormal;
	};

	class VertexBuffer
	{
	public:
		VertexBuffer();
		~VertexBuffer();

		bool CreateFromVector(const std::vector<Vertex>& vertices, ID3D11Device* device);

		ID3D11Buffer* GetVertexBuffer() const { return m_vertexBuffer; }
		std::vector<Vertex>& GetVertices() { return m_vertices; }
		unsigned int GetVertexCount() const { return m_vertexCount; }

	private:
		std::vector<Vertex> m_vertices;
		ID3D11Buffer* m_vertexBuffer = nullptr;
		unsigned int m_vertexCount = 0;
	};
}
