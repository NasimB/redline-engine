#pragma once
#include "mesh.h"

namespace Redline
{
	class MeshRenderer
	{
	public:
		MeshRenderer();
		~MeshRenderer();

		Mesh* mesh;
		std::vector<Material> materials;

		bool m_casting_shadows;
		bool m_active;
	};
}
