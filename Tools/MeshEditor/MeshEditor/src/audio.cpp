#include "audio.h"

Redline::Audio::Audio()
	: m_xaudio2(nullptr),
	  m_mastering_voice(nullptr)
{
}


Redline::Audio::~Audio()
{
	if (m_mastering_voice)
		m_mastering_voice->DestroyVoice();

	if (m_xaudio2)
		m_xaudio2->Release();

	CoUninitialize();
}


bool Redline::Audio::Initialize()
{
	CoInitializeEx(nullptr, COINIT_MULTITHREADED);

	if (FAILED(XAudio2Create(&m_xaudio2)))
	{
		MessageBoxA(nullptr, "Audio engine initialization failed !", "Fatal Error", MB_OK | MB_ICONERROR);
		return false;
	}

	m_xaudio2->CreateMasteringVoice(&m_mastering_voice);
	return true;
}
