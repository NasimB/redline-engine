#include "debug.h"

#include <fstream>
#include <string>
#include <d3d11.h>
#include <SimpleMath.h>
#include "../../../Libraries/Imgui/imgui.h"

Redline::Debug* Redline::Debug::m_debug = nullptr;


Redline::Debug::Debug()
{
	m_debug = this;

	std::ofstream myfile("debug.log", std::ios::trunc);
	myfile.close();

	m_debugArray.reserve(static_cast<size_t>(maxSize));
}


Redline::Debug::~Debug()
{
}


void Redline::Debug::AddDebugLog(const std::string& text, int type, bool showOnConsole)
{
	std::string prefix = type == DEBUG_LOG ? "[Log] " : type == DEBUG_ERROR ? "[Error] " : "[Warn] ";
	std::string suffix = "\n";
	std::string log = prefix + text + suffix;

	OutputDebugStringA(log.c_str());

	while (static_cast<int>(m_debugArray.size()) > maxSize)
	{
		m_debugArray.erase(m_debugArray.begin());
	}

	if (showOnConsole)
	{
		m_debugArray.emplace_back(log);
	}

	std::ofstream myfile("debug.log", std::ios::app);
	std::string s(log.begin(), log.end());

	if (!myfile.is_open())
	{
		OutputDebugStringA("ERROR: Can't open log file\n");
		return;
	}

	SYSTEMTIME time;
	GetLocalTime(&time);
	myfile << "[" << static_cast<int>(time.wDay) << "/" << static_cast<int>(time.wMonth) << "/" << static_cast<int>(time.wYear) << " - " << static_cast<int>(time.wHour) << ":" << (std::to_string(time.wMinute).size() == 1 ? "0" + std::to_string(time.wMinute) : std::to_string(time.wMinute)) << ":" << (std::to_string(time.wSecond).size() == 1 ? "0" + std::to_string(time.wSecond) : std::to_string(time.wSecond)) << "] " << s;
	myfile.close();
}


void Redline::Debug::Log(const std::string& text, bool showOnConsole)
{
	m_debug->AddDebugLog(text, DEBUG_LOG, showOnConsole);
}


void Redline::Debug::Err(const std::string& text, bool showOnConsole)
{
	m_debug->AddDebugLog(text, DEBUG_ERROR, showOnConsole);
}


void Redline::Debug::Warn(const std::string& text, bool showOnConsole)
{
	m_debug->AddDebugLog(text, DEBUG_WARN, showOnConsole);
}


void Redline::Debug::DrawConsole()
{
	std::string raw = "";
	for (size_t i = 0; i < m_debug->m_debugArray.size(); ++i)
		raw += m_debug->m_debugArray[i];

	ImGui::SetNextWindowPos(ImVec2(5, 5), ImGuiSetCond_FirstUseEver);
	ImGui::Begin("Debug console");
	ImGui::Text(raw.c_str());
	ImGui::End();
}


void Redline::Debug::Line(const DirectX::SimpleMath::Vector3& startPos, const DirectX::SimpleMath::Vector3& endPos, const DirectX::SimpleMath::Vector3& color)
{
	DebugLine debugLine;
	debugLine.startPos = static_cast<DirectX::XMFLOAT3>(startPos);
	debugLine.endPos = static_cast<DirectX::XMFLOAT3>(endPos);
	debugLine.color = static_cast<DirectX::XMFLOAT3>(color);
	m_debug->linesArray.emplace_back(debugLine);
}
