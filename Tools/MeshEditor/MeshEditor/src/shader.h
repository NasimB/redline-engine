#pragma once

#include <string>
#include <d3d11.h>
#include <d3dcompiler.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib,"d3dcompiler.lib")

namespace Redline
{
	class Shader
	{
	public:
		Shader();
		~Shader();

		bool CreateFromFile(const std::string& filePath, ID3D11Device* device, ID3D11DeviceContext* deviceContext, const std::string& vertexShader = "VS", const std::string& pixelShader = "PS");

		ID3D11PixelShader* GetPixelShader() const { return m_pixel_shader; }
		ID3D11VertexShader* GetVertexShader() const { return m_vertex_shader; }
		ID3D11InputLayout* GetInputLayout() const { return m_vertex_layout; }

	private:
		static HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);

		ID3D11VertexShader* m_vertex_shader = nullptr;
		ID3D11PixelShader* m_pixel_shader = nullptr;
		ID3D11InputLayout* m_vertex_layout = nullptr;
	};
}
