#include "meshrenderer.h"


Redline::MeshRenderer::MeshRenderer() : 
	mesh(nullptr), 
	m_casting_shadows(true), 
	m_active(true)
{
}


Redline::MeshRenderer::~MeshRenderer()
{
	mesh = nullptr;
}
