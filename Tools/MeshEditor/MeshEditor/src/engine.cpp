#include "engine.h"
#include "../../../Libraries/Imgui/imgui.h"
#include "../../../Libraries/Imgui/imgui_impl_dx11.h"
#include <ctime>


Redline::Engine* Redline::Engine::m_engine = nullptr;

using namespace std;
using namespace DirectX::SimpleMath;

Redline::Engine::Engine()
{
	srand(static_cast<int>(time(nullptr)));
	m_input_pressed = false;
}


Redline::Engine::~Engine()
{
}


bool Redline::Engine::Initialize()
{
	// Assign the singleton instance
	if (!m_engine) m_engine = this;
	else return false;

	//Config initialization
	if (!m_config.Initialize())
	{
		Debug::Log("Failed loading config class!", false);
		return false;
	}

	bool fullscreen = m_config.fullscreen;
	int width = m_config.width;
	int height = m_config.height;
	if (fullscreen) GetDesktopResolution(width, height);

	// Window creation
	if (!m_window.Initialize(fullscreen, width, height))
	{
		Debug::Log("Failed loading window class!", false);
		return false;
	}

	// DirectInput initialization
	if (!m_input.Initialize(m_window.GetHInstance(), m_window.GetHWND(), width, height))
	{
		Debug::Log("Failed loading input class!", false);
		return false;
	}

	// Timer initialization
	if (!m_timer.Initialize())
	{
		Debug::Log("Failed loading timer class!", false);
		return false;
	}

	// Camera initialization 
	if (!m_camera.Initialize(width, height))
	{
		Debug::Log("Failed loading camera class!", false);
		return false;
	}

	// Renderer initialization
	if (!m_renderer.Initialize(m_window.GetHWND(), m_config.vsync))
	{
		Debug::Log("Failed initializing renderer device!", false);
		return false;
	}

	// Resources Manager initialization
	if (!m_resources.Initialize(m_config.multithreading))
	{
		Debug::Log("Failed loading ressources class!", false);
		return false;
	}

	// Audio initialization
	if (!m_audio.Initialize())
	{
		Debug::Log("Failed loading audio class!", false);
		return false;
	}

	// Skybox initialization
	if (!m_renderer.InitializeSkybox())
	{
		Debug::Log("Failed initializing skybox!", false);
		return false;
	}

	// Editor initialization
	if(!m_editor.Initialize())
	{
		Debug::Log("Failed initializing editor!", false);
		return false;
	}

	return true;
}


bool Redline::Engine::Shutdown()
{
	for (GameObject* gameobject : m_gameobjects)
		delete gameobject;

	for (Sound* sound : m_sounds)
		delete sound;

	for (Light* light : m_lights)
		delete light;

	m_resources.Shutdown();
	return true;
}


int Redline::Engine::Run()
{
	MSG message;

	int return_code = 0;

	while (!complete)
	{
		ImGui_ImplDX11_NewFrame();

		m_input.Update();
		m_timer.Update();

		// Win32 message manager
		while (PeekMessage(&message, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&message);
			DispatchMessage(&message);
		}

		// We have to exit the program, we break the loop
		if (message.message == WM_QUIT || m_input.IsEscapePressed())
		{
			complete = true;
			return_code = static_cast<int>(message.wParam);
		}
		else
		{
			complete = !Update();
		}
	}

	Shutdown();

	return return_code;
}


bool Redline::Engine::Update()
{
	string name = "Redline Engine - ";
	name += BUILDTYPE;
	name += " - FPS : " + to_string(m_timer.GetFPS());
	SetWindowTextA(m_window.GetHWND(), name.c_str());

	// Timer update
	float delta_time = m_timer.GetDeltaTime();

	m_editor.Update(delta_time);

	// Camera Update
	m_camera.Update(delta_time);

	if (!m_renderer.Render(m_gameobjects, delta_time))
	{
		Debug::Log("Render failed !");
		return false;
	}
	return true;
}


Redline::GameObject* Redline::Engine::AddGameObject(const string& meshName)
{
	GameObject* gameObject = new GameObject();
	gameObject->Initialize(meshName);
	m_engine->m_gameobjects.emplace_back(gameObject);
	return gameObject;
}


void Redline::Engine::AddGameObject(GameObject* gameObject)
{
	m_engine->m_gameobjects.emplace_back(gameObject);
}


void Redline::Engine::RemoveGameObject(GameObject* gameObject)
{
	for (size_t i = 0; i < m_engine->m_gameobjects.size(); ++i)
	{
		if (m_engine->m_gameobjects[i] == gameObject)
		{
			m_engine->m_gameobjects.erase(m_engine->m_gameobjects.begin() + i);
			delete gameObject;
			return;
		}
	}
}


Redline::Light* Redline::Engine::AddLight(const LightType& lightType)
{
	Light* light = new Light(lightType);
	m_engine->m_lights.emplace_back(light);
	return light;
}


void Redline::Engine::RemoveLight(Light* light)
{
	for (size_t i = 0; i < m_engine->m_lights.size(); ++i)
	{
		if (m_engine->m_lights[i] == light)
		{
			m_engine->m_lights.erase(m_engine->m_lights.begin() + i);
			delete light;
			return;
		}
	}
}


/*void Redline::Engine::AddRigidBody(btRigidBody* rigidBody)
{
	m_engine->m_physics.AddRigidBody(rigidBody);
}


void Redline::Engine::RemoveRigidBody(btRigidBody* rigidBody)
{
	m_engine->m_physics.RemoveRigidBody(rigidBody);
}*/


Redline::Sound* Redline::Engine::AddSound(const string& name)
{
	Sound* sound = new Sound();

	if (!sound->Initialize(m_engine->m_audio, name))
	{
		delete sound;
		return nullptr;
	}

	m_engine->m_sounds.emplace_back(sound);
	return sound;
}


void Redline::Engine::RemoveSound(Sound* sound)
{
	for (size_t i = 0; i < m_engine->m_sounds.size(); ++i)
	{
		if (m_engine->m_sounds[i] == sound)
		{
			m_engine->m_sounds.erase(m_engine->m_sounds.begin() + i);
			sound->Stop();
			delete sound;
			return;
		}
	}
}

void Redline::Engine::GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	// Get a handle to the desktop window
	const HWND hDesktop = GetDesktopWindow();
	// Get the size of screen to the variable desktop
	GetWindowRect(hDesktop, &desktop);
	// The top left corner will have coordinates (0,0)
	// and the bottom right corner will have coordinates
	// (horizontal, vertical)
	horizontal = desktop.right;
	vertical = desktop.bottom;
}
