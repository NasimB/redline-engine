#pragma once

#include <btBulletDynamicsCommon.h>
namespace Redline
{
	class PhysicsDebug;

	class Physics
	{
	public:
		Physics();
		~Physics();

		bool Initialize();
		bool Update(float delta_time) const;
		void DrawDebug() const;
		void Shutdown();

		void AddRigidBody(btRigidBody* rigidBody) const;
		void RemoveRigidBody(btRigidBody* rigidBody) const;

		void AddVehicle(btRaycastVehicle* vehicle) const;
		void RemoveVehicle(btRaycastVehicle* vehicle) const;

		btDiscreteDynamicsWorld* GetWorld() const { return myWorld; }

	private:
		btDiscreteDynamicsWorld* myWorld;
		btBroadphaseInterface* myBroadphase;
		btCollisionDispatcher* myDispatcher;
		btDefaultCollisionConfiguration* myCollisionConfiguration;
		btSequentialImpulseConstraintSolver* mySequentialImpulseConstraintSolver;
		PhysicsDebug* m_physics_debug;
	};
}

