#include "gameobject.h"
#include "engine.h"

using namespace std;

Redline::GameObject::GameObject()
{

}

Redline::GameObject::~GameObject()
{

}


void Redline::GameObject::Initialize(const string& meshName)
{
	string path = "data\\meshes\\" + meshName + ".rmf";
	Mesh* mesh = Resources::GetMesh(path);
	if (mesh != nullptr)
	{
		m_mesh_renderer.mesh = mesh;
		m_mesh_renderer.materials = mesh->GetMaterials();
	}
}

void Redline::GameObject::Initialize(Mesh* mesh)
{
	if (mesh != nullptr)
	{
		m_mesh_renderer.mesh = mesh;
		m_mesh_renderer.materials = mesh->GetMaterials();
	}
}


void Redline::GameObject::PrepareDraw()
{
	m_transform.RebuildMatrix();
}
