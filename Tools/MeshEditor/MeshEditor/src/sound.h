#pragma once

#include "audio.h"

#include <string>

namespace Redline
{
	class Sound
	{
	public:
		Sound();
		~Sound();

		bool Initialize(Audio& audio, const std::string& name);
		void Play(bool loop);
		void Stop() const;
		void SetVolume(float volume) const;
		float GetVolume() const;

	private:
		enum Id
		{
			ID_RIFF = 'FFIR',
			ID_WAVE = 'EVAW',
			ID_FORMAT = ' tmf',
			ID_DATA = 'atad'
		};

		bool FindChunk(Id id);

		bool ReadChunkData(void* buffer, DWORD buffer_size) const;

		HANDLE m_file;
		DWORD m_chunk_size;
		DWORD m_chunk_position;
		BYTE* m_sound_buffer;
		WAVEFORMATEXTENSIBLE m_wave_format;
		XAUDIO2_BUFFER m_buffer;
		IXAudio2SourceVoice* m_source_voice;
	};
}
