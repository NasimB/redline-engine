#pragma once

#include "transform.h"
#include "mesh.h"

#include "meshrenderer.h"

namespace Redline
{
	class GameObject
	{
	public:
		GameObject();
		virtual ~GameObject();

		void Initialize(const std::string& meshName);
		void Initialize(Mesh* mesh);
		virtual void Update(float deltaTime) { }

		void PrepareDraw();

		MeshRenderer& GetMeshRenderer() { return m_mesh_renderer; }
		Transform& GetTransform() { return m_transform; }

	protected:
		Transform m_transform;
		MeshRenderer m_mesh_renderer;
	};
}
