#pragma once

#include "mesh.h"
#include "texture.h"
#include "shader.h"

#include <vector>
#include <map>

namespace Redline
{
	class Resources
	{
	public:
		Resources();
		~Resources();

		bool Initialize(bool useMultiThreading);
		static bool Shutdown();

		static Texture* GetTexture(const std::string& path);
		static Shader* GetShader(const std::string& path);
		static Mesh* GetMesh(const std::string& path);

		static Resources* m_resources;

	private:
		bool LoadTextures(const std::vector<std::string>& paths);
		Texture* LoadTexture(const std::string& path);

		bool LoadShaders(const std::vector<std::string>& paths);
		bool LoadMeshes(const std::vector<std::string>& paths);

		std::map<std::string, Texture*> m_textures;
		std::map<std::string, Shader*> m_shaders;
		std::map<std::string, Mesh*> m_meshes;

		std::string m_start_directory;
	};
}
