#ifndef _SPRITE_H
#define _SPRITE_H

#include <d3d9.h>
#include <d3dx9.h>
#include "texture.h"

class Sprite {
public:
	// Constructor and destructor
	Sprite();
	~Sprite();

	bool Initialize(LPDIRECT3DDEVICE9 device, const wchar_t* name, int x_divisions, int y_divisions);
	bool Draw(LPD3DXSPRITE sprite);

	// Position
	void SetPosition(D3DXVECTOR2 position)            { m_position = position;					RebuildMatrix(); }
	void SetPosition(float x, float y)                { m_position.x = x; m_position.y = y;		RebuildMatrix(); }
	void MoveBy(D3DXVECTOR2 position)                 { m_position += position;					RebuildMatrix(); }
	void MoveBy(float x, float y)                     { m_position.x += x; m_position.y += y;	RebuildMatrix(); }
	D3DXVECTOR2 GetPosition()                         { return m_position; }

	// Scale
	void SetScale(D3DXVECTOR2 scale)                  { m_scale = scale;						RebuildMatrix(); }
	void SetScale(float x, float y)                   { m_scale.x = x; m_scale.y = y;			RebuildMatrix(); }
	void SetScale(float xy)                           { m_scale.x = xy; m_scale.y = xy;			RebuildMatrix(); }
	void ScaleBy(float scale)                         { m_scale *= scale;						RebuildMatrix(); }
	D3DXVECTOR2 GetScale()                            { return m_scale; }

	// Pivot
	void SetPivot(D3DXVECTOR2 pivot)                  { m_pivot = pivot;						RebuildMatrix(); }
	void SetPivot(float x, float y)                   { m_pivot.x = x; m_pivot.y = y;			RebuildMatrix(); }
	D3DXVECTOR2 GetPivot()                            { return m_pivot; }

	// Rotation
	void SetRotation(float degrees)                   { m_rotation = degrees;					RebuildMatrix(); }
	void RotateBy(float degrees)                      { m_rotation += degrees;					RebuildMatrix(); }
	float GetRotation()                               { return m_rotation; }

	// Color
	void SetColor(D3DCOLOR color)                     { m_color = color; }
	void SetColor(byte r, byte g, byte b, byte a)     { m_color = D3DCOLOR_ARGB(a, r, g, b); }
	void SetColor(byte r, byte g, byte b)             { m_color = D3DCOLOR_XRGB(r, g, b); }
	D3DCOLOR GetColor()                               { return m_color; }

	// Visible
	void SetVisible(bool visible)                     { m_visible = visible; }
	bool GetVisible()                                 { return m_visible; }

	// Getters
	Texture& GetTexture() { return m_texture; }

private:
	void RebuildMatrix();

	Texture					m_texture;
	D3DXVECTOR2             m_texture_size;
	D3DXVECTOR2             m_scale;
	D3DXVECTOR2             m_position;
	D3DXVECTOR2             m_pivot;
	float                   m_rotation;
	D3DCOLOR                m_color;
	bool                    m_visible;
	D3DXMATRIX              m_transform;
};

#endif