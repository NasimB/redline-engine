#include "light.h"

Redline::Light::Light() :
	m_position(0, 0, 0),
	m_direction(0, -1, 0),
	m_lightType(LIGHT_DIRECTIONAL),
	m_color(1, 1, 1)
{
}


Redline::Light::Light(LightType type) :
	m_position(0, 0, 0),
	m_direction(0, -1, 0),
	m_lightType(type),
	m_color(1, 1, 1)
{
}


Redline::Light::~Light()
{
}
