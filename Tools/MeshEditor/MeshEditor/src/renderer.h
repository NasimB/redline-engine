#pragma once

#if defined(DEBUG) || defined(_DEBUG)
#ifndef D3D_DEBUG_INFO
#define D3D_DEBUG_INFO
#endif
#endif

#include <d3d11.h>
#include <wrl.h>
#include <vector>
#include <DirectXMath.h>
#include <SimpleMath.h>

#include "gameobject.h"
#include "texture.h"
#include "shader.h"
#include "vertexbuffer.h"
#include "indexbuffer.h"
#include "light.h"
#include "skybox.h"
#include "rendertarget.h"

#define MAX_LIGHTS 63
#define MAX_CASCADES 4

namespace Redline
{
	struct ConstantBuffer
	{
		DirectX::XMMATRIX mWorldToView;
		int useShadow;
		int EnvNumMipMaps;
	};

	struct MatrixBuffer
	{
		DirectX::XMMATRIX mWorld;
		DirectX::XMMATRIX mView;
		DirectX::XMMATRIX mProjection;
	};

	struct LightStruct
	{
		DirectX::SimpleMath::Vector3 color;
		float intensity;
		DirectX::SimpleMath::Vector3 position;
		int type;
		DirectX::SimpleMath::Vector3 direction;
		float angle; // for spot lights
	};

	struct LightBuffer
	{
		LightStruct lights[MAX_LIGHTS];
		int numLights;
		int padding[3];
	};

	struct ShadowsBuffer
	{
		DirectX::XMMATRIX mView;
		DirectX::XMMATRIX mProjectionOne;
		DirectX::XMMATRIX mProjectionTwo;
		DirectX::XMMATRIX mProjectionThree;
		float shadowResolution;
	};

	struct SSAOBuffer
	{
		DirectX::XMVECTOR lightDirection;
		float halfWidth;
		float halfHeight;
		float aspectRatio;
		float lightIntensity;
	};

	struct EnvFilteringBuffer
	{
		DirectX::XMVECTOR vNormalAndRoughness;
		DirectX::XMVECTOR vUp;
	};

	struct LuminanceBuffer
	{
		float deltaTime;
		float minExposure;
		float maxExposure;
		float adaptationSpeed;
	};

	class Renderer
	{
	public:
		Renderer();
		~Renderer();

		bool Initialize(HWND hwnd, bool vSync);
		bool InitializeSkybox();

		bool Render(const std::vector<GameObject*>& gameobjects, float delta_time);

		Texture* CreateTextureFromFile(const std::string& textureName) const;
		Shader* CreateShaderFromFile(const std::string& shaderName) const;
		VertexBuffer* CreateVertexBuffer(const std::vector<Vertex>& vertices) const;
		IndexBuffer* CreateIndexBuffer(const std::vector<unsigned int>& indices) const;

	private:
		void DrawSkybox(const DirectX::SimpleMath::Matrix& viewMatrix, const DirectX::SimpleMath::Matrix& projectionMatrix, const DirectX::SimpleMath::Vector3& cameraPosition, float farPlane);

		void DrawDebug() const;

		bool InitializeGBuffers();
		bool InitializeShadowMap();
		bool InitializePostProcess();

		bool InitializeSSAO();
		void ShutdownSSAO();

		bool InitializeBloom();
		void ShutdownBloom();

		bool InitializeTonemapping();
		void ShutdownTonemapping();

		bool InitializeVolumetricFog();
		void ShutdownVolumetricFog();

		bool InitializeEnvironement();
		void ShutdownEnvironement();

		void RenderToGBuffers(const std::vector<GameObject*>& gameobjects);
		void GetCorners(DirectX::SimpleMath::Vector3* corners, const float& nearClip, const float& farClip) const;
		void RenderToShadowMap(const std::vector<GameObject*>& gameobjects, unsigned int cascadeIndex, float minDistance, float maxDistance);
		void RenderToEnvironementMap(const std::vector<GameObject*>& gameobjects);

		void SwitchToBackbuffer() const;
		void SwitchToGBuffers() const;

		void ClearRTInputsAndOutputs() const;

		void BuildLightsBuffer(const std::vector<Light*>& lights);

		void RenderToScreen();
		void RenderDeferredImage();
		void RenderSSAOPass();
		void RenderFogPass();
		void RenderTonemappingBloomPass(float delta_time);

		void DisableZTest() const;
		void EnableZTest() const;

		void BuildViewFrustum(const DirectX::SimpleMath::Matrix& view, const DirectX::SimpleMath::Matrix& projection);
		bool IsVisibleOnScreen(const DirectX::SimpleMath::Vector3& position, float radius) const;
		float GetOnScreenSize(const DirectX::SimpleMath::Vector3& position, float radius, const DirectX::SimpleMath::Vector3& cameraPosition, float fov) const;


		bool m_vsync_enabled;
		int m_window_width;
		int m_window_height;
		int m_width;
		int m_height;

		int m_sampleCount = 1;
		int m_sampleQuality = 0;

		bool m_isInitialized = false;

		D3D_DRIVER_TYPE m_driverType = D3D_DRIVER_TYPE_NULL;
		D3D_FEATURE_LEVEL m_featureLevel = D3D_FEATURE_LEVEL_11_0;
		Microsoft::WRL::ComPtr<ID3D11Device> m_device;
		Microsoft::WRL::ComPtr<ID3D11DeviceContext> m_deviceContext;
		Microsoft::WRL::ComPtr<IDXGISwapChain> m_swapChain;
		Microsoft::WRL::ComPtr<ID3D11RenderTargetView> m_renderTargetView;
#ifdef _DEBUG
		Microsoft::WRL::ComPtr<ID3D11Debug> m_d3dDebug;
#endif // _DEBUG

		Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_rasterizerState;
		Microsoft::WRL::ComPtr<ID3D11SamplerState> m_linearSampler;

		ID3D11Buffer* m_constants_buffer;
		ID3D11Buffer* m_bufferLights;
		ID3D11Buffer* m_bufferMatrix;

		ID3D11Buffer* m_bufferMaterial;
		ID3D11Buffer* m_bufferVSSkybox;
		ID3D11Buffer* m_bufferPSSkybox;

		Skybox* m_skybox;
		Shader* m_wireframeShader;
		Shader* m_blurHShader;
		Shader* m_blurVShader;
		Shader* m_unlit_shader;


		// Deferred rendering variables and constants
		static const unsigned int BUFFER_COUNT = 3;
		RenderTarget* m_renderTargets[BUFFER_COUNT];

		float m_renderScale = 1.0f;

		ID3D11Texture2D* m_depthStencil;
		ID3D11DepthStencilView* m_depthStencilView;
		ID3D11DepthStencilState* m_depthStencilState;
		ID3D11DepthStencilState* m_depthDisabledStencilState;

		ID3D11SamplerState* m_pointSampler;

		D3D11_VIEWPORT m_viewport;

		Shader* m_GBufferShader;
		Shader* m_deferredShader;

		// Real-time environnement cubemap
		ID3D11Texture2D* m_environmentDepthStencil;
		ID3D11DepthStencilView* m_environmentDepthStencilView;
		D3D11_VIEWPORT m_environmentViewport;
		RenderTarget* m_environment_cubemap;
		RenderTarget* m_environment_cubemap_filtered;
		Shader* m_environement_filtering_shader;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_env_filtering_buffer;
		Shader* m_simple_shader;
		
		// Frustrum culling
		DirectX::SimpleMath::Plane m_frustum[6];

		// Lights
		LightBuffer m_buffer;

		// Shadow Mapping
		static const unsigned int NUM_CASCADES = 3;
		bool m_shadowEnabled = true;
		Shader* m_shadowShader;

		RenderTarget* m_shadowMapsRenderTargets[NUM_CASCADES];
		
		D3D11_VIEWPORT m_shadowViewport;

		ID3D11Texture2D* m_shadowMapDepth;
		ID3D11DepthStencilView* m_shadowMapDepthView;

		DirectX::SimpleMath::Matrix m_light_view_matrix;
		DirectX::SimpleMath::Matrix m_light_projection_matrix[NUM_CASCADES];
		ID3D11Buffer* m_bufferLightMatrix;
		ID3D11SamplerState* m_shadowSampler;

		const float m_cascadeSplits[4] = {1.1f, 50.0f, 175.0f, 500.0f};


		// Post Process
		RenderTarget* m_postProcessRT_A;
		RenderTarget* m_postProcessRT_B;
		ID3D11SamplerState* m_linear_clamp_sampler;

		// Atmospheric fog
		Shader* m_fog_shader;

		// SSAO
		RenderTarget* m_ssaoRT;
		RenderTarget* m_ssaoBlurRT;
		Shader* m_ssaoShader;
		Shader* m_ssaoBlurXShader;
		Shader* m_ssaoBlurYShader;
		bool m_ssaoEnabled = true;
		ID3D11Buffer* m_bufferSSAO;
		const float m_ssaoRenderScale = 0.75f;
		float m_ssaoWidth;
		float m_ssaoHeight;

		// Bloom & tonemapping
		RenderTarget* m_tonemapped_RT;
		RenderTarget* m_bloomBlur_A_RT;
		RenderTarget* m_bloomBlur_B_RT;
		Shader* m_combine_shader;
		const float m_bloomRenderScale = 0.25f;
		float m_bloomWidth;
		float m_bloomHeight;

		RenderTarget* m_exposure_A_RT;
		RenderTarget* m_exposure_B_RT;
		Shader* m_adaptation_shader;
		ID3D11Buffer* m_luminance_buffer;
		bool m_use_A_buffer;


		// Debug stats
		size_t m_draw_call_count;
	};
}
