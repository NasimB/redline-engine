#include "indexbuffer.h"

Redline::IndexBuffer::IndexBuffer()
{
}


Redline::IndexBuffer::~IndexBuffer()
{
	if (m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = nullptr;
	}
}


bool Redline::IndexBuffer::CreateFromVector(const std::vector<unsigned int>& indices, ID3D11Device* device)
{
	if (indices.empty())
		return false;

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = static_cast<UINT>(sizeof(unsigned int) * indices.size());
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = indices.data();

	if (FAILED(device->CreateBuffer(&bd, &InitData, &m_indexBuffer)))
		return false;

	m_indicesCount = static_cast<unsigned int>(indices.size());
	m_indices = indices;

	return true;
}
