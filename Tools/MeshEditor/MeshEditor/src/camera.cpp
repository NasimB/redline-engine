#include "camera.h"

using namespace DirectX::SimpleMath;

Redline::Camera::Camera() :
	m_default_forward(0.0f, 0.0f, 1.0f),
	m_default_right(1.0f, 0.0f, 0.0f),
	m_default_up(0.0f, 1.0f, 0.0f),
	m_rotation_matrix(),
	m_position(0.0f, 0.0f, 0.0f),
	m_rotation(0.0f, 0.0f, 0.0f),
	m_forward(0.0f, 0.0f, 1.0f),
	m_up(0.0f, 1.0f, 0.0f),
	m_right(1.0f, 0.0f, 0.0f)
{
}


Redline::Camera::~Camera()
{
}


bool Redline::Camera::Initialize(unsigned int width, unsigned int height)
{
	m_aspect = width / static_cast<float>(height);

	m_view_matrix = Matrix::CreateLookAt(m_position, m_position + m_forward, m_up);
	m_projection_matrix = Matrix::CreatePerspectiveFieldOfView(m_fov, m_aspect, m_near_clip_plane, m_far_clip_plane);
	return true;
}


void Redline::Camera::Update(float deltaTime)
{
	if (m_rebuildProjMatrix)
	{
		m_projection_matrix = Matrix::CreatePerspectiveFieldOfView(m_fov, m_aspect, m_near_clip_plane, m_far_clip_plane);
	}

	if (m_rebuildViewMatrix)
	{
		m_rotation_matrix = Matrix::CreateFromYawPitchRoll(m_rotation.y * float(DEG2RAD), m_rotation.x * float(DEG2RAD), m_rotation.z * float(DEG2RAD));
		
		m_forward = Vector3::Transform(m_default_forward, m_rotation_matrix);
		m_right = Vector3::Transform(m_default_right, m_rotation_matrix);
		m_up = Vector3::Transform(m_default_up, m_rotation_matrix);
		m_view_matrix = Matrix::CreateLookAt(m_position, m_position + m_forward, m_up);
	}
}
