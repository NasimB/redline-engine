#include "engine.h"

#define NOMINMAX
#include <Windows.h>

extern "C" _declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;

Redline::Engine* engine = nullptr;


int CALLBACK WinMain(HINSTANCE instance, HINSTANCE previous_instance, PSTR command_line, INT show_flag)
{
	engine = new Redline::Engine();

	if (!engine->Initialize())
	{
		// Engine failed to initialize
		MessageBoxA(nullptr, "Engine initialization failed !", "Fatal Error", MB_OK | MB_ICONERROR);
		return 0;
	}

	int return_code = engine->Run();

	delete engine;

	return return_code;
}
