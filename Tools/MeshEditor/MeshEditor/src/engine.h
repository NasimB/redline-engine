#pragma once

#include "renderer.h"
//#include "../game/game.h"
#include "window.h"
#include "light.h"
#include "camera.h"
#include "timer.h"
#include "input.h"
#include "resources.h"
#include "gameobject.h"
#include "config.h"
#include "audio.h"
#include "sound.h"
#include "debug.h"
#include "editor.h"

#include <vector>

#define BUILDTYPE "Alpha-DEV"

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if (p) { (p)->Release(); (p)=nullptr; } }
#endif

namespace Redline
{
	class Engine
	{
	public:
		Engine();
		~Engine();

		bool Initialize();
		bool Shutdown();
		int Run();
		bool Update();

		static GameObject* AddGameObject(const std::string& meshName);
		static void AddGameObject(GameObject* gameObject);
		static void RemoveGameObject(GameObject* gameObject);

		static Light* AddLight(const LightType& lightType);
		static void RemoveLight(Light* light);

		static Sound* AddSound(const std::string& name);
		static void RemoveSound(Sound* sound);

		// Getters
		static Renderer* GetRenderer() { return &m_engine->m_renderer; }
		static Camera* GetCamera() { return &m_engine->m_camera; }
		static Input GetInput() { return m_engine->m_input; }
		static Config GetConfig() { return m_engine->m_config; }
		static Timer* GetTimer() { return &m_engine->m_timer; }
		static Window* GetWindow() { return &m_engine->m_window; }

		static Light* GetLight(size_t index) { return m_engine->m_lights[index]; }
		static std::vector<Light*> GetLights() { return m_engine->m_lights; }

	private:
		static void GetDesktopResolution(int& horizontal, int& vertical);

		Window m_window;
		Timer m_timer;
		Camera m_camera;
		Input m_input;
		Audio m_audio;
		Resources m_resources;
		Debug m_debug;
		Config m_config;
		Renderer m_renderer;

		Editor m_editor;

		bool m_input_pressed;

		std::vector<Sound*> m_sounds;
		std::vector<GameObject*> m_gameobjects;
		std::vector<Light*> m_lights;

		static Engine* m_engine;

		bool complete = false;
	};
}
