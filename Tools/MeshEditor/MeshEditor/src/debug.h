#pragma once

#include <vector>
#include <d3d11.h>
#include <SimpleMath.h>
#include <DirectXMath.h>

namespace Redline
{
	struct DebugLine
	{
		DirectX::XMFLOAT3 startPos;
		DirectX::XMFLOAT3 endPos;
		DirectX::XMFLOAT3 color;
	};

	enum DebugType
	{
		DEBUG_LOG = 0,
		DEBUG_ERROR,
		DEBUG_WARN
	};

	class Debug
	{
	public:
		Debug();
		~Debug();

		static void Log(const std::string& text, bool showOnConsole = true);
		static void Err(const std::string& text, bool showOnConsole = true);
		static void Warn(const std::string& text, bool showOnConsole = true);

		static void DrawConsole();

		static void Line(const DirectX::SimpleMath::Vector3& startPos, const DirectX::SimpleMath::Vector3& endPos, const DirectX::SimpleMath::Vector3& color);

		static void SetMaxSize(int value) { m_debug->maxSize = value; }

		static void SetWireframeMode(int value) { m_debug->wireframeMode = value; }
		static int GetWireframeMode() { return m_debug->wireframeMode; }
		static std::vector<DebugLine> GetDebugLines() { return m_debug->linesArray; }
		static void ClearDebugLines() { return m_debug->linesArray.clear(); }

	private:
		void AddDebugLog(const std::string& text, int type = 0, bool showOnConsole = true);
		std::vector<std::string> m_debugArray;

		static Debug* m_debug;

		int maxSize = 10;
		int wireframeMode = 0;

		std::vector<DebugLine> linesArray;
	};
}
