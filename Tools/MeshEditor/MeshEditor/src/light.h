#pragma once

#include <d3d11.h>
#include <SimpleMath.h>
#include <algorithm>

namespace Redline
{
	enum LightType
	{
		LIGHT_DIRECTIONAL = 0,
		LIGHT_POINT,
		LIGHT_SPOT
	};

	class Light
	{
	public:
		Light();
		explicit Light(LightType type);
		~Light();

		void SetColor(float red, float green, float blue)
		{
			m_color.x = powf(red, 2.2f);
			m_color.y = pow(green, 2.2f);
			m_color.z = pow(blue, 2.2f);
		};
		void SetColor(const DirectX::SimpleMath::Vector3& color) { m_color = color; };
		DirectX::SimpleMath::Vector3 GetColor() const { return m_color; }

		void SetDirection(float x, float y, float z)
		{
			m_direction = DirectX::SimpleMath::Vector3(x, y, z);
			m_direction.Normalize();
		};

		void SetDirection(const DirectX::SimpleMath::Vector3& direction)
		{
			m_direction = direction;
			m_direction.Normalize();
		};
		DirectX::SimpleMath::Vector3 GetDirection() const { return m_direction; }

		void SetPosition(float x, float y, float z)
		{
			m_position.x = x;
			m_position.y = y;
			m_position.z = z;
		};
		void SetPosition(const DirectX::SimpleMath::Vector3& pos) { m_position = pos; };
		DirectX::SimpleMath::Vector3 GetPosition() const { return m_position; }

		void SetIntensity(float value) { m_intensity = (std::max)(value, 0.0f); };
		float GetIntensity() const { return m_intensity; }

		// Set the angle (for spot lights only) (in degrees)
		void SetAngle(float value) { m_angle = (std::max)((std::min)(value, 360.0f), 0.0f); };
		// Returns the angle for the light (for spot lights only)
		float GetAngle() const { return m_angle; }

		void SetType(LightType type) { m_lightType = type; };
		LightType GetType() const { return m_lightType; }

		void SetActive(bool value) { m_enabled = value; };
		bool IsActive() const { return m_enabled; }

		void SetCastShadows(bool value) { m_cast_shadows = value; };
		bool IsCastingShadows() const { return m_cast_shadows; }

	protected:
		DirectX::SimpleMath::Vector3 m_position;
		DirectX::SimpleMath::Vector3 m_direction;
		LightType m_lightType;
		DirectX::SimpleMath::Vector3 m_color;

		float m_angle = 25.0f;
		float m_intensity = 4.0f;

		bool m_enabled = true;
		bool m_cast_shadows = false;
	};
}
