#include "sound.h"

Redline::Sound::Sound()
	: m_file(nullptr),
	  m_chunk_size(0),
	  m_chunk_position(0),
	  m_sound_buffer(nullptr),
	  m_source_voice(nullptr)
{
	ZeroMemory(&m_wave_format, sizeof(WAVEFORMATEXTENSIBLE));
	ZeroMemory(&m_buffer, sizeof(XAUDIO2_BUFFER));
}


Redline::Sound::~Sound()
{
	if (m_file)
		CloseHandle(m_file);

	if (m_source_voice)
	{
		m_source_voice->DestroyVoice();
	}

	delete[] m_sound_buffer;
}


bool Redline::Sound::Initialize(Audio& audio, const std::string& name)
{
	if (audio.GetMasteringVoice() == nullptr)
		return true;

	std::string path = "data\\sounds\\" + name + ".wav";

	m_file = CreateFileA(path.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);

	if (m_file == INVALID_HANDLE_VALUE)
		return false;

	if (!FindChunk(ID_RIFF))
		return false;

	DWORD file_type = 0;

	if (!ReadChunkData(&file_type, sizeof(DWORD)))
		return false;

	if (file_type != ID_WAVE)
		return false;

	if (!FindChunk(ID_FORMAT))
		return false;

	if (!ReadChunkData(&m_wave_format, m_chunk_size))
		return false;

	if (!FindChunk(ID_DATA))
		return false;

	m_sound_buffer = new BYTE[static_cast<size_t>(m_chunk_size)];

	if (!ReadChunkData(m_sound_buffer, m_chunk_size))
		return false;

	m_buffer.AudioBytes = m_chunk_size;
	m_buffer.pAudioData = m_sound_buffer;
	m_buffer.Flags = XAUDIO2_END_OF_STREAM;

	HRESULT result = audio.GetXAudio()->CreateSourceVoice(&m_source_voice, reinterpret_cast<WAVEFORMATEX*>(&m_wave_format));
	if (FAILED(result))
		return false;

	CloseHandle(m_file);
	m_file = nullptr;

	return true;
}


void Redline::Sound::Play(bool loop)
{
	if (m_source_voice)
	{
		Stop();
		m_buffer.PlayBegin = 0;

		m_buffer.LoopCount = loop ? XAUDIO2_LOOP_INFINITE : 0;

		m_source_voice->SubmitSourceBuffer(&m_buffer);
		m_source_voice->Start();
	}
}


void Redline::Sound::Stop() const
{
	if (m_source_voice)
	{
		m_source_voice->Stop();
		m_source_voice->FlushSourceBuffers();
	}
}


void Redline::Sound::SetVolume(float volume) const
{
	if (m_source_voice)
	{
		m_source_voice->SetVolume(volume);
	}
}


float Redline::Sound::GetVolume() const
{
	if (m_source_voice)
	{
		float volume = 0.0f;
		m_source_voice->GetVolume(&volume);
		return volume;
	}
	return 0.0f;
}


bool Redline::Sound::FindChunk(Id id)
{
	LARGE_INTEGER li;
	li.QuadPart = 0;

	if (SetFilePointerEx(m_file, li, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
		return false;

	DWORD chunk_type = 0;
	DWORD chunk_data_size = 0;
	DWORD offset = 0;
	DWORD read = 0;

	while (true)
	{
		if (!ReadFile(m_file, &chunk_type, sizeof(DWORD), &read, nullptr))
			return false;

		if (!ReadFile(m_file, &chunk_data_size, sizeof(DWORD), &read, nullptr))
			return false;

		if (chunk_type == ID_RIFF)
			chunk_data_size = 4;

		li.QuadPart = chunk_data_size;
		if (SetFilePointerEx(m_file, li, nullptr, FILE_CURRENT) == INVALID_SET_FILE_POINTER)
			return false;

		offset += 8;

		if (chunk_type == id)
		{
			m_chunk_size = chunk_data_size;
			m_chunk_position = offset;
			return true;
		}

		offset += chunk_data_size;
	}
}


bool Redline::Sound::ReadChunkData(void* buffer, DWORD buffer_size) const
{
	LARGE_INTEGER li;
	li.QuadPart = m_chunk_position;
	if (SetFilePointerEx(m_file, li, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
		return false;

	DWORD read = 0;
	if (!ReadFile(m_file, buffer, buffer_size, &read, nullptr))
		return false;

	return true;
}
