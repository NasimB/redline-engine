#include "mesh.h"
#include "engine.h"

#include <fstream>
#include "stringtools.h"

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

Redline::Mesh::Mesh()
{
}


Redline::Mesh::~Mesh()
{
	for (SubMesh sub : m_submeshes)
	{
		delete sub.vBuffer;
		delete sub.iBuffer;
	}
}


bool Redline::Mesh::Initialize(const std::vector<SubMesh>& submeshes, const std::vector<Material>& materials)
{
	m_submeshes = submeshes;
	m_materials = materials;
	return true;
}


bool Redline::Mesh::Initialize(const string& filename)
{
	if (StringTools::HasEnding(filename, ".rmf"))
		return LoadRedlineMesh(filename);

	return LoadOtherMeshFile(filename);
}


bool Redline::Mesh::LoadRedlineMesh(const string& filename)
{
	ifstream fileIn(filename, ios_base::binary);

	if (!fileIn.is_open())
		return false;

	MeshHeader header;
	fileIn.read(reinterpret_cast<char*>(&header), sizeof(MeshHeader));

	if (header.fileType[0] != 'R' || header.fileType[1] != 'M' || header.fileType[2] != 'F')
	{
		MessageBoxA(nullptr, "Cannot read mesh file : Filetype is invalid", "Fatal Error", MB_OK | MB_ICONERROR);
		return false;
	}

	if (header.fileType[3] == '3')
	{
		const size_t materialsCount = static_cast<size_t>(header.materialsCount);
		m_materials.reserve(materialsCount);
		for (size_t i = 0; i < materialsCount; ++i)
		{
			MaterialStruct3 mat;
			fileIn.read(reinterpret_cast<char*>(&mat), sizeof(MaterialStruct3));

			string str = "data\\textures\\";
			str += mat.diffuseTextureName;

			if (!StringTools::HasEnding(mat.diffuseTextureName, ".dds") && !StringTools::HasEnding(mat.diffuseTextureName, ".DDS"))
				str += ".dds";

			Texture* diffuseTexture = Resources::GetTexture(str);

			str = "data\\textures\\";
			str += mat.normalTextureName;

			if (!StringTools::HasEnding(mat.normalTextureName, ".dds") && !StringTools::HasEnding(mat.normalTextureName, ".DDS"))
				str += ".dds";

			Texture* normalTexture = Resources::GetTexture(str);

			Material material;
			material.SetName(mat.name);
			material.Initialize(diffuseTexture, normalTexture);
			material.SetDiffuseColor(mat.diffuseColor[0], mat.diffuseColor[1], mat.diffuseColor[2]);
			m_materials.emplace_back(material);
		}

		const size_t objectsCount = static_cast<size_t>(header.objectsCount);
		m_submeshes.reserve(objectsCount);
		for (size_t i = 0; i < objectsCount; ++i)
		{
			SubMesh subMesh;

			unsigned int value = 0;

			fileIn.read(reinterpret_cast<char*>(&value), sizeof(unsigned int));
			const size_t vertexCount = static_cast<size_t>(value);

			fileIn.read(reinterpret_cast<char*>(&value), sizeof(unsigned int));
			const size_t indexCount = static_cast<size_t>(value);

			fileIn.read(reinterpret_cast<char*>(&subMesh.materialID), sizeof(unsigned int));

			const float weight = 1 / static_cast<float>(vertexCount);
			Vector3 avgPos(0, 0, 0);

			vector<Vertex> vertices;
			vertices.reserve(vertexCount);

			struct VertexFile
			{
				float position[3];
				float normal[3];
				float texture[2];
			};

			for (size_t j = 0; j < vertexCount; ++j)
			{
				VertexFile vf;
				fileIn.read(reinterpret_cast<char*>(&vf), sizeof(VertexFile));

				Vertex v;
				const Vector3 position = Vector3(vf.position[0], vf.position[1], -vf.position[2]);
				v.position = static_cast<DirectX::XMFLOAT3>(position);
				v.normal = static_cast<DirectX::XMFLOAT3>(Vector3(vf.normal[0], vf.normal[1], -vf.normal[2]));
				v.texture = static_cast<DirectX::XMFLOAT2>(Vector2(vf.texture[0], vf.texture[1]));
				vertices.emplace_back(v);

				avgPos += position * weight;
				m_boundingSphereRadius = max(m_boundingSphereRadius, position.LengthSquared());
			}


			VertexBuffer* vBuffer = Engine::GetRenderer()->CreateVertexBuffer(vertices);
			subMesh.vBuffer = vBuffer;

			vector<unsigned int> indices(indexCount);
			for (size_t j = 0; j < indexCount; ++j)
			{
				indices[j] = static_cast<unsigned int>(j);
			}

			IndexBuffer* iBuffer = Engine::GetRenderer()->CreateIndexBuffer(indices);
			subMesh.iBuffer = iBuffer;

			subMesh.boundingSphereCenter = avgPos;
			subMesh.boundingSphereRadius = ComputeBoundingSphere(avgPos, vertices);

			m_submeshes.emplace_back(subMesh);
		}

		m_boundingSphereRadius = sqrt(m_boundingSphereRadius);
	}
	else if(header.fileType[3] == '4')
	{
		const size_t materialsCount = static_cast<size_t>(header.materialsCount);
		m_materials.reserve(materialsCount);
		for (size_t i = 0; i < materialsCount; ++i)
		{
			MaterialStruct mat;
			fileIn.read(reinterpret_cast<char*>(&mat), sizeof(MaterialStruct));

			Texture* diffuseTexture = nullptr;
			if (strcmp(mat.albedo_name, "") != 0)
			{
				string str = "data\\textures\\";
				str += mat.albedo_name;

				if (!StringTools::HasEnding(mat.albedo_name, ".dds") && !StringTools::HasEnding(mat.albedo_name, ".DDS"))
					str += ".dds";

				diffuseTexture = Resources::GetTexture(str);
			}

			Texture* normalTexture = nullptr;
			if (strcmp(mat.normal_name, "") != 0)
			{
				string str = "data\\textures\\";
				str += mat.normal_name;

				if (!StringTools::HasEnding(mat.normal_name, ".dds") && !StringTools::HasEnding(mat.normal_name, ".DDS"))
					str += ".dds";

				normalTexture = Resources::GetTexture(str);
			}

			Material material;
			material.Initialize(diffuseTexture, normalTexture);
			material.SetName(mat.material_name);
			material.SetDiffuseColor(mat.albedo_color[0], mat.albedo_color[1], mat.albedo_color[2], true);
			material.SetRoughness(mat.roughness);
			material.SetMetalic(mat.metallic);

			m_materials.emplace_back(material);
		}

		const size_t objectsCount = static_cast<size_t>(header.objectsCount);
		m_submeshes.reserve(objectsCount);
		for (size_t i = 0; i < objectsCount; ++i)
		{
			SubMesh subMesh;

			unsigned int value = 0;

			fileIn.read(reinterpret_cast<char*>(&value), sizeof(unsigned int));
			const size_t vertexCount = static_cast<size_t>(value);

			fileIn.read(reinterpret_cast<char*>(&value), sizeof(unsigned int));
			const size_t indexCount = static_cast<size_t>(value);

			fileIn.read(reinterpret_cast<char*>(&subMesh.materialID), sizeof(unsigned int));

			const float weight = 1 / static_cast<float>(vertexCount);
			Vector3 avgPos(0, 0, 0);

			vector<Vertex> vertices;
			vertices.reserve(vertexCount);

			for (size_t j = 0; j < vertexCount; ++j)
			{
				Vertex vertex;
				fileIn.read(reinterpret_cast<char*>(&vertex), sizeof(Vertex));
				vertices.emplace_back(vertex);

				Vector3 position = vertex.position;
				avgPos += position * weight;
				m_boundingSphereRadius = max(m_boundingSphereRadius, position.LengthSquared());
			}


			VertexBuffer* vBuffer = Engine::GetRenderer()->CreateVertexBuffer(vertices);
			subMesh.vBuffer = vBuffer;

			unsigned int* indices_array = new unsigned int[indexCount];
			fileIn.read(reinterpret_cast<char*>(indices_array), indexCount * sizeof(unsigned int));

			vector<unsigned int> indices;
			indices.insert(indices.end(), &indices_array[0], &indices_array[indexCount]);

			IndexBuffer* iBuffer = Engine::GetRenderer()->CreateIndexBuffer(indices);
			subMesh.iBuffer = iBuffer;

			subMesh.boundingSphereCenter = avgPos;
			subMesh.boundingSphereRadius = ComputeBoundingSphere(avgPos, vertices);

			m_submeshes.emplace_back(subMesh);
		}

		m_boundingSphereRadius = sqrt(m_boundingSphereRadius);
	}

	fileIn.close();

	return true;
}


bool Redline::Mesh::LoadOtherMeshFile(const string& filePath)
{
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(filePath.c_str(), aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_TransformUVCoords | aiProcess_ConvertToLeftHanded);

	// If the import failed, report it
	if (!scene)
	{
		MessageBoxA(NULL, importer.GetErrorString(), "Fatal Error", MB_OK);
		return false;
	}

	for (unsigned int i = 0; i < scene->mNumMeshes; ++i)
	{
		aiMesh* mesh = scene->mMeshes[i];

		vector<Vertex> vertices(mesh->mNumVertices);
		vector<unsigned int> indices;

		const float weight = 1 / static_cast<float>(mesh->mNumVertices);
		Vector3 avgPos(0, 0, 0);

		for (unsigned int j = 0; j < mesh->mNumVertices; ++j)
		{
			// Position
			aiVector3D vec3 = scene->mRootNode->mTransformation * mesh->mVertices[j];
			Vector3 position(vec3.x, vec3.z, vec3.y);
			vertices[j].position = static_cast<XMFLOAT3>(position);

			// Texture Coordinate
			if (mesh->HasTextureCoords(0))
			{
				vec3 = mesh->mTextureCoords[0][j];
				vertices[j].texture = XMFLOAT2(vec3.x, vec3.y);
			}
			else
			{
				vertices[j].texture = XMFLOAT2(0.0f, 0.0f);
			}

			// Normal
			vec3 = mesh->mNormals[j];
			Vector3 normal(vec3.x, vec3.z, vec3.y);
			normal.Normalize();
			vertices[j].normal = static_cast<XMFLOAT3>(normal);

			if (mesh->HasTangentsAndBitangents())
			{
				// Tangent
				vec3 = mesh->mTangents[j];
				Vector3 tangent(vec3.x, vec3.z, vec3.y);
				tangent.Normalize();
				vertices[j].tangent = static_cast<XMFLOAT3>(tangent);

				// Binormal
				vec3 = mesh->mBitangents[j];
				Vector3 binormal(vec3.x, vec3.z, vec3.y);
				binormal.Normalize();
				vertices[j].binormal = static_cast<XMFLOAT3>(binormal);
			}
			else
			{
				vertices[j].tangent = XMFLOAT3(1.0f, 0.0f, 0.0f);
				vertices[j].binormal = XMFLOAT3(0.0f, 0.0f, 1.0f);
			}

			avgPos += position * weight;
			m_boundingSphereRadius = max(m_boundingSphereRadius, position.LengthSquared());
		}

		VertexBuffer* vb = Engine::GetRenderer()->CreateVertexBuffer(vertices);
		if (!vb)
		{
			delete vb;
			importer.FreeScene();
			return false;
		}


		for (unsigned int k = 0; k < mesh->mNumFaces; ++k)
		{
			const unsigned int indicesCount = mesh->mFaces[k].mNumIndices;
			for (unsigned int j = 0; j < indicesCount; ++j)
			{
				indices.emplace_back(mesh->mFaces[k].mIndices[j]);
			}
		}

		IndexBuffer* ib = Engine::GetRenderer()->CreateIndexBuffer(indices);
		if (!ib)
		{
			delete vb;
			delete ib;
			importer.FreeScene();
			return false;
		}

		SubMesh sub;
		sub.materialID = mesh->mMaterialIndex;
		sub.vBuffer = vb;
		sub.iBuffer = ib;
		sub.boundingSphereCenter = avgPos;
		sub.boundingSphereRadius = ComputeBoundingSphere(avgPos, vertices);
		m_submeshes.emplace_back(sub);
	}

	m_boundingSphereRadius = sqrt(m_boundingSphereRadius);


	for (unsigned int i = 0; i < scene->mNumMaterials; ++i)
	{
		aiMaterial* material = scene->mMaterials[i];
		Material mat;
		mat.Initialize(nullptr, nullptr);

		aiColor3D color(0.f, 0.f, 0.f);
		material->Get(AI_MATKEY_COLOR_DIFFUSE, color);
		mat.SetDiffuseColor(abs(color.r), abs(color.g), abs(color.b));

		aiString name;
		material->Get(AI_MATKEY_NAME, name);
		mat.SetName(name.C_Str());

		if (material->GetTextureCount(aiTextureType_DIFFUSE) > 0)
		{
			aiString path;
			material->GetTexture(aiTextureType_DIFFUSE, 0, &path);

			Texture* texture = Resources::GetTexture(string(path.data));
			mat.SetDiffuseTexture(texture);
		}

		if (material->GetTextureCount(aiTextureType_NORMALS) > 0)
		{
			aiString path;
			material->GetTexture(aiTextureType_NORMALS, 0, &path);

			Texture* texture = Resources::GetTexture(string(path.data));
			mat.SetNormalTexture(texture);
		}

		m_materials.emplace_back(mat);
	}

	importer.FreeScene();
	return true;
}


float Redline::Mesh::ComputeBoundingSphere(const Vector3& avgPos, const vector<Vertex>& vertices) const
{
	float radius = 0.0f;

	const size_t verticesCount = vertices.size();
	for (size_t i = 0; i < verticesCount; ++i)
	{
		radius = max(radius, Vector3::DistanceSquared(vertices[i].position, avgPos));
	}

	return sqrt(radius);
}
