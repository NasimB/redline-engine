#pragma once

#include <d3d11.h>
#include <SimpleMath.h>

#define DEG2RAD M_PI / 180
#define RAD2DEG 180 / M_PI

namespace Redline
{
	class Transform
	{
	public:
		Transform();
		~Transform();

		// Position
		void SetPosition(const DirectX::SimpleMath::Vector3& pos)
		{
			m_position = pos;
			isPosDirty = true;
		}

		void SetPosition(DirectX::SimpleMath::Vector3* pos)
		{
			m_position = *pos;
			isPosDirty = true;
		}

		void SetPosition(float x, float y, float z)
		{
			m_position.x = x;
			m_position.y = y;
			m_position.z = z;
			isPosDirty = true;
		}

		void Translate(const DirectX::SimpleMath::Vector3& pos)
		{
			m_position += pos;
			isPosDirty = true;
		}

		void Translate(DirectX::SimpleMath::Vector3* pos)
		{
			m_position += *pos;
			isPosDirty = true;
		}

		void Translate(float x, float y, float z)
		{
			m_position.x += x;
			m_position.y += y;
			m_position.z += z;
			isPosDirty = true;
		}

		DirectX::SimpleMath::Vector3 GetPosition() const { return m_position; }

		// Scale
		void SetScale(const DirectX::SimpleMath::Vector3& scale)
		{
			m_scale = scale;
			isScaleDirty = true;
		}

		void SetScale(DirectX::SimpleMath::Vector3* scale)
		{
			m_scale = *scale;
			isScaleDirty = true;
		}

		void SetScale(float x, float y, float z)
		{
			m_scale.x = x;
			m_scale.y = y;
			m_scale.z = z;
			isScaleDirty = true;
		}

		void SetScale(float scale)
		{
			m_scale.x = m_scale.y = m_scale.z = scale;
			isScaleDirty = true;
		}

		void ScaleBy(float scale)
		{
			m_scale.x *= scale;
			m_scale.y *= scale;
			m_scale.z *= scale;
			isScaleDirty = true;
		}

		DirectX::SimpleMath::Vector3 GetScale() const { return m_scale; }

		// Rotation
		void SetRotation(const DirectX::SimpleMath::Quaternion& rotation)
		{
			m_rotation = rotation;
			isRotDirty = true;
		}

		// Sets rotation (in radians)
		void SetRotation(const DirectX::SimpleMath::Vector3& rotation)
		{
			m_rotation = DirectX::SimpleMath::Quaternion::CreateFromYawPitchRoll(rotation.y, rotation.x, rotation.z);
			isRotDirty = true;
		}

		// Sets rotation (in radians)
		void SetRotation(DirectX::SimpleMath::Vector3* rotation)
		{
			m_rotation = DirectX::SimpleMath::Quaternion::CreateFromYawPitchRoll(rotation->y, rotation->x, rotation->z);
			isRotDirty = true;
		}

		// Sets rotation (in radians)
		void SetRotation(float x, float y, float z)
		{
			m_rotation = DirectX::SimpleMath::Quaternion::CreateFromYawPitchRoll(y, x, z);
			isRotDirty = true;
		}

		void SetRotation(float w, float x, float y, float z)
		{
			m_rotation.w = w;
			m_rotation.x = x;
			m_rotation.y = y;
			m_rotation.z = z;
			isRotDirty = true;
		}

		DirectX::SimpleMath::Quaternion GetRotation() const { return m_rotation; }

		// Matrix
		DirectX::SimpleMath::Matrix GetMatrix() const { return m_matrix; };

		void RebuildMatrix();

		bool useMatrix = false;
		bool showGizmo = false;

	private:
		DirectX::SimpleMath::Vector3 m_scale;
		DirectX::SimpleMath::Vector3 m_position;
		DirectX::SimpleMath::Vector3 m_localposition;
		DirectX::SimpleMath::Quaternion m_rotation;
		DirectX::SimpleMath::Quaternion m_localrotation;

		DirectX::SimpleMath::Matrix m_positionMatrix;
		DirectX::SimpleMath::Matrix m_rotationMatrix;
		DirectX::SimpleMath::Matrix m_scaleMatrix;
		DirectX::SimpleMath::Matrix m_matrix;

		bool isPosDirty;
		bool isRotDirty;
		bool isScaleDirty;
	};
}
