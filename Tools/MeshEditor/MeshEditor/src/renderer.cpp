#include "renderer.h"
#include "engine.h"
#include "../../../Libraries/Imgui/imgui.h"
#include "../../../Libraries/Imgui/imgui_impl_dx11.h"
#include "debug.h"
#include "mesh.h"

using namespace Microsoft::WRL;
using namespace std;
using namespace DirectX::SimpleMath;

Redline::Renderer::Renderer() :
	m_vsync_enabled(false),
	m_window_width(1280),
	m_window_height(720),
	m_width(1280),
	m_height(720),
	m_device(nullptr),
	m_deviceContext(nullptr),
	m_swapChain(nullptr),
	m_renderTargetView(nullptr),
	m_rasterizerState(nullptr),
	m_linearSampler(nullptr),
	m_constants_buffer(nullptr),
	m_bufferLights(nullptr),
	m_bufferMatrix(nullptr),
	m_bufferMaterial(nullptr),
	m_bufferVSSkybox(nullptr),
	m_bufferPSSkybox(nullptr),
	m_skybox(nullptr),
	m_wireframeShader(nullptr),
	m_blurHShader(nullptr),
	m_blurVShader(nullptr),
	m_unlit_shader(nullptr),
	m_depthStencil(nullptr),
	m_depthStencilView(nullptr),
	m_depthStencilState(nullptr),
	m_depthDisabledStencilState(nullptr),
	m_pointSampler(nullptr),
	m_GBufferShader(nullptr),
	m_deferredShader(nullptr),
	m_environmentDepthStencil(nullptr),
	m_environmentDepthStencilView(nullptr),
	m_environment_cubemap(nullptr),
	m_environment_cubemap_filtered(nullptr),
	m_environement_filtering_shader(nullptr),
	m_env_filtering_buffer(nullptr),
	m_simple_shader(nullptr),
	m_shadowShader(nullptr),
	m_shadowMapDepth(nullptr),
	m_shadowMapDepthView(nullptr),
	m_bufferLightMatrix(nullptr),
	m_shadowSampler(nullptr),
	m_postProcessRT_A(nullptr),
	m_postProcessRT_B(nullptr),
	m_linear_clamp_sampler(nullptr),
	m_fog_shader(nullptr),
	m_ssaoRT(nullptr),
	m_ssaoBlurRT(nullptr),
	m_ssaoShader(nullptr),
	m_ssaoBlurXShader(nullptr),
	m_ssaoBlurYShader(nullptr),
	m_bufferSSAO(nullptr),
	m_ssaoWidth(1280),
	m_ssaoHeight(720),
	m_tonemapped_RT(nullptr),
	m_bloomBlur_A_RT(nullptr),
	m_bloomBlur_B_RT(nullptr),
	m_combine_shader(nullptr),
	m_bloomWidth(1280),
	m_bloomHeight(720),
	m_exposure_A_RT(nullptr),
	m_exposure_B_RT(nullptr),
	m_adaptation_shader(nullptr),
	m_luminance_buffer(nullptr),
	m_use_A_buffer(false),
	m_draw_call_count(0)
{
}


Redline::Renderer::~Renderer()
{
	m_device.Reset();

	m_deviceContext->ClearState();
	m_deviceContext.Reset();

	m_renderTargetView.Reset();

	m_swapChain.Reset();

	ImGui_ImplDX11_Shutdown();

	SAFE_RELEASE(m_linear_clamp_sampler);



	ShutdownEnvironement();

	ShutdownTonemapping();

	ShutdownBloom();

	ShutdownSSAO();

	ShutdownVolumetricFog();

	if (m_postProcessRT_A)
	{
		delete m_postProcessRT_A;
		m_postProcessRT_A = nullptr;
	}

	if (m_postProcessRT_B)
	{
		delete m_postProcessRT_B;
		m_postProcessRT_B = nullptr;
	}


	SAFE_RELEASE(m_shadowMapDepthView);
	SAFE_RELEASE(m_shadowMapDepth);

	for (unsigned int i = 0; i < NUM_CASCADES; ++i)
	{
		delete m_shadowMapsRenderTargets[i];
		m_shadowMapsRenderTargets[i] = nullptr;
	}

	if (m_shadowShader)
	{
		delete m_shadowShader;
		m_shadowShader = nullptr;
	}

	SAFE_RELEASE(m_pointSampler);
	SAFE_RELEASE(m_depthDisabledStencilState);
	SAFE_RELEASE(m_depthStencilState);
	SAFE_RELEASE(m_depthStencilView);
	SAFE_RELEASE(m_depthStencil);

	for (unsigned int i = 0; i < BUFFER_COUNT; i++)
	{
		delete m_renderTargets[i];
		m_renderTargets[i] = nullptr;
	}

	m_rasterizerState.Reset();
	m_linearSampler.Reset();
	SAFE_RELEASE(m_shadowSampler);
	SAFE_RELEASE(m_constants_buffer);
	SAFE_RELEASE(m_bufferMatrix);
	SAFE_RELEASE(m_bufferVSSkybox);
	SAFE_RELEASE(m_bufferPSSkybox);
	SAFE_RELEASE(m_bufferMaterial);
	SAFE_RELEASE(m_bufferLights);

	if (m_GBufferShader)
	{
		delete m_GBufferShader;
		m_GBufferShader = nullptr;
	}

	if (m_wireframeShader)
	{
		delete m_wireframeShader;
		m_wireframeShader = nullptr;
	}

	if (m_deferredShader)
	{
		delete m_deferredShader;
		m_deferredShader = nullptr;
	}

	if (m_skybox)
	{
		delete m_skybox;
		m_skybox = nullptr;
	}

#ifdef _DEBUG
	m_d3dDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
	m_d3dDebug.Reset();
#endif // _DEBUG
}


bool Redline::Renderer::Initialize(HWND hwnd, bool vSync)
{
	m_vsync_enabled = vSync;

	RECT rect;
	GetClientRect(hwnd, &rect);

	m_window_width = m_width = rect.right - rect.left;
	m_window_height = m_height = rect.bottom - rect.top;

	m_renderScale = Engine::GetConfig().render_scale;

	HRESULT hr = S_OK;

	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		m_driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDevice(nullptr, m_driverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, m_device.ReleaseAndGetAddressOf(), &m_featureLevel, m_deviceContext.ReleaseAndGetAddressOf());

		if (hr == E_INVALIDARG)
		{
			// DirectX 11.0 platforms will not recognize D3D_FEATURE_LEVEL_11_1 so we need to retry without it
			hr = D3D11CreateDevice(nullptr, m_driverType, nullptr, createDeviceFlags, &featureLevels[1], numFeatureLevels - 1,
				D3D11_SDK_VERSION, m_device.ReleaseAndGetAddressOf(), &m_featureLevel, m_deviceContext.ReleaseAndGetAddressOf());
		}

		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
	{
		Debug::Err("Failed to create rendering device!");
		return false;
	}

	// Obtain DXGI factory from device (since we used nullptr for pAdapter above)
	IDXGIFactory1* dxgiFactory = nullptr;
	{
		ComPtr<IDXGIDevice> dxgiDevice = nullptr;
		hr = m_device.As(&dxgiDevice);
		if (SUCCEEDED(hr))
		{
			IDXGIAdapter* adapter = nullptr;
			hr = dxgiDevice->GetAdapter(&adapter);
			if (SUCCEEDED(hr))
			{
				hr = adapter->GetParent(__uuidof(IDXGIFactory1), reinterpret_cast<void**>(&dxgiFactory));
				adapter->Release();
			}
			dxgiDevice.Reset();
		}
	}

	if (FAILED(hr))
	{
		Debug::Err("Failed to obtain the DXGI factory!");
		return false;
	}

	UINT numerator = 0;
	UINT denominator = 0;
	DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM;
	{
		IDXGIAdapter* adapter;
		// Use the factory to create an adapter for the primary graphics interface (video card).
		hr = dxgiFactory->EnumAdapters(0, &adapter);
		if (FAILED(hr))
		{
			return false;
		}

		IDXGIOutput* adapterOutput;
		// Enumerate the primary adapter output (monitor).
		hr = adapter->EnumOutputs(0, &adapterOutput);
		if (FAILED(hr))
		{
			return false;
		}

		UINT numModes;
		// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
		hr = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
		if (FAILED(hr))
		{
			return false;
		}

		// Create a list to hold all the possible display modes for this monitor/video card combination.
		DXGI_MODE_DESC * displayModeList = new DXGI_MODE_DESC[numModes];
		if (!displayModeList)
		{
			return false;
		}

		// Now fill the display mode list structures.
		hr = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
		if (FAILED(hr))
		{
			return false;
		}

		// Now go through all the display modes and find the one that matches the screen width and height.
		// When a match is found store the numerator and denominator of the refresh rate for that monitor.
		UINT width = static_cast<UINT>(m_window_width);
		UINT height = static_cast<UINT>(m_window_height);
		for (UINT i = 0; i < numModes; i++)
		{
			if (displayModeList[i].Width == width && displayModeList[i].Height == height)
			{
				if (displayModeList[i].RefreshRate.Numerator > numerator)
				{
					numerator = displayModeList[i].RefreshRate.Numerator;
					denominator = displayModeList[i].RefreshRate.Denominator;
					format = displayModeList[i].Format;
				}
			}
		}
	}

#ifdef _DEBUG
	m_device.As(&m_d3dDebug);
#endif // _DEBUG

	// DirectX 11.0 systems
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = m_width;
	sd.BufferDesc.Height = m_height;
	sd.BufferDesc.Format = format;
	sd.BufferDesc.RefreshRate.Numerator = numerator;
	sd.BufferDesc.RefreshRate.Denominator = denominator;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = hwnd;
	sd.SampleDesc.Count = m_sampleCount;
	sd.SampleDesc.Quality = m_sampleQuality;
	sd.Windowed = true;

	hr = dxgiFactory->CreateSwapChain(m_device.Get(), &sd, m_swapChain.ReleaseAndGetAddressOf());

	dxgiFactory->MakeWindowAssociation(hwnd, 0/*DXGI_MWA_NO_ALT_ENTER*/);

	dxgiFactory->Release();

	if (FAILED(hr))
	{
		Debug::Err("Failed to create the swap chain!");
		return false;
	}

	m_deviceContext->OMSetRenderTargets(0, nullptr, nullptr);

	ID3D11Texture2D* pBackBuffer = nullptr;

	if (FAILED(m_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer))))
		return false;

	hr = m_device->CreateRenderTargetView(pBackBuffer, nullptr, m_renderTargetView.GetAddressOf());
	pBackBuffer->Release();

	if (FAILED(hr))
	{
		Debug::Err("Failed to create the render target view!");
		return false;
	}

	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	hr = m_device->CreateRasterizerState(&rasterDesc, m_rasterizerState.ReleaseAndGetAddressOf());
	if (FAILED(hr))
	{
		Debug::Err("Failed to create the rasterizer state!");
		return false;
	}

	m_deviceContext->RSSetState(m_rasterizerState.Get());

	m_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	const int filterMode = Engine::GetConfig().texture_filtering;
	const int minLOD = Engine::GetConfig().min_tex_lod;
	const int maxLOD = Engine::GetConfig().max_tex_lod;
	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));

	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;

	if (filterMode == 2 || filterMode == 4 || filterMode == 8 || filterMode == 16) // Enable ansiotropic filtering
	{
		sampDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		sampDesc.MaxAnisotropy = filterMode;
	}

	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = static_cast<FLOAT>(minLOD);
	sampDesc.MaxLOD = static_cast<FLOAT>(maxLOD);
	hr = m_device->CreateSamplerState(&sampDesc, m_linearSampler.GetAddressOf());
	if (FAILED(hr))
	{
		Debug::Err("Failed to create the linear sampler!");
		return false;
	}


	m_deferredShader = new Shader();
	if (!m_deferredShader->CreateFromFile("data\\shaders\\default.fx", m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile default shader!");
		return false;
	}

	// Create buffers
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	// Create constant buffer
	bd.ByteWidth = sizeof(ConstantBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_constants_buffer)))
	{
		Debug::Err("Failed to create the constant buffer!");
		return false;
	}

	// Create matrix buffer 
	bd.ByteWidth = sizeof(MatrixBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_bufferMatrix)))
	{
		Debug::Err("Failed to create the matrix buffer!");
		return false;
	}

	// Create material buffer 
	bd.ByteWidth = sizeof(MaterialBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_bufferMaterial)))
	{
		Debug::Err("Failed to create the material buffer!");
		return false;
	}

	// Create material buffer 
	bd.ByteWidth = sizeof(LightBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_bufferLights)))
	{
		Debug::Err("Failed to create light buffer!");
		return false;
	}

	m_width = max(static_cast<int>(m_width * m_renderScale), 1);
	m_height = max(static_cast<int>(m_height * m_renderScale), 1);

	if (!InitializeGBuffers())
	{
		Debug::Err("Failed to initialize the GBuffers!");
		return false;
	}

	m_shadowEnabled = Engine::GetConfig().shadow_res > 0;
	if (m_shadowEnabled)
	{
		if (!InitializeShadowMap())
		{
			Debug::Err("Failed to initialize the shadow maps!");
			return false;
		}
	}

	m_wireframeShader = new Shader();
	if (!m_wireframeShader->CreateFromFile("data\\shaders\\wireframe.fx", m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile the wireframe shader!");
		return false;
	}

	m_blurHShader = new Shader();
	if (!m_blurHShader->CreateFromFile("data\\shaders\\blurH.fx", m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile the horizontal blur shader!");
		return false;
	}

	m_blurVShader = new Shader();
	if (!m_blurVShader->CreateFromFile("data\\shaders\\blurV.fx", m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile the vertical blur shader!");
		return false;
	}

	m_unlit_shader = new Shader();
	if (!m_unlit_shader->CreateFromFile("data\\shaders\\unlit.fx", m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile the unlit shader!");
		return false;
	}

	if (!InitializePostProcess())
	{
		Debug::Err("Failed to initialize post process!");
		return false;
	}

	if (!InitializeEnvironement())
	{
		Debug::Err("Failed to initialize environement reflections!");
		return false;
	}

	if (!ImGui_ImplDX11_Init(hwnd, m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to initialize Dear Imgui!");
		return false;
	}

	m_deviceContext->IASetInputLayout(m_GBufferShader->GetInputLayout());

	/*m_skyboxTexture = new Texture();
	if (!m_skyboxTexture->CreateFromFile("data\\textures\\skybox.dds", m_device.Get()))
	{
		Debug::Err("Failed to load the skybox texture!");
		return false;
	}*/
	return true;
}


bool Redline::Renderer::InitializeSkybox()
{
	m_skybox = new Skybox();
	m_skybox->Initialize(Engine::GetCamera());

	// Create constant buffer 
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	bd.ByteWidth = sizeof(SkyboxVSBuffer);
	// Create vertex shader buffer 
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_bufferVSSkybox)))
		return false;

	bd.ByteWidth = sizeof(SkyboxPSBuffer);
	// Create pixel shader buffer 
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_bufferPSSkybox)))
		return false;

	m_deviceContext->UpdateSubresource(m_bufferVSSkybox, 0, nullptr, &m_skybox->GetVSBuffer(), 0, 0);
	m_deviceContext->UpdateSubresource(m_bufferPSSkybox, 0, nullptr, &m_skybox->GetPSBuffer(), 0, 0);
	return true;
}


bool Redline::Renderer::InitializeGBuffers()
{
	// POSITION BUFFER
	m_renderTargets[0] = new RenderTarget();
	if (!m_renderTargets[0]->Initialize(m_device.Get(), m_width, m_height, DXGI_FORMAT_R16G16B16A16_FLOAT, true))
	{
		return false;
	}

	// NORMAL BUFFER (and metalic in alpha channel)
	m_renderTargets[1] = new RenderTarget();
	if (!m_renderTargets[1]->Initialize(m_device.Get(), m_width, m_height, DXGI_FORMAT_R16G16B16A16_FLOAT, true))
	{
		return false;
	}

	// COLOR BUFFER (and roughness in alpha channel)
	m_renderTargets[2] = new RenderTarget();
	if (!m_renderTargets[2]->Initialize(m_device.Get(), m_width, m_height, DXGI_FORMAT_B8G8R8A8_UNORM_SRGB, true))
	{
		return false;
	}

	// Set up the description of the depth buffer
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = m_width;
	depthBufferDesc.Height = m_height;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	// Create the texture for the depth buffer using the filled out description
	if (FAILED(m_device->CreateTexture2D(&depthBufferDesc, NULL, &m_depthStencil)))
	{
		return false;
	}

	// Set up the depth stencil view description
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	// Create the depth stencil view
	if (FAILED(m_device->CreateDepthStencilView(m_depthStencil, &depthStencilViewDesc, &m_depthStencilView)))
	{
		return false;
	}

	// Setup the viewport for rendering.
	m_viewport.Width = static_cast<float>(m_width);
	m_viewport.Height = static_cast<float>(m_height);
	m_viewport.MinDepth = 0.0f;
	m_viewport.MaxDepth = 1.0f;
	m_viewport.TopLeftX = 0.0f;
	m_viewport.TopLeftY = 0.0f;

	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

	if (FAILED(m_device->CreateSamplerState(&sampDesc, &m_pointSampler)))
		return false;

	m_GBufferShader = new Shader();
	if (!m_GBufferShader->CreateFromFile("data\\shaders\\deferred.fx", m_device.Get(), m_deviceContext.Get()))
		return false;


	// Set up the description of the stencil state.
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create the depth stencil state
	if (FAILED(m_device->CreateDepthStencilState(&depthStencilDesc, &m_depthStencilState)))
	{
		return false;
	}

	// Set the depth stencil state.
	m_deviceContext->OMSetDepthStencilState(m_depthStencilState, 1);

	depthStencilDesc.DepthEnable = false;
	depthStencilDesc.StencilEnable = false;
	// Create the depth stencil state
	if (FAILED(m_device->CreateDepthStencilState(&depthStencilDesc, &m_depthDisabledStencilState)))
	{
		return false;
	}

	return true;
}


bool Redline::Renderer::InitializeShadowMap()
{
	const int resolution = Engine::GetConfig().shadow_res;
	for (unsigned int i = 0; i < NUM_CASCADES; ++i)
	{
		m_shadowMapsRenderTargets[i] = new RenderTarget();
		if (!m_shadowMapsRenderTargets[i]->Initialize(m_device.Get(), resolution, resolution, DXGI_FORMAT_R32_FLOAT, true))
		{
			Debug::Err("Failed to create a shadow map render target! (index " + to_string(i) + ")");
			return false;
		}
	}

	// Set up the description of the depth buffer
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = resolution;
	depthBufferDesc.Height = resolution;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	// Create the texture for the depth buffer using the filled out description
	if (FAILED(m_device->CreateTexture2D(&depthBufferDesc, NULL, &m_shadowMapDepth)))
	{
		Debug::Err("Failed to create the shadow map depth buffer!");
		return false;
	}

	// Set up the depth stencil view description
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = depthBufferDesc.Format;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	// Create the depth stencil view
	if (FAILED(m_device->CreateDepthStencilView(m_shadowMapDepth, &depthStencilViewDesc, &m_shadowMapDepthView)))
	{
		Debug::Err("Failed to create the shadow map depth stencil view!");
		return false;
	}


	m_shadowShader = new Shader();
	if (!m_shadowShader->CreateFromFile("data\\shaders\\shadows.fx", m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile the shadow shader!");
		return false;
	}


	m_shadowViewport.Width = static_cast<float>(resolution);
	m_shadowViewport.Height = static_cast<float>(resolution);
	m_shadowViewport.MinDepth = 0.0f;
	m_shadowViewport.MaxDepth = 1.0f;
	m_shadowViewport.TopLeftX = 0.0f;
	m_shadowViewport.TopLeftY = 0.0f;


	// Create buffers
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	// Create constant buffer
	bd.ByteWidth = sizeof(ShadowsBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_bufferLightMatrix)))
	{
		Debug::Err("Failed to create the shadow buffer!");
		return false;
	}


	const int filterMode = Engine::GetConfig().texture_filtering;
	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));

	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;

	if (filterMode == 2 || filterMode == 4 || filterMode == 8 || filterMode == 16) // Enable ansiotropic filtering
	{
		sampDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		sampDesc.MaxAnisotropy = filterMode;
	}

	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = 1;

	if (FAILED(m_device->CreateSamplerState(&sampDesc, &m_shadowSampler)))
	{
		Debug::Err("Failed to create the shadow sampler!");
		return false;
	}

	return true;
}


bool Redline::Renderer::InitializePostProcess()
{
	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = 100;

	if (FAILED(m_device->CreateSamplerState(&sampDesc, &m_linear_clamp_sampler)))
	{
		Debug::Err("Failed to create the linear clamp sampler!");
		return false;
	}


	m_postProcessRT_A = new RenderTarget();
	if (!m_postProcessRT_A->Initialize(m_device.Get(), m_width, m_height, DXGI_FORMAT_R16G16B16A16_UNORM, true))
	{
		Debug::Err("Failed to create a post process render target!");
		return false;
	}

	m_postProcessRT_B = new RenderTarget();
	if (!m_postProcessRT_B->Initialize(m_device.Get(), m_width, m_height, DXGI_FORMAT_R16G16B16A16_UNORM, true))
	{
		Debug::Err("Failed to create a post process render target!");
		return false;
	}


	if (!InitializeSSAO())
		return false;

	if (!InitializeBloom())
		return false;

	if (!InitializeTonemapping())
		return false;

	if (!InitializeVolumetricFog())
		return false;

	return true;
}


bool Redline::Renderer::InitializeSSAO()
{
	const int ssao_technique = Engine::GetConfig().ssao_technique;
	m_ssaoEnabled = ssao_technique > 0;

	m_ssaoWidth = static_cast<float>(m_width) * m_ssaoRenderScale;
	m_ssaoHeight = static_cast<float>(m_height) * m_ssaoRenderScale;
	const unsigned int ssaoWidth = static_cast<unsigned int>(m_ssaoWidth);
	const unsigned int ssaoHeight = static_cast<unsigned int>(m_ssaoHeight);

	m_ssaoRT = new RenderTarget();
	if (!m_ssaoRT->Initialize(m_device.Get(), ssaoWidth, ssaoHeight, DXGI_FORMAT_R16G16B16A16_UNORM, true))
	{
		Debug::Err("Failed to create the SSAO render target!");
		return false;
	}

	if (!m_ssaoEnabled)
		return true;

	const string path = ssao_technique == 1 ? "data\\shaders\\ssao.fx" : "data\\shaders\\mxao.fx";

	m_ssaoBlurRT = new RenderTarget();
	if (!m_ssaoBlurRT->Initialize(m_device.Get(), ssaoWidth, ssaoHeight, DXGI_FORMAT_R16G16B16A16_UNORM, true))
	{
		Debug::Err("Failed to create the SSAO blur render target!");
		return false;
	}

	m_ssaoShader = new Shader();
	if (!m_ssaoShader->CreateFromFile(path, m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile the SSAO shader!");
		return false;
	}

	m_ssaoBlurXShader = new Shader();
	if (!m_ssaoBlurXShader->CreateFromFile(path, m_device.Get(), m_deviceContext.Get(), "VS", "PS_BlurX"))
	{
		Debug::Err("Failed to compile the SSAO Blur X shader!");
		return false;
	}

	m_ssaoBlurYShader = new Shader();
	if (!m_ssaoBlurYShader->CreateFromFile(path, m_device.Get(), m_deviceContext.Get(), "VS", "PS_BlurY"))
	{
		Debug::Err("Failed to compile the SSAO Blur Y shader!");
		return false;
	}

	// Create SSAO buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(SSAOBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_bufferSSAO)))
	{
		Debug::Err("Failed to create the SSAO buffer!");
		return false;
	}

	return true;
}


void Redline::Renderer::ShutdownSSAO()
{
	SAFE_RELEASE(m_bufferSSAO);

	if (m_ssaoShader)
	{
		delete m_ssaoShader;
		m_ssaoShader = nullptr;
	}

	if (m_ssaoBlurXShader)
	{
		delete m_ssaoBlurXShader;
		m_ssaoBlurXShader = nullptr;
	}

	if (m_ssaoBlurYShader)
	{
		delete m_ssaoBlurYShader;
		m_ssaoBlurYShader = nullptr;
	}

	if (m_ssaoBlurRT)
	{
		delete m_ssaoBlurRT;
		m_ssaoBlurRT = nullptr;
	}

	if (m_ssaoRT)
	{
		delete m_ssaoRT;
		m_ssaoRT = nullptr;
	}
}


bool Redline::Renderer::InitializeBloom()
{
	m_bloomWidth = static_cast<float>(m_width) * m_bloomRenderScale;
	m_bloomHeight = static_cast<float>(m_height) * m_bloomRenderScale;
	const unsigned int bloomWidth = static_cast<unsigned int>(m_bloomWidth);
	const unsigned int bloomHeight = static_cast<unsigned int>(m_bloomHeight);

	m_bloomBlur_A_RT = new RenderTarget();
	if (!m_bloomBlur_A_RT->Initialize(m_device.Get(), bloomWidth, bloomHeight, DXGI_FORMAT_R16G16B16A16_UNORM))
	{
		Debug::Err("Failed to create the bloom blur render target!");
		return false;
	}

	m_bloomBlur_B_RT = new RenderTarget();
	if (!m_bloomBlur_B_RT->Initialize(m_device.Get(), bloomWidth, bloomHeight, DXGI_FORMAT_R16G16B16A16_UNORM))
	{
		Debug::Err("Failed to create the bloom blur render target!");
		return false;
	}

	return true;
}


void Redline::Renderer::ShutdownBloom()
{
	if (m_bloomBlur_B_RT)
	{
		delete m_bloomBlur_B_RT;
		m_bloomBlur_B_RT = nullptr;
	}

	if (m_bloomBlur_A_RT)
	{
		delete m_bloomBlur_A_RT;
		m_bloomBlur_A_RT = nullptr;
	}
}


bool Redline::Renderer::InitializeTonemapping()
{
	m_combine_shader = new Shader();
	if (!m_combine_shader->CreateFromFile("data\\shaders\\tonemapping.fx", m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile the bloom combine shader!");
		return false;
	}

	m_adaptation_shader = new Shader();
	if (!m_adaptation_shader->CreateFromFile("data\\shaders\\tonemapping.fx", m_device.Get(), m_deviceContext.Get(), "VS", "PS_LuminanceAdaptation"))
	{
		Debug::Err("Failed to compile the exposure adaptation shader!");
		return false;
	}

	// Create luminance buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(LuminanceBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, &m_luminance_buffer)))
	{
		Debug::Err("Failed to create the tonemapping buffer!");
		return false;
	}

	m_tonemapped_RT = new RenderTarget();
	if (!m_tonemapped_RT->Initialize(m_device.Get(), m_width, m_height, DXGI_FORMAT_R16G16B16A16_UNORM, true))
	{
		Debug::Err("Failed to create the tonemapped render target!");
		return false;
	}

	m_exposure_A_RT = new RenderTarget();
	if (!m_exposure_A_RT->Initialize(m_device.Get(), 1, 1, DXGI_FORMAT_R16_FLOAT))
	{
		Debug::Err("Failed to create the exposure render target!");
		return false;
	}

	m_exposure_B_RT = new RenderTarget();
	if (!m_exposure_B_RT->Initialize(m_device.Get(), 1, 1, DXGI_FORMAT_R16_FLOAT))
	{
		Debug::Err("Failed to create the exposure render target!");
		return false;
	}

	return true;
}


void Redline::Renderer::ShutdownTonemapping()
{
	SAFE_RELEASE(m_luminance_buffer);

	if (m_tonemapped_RT)
	{
		delete m_tonemapped_RT;
		m_tonemapped_RT = nullptr;
	}

	if (m_exposure_A_RT)
	{
		delete m_exposure_A_RT;
		m_exposure_A_RT = nullptr;
	}

	if (m_exposure_B_RT)
	{
		delete m_exposure_B_RT;
		m_exposure_B_RT = nullptr;
	}

	if (m_combine_shader)
	{
		delete m_combine_shader;
		m_combine_shader = nullptr;
	}

	if (m_adaptation_shader)
	{
		delete m_adaptation_shader;
		m_adaptation_shader = nullptr;
	}
}


bool Redline::Renderer::InitializeVolumetricFog()
{
	m_fog_shader = new Shader();
	if (!m_fog_shader->CreateFromFile("data\\shaders\\fog.fx", m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile the fog shader!");
		return false;
	}

	return true;
}


void Redline::Renderer::ShutdownVolumetricFog()
{
	if (m_fog_shader)
	{
		delete m_fog_shader;
		m_fog_shader = nullptr;
	}
}


bool Redline::Renderer::InitializeEnvironement()
{
	const int cubemap_res = max(Engine::GetConfig().cubemap_res, 16);
	m_environment_cubemap = new RenderTarget();
	if (!m_environment_cubemap->Initialize(m_device.Get(), cubemap_res, cubemap_res, DXGI_FORMAT_R10G10B10A2_UNORM, true, 6))
	{
		Debug::Err("Failed to create the environement cubemap!");
		return false;
	}

	m_environment_cubemap_filtered = new RenderTarget();
	if (!m_environment_cubemap_filtered->Initialize(m_device.Get(), cubemap_res, cubemap_res, DXGI_FORMAT_R10G10B10A2_UNORM, true, 6))
	{
		Debug::Err("Failed to create the environement filtering cubemap!");
		return false;
	}

	// Set up the description of the depth buffer
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = cubemap_res;
	depthBufferDesc.Height = cubemap_res;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	// Create the texture for the depth buffer using the filled out description
	if (FAILED(m_device->CreateTexture2D(&depthBufferDesc, NULL, &m_environmentDepthStencil)))
	{
		Debug::Err("Failed to create the environement depth buffer!");
		return false;
	}

	// Set up the depth stencil view description
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = depthBufferDesc.Format;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	// Create the depth stencil view
	if (FAILED(m_device->CreateDepthStencilView(m_environmentDepthStencil, &depthStencilViewDesc, &m_environmentDepthStencilView)))
	{
		Debug::Err("Failed to create the environement depth stencil view!");
		return false;
	}

	m_environmentViewport.Width = static_cast<float>(cubemap_res);
	m_environmentViewport.Height = static_cast<float>(cubemap_res);
	m_environmentViewport.MinDepth = 0.0f;
	m_environmentViewport.MaxDepth = 1.0f;
	m_environmentViewport.TopLeftX = 0.0f;
	m_environmentViewport.TopLeftY = 0.0f;


	m_environement_filtering_shader = new Shader();
	if (!m_environement_filtering_shader->CreateFromFile("data\\shaders\\envfiltering.fx", m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile the environement filtering shader!");
		return false;
	}

	m_simple_shader = new Shader();
	if (!m_simple_shader->CreateFromFile("data\\shaders\\simple.fx", m_device.Get(), m_deviceContext.Get()))
	{
		Debug::Err("Failed to compile the environement simple shader!");
		return false;
	}


	// Create buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(EnvFilteringBuffer);
	if (FAILED(m_device->CreateBuffer(&bd, nullptr, m_env_filtering_buffer.ReleaseAndGetAddressOf())))
	{
		Debug::Err("Failed to create the environement filtering shader buffer!");
		return false;
	}
	return true;
}


void Redline::Renderer::ShutdownEnvironement()
{
	SAFE_RELEASE(m_environmentDepthStencilView);
	SAFE_RELEASE(m_environmentDepthStencil);

	if (m_unlit_shader)
	{
		delete m_unlit_shader;
		m_unlit_shader = nullptr;
	}

	if (m_combine_shader)
	{
		delete m_combine_shader;
		m_combine_shader = nullptr;
	}

	if (m_simple_shader)
	{
		delete m_simple_shader;
		m_simple_shader = nullptr;
	}

	if (m_environment_cubemap)
	{
		delete m_environment_cubemap;
		m_environment_cubemap = nullptr;
	}

	if (m_environment_cubemap_filtered)
	{
		delete m_environment_cubemap_filtered;
		m_environment_cubemap_filtered = nullptr;
	}

	m_env_filtering_buffer.Reset();
}


void Redline::Renderer::RenderToGBuffers(const vector<GameObject*>& gameobjects)
{
	BuildViewFrustum(Engine::GetCamera()->GetViewMatrix(), Engine::GetCamera()->GetProjectionMatrix());

	vector<GameObject*> objects;

	// Camera Field of View in radians
	float cameraFOV = Engine::GetCamera()->GetFOV() * float(DEG2RAD);

	// Camera Position
	Vector3 cameraPosition = Engine::GetCamera()->GetPosition();

	const size_t gameObjectsCount = gameobjects.size();
	for (size_t i = 0; i < gameObjectsCount; ++i)
	{
		GameObject* go = gameobjects[i];

		MeshRenderer renderer = go->GetMeshRenderer();

		if (!renderer.m_active)
			continue;

		go->PrepareDraw();

		Transform transform = go->GetTransform();
		const Vector3 scale = transform.GetScale();
		const Vector3 position = transform.GetPosition();
		const float maxScale = max(max(scale.x, scale.y), scale.z);
		const float radius = go->GetMeshRenderer().mesh->GetBoundingSphereRadius();

		if (!IsVisibleOnScreen(position, radius * maxScale))
			continue;

		if (GetOnScreenSize(position, radius * maxScale, cameraPosition, cameraFOV) * m_height >= 1.0f)
			objects.emplace_back(go);
	}

	m_deviceContext->RSSetViewports(1, &m_viewport);

	// Clear the render targets and depth buffer
	for (int i = 0; i < BUFFER_COUNT; ++i)
	{
		m_renderTargets[i]->Clear(m_deviceContext.Get());
	}
	m_deviceContext->ClearDepthStencilView(m_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);


	if (objects.empty())
		return;


	m_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_deviceContext->VSSetShader(m_GBufferShader->GetVertexShader(), nullptr, 0);
	m_deviceContext->PSSetShader(m_GBufferShader->GetPixelShader(), nullptr, 0);

	m_deviceContext->PSSetSamplers(0, 1, m_linearSampler.GetAddressOf());

	const size_t objectsCount = objects.size();
	for (size_t i = 0; i < objectsCount; ++i)
	{
		GameObject* go = objects[i];
		Transform transform = go->GetTransform();
		Mesh* mesh = go->GetMeshRenderer().mesh;

		const vector<SubMesh>& submeshes = mesh->GetSubMeshes();
		const vector<Material>& materials = go->GetMeshRenderer().materials;

		const Vector3 scale = transform.GetScale();
		const float maxScale = max(max(scale.x, scale.y), scale.z);

		// Update variables
		MatrixBuffer mb;
		mb.mWorld = transform.GetMatrix().Transpose();
		mb.mView = Engine::GetCamera()->GetViewMatrix().Transpose();
		mb.mProjection = Engine::GetCamera()->GetProjectionMatrix().Transpose();
		m_deviceContext->UpdateSubresource(m_bufferMatrix, 0, nullptr, &mb, 0, 0);
		m_deviceContext->VSSetConstantBuffers(0, 1, &m_bufferMatrix);

		const size_t submeshesCount = submeshes.size();
		for (size_t j = 0; j < submeshesCount; ++j)
		{
			const Vector3 center = Vector3::Transform(submeshes[j].boundingSphereCenter * maxScale, transform.GetRotation());
			const Vector3 position = transform.GetPosition() + center;
			const float radius = submeshes[j].boundingSphereRadius;
			const bool isVisible = submeshes.size() > 1 ? IsVisibleOnScreen(position, radius * maxScale) : true;

			if (!isVisible)
				continue;

			const float pixelSize = GetOnScreenSize(position, radius * maxScale, cameraPosition, cameraFOV) * m_height;

			if (pixelSize < 1.0f)
				continue;

			const size_t materialID = min(static_cast<size_t>(submeshes[j].materialID), materials.size() - 1);
			Material mat = materials[materialID];

			MaterialBuffer matBuff = mat.GetBuffer();
			m_deviceContext->UpdateSubresource(m_bufferMaterial, 0, nullptr, &matBuff, 0, 0);
			m_deviceContext->PSSetConstantBuffers(1, 1, &m_bufferMaterial);

			// Set texture and sampler and texture for the material to render
			Texture* diffuseTexture = mat.GetDiffuseTexture();
			if (diffuseTexture)
			{
				m_deviceContext->PSSetShaderResources(0, 1, diffuseTexture->GetTexture().GetAddressOf());
			}

			Texture* normalTexture = mat.GetNormalTexture();
			if (normalTexture)
			{
				m_deviceContext->PSSetShaderResources(1, 1, normalTexture->GetTexture().GetAddressOf());
			}

			// Set vertex buffer
			UINT stride = sizeof(Vertex);
			UINT offset = 0;
			ID3D11Buffer* const buffer = submeshes[j].vBuffer->GetVertexBuffer();
			m_deviceContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

			// Set index buffer
			m_deviceContext->IASetIndexBuffer(submeshes[j].iBuffer->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

			// Render the sub-mesh
			m_deviceContext->DrawIndexed(submeshes[j].iBuffer->GetIndicesCount(), 0, 0);
			m_draw_call_count++;

		}
	}


	// Generate Mip-Maps
	for (int i = 0; i < BUFFER_COUNT; ++i)
	{
		m_renderTargets[i]->GenerateMips(m_deviceContext.Get());
	}
}


void Redline::Renderer::GetCorners(Vector3* corners, const float& nearClip, const float& farClip) const
{
	const Matrix invProjMatrix = Engine::GetCamera()->GetProjectionMatrix().Invert();
	const Matrix invViewMatrix = Engine::GetCamera()->GetViewMatrix().Invert();

	// Homogenous points for source cube in clip-space
	// With -1 to 1 in x/y and 0 to 1 in z (D3D)
	Vector4 v[8] =
	{
		{-1, -1, nearClip, 1},
		{-1, 1, nearClip, 1},
		{1, 1, nearClip, 1},
		{1, -1, nearClip, 1},
		{-1, -1, farClip, 1},
		{-1, 1, farClip, 1},
		{1, 1, farClip, 1},
		{1, -1, farClip, 1}
	};

	for (int i = 0; i < 8; i++)
	{
		// 4x4 * 4x1 matrix/vector multiplication 
		v[i] = Vector4::Transform(v[i], invProjMatrix);
		v[i] = Vector4::Transform(v[i], invViewMatrix);

		// Homogenous to cartesian conversion
		corners[i] = Vector3(v[i] / v[i].w);
	}
}

// Exponential to linear depth
float linearDepth(float depthSample)
{
	const float zNear = Redline::Engine::GetCamera()->GetClipPlanes().x;
	const float zFar = Redline::Engine::GetCamera()->GetClipPlanes().y;

	depthSample = 2.0f * depthSample - 1.0f;
	float zLinear = 2.0f * zNear * zFar / (zFar + zNear - depthSample * (zFar - zNear));
	return zLinear;
}

// Linear to exponential depth
float depthSample(float linearDepth)
{
	const float zNear = Redline::Engine::GetCamera()->GetClipPlanes().x;
	const float zFar = Redline::Engine::GetCamera()->GetClipPlanes().y;

	float nonLinearDepth = (zFar + zNear - 2.0f * zNear * zFar / linearDepth) / (zFar - zNear);
	nonLinearDepth = (nonLinearDepth + 1.0f) / 2.0f;
	return nonLinearDepth;
}


void Redline::Renderer::RenderToShadowMap(const vector<GameObject*>& gameobjects, unsigned int cascadeIndex, float minDistance, float maxDistance)
{
	// MATRIX CALCULATION
	{
		const Vector3 at(0, 0, 0);
		const Vector3 pos = Engine::GetLight(0)->GetDirection();
		const Vector3 up(0, 1, 0);

		m_light_view_matrix = Matrix::CreateLookAt(pos, at, up);

		Vector3 corners[8];
		// Get frustrum corners in world space
		GetCorners(corners, depthSample(minDistance), depthSample(maxDistance));

		float minX, maxX, minY, maxY, minZ, maxZ;

		minX = minY = minZ = FLT_MAX;
		maxX = maxY = maxZ = -FLT_MAX;

		for (int i = 0; i < 8; ++i)
		{
			// Convert from world space to light space
			const Vector3 cornerViewSpace = Vector3::Transform(corners[i], m_light_view_matrix);

			minX = min(cornerViewSpace.x, minX);
			maxX = max(cornerViewSpace.x, maxX);
			minY = min(cornerViewSpace.y, minY);
			maxY = max(cornerViewSpace.y, maxY);
			minZ = min(cornerViewSpace.z, minZ);
			maxZ = max(cornerViewSpace.z, maxZ);
		}

		float averageZ = minZ + (maxZ - minZ) * 0.5f;

		m_light_projection_matrix[cascadeIndex] = Matrix::CreateOrthographicOffCenter(minX, maxX, maxY, minY, -averageZ - 500.0f, -averageZ + 500.0f);
	}

	BuildViewFrustum(m_light_view_matrix, m_light_projection_matrix[cascadeIndex]);

	vector<GameObject*> objects;
	const size_t goCount = gameobjects.size();
	for (size_t i = 0; i < goCount; ++i)
	{
		GameObject* go = gameobjects[i];
		Mesh* mesh = go->GetMeshRenderer().mesh;
		MeshRenderer renderer = go->GetMeshRenderer();

		if (!renderer.m_casting_shadows || !renderer.m_active)
			continue;

		if (!mesh->GetCastShadow())
			continue;

		Transform transform = go->GetTransform();

		go->PrepareDraw();

		const Vector3 scale = transform.GetScale();
		const float maxScale = max(max(scale.x, scale.y), scale.z);
		const float radius = mesh->GetBoundingSphereRadius();

		if (IsVisibleOnScreen(transform.GetPosition(), radius * maxScale))
		{
			objects.emplace_back(go);
		}
	}

	m_deviceContext->RSSetViewports(1, &m_shadowViewport);

	ID3D11ShaderResourceView* nullSRV[4] = { nullptr, nullptr, nullptr, nullptr };
	m_deviceContext->PSSetShaderResources(0, 4, nullSRV);

	// Clear the depth buffer
	m_shadowMapsRenderTargets[cascadeIndex]->Clear(m_deviceContext.Get(), 1.0f, 1.0f, 1.0f, 1.0f);
	m_deviceContext->ClearDepthStencilView(m_shadowMapDepthView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	m_deviceContext->OMSetRenderTargets(1, m_shadowMapsRenderTargets[cascadeIndex]->GetRenderTargetView().GetAddressOf(), m_shadowMapDepthView);

	// 3D DRAWING
	if (!objects.empty())
	{
		m_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		m_deviceContext->VSSetShader(m_shadowShader->GetVertexShader(), nullptr, 0);
		m_deviceContext->PSSetShader(m_shadowShader->GetPixelShader(), nullptr, 0);

		m_deviceContext->PSSetSamplers(0, 1, m_linearSampler.GetAddressOf());

		const size_t objectsCount = objects.size();
		for (size_t i = 0; i < objectsCount; ++i)
		{
			GameObject* go = objects[i];
			Transform transform = go->GetTransform();
			Mesh* mesh = go->GetMeshRenderer().mesh;

			const vector<SubMesh>& submeshes = mesh->GetSubMeshes();
			const vector<Material>& materials = go->GetMeshRenderer().materials;

			const Vector3 scale = transform.GetScale();
			const float maxScale = max(max(scale.x, scale.y), scale.z);

			// Update variables
			MatrixBuffer mb;
			mb.mWorld = transform.GetMatrix().Transpose();
			mb.mView = m_light_view_matrix.Transpose();
			mb.mProjection = m_light_projection_matrix[cascadeIndex].Transpose();
			m_deviceContext->UpdateSubresource(m_bufferMatrix, 0, nullptr, &mb, 0, 0);
			m_deviceContext->VSSetConstantBuffers(0, 1, &m_bufferMatrix);

			const size_t submeshesCount = submeshes.size();
			for (size_t j = 0; j < submeshesCount; ++j)
			{
				SubMesh submesh = submeshes[j];

				const Vector3 center = Vector3::Transform(submesh.boundingSphereCenter * maxScale, transform.GetRotation());
				const Vector3 position = transform.GetPosition() + center;
				const float radius = submesh.boundingSphereRadius;

				if (IsVisibleOnScreen(position, radius * maxScale))
				{
					const size_t materialID = min(static_cast<size_t>(submesh.materialID), materials.size() - 1);
					Material mat = materials[materialID];

					MaterialBuffer matBuff = mat.GetBuffer();
					m_deviceContext->UpdateSubresource(m_bufferMaterial, 0, nullptr, &matBuff, 0, 0);
					m_deviceContext->PSSetConstantBuffers(1, 1, &m_bufferMaterial);

					// Set texture and sampler and texture for the material to render
					Texture* diffuseTexture = mat.GetDiffuseTexture();
					if (diffuseTexture)
					{
						ComPtr<ID3D11ShaderResourceView> tex = diffuseTexture->GetTexture();
						m_deviceContext->PSSetShaderResources(0, 1, tex.GetAddressOf());
					}

					// Set vertex buffer
					UINT stride = sizeof(Vertex);
					UINT offset = 0;
					ID3D11Buffer* const buffer = submesh.vBuffer->GetVertexBuffer();
					m_deviceContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

					// Set index buffer
					m_deviceContext->IASetIndexBuffer(submesh.iBuffer->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

					// Render the sub-mesh
					m_deviceContext->DrawIndexed(submesh.iBuffer->GetIndicesCount(), 0, 0);
					m_draw_call_count++;
				}
			}
		}
	}

	m_shadowMapsRenderTargets[cascadeIndex]->GenerateMips(m_deviceContext.Get());

	EnableZTest();
}


void Redline::Renderer::RenderToEnvironementMap(const vector<GameObject*>& gameobjects)
{
	Matrix projection_matrix = Matrix::CreatePerspectiveFieldOfView(float(M_PI) / 2.0f, 1.0f, 0.1f, 250.0f);

	Vector3 pos = Engine::GetCamera()->GetPosition() + Engine::GetCamera()->GetForwardVector();

	m_deviceContext->RSSetViewports(1, &m_environmentViewport);
	m_deviceContext->PSSetSamplers(0, 1, m_linearSampler.GetAddressOf());

	for (int i = 0; i < 6; ++i)
	{
		Vector3 at(0, 0, 1);
		Vector3 up(0, 1, 0);

		switch (i)
		{
		case 0://FACE_POSITIVE_X:
			at = Vector3(-1.0f, 0.0f, 0.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 1://FACE_NEGATIVE_X:
			at = Vector3(1.0f, 0.0f, 0.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 2://FACE_POSITIVE_Y:
			at = Vector3(0.0f, 1.0f, 0.0f);
			up = Vector3(0.0f, 0.0f, -1.0f);
			break;
		case 3://FACE_NEGATIVE_Y:
			at = Vector3(0.0f, -1.0f, 0.0f);
			up = Vector3(0.0f, 0.0f, 1.0f);
			break;
		case 4://FACE_POSITIVE_Z:
			at = Vector3(0.0f, 0.0f, 1.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 5://FACE_NEGATIVE_Z:
			at = Vector3(0.0f, 0.0f, -1.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		}

		Matrix view_matrix = Matrix::CreateLookAt(pos, pos + at, up);

		BuildViewFrustum(view_matrix, projection_matrix);

		vector<GameObject*> objects;
		const size_t goCount = gameobjects.size();
		for (size_t j = 0; j < goCount; ++j)
		{
			GameObject* go = gameobjects[j];

			MeshRenderer renderer = go->GetMeshRenderer();

			if (!renderer.m_active)
				continue;

			go->PrepareDraw();

			Transform transform = go->GetTransform();
			Vector3 scale = transform.GetScale();
			Vector3 position = transform.GetPosition();
			float maxScale = max(max(scale.x, scale.y), scale.z);
			float radius = go->GetMeshRenderer().mesh->GetBoundingSphereRadius();


			if (IsVisibleOnScreen(position, radius * maxScale))
			{
				if (GetOnScreenSize(position, radius * maxScale, pos, float(M_PI) / 2.0f) * m_environmentViewport.Height >= 4.0f)
					objects.emplace_back(go);
			}
		}

		// Clear the depth buffer
		m_deviceContext->ClearDepthStencilView(m_environmentDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

		m_deviceContext->OMSetRenderTargets(1, m_environment_cubemap->GetRenderTargetView(i).GetAddressOf(), m_environmentDepthStencilView);

		// TODO: OPTI: Precompute scale matrix
		DrawSkybox(view_matrix, projection_matrix, pos, 100.0f);

		if (objects.empty())
			continue;

		m_deviceContext->VSSetShader(m_simple_shader->GetVertexShader(), nullptr, 0);
		m_deviceContext->PSSetShader(m_simple_shader->GetPixelShader(), nullptr, 0);

		// Update variables
		//SkyboxPSBuffer skybox_ps_buffer = m_skybox->GetPSBuffer();
		//m_deviceContext->UpdateSubresource(m_bufferPSSkybox, 0, nullptr, &skybox_ps_buffer, 0, 0);
		m_deviceContext->PSSetConstantBuffers(2, 1, &m_bufferPSSkybox);

		const size_t objectsCount = objects.size();
		for (size_t k = 0; k < objectsCount; ++k)
		{
			GameObject* go = objects[k];
			Transform transform = go->GetTransform();
			Mesh* mesh = go->GetMeshRenderer().mesh;

			vector<SubMesh>& submeshes = mesh->GetSubMeshes();
			vector<Material>& materials = go->GetMeshRenderer().materials;

			Vector3 scale = transform.GetScale();
			float maxScale = max(max(scale.x, scale.y), scale.z);

			// Update variables
			MatrixBuffer mb;
			mb.mWorld = transform.GetMatrix().Transpose();
			mb.mView = view_matrix.Transpose();
			mb.mProjection = projection_matrix.Transpose();
			m_deviceContext->UpdateSubresource(m_bufferMatrix, 0, nullptr, &mb, 0, 0);
			m_deviceContext->VSSetConstantBuffers(0, 1, &m_bufferMatrix);

			size_t submeshesCount = submeshes.size();
			for (size_t j = 0; j < submeshesCount; ++j)
			{
				const Vector3 center = Vector3::Transform(submeshes[j].boundingSphereCenter * maxScale, transform.GetRotation());
				const Vector3 position = transform.GetPosition() + center;
				const float radius = submeshes[j].boundingSphereRadius;
				const bool isVisible = submeshes.size() > 1 ? IsVisibleOnScreen(position, radius * maxScale) : true;

				if (!isVisible)
					continue;

				size_t materialID = min(static_cast<size_t>(submeshes[j].materialID), materials.size() - 1);
				Material mat = materials[materialID];

				MaterialBuffer matBuff = mat.GetBuffer();
				m_deviceContext->UpdateSubresource(m_bufferMaterial, 0, nullptr, &matBuff, 0, 0);
				m_deviceContext->PSSetConstantBuffers(1, 1, &m_bufferMaterial);

				// Set texture and sampler and texture for the material to render
				Texture* diffuseTexture = mat.GetDiffuseTexture();
				if (diffuseTexture)
				{
					ComPtr<ID3D11ShaderResourceView> tex = diffuseTexture->GetTexture();
					m_deviceContext->PSSetShaderResources(0, 1, tex.GetAddressOf());
				}

				// Set vertex buffer
				UINT stride = sizeof(Vertex);
				UINT offset = 0;
				ID3D11Buffer* buffer = submeshes[j].vBuffer->GetVertexBuffer();
				m_deviceContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

				// Set index buffer
				m_deviceContext->IASetIndexBuffer(submeshes[j].iBuffer->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

				// Render the sub-mesh
				m_deviceContext->DrawIndexed(submeshes[j].iBuffer->GetIndicesCount(), 0, 0);
				m_draw_call_count++;

			}
		}

	}


	m_environment_cubemap->GenerateMips(m_deviceContext.Get());

	m_deviceContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	m_deviceContext->VSSetShader(m_environement_filtering_shader->GetVertexShader(), nullptr, 0);
	m_deviceContext->PSSetShader(m_environement_filtering_shader->GetPixelShader(), nullptr, 0);

	DisableZTest();

	int cubemap_res = Engine::GetConfig().cubemap_res;

	D3D11_VIEWPORT viewport;
	viewport.Width = static_cast<FLOAT>(cubemap_res);
	viewport.Height = static_cast<FLOAT>(cubemap_res);
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;



	// TODO: Faces in inner loop, mips in outer
	const int num_mips = m_environment_cubemap->GetNumberOfMips();
	for (int i = 0; i < 6; ++i)
	{
		Vector3 at(0, 0, 1);
		Vector3 up(0, 1, 0);

		switch (i)
		{
		case 0://FACE_POSITIVE_X:
			at = Vector3(1.0f, 0.0f, 0.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 1://FACE_NEGATIVE_X:
			at = Vector3(-1.0f, 0.0f, 0.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 2://FACE_POSITIVE_Y:
			at = Vector3(0.0f, 1.0f, 0.0f);
			up = Vector3(0.0f, 0.0f, -1.0f);
			break;
		case 3://FACE_NEGATIVE_Y:
			at = Vector3(0.0f, -1.0f, 0.0f);
			up = Vector3(0.0f, 0.0f, 1.0f);
			break;
		case 4://FACE_POSITIVE_Z:
			at = Vector3(0.0f, 0.0f, 1.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		case 5://FACE_NEGATIVE_Z:
			at = Vector3(0.0f, 0.0f, -1.0f);
			up = Vector3(0.0f, 1.0f, 0.0f);
			break;
		}

		for (int j = 0; j < num_mips - 2; ++j)
		{
			// Get mip resolution
			float res = static_cast<float>(cubemap_res);
			for (int div = 0; div < j; ++div)
				res /= 2;

			float roughness = j / static_cast<float>(num_mips - 2);
			EnvFilteringBuffer buffer = {
				Vector4(at.x, at.y, at.z, roughness),
				Vector4(up.x, up.y, up.z, res)
			};

			viewport.Height = viewport.Width = res;
			m_deviceContext->RSSetViewports(1, &viewport);

			m_deviceContext->UpdateSubresource(m_env_filtering_buffer.Get(), 0, nullptr, &buffer, 0, 0);
			m_deviceContext->PSSetConstantBuffers(0, 1, m_env_filtering_buffer.GetAddressOf());

			m_deviceContext->OMSetRenderTargets(1, m_environment_cubemap_filtered->GetRenderTargetView(i, j).GetAddressOf(), nullptr);
			m_deviceContext->PSSetShaderResources(0, 1, m_environment_cubemap->GetShaderRessourceView().GetAddressOf());

			m_deviceContext->Draw(3, 0);
			m_draw_call_count++;
		}
	}

	EnableZTest();
}


void Redline::Renderer::SwitchToBackbuffer() const
{
	m_deviceContext->OMSetRenderTargets(1, m_renderTargetView.GetAddressOf(), nullptr);
}


void Redline::Renderer::SwitchToGBuffers() const
{
	ID3D11RenderTargetView* rtViews[BUFFER_COUNT];
	for (int i = 0; i < BUFFER_COUNT; ++i)
	{
		rtViews[i] = m_renderTargets[i]->GetRenderTargetView().Get();
	}
	m_deviceContext->OMSetRenderTargets(BUFFER_COUNT, rtViews, m_depthStencilView);
}


void Redline::Renderer::ClearRTInputsAndOutputs() const
{
	ID3D11RenderTargetView* rtViews[] = {
		nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr
	};

	ID3D11ShaderResourceView* srViews[] = {
		nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr
	};

	m_deviceContext->OMSetRenderTargets(8, rtViews, nullptr);
	m_deviceContext->PSSetShaderResources(0, 8, srViews);
}


void Redline::Renderer::BuildLightsBuffer(const vector<Light*>& lights)
{
	vector<Light*> viewedLights;

	const size_t lightsCount = min(lights.size(), MAX_LIGHTS);
	for (size_t i = 0; i < lightsCount; ++i)
	{
		Light* light = lights[i];

		if (!light->IsActive()) continue;

		LightType type = light->GetType();
		if (type != LIGHT_POINT || IsVisibleOnScreen(light->GetPosition(), pow(light->GetIntensity(), 2) * 100.0f))
		{
			viewedLights.emplace_back(light);
		}
	}
	const Matrix viewMatrix = Engine::GetCamera()->GetViewMatrix();
	const size_t viewedLightsCount = viewedLights.size();
	for (size_t i = 0; i < viewedLightsCount; ++i)
	{
		Light* light = viewedLights[i];

		m_buffer.lights[i].position = Vector3::Transform(light->GetPosition(), viewMatrix);
		m_buffer.lights[i].intensity = light->GetIntensity();
		m_buffer.lights[i].color = light->GetColor();
		m_buffer.lights[i].type = light->GetType();
		m_buffer.lights[i].direction = Vector3::TransformNormal(light->GetDirection(), viewMatrix);
		m_buffer.lights[i].angle = light->GetAngle() * float(DEG2RAD) * 0.5f;
	}

	m_buffer.numLights = static_cast<int>(viewedLightsCount);
}


void Redline::Renderer::RenderToScreen()
{
	D3D11_VIEWPORT screenViewport;
	screenViewport.TopLeftX = 0.0f;
	screenViewport.TopLeftY = 0.0f;
	screenViewport.MinDepth = 0.0f;
	screenViewport.MaxDepth = 1.0f;
	screenViewport.Width = static_cast<FLOAT>(m_window_width);
	screenViewport.Height = static_cast<FLOAT>(m_window_height);
	m_deviceContext->RSSetViewports(1, &screenViewport);

	m_deviceContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	m_deviceContext->VSSetShader(m_unlit_shader->GetVertexShader(), nullptr, 0);
	m_deviceContext->PSSetShader(m_unlit_shader->GetPixelShader(), nullptr, 0);

	m_postProcessRT_A->GenerateMips(m_deviceContext.Get());

	m_deviceContext->PSSetShaderResources(0, 1, m_postProcessRT_A->GetShaderRessourceView().GetAddressOf());
	m_deviceContext->PSSetSamplers(0, 1, m_linearSampler.GetAddressOf());

	m_deviceContext->Draw(3, 0);
	m_draw_call_count++;
}


void Redline::Renderer::RenderDeferredImage()
{
	m_deviceContext->OMSetRenderTargets(1, m_postProcessRT_A->GetRenderTargetView().GetAddressOf(), m_depthStencilView);

	m_deviceContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	m_deviceContext->VSSetShader(m_deferredShader->GetVertexShader(), nullptr, 0);
	m_deviceContext->PSSetShader(m_deferredShader->GetPixelShader(), nullptr, 0);

	ID3D11ShaderResourceView* gBufferSRV[BUFFER_COUNT + 1];
	for (int i = 0; i < BUFFER_COUNT; ++i)
	{
		gBufferSRV[i] = m_renderTargets[i]->GetShaderRessourceView().Get();
	}

	gBufferSRV[BUFFER_COUNT] = m_ssaoRT->GetShaderRessourceView().Get();

	m_deviceContext->PSSetShaderResources(0, BUFFER_COUNT + 1, gBufferSRV);

	ID3D11SamplerState* samplers[] = { m_pointSampler, m_linearSampler.Get(), m_shadowSampler };
	m_deviceContext->PSSetSamplers(0, 3, samplers);

	if (m_shadowEnabled)
	{
		ID3D11ShaderResourceView* shadowSRV[NUM_CASCADES];
		for (int i = 0; i < NUM_CASCADES; ++i)
		{
			shadowSRV[i] = m_shadowMapsRenderTargets[i]->GetShaderRessourceView().Get();
		}
		m_deviceContext->PSSetShaderResources(BUFFER_COUNT + 1, NUM_CASCADES, shadowSRV);
	}

	// Set texture
	m_deviceContext->PSSetShaderResources(BUFFER_COUNT + NUM_CASCADES + 1, 1, m_environment_cubemap_filtered->GetShaderRessourceView().GetAddressOf());



	ConstantBuffer cb;
	cb.mWorldToView = Engine::GetCamera()->GetViewMatrix().Invert().Transpose();
	cb.useShadow = m_shadowEnabled ? 1 : 0;
	cb.EnvNumMipMaps = m_environment_cubemap->GetNumberOfMips() - 2;
	m_deviceContext->UpdateSubresource(m_constants_buffer, 0, nullptr, &cb, 0, 0);

	BuildLightsBuffer(Engine::GetLights());
	m_deviceContext->UpdateSubresource(m_bufferLights, 0, nullptr, &m_buffer, 0, 0);
	ID3D11Buffer* buffers[3] = { m_constants_buffer, m_bufferLights, nullptr };

	if (m_shadowEnabled)
	{
		ShadowsBuffer sb;
		sb.mView = m_light_view_matrix.Transpose();
		sb.mProjectionOne = m_light_projection_matrix[0].Transpose();
		sb.mProjectionTwo = m_light_projection_matrix[1].Transpose();
		sb.mProjectionThree = m_light_projection_matrix[2].Transpose();
		sb.shadowResolution = static_cast<float>(Engine::GetConfig().shadow_res);
		m_deviceContext->UpdateSubresource(m_bufferLightMatrix, 0, nullptr, &sb, 0, 0);
		buffers[2] = m_bufferLightMatrix;
	}

	m_deviceContext->PSSetConstantBuffers(0, 3, buffers);
	m_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	m_deviceContext->Draw(3, 0);
	m_draw_call_count++;
}


void Redline::Renderer::RenderSSAOPass()
{
	if (!m_ssaoEnabled)
	{
		const float clearColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		m_deviceContext->ClearRenderTargetView(m_ssaoRT->GetRenderTargetView().Get(), clearColor);
		return;
	}

	m_deviceContext->OMSetRenderTargets(1, m_ssaoRT->GetRenderTargetView().GetAddressOf(), nullptr);

	D3D11_VIEWPORT viewport;
	viewport.Width = m_ssaoWidth;
	viewport.Height = m_ssaoHeight;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	m_deviceContext->RSSetViewports(1, &viewport);

	m_deviceContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	m_deviceContext->VSSetShader(m_ssaoShader->GetVertexShader(), nullptr, 0);
	m_deviceContext->PSSetShader(m_ssaoShader->GetPixelShader(), nullptr, 0);

	Matrix viewMatrix = Engine::GetCamera()->GetViewMatrix();
	DirectX::XMVECTOR lightDir = Vector3::TransformNormal(Engine::GetLight(0)->GetDirection(), viewMatrix);
	float lightIntensity = Engine::GetLight(0)->GetIntensity();

	SSAOBuffer buffer = {
		lightDir,
		1.0f / m_ssaoWidth,
		1.0f / m_ssaoHeight,
		m_ssaoWidth / m_ssaoHeight,
		lightIntensity
	};
	m_deviceContext->UpdateSubresource(m_bufferSSAO, 0, nullptr, &buffer, 0, 0);

	m_deviceContext->PSSetConstantBuffers(0, 1, &m_bufferSSAO);

	ID3D11ShaderResourceView* srViews[] = { m_renderTargets[0]->GetShaderRessourceView().Get(), m_renderTargets[1]->GetShaderRessourceView().Get(), m_renderTargets[2]->GetShaderRessourceView().Get() };
	m_deviceContext->PSSetShaderResources(0, 3, srViews);
	m_deviceContext->PSSetSamplers(0, 1, &m_linear_clamp_sampler);

	//m_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	m_deviceContext->Draw(3, 0);
	m_draw_call_count++;

	// BLUR X
	{
		m_deviceContext->OMSetRenderTargets(1, m_ssaoBlurRT->GetRenderTargetView().GetAddressOf(), nullptr);
		m_deviceContext->PSSetShader(m_ssaoBlurXShader->GetPixelShader(), nullptr, 0);

		m_deviceContext->PSSetShaderResources(2, 1, m_ssaoRT->GetShaderRessourceView().GetAddressOf());

		m_deviceContext->Draw(3, 0);
		m_draw_call_count++;
	}

	// BLUR Y
	{
		ID3D11ShaderResourceView* emptySRV[] = { nullptr };
		m_deviceContext->PSSetShaderResources(2, 1, emptySRV);

		m_deviceContext->OMSetRenderTargets(1, m_ssaoRT->GetRenderTargetView().GetAddressOf(), nullptr);
		m_deviceContext->PSSetShader(m_ssaoBlurYShader->GetPixelShader(), nullptr, 0);

		m_deviceContext->PSSetShaderResources(2, 1, m_ssaoBlurRT->GetShaderRessourceView().GetAddressOf());

		m_deviceContext->Draw(3, 0);
		m_draw_call_count++;
	}

	m_deviceContext->RSSetViewports(1, &m_viewport);
}


void Redline::Renderer::RenderFogPass()
{
	m_deviceContext->OMSetRenderTargets(1, m_postProcessRT_B->GetRenderTargetView().GetAddressOf(), m_depthStencilView);

	m_deviceContext->RSSetViewports(1, &m_viewport);

	m_deviceContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	SkyboxVSBuffer svb = m_skybox->GetVSBuffer();
	m_deviceContext->UpdateSubresource(m_bufferVSSkybox, 0, nullptr, &svb, 0, 0);

	SkyboxPSBuffer spb = m_skybox->GetPSBuffer();
	m_deviceContext->UpdateSubresource(m_bufferPSSkybox, 0, nullptr, &spb, 0, 0);

	m_deviceContext->VSSetShader(m_fog_shader->GetVertexShader(), nullptr, 0);
	m_deviceContext->PSSetShader(m_fog_shader->GetPixelShader(), nullptr, 0);

	ID3D11Buffer* buffers[4] = { m_constants_buffer, m_bufferVSSkybox, m_bufferPSSkybox, nullptr };

	if (m_shadowEnabled)
	{
		ID3D11ShaderResourceView* srViews[NUM_CASCADES];
		for (int i = 0; i < NUM_CASCADES; ++i)
		{
			srViews[i] = m_shadowMapsRenderTargets[i]->GetShaderRessourceView().Get();
		}
		m_deviceContext->PSSetShaderResources(2, NUM_CASCADES, srViews);
		m_deviceContext->PSSetSamplers(1, 1, m_linearSampler.GetAddressOf());

		buffers[3] = m_bufferLightMatrix;
	}

	m_deviceContext->PSSetConstantBuffers(0, 4, buffers);

	ID3D11ShaderResourceView* srViews[] = { m_postProcessRT_A->GetShaderRessourceView().Get(), m_renderTargets[0]->GetShaderRessourceView().Get() };
	m_deviceContext->PSSetShaderResources(0, 2, srViews);
	m_deviceContext->PSSetSamplers(0, 1, &m_pointSampler);

	m_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	m_deviceContext->Draw(3, 0);
	m_draw_call_count++;
}


void Redline::Renderer::RenderTonemappingBloomPass(float delta_time)
{
	m_use_A_buffer = !m_use_A_buffer;

	m_deviceContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

	// It's the same full screen triangle vertex shader for all theses passes, so set it once now
	m_deviceContext->VSSetShader(m_combine_shader->GetVertexShader(), nullptr, 0);

	// Same for the sampler
	m_deviceContext->PSSetSamplers(0, 1, &m_linear_clamp_sampler);

	// Generate mips on the last buffer used, to compute luminance on the highest mip (1x1)
	m_postProcessRT_B->GenerateMips(m_deviceContext.Get());

	D3D11_VIEWPORT viewport;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;

	// Exposure adaptation
	{
		ID3D11RenderTargetView* renderTarget = (m_use_A_buffer ? m_exposure_A_RT : m_exposure_B_RT)->GetRenderTargetView().Get();
		m_deviceContext->OMSetRenderTargets(1, &renderTarget, nullptr);

		viewport.Width = static_cast<FLOAT>(1);
		viewport.Height = static_cast<FLOAT>(1);
		m_deviceContext->RSSetViewports(1, &viewport);

		m_deviceContext->PSSetShader(m_adaptation_shader->GetPixelShader(), nullptr, 0);

		LuminanceBuffer buffer =
		{
			delta_time,
			0.5f,
			2.5f,
			4.0f
		};
		m_deviceContext->UpdateSubresource(m_luminance_buffer, 0, nullptr, &buffer, 0, 0);

		m_deviceContext->PSSetConstantBuffers(0, 1, &m_luminance_buffer);

		ID3D11ShaderResourceView* srv[] = { m_postProcessRT_B->GetShaderRessourceView().Get(), (m_use_A_buffer ? m_exposure_B_RT : m_exposure_A_RT)->GetShaderRessourceView().Get() };
		m_deviceContext->PSSetShaderResources(0, 2, srv);

		m_deviceContext->Draw(3, 0);
		m_draw_call_count++;
	}

	viewport.Width = m_bloomWidth;
	viewport.Height = m_bloomHeight;
	m_deviceContext->RSSetViewports(1, &viewport);

	for (int i = 0; i < 2; ++i)
	{
		// BLUR X
		{
			ID3D11ShaderResourceView* emptySRV[] = { nullptr };
			m_deviceContext->PSSetShaderResources(0, 1, emptySRV);

			m_deviceContext->OMSetRenderTargets(1, m_bloomBlur_B_RT->GetRenderTargetView().GetAddressOf(), nullptr);
			m_deviceContext->PSSetShader(m_blurHShader->GetPixelShader(), nullptr, 0);

			m_deviceContext->PSSetShaderResources(0, 1, (i == 0 ? m_postProcessRT_B : m_bloomBlur_A_RT)->GetShaderRessourceView().GetAddressOf());

			m_deviceContext->Draw(3, 0);
			m_draw_call_count++;
		}

		// BLUR Y
		{
			ID3D11ShaderResourceView* emptySRV[] = { nullptr };
			m_deviceContext->PSSetShaderResources(0, 1, emptySRV);

			m_deviceContext->OMSetRenderTargets(1, m_bloomBlur_A_RT->GetRenderTargetView().GetAddressOf(), nullptr);
			m_deviceContext->PSSetShader(m_blurVShader->GetPixelShader(), nullptr, 0);

			m_deviceContext->PSSetShaderResources(0, 1, m_bloomBlur_B_RT->GetShaderRessourceView().GetAddressOf());

			m_deviceContext->Draw(3, 0);
			m_draw_call_count++;
		}
	}


	// Tonemapping + bloom
	{
		viewport.Width = static_cast<FLOAT>(m_width);
		viewport.Height = static_cast<FLOAT>(m_height);
		m_deviceContext->RSSetViewports(1, &viewport);

		ID3D11ShaderResourceView* emptySRV[] = { nullptr };
		m_deviceContext->PSSetShaderResources(0, 1, emptySRV);

		m_deviceContext->OMSetRenderTargets(1, m_postProcessRT_A->GetRenderTargetView().GetAddressOf(), nullptr);
		m_deviceContext->PSSetShader(m_combine_shader->GetPixelShader(), nullptr, 0);

		ID3D11ShaderResourceView* srv[] = { m_postProcessRT_B->GetShaderRessourceView().Get(), m_bloomBlur_A_RT->GetShaderRessourceView().Get(), (m_use_A_buffer ? m_exposure_A_RT : m_exposure_B_RT)->GetShaderRessourceView().Get() };
		m_deviceContext->PSSetShaderResources(0, 3, srv);

		m_deviceContext->Draw(3, 0);
		m_draw_call_count++;
	}
}


void Redline::Renderer::DisableZTest() const
{
	m_deviceContext->OMSetDepthStencilState(m_depthDisabledStencilState, 1);
}


void Redline::Renderer::EnableZTest() const
{
	m_deviceContext->OMSetDepthStencilState(m_depthStencilState, 1);
}


void Redline::Renderer::BuildViewFrustum(const Matrix& view, const Matrix& projection)
{
	Matrix viewProjection = view * projection;

	// Left plane
	m_frustum[0].x = viewProjection._14 + viewProjection._11;
	m_frustum[0].y = viewProjection._24 + viewProjection._21;
	m_frustum[0].z = viewProjection._34 + viewProjection._31;
	m_frustum[0].w = viewProjection._44 + viewProjection._41;

	// Right plane
	m_frustum[1].x = viewProjection._14 - viewProjection._11;
	m_frustum[1].y = viewProjection._24 - viewProjection._21;
	m_frustum[1].z = viewProjection._34 - viewProjection._31;
	m_frustum[1].w = viewProjection._44 - viewProjection._41;

	// Top plane
	m_frustum[2].x = viewProjection._14 - viewProjection._12;
	m_frustum[2].y = viewProjection._24 - viewProjection._22;
	m_frustum[2].z = viewProjection._34 - viewProjection._32;
	m_frustum[2].w = viewProjection._44 - viewProjection._42;

	// Bottom plane
	m_frustum[3].x = viewProjection._14 + viewProjection._12;
	m_frustum[3].y = viewProjection._24 + viewProjection._22;
	m_frustum[3].z = viewProjection._34 + viewProjection._32;
	m_frustum[3].w = viewProjection._44 + viewProjection._42;

	// Near plane
	m_frustum[4].x = viewProjection._13;
	m_frustum[4].y = viewProjection._23;
	m_frustum[4].z = viewProjection._33;
	m_frustum[4].w = viewProjection._43;

	// Far plane
	m_frustum[5].x = viewProjection._14 - viewProjection._13;
	m_frustum[5].y = viewProjection._24 - viewProjection._23;
	m_frustum[5].z = viewProjection._34 - viewProjection._33;
	m_frustum[5].w = viewProjection._44 - viewProjection._43;

	// Normalize planes
	for (size_t i = 0; i < 6; ++i)
	{
		m_frustum[i].Normalize();
	}
}


bool Redline::Renderer::IsVisibleOnScreen(const Vector3& position, float radius) const
{
	for (size_t i = 0; i < 6; ++i)
	{
		if (m_frustum[i].DotCoordinate(position) + radius < 0)
		{
			return false;
		}
	}

	return true;
}


float Redline::Renderer::GetOnScreenSize(const DirectX::SimpleMath::Vector3 & position, float radius, const DirectX::SimpleMath::Vector3 & cameraPosition, float fov) const
{
	float distanceSquared = Vector3::DistanceSquared(position, cameraPosition);
	float radiusSquared = radius * radius;
	if (radiusSquared > distanceSquared)
		return 1.0f;
	float pixelRadius = cosf(fov * 0.5f) * radius / sqrt(distanceSquared - radiusSquared);
	return pixelRadius;
}


bool Redline::Renderer::Render(const vector<GameObject*>& gameobjects, float delta_time)
{
	Engine::GetTimer()->StartCounter();
	m_draw_call_count = 0;

	ClearRTInputsAndOutputs();

	EnableZTest();
	if (m_shadowEnabled)
	{
		for (unsigned int i = 0; i < NUM_CASCADES; ++i)
		{
			RenderToShadowMap(gameobjects, i, m_cascadeSplits[i] - 2.5f, m_cascadeSplits[i + 1] + 2.5f);
		}
	}

	ClearRTInputsAndOutputs();

	RenderToEnvironementMap(gameobjects);

	ClearRTInputsAndOutputs();

	// Fill GBuffers pass
	SwitchToGBuffers();
	RenderToGBuffers(gameobjects);

	ClearRTInputsAndOutputs();

	// Deferred shading
	DisableZTest();
	RenderSSAOPass();

	ClearRTInputsAndOutputs();

	RenderDeferredImage();
	RenderFogPass();

	EnableZTest();
	DrawSkybox(Engine::GetCamera()->GetViewMatrix(), Engine::GetCamera()->GetProjectionMatrix(), Engine::GetCamera()->GetPosition(), Engine::GetCamera()->GetClipPlanes().y);

	ClearRTInputsAndOutputs();

	DisableZTest();

	RenderTonemappingBloomPass(delta_time);

	SwitchToBackbuffer();
	RenderToScreen();

#ifdef _DEBUG
	ImGui::SetNextWindowPos(ImVec2(5, 5), ImGuiSetCond_FirstUseEver);
	ImGui::Begin("Render stats");
	double time = Engine::GetTimer()->GetCounter();
	ImGui::Text("Render Time: %.2f ms", time);
	const float targetFrameRate = 60.0f;
	const float targetFrameTime = (1.0f / targetFrameRate) * 1000.0f;
	float fraction = static_cast<float>(time) / targetFrameTime;
	ImGui::Text("Budget utilization (%.2f ms allowed):", targetFrameTime);
	ImGui::ProgressBar(fraction);
	ImGui::Text("Draw calls: %u", m_draw_call_count);
	ImGui::End();

	DisableZTest();
	DrawDebug();
#endif

	ImGui::Render();

	// switch the back buffer and the front buffer
	m_swapChain->Present(m_vsync_enabled ? 1 : 0, 0);
	return true;
}


void Redline::Renderer::DrawSkybox(const Matrix& viewMatrix, const Matrix& projectionMatrix, const Vector3& cameraPosition, float farPlane)
{
	m_skybox->UpdateCamera(cameraPosition, farPlane);

	SubMesh submesh = m_skybox->GetMesh()->GetSubMeshes()[0];

	MatrixBuffer mb;
	mb.mWorld = m_skybox->GetMatrix().Transpose();
	mb.mView = viewMatrix.Transpose();
	mb.mProjection = projectionMatrix.Transpose();
	m_deviceContext->UpdateSubresource(m_bufferMatrix, 0, nullptr, &mb, 0, 0);

	SkyboxVSBuffer svb = m_skybox->GetVSBuffer();
	m_deviceContext->UpdateSubresource(m_bufferVSSkybox, 0, nullptr, &svb, 0, 0);

	SkyboxPSBuffer spb = m_skybox->GetPSBuffer();
	m_deviceContext->UpdateSubresource(m_bufferPSSkybox, 0, nullptr, &spb, 0, 0);

	Shader* skyboxShader = m_skybox->GetShader();
	m_deviceContext->VSSetShader(skyboxShader->GetVertexShader(), nullptr, 0);
	m_deviceContext->PSSetShader(skyboxShader->GetPixelShader(), nullptr, 0);

	ID3D11InputLayout* layout = skyboxShader->GetInputLayout();
	m_deviceContext->IAGetInputLayout(&layout);

	ID3D11Buffer* buffers[] = { m_constants_buffer, m_bufferMatrix, m_bufferVSSkybox, m_bufferPSSkybox };
	m_deviceContext->VSSetConstantBuffers(0, 4, buffers);
	m_deviceContext->PSSetConstantBuffers(0, 4, buffers);

	// Set vertex buffer
	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	ID3D11Buffer* const buffer = submesh.vBuffer->GetVertexBuffer();
	m_deviceContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

	// Set index buffer
	m_deviceContext->IASetIndexBuffer(submesh.iBuffer->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

	// Render a triangle
	m_deviceContext->DrawIndexed(submesh.iBuffer->GetIndicesCount(), 0, 0);
	m_draw_call_count++;
}


void Redline::Renderer::DrawDebug() const
{
	vector<DebugLine> debugLines = Debug::GetDebugLines();

	const size_t linesCount = debugLines.size();

	if (linesCount > 0)
	{
		vector<Vertex> vertices;

		for (size_t i = 0; i < linesCount; ++i)
		{
			Vertex vtx;
			vtx.position = debugLines[i].startPos;
			vtx.normal = debugLines[i].color;
			vertices.emplace_back(vtx);

			vtx.position = debugLines[i].endPos;
			vertices.emplace_back(vtx);
		}

		VertexBuffer* buff = CreateVertexBuffer(vertices);

		UINT stride = sizeof(Vertex);
		UINT offset = 0;
		ID3D11Buffer* const buffer = buff->GetVertexBuffer();
		m_deviceContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);
		m_deviceContext->IASetIndexBuffer(nullptr, static_cast<DXGI_FORMAT>(0), 0);

		MatrixBuffer mb;
		mb.mWorld = Matrix::Identity.Transpose();
		mb.mView = Engine::GetCamera()->GetViewMatrix().Transpose();
		mb.mProjection = Engine::GetCamera()->GetProjectionMatrix().Transpose();
		m_deviceContext->UpdateSubresource(m_bufferMatrix, 0, nullptr, &mb, 0, 0);
		m_deviceContext->VSSetConstantBuffers(0, 1, &m_bufferMatrix);

		m_deviceContext->VSSetShader(m_wireframeShader->GetVertexShader(), nullptr, 0);
		m_deviceContext->PSSetShader(m_wireframeShader->GetPixelShader(), nullptr, 0);

		m_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
		m_deviceContext->Draw(static_cast<UINT>(vertices.size()), 0);

		delete buff;
		Debug::ClearDebugLines();
	}

	Debug::DrawConsole();
}


Redline::Texture* Redline::Renderer::CreateTextureFromFile(const string& textureName) const
{
	Texture* texture = new Texture();
	if (!texture->CreateFromFile(textureName, m_device.Get()))
	{
		delete texture;
		return nullptr;
	}
	return texture;
}


Redline::Shader* Redline::Renderer::CreateShaderFromFile(const string& shaderName) const
{
	Shader* shader = new Shader();
	if (!shader->CreateFromFile(shaderName, m_device.Get(), m_deviceContext.Get()))
	{
		delete shader;
		return nullptr;
	}
	return shader;
}


Redline::VertexBuffer* Redline::Renderer::CreateVertexBuffer(const vector<Vertex>& vertices) const
{
	VertexBuffer* buffer = new VertexBuffer();
	if (!buffer->CreateFromVector(vertices, m_device.Get()))
	{
		delete buffer;
		return nullptr;
	}
	return buffer;
}


Redline::IndexBuffer* Redline::Renderer::CreateIndexBuffer(const vector<unsigned int>& indices) const
{
	IndexBuffer* buffer = new IndexBuffer();
	if (!buffer->CreateFromVector(indices, m_device.Get()))
	{
		delete buffer;
		return nullptr;
	}
	return buffer;
}
