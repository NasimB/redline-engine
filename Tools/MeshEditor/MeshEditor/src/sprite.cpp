#include "sprite.h"
#include <wchar.h>

Sprite::Sprite()
	: m_texture_size(0.0f, 0.0f),
	m_scale(1.0f, 1.0f),
	m_position(0.0f, 0.0f),
	m_pivot(0.5f, 0.5f),
	m_rotation(0.0f),
	m_color(0xFFFFFFFF),
	m_visible(true) {

}


Sprite::~Sprite() 
{

}


bool Sprite::Initialize(LPDIRECT3DDEVICE9 device, const wchar_t* name, int x_divisions, int y_divisions) 
{
	if (!m_texture.Initialize(device, name, x_divisions, y_divisions))
		return false;

	return true;
}


bool Sprite::Draw(LPD3DXSPRITE sprite) 
{
	if (!m_visible)
		return true;

	sprite->SetTransform(&m_transform);

	if (!m_texture.Draw(sprite, m_color))
		return false;

	return true;
}


void Sprite::RebuildMatrix() 
{
	D3DXMATRIX pivot, scale, rotation, translation;

	D3DXVECTOR2 texture_size = m_texture.GetTextureSize();

	D3DXMatrixTranslation(&pivot, -(texture_size.x * m_pivot.x), -(texture_size.y * m_pivot.y), 0.0f);

	D3DXMatrixScaling(&scale, m_scale.x, m_scale.y, 1.0f);

	D3DXMatrixRotationZ(&rotation, m_rotation * (D3DX_PI / 180.0f));

	D3DXMatrixTranslation(&translation, m_position.x, m_position.y, 0.0f);

	m_transform = pivot * scale * rotation * translation;
}