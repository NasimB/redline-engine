#include "timer.h"
#include "debug.h"

Redline::Timer::Timer() :
	m_start_time(0),
	m_ticks_per_ms(0),
	m_frame_tick(0),
	m_last_check(0),
	m_fps(0)
{
}

Redline::Timer::~Timer()
{
}


bool Redline::Timer::Initialize()
{
	INT64 frequency;

	QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&frequency));
	if (frequency == 0)
		return false;

	m_ticks_per_ms = static_cast<float>(frequency) * 0.001f;

	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&m_start_time));
	m_last_check = m_start_time;
	return true;
}


void Redline::Timer::Update()
{
	INT64 current_time;

	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&current_time));

	INT64 diff = current_time - m_last_check;
	float time = static_cast<float>(diff) / m_ticks_per_ms;
	time *= 0.001f;

	if (time >= 1.0)
	{
		m_fps = m_frame_tick;
		m_frame_tick = 0;
		m_last_check = current_time;
	}

	m_frame_tick++;
}


float Redline::Timer::GetDeltaTime()
{
	INT64 current_time;

	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&current_time));

	INT64 time_difference = current_time - m_start_time;
	float delta_time = static_cast<float>(time_difference) / m_ticks_per_ms;
	delta_time *= 0.001f;

	m_start_time = current_time;

	return delta_time;
}


unsigned int Redline::Timer::GetFPS() const
{
	return static_cast<unsigned int>(m_fps);
}


void Redline::Timer::StartCounter()
{
	LARGE_INTEGER li;

	if (!QueryPerformanceFrequency(&li))
		Debug::Err("QueryPerformanceFrequency failed!");

	PCFreq = double(li.QuadPart) / 1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}


double Redline::Timer::GetCounter() const
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart - CounterStart) / PCFreq;
}

double Redline::Timer::GetTime() const
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return static_cast<double>(li.QuadPart) / PCFreq;
}
