#pragma once

#include <d3d11.h>
#include <SimpleMath.h>

#include "camera.h"
#include "mesh.h"
#include "shader.h"

namespace Redline
{
	struct SkyboxVSBuffer
	{
		DirectX::XMVECTOR v3CameraPos; // The camera's current position
		DirectX::XMVECTOR v3InvWavelength; // 1 / pow(wavelength, 4) for the red, green, and blue channels
		float fCameraHeight; // The camera's current height
		float fCameraHeight2; // fCameraHeight^2
		float fOuterRadius; // The outer (atmosphere) radius
		float fOuterRadius2; // fOuterRadius^2
		float fInnerRadius; // The inner (planetary) radius
		float fInnerRadius2; // fInnerRadius^2
		float fKrESun; // Kr * ESun
		float fKmESun; // Km * ESun
		float fKr4PI; // Kr * 4 * PI
		float fKm4PI; // Km * 4 * PI
		float fScale; // 1 / (fOuterRadius - fInnerRadius)
		float fScaleDepth; // 
		float fScaleOverScaleDepth; // fScale / fScaleDepth
	};

	struct SkyboxPSBuffer
	{
		DirectX::XMVECTOR g; // X: G, Y: G^2
		DirectX::XMVECTOR lightDirection;
		DirectX::XMVECTOR cameraPosition;
	};

	class Skybox
	{
	public:
		Skybox();
		~Skybox();

		bool Initialize(Camera* camera);
		void UpdateCamera(const DirectX::SimpleMath::Vector3& cameraPosition, float farPlane);

		Mesh* GetMesh() const { return m_mesh; }
		Shader* GetShader() const { return m_shader; }

		SkyboxVSBuffer& GetVSBuffer() { return m_vsBuffer; }
		SkyboxPSBuffer& GetPSBuffer() { return m_psBuffer; }

		DirectX::SimpleMath::Matrix& GetMatrix() { return m_worldMatrix; }

	private:
		void SetSkyboxParameters(const DirectX::SimpleMath::Vector3& cameraPosition);

		Shader* m_shader = nullptr;
		Mesh* m_mesh = nullptr;

		SkyboxVSBuffer m_vsBuffer;
		SkyboxPSBuffer m_psBuffer;

		DirectX::SimpleMath::Matrix m_worldMatrix;

		DirectX::SimpleMath::Matrix m_translationMatrix;
		DirectX::SimpleMath::Matrix m_scaleMatrix;

		const float m_Kr = 0.0025f; // Rayleigh scattering constant
		const float m_Kr4PI = m_Kr * 4.0f * float(M_PI);
		const float m_Km = 0.0005f; // Mie scattering constant
		const float m_Km4PI = m_Km * 4.0f * float(M_PI);
		const float m_ESun = 50.0f; // Sun brightness constant
		const float m_g = -0.991f; // The Mie phase asymmetry factor

		const float m_fInnerRadius = 1.000f;
		const float m_fOuterRadius = 1.025f;
		const float m_fScale = 1 / (m_fOuterRadius - m_fInnerRadius);

		const float m_fWavelength[3] = {0.650f, 0.570f, 0.475f}; // 650 nm for red, 570 nm for green, 475 nm for blue

		const float m_fScaleDepth = 0.47f;
	};
}
