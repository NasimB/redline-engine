#pragma once

namespace Redline
{
	class Config
	{
	public:
		Config();
		~Config();

		bool Initialize();

		bool fullscreen = false;
		bool multithreading = false;
		bool vsync = true;
		int width = 1280;
		int height = 720;
		int shadow_res = 2048;
		int cubemap_res = 128;
		int texture_filtering = 4;
		int min_tex_lod = 0;
		int max_tex_lod = 64;
		int ssao_technique = 2;
		float render_scale = 1.0f;
	};
}
