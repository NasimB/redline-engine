#include "window.h"

Redline::Window::Window()
	: m_hwnd(nullptr),
	  m_hinstance(nullptr),
	  m_name(L"Redline Engine")
{
}


Redline::Window::~Window()
{
	DestroyWindow(m_hwnd);
	UnregisterClass(m_name.c_str(), m_hinstance);
}


bool Redline::Window::Initialize(bool fullscreen, int width, int height)
{
	RECT rect;
	DWORD window_style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
	m_hinstance = GetModuleHandle(nullptr);

	rect.top = (GetSystemMetrics(SM_CYSCREEN) / 2) - (height / 2);
	rect.bottom = rect.top + height;
	rect.left = (GetSystemMetrics(SM_CXSCREEN) / 2) - (width / 2);
	rect.right = rect.left + width;

	AdjustWindowRect(&rect, window_style, FALSE);

	WNDCLASSEX window_class;
	window_class.style = CS_HREDRAW | CS_VREDRAW | WS_EX_TOPMOST;
	window_class.lpfnWndProc = WndProc;
	window_class.cbClsExtra = 0;
	window_class.cbWndExtra = 0;
	window_class.hInstance = m_hinstance;
	window_class.hIcon = LoadIcon(nullptr, IDI_WINLOGO);
	window_class.hIconSm = window_class.hIcon;
	window_class.hCursor = LoadCursor(nullptr, IDC_ARROW);
	window_class.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW);
	window_class.lpszMenuName = nullptr;
	window_class.lpszClassName = m_name.c_str();
	window_class.cbSize = sizeof(WNDCLASSEX);
	RegisterClassEx(&window_class);


	if (fullscreen)
	{
		/*DEVMODE dmScreenSettings;

		// If full screen set the screen to maximum size of the users desktop and 32bit
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = static_cast<unsigned long>(width);
		dmScreenSettings.dmPelsHeight = static_cast<unsigned long>(height);
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);*/

		m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_name.c_str(), m_name.c_str(), WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP, 0, 0, width, height, nullptr, nullptr, m_hinstance, nullptr);
	}
	else
	{
		m_hwnd = CreateWindowEx(0, m_name.c_str(), m_name.c_str(), window_style, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, nullptr, nullptr, m_hinstance, nullptr);
	}

	if (!m_hwnd)
	{
		MessageBoxA(nullptr, "Window creation failed !", "Fatal Error", MB_OK | MB_ICONERROR);
		return false;
	}

	SetMenu(m_hwnd, nullptr);
	ShowWindow(m_hwnd, SW_SHOW);
	ShowCursor(true);
	return true;
}

extern LRESULT ImGui_ImplDX11_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT CALLBACK Redline::Window::WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	if (ImGui_ImplDX11_WndProcHandler(hwnd, message, wparam, lparam))
		return true;

	switch (message)
	{
	case WM_CLOSE:
	case WM_DESTROY:
		{
			PostQuitMessage(WM_QUIT);
			break;
		}
	default: break;
	}
	return DefWindowProc(hwnd, message, wparam, lparam);
}
