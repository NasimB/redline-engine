#pragma once

#include <vector>
#include "mesh.h"
#include "gameobject.h"
#pragma comment(lib, "assimp-vc140-mt.lib")


namespace Redline
{
	class Renderer;

	class Editor
	{
	public:
		Editor();
		~Editor();

		bool Initialize();

		bool LoadMeshFile(const std::string &filePath);

		bool SaveMeshFile(const std::string &filePath) const;

		void Update(float delta_time);
		

	private:
		void UpdateCamera(float delta_time);

		void UpdateFreeCam(float delta_time);
		void UpdateRotateAroundCam(float delta_time);

		void UpdateUI(float delta_time);

		void OpenFileDialog(HWND hwnd, LPCSTR filter);
		void SaveFileDialog(HWND hwnd, LPCSTR filter) const;

		Mesh* m_mesh;
		GameObject* m_gameobject;

		float phi = 0.0f;
		float theta = 0.0f;
		float height = 0.0f;
		float distance = 10.0f;

		int m_camera_mode = 1;

		float m_cameraSpeedMultiplier = 1.0f;

		char m_texture_folder[64];

		DirectX::SimpleMath::Vector3 m_rotation_offset;
	};
}
