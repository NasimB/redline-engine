#include "transform.h"
#include "debug.h"

using namespace DirectX::SimpleMath;

Redline::Transform::Transform() :
	m_scale(1.0f),
	m_position(0.0f, 0.0f, 0.0f),
	m_rotation(),
	m_localrotation(),
	isPosDirty(false),
	isRotDirty(false),
	isScaleDirty(false)
{
}


Redline::Transform::~Transform()
{
}


void Redline::Transform::RebuildMatrix()
{
	if (useMatrix)
		return;

	if (isPosDirty)
		m_positionMatrix = Matrix::CreateTranslation(m_position);

	if (isRotDirty)
		m_rotationMatrix = Matrix::CreateFromQuaternion(m_rotation);

	if (isScaleDirty)
		m_scaleMatrix = Matrix::CreateScale(m_scale);

	if (isPosDirty || isRotDirty || isScaleDirty)
		m_matrix = (m_scaleMatrix * m_rotationMatrix * m_positionMatrix);

	isPosDirty = false;
	isRotDirty = false;
	isScaleDirty = false;
	
	if (showGizmo)
	{
		const bool asleep = false;
		const Vector3 up = Vector3::Transform(Vector3::Up, m_rotation);
		const Vector3 right = Vector3::Transform(Vector3::Right, m_rotation);
		const Vector3 forward = Vector3::Transform(-Vector3::Forward, m_rotation);
		Debug::Line(m_position, m_position + right, Vector3(asleep ? 0.5f : 1.0f, 0.0f, 0.0f));
		Debug::Line(m_position, m_position + up, Vector3(0.0f, asleep ? 0.5f : 1.0f, 0.0f));
		Debug::Line(m_position, m_position + forward, Vector3(0.0f, 0.0f, asleep ? 0.5f : 1.0f));
	}
}
