#pragma once

#define DIRECTINPUT_VERSION 0x0800

#include <dinput.h>
#include <dinputd.h>
#include <d3d11.h>
#include <SimpleMath.h>

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

namespace Redline
{
	enum MOUSE_BUTTONS
	{
		MOUSE_LEFT = 0,
		MOUSE_RIGHT = 1
	};

	class Input
	{
	public:
		Input();
		~Input();

		bool Initialize(HINSTANCE, HWND, int, int);
		bool Update();

		bool IsEscapePressed();
		static bool GetButtonPressed(int keyCode);
		static bool GetButtonDown(int keyCode);
		static bool GetButtonUp(int keyCode);
		static bool GetMouseButtonPressed(int keyCode);
		static DirectX::SimpleMath::Vector2 GetMouseDelta() { return m_input->mouseDelta; }
		static POINT GetMouseLocation(bool absolute);
		static DIJOYSTATE2 GetWheelState() { return m_input->m_wheelState; }
		static bool IsWheelConnected() { return m_input->m_wheelConnected; }


	private:
		bool ReadKeyboard();
		bool ReadMouse();
		bool ReadWheel();
		void ProcessInput();
		static BOOL CALLBACK EnumJoystickObjectsCallback(const DIDEVICEOBJECTINSTANCE* pdidInstance, void* pvRef);

		struct DI_ENUM_CONTEXT
		{
			DIJOYCONFIG* pPreferredJoyCfg;
			bool bPreferredJoyCfgValid;
		};

		static Input* m_input;
		IDirectInput8* m_directInput;
		IDirectInputDevice8* m_keyboard;
		IDirectInputDevice8* m_mouse;
		IDirectInputDevice8* m_wheel;
		DirectX::SimpleMath::Vector2 mouseDelta;
		unsigned char m_previousKeyboardState[256];
		unsigned char m_keyboardState[256];
		DIMOUSESTATE m_mouseState;
		DIJOYSTATE2 m_wheelState;
		int m_screenWidth, m_screenHeight;
		int m_mouseX, m_mouseY;

		bool m_isKeyboardAquired;
		bool m_isMouseAquired;

		bool m_wheelConnected;
		bool m_isWheelAquired;
	};
}
