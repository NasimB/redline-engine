#pragma once

#include <d3d11.h>
#include <SimpleMath.h>

#define _USE_MATH_DEFINES
#include <math.h>
#define DEG2RAD M_PI / 180
#define RAD2DEG 180 / M_PI

namespace Redline
{
	class Camera
	{
	public:
		Camera();
		~Camera();

		bool Initialize(unsigned int width, unsigned int height);

		// Updates the camera vectors and matrices
		void Update(float deltaTime);

		// Returns the clipping planes (x: near, y: far)
		DirectX::SimpleMath::Vector2 GetClipPlanes() const { return DirectX::SimpleMath::Vector2(m_near_clip_plane, m_far_clip_plane); }
		// Sets the camera's clipping planes
		void SetClipPlanes(float near_clip, float far_clip)
		{
			m_near_clip_plane = near_clip;
			m_far_clip_plane = far_clip;
			m_rebuildProjMatrix = true;
		}

		// Sets the camera's field of view (in degrees)
		void SetFOV(float fov)
		{
			m_fov = fov * float(DEG2RAD);
			m_rebuildProjMatrix = true;
		}

		// Gets the camera's field of view (in degrees)
		float GetFOV() const { return m_fov * float(RAD2DEG); }
		// Sets the camera's aspect ratio (width / height)
		void SetAspectRatio(float ratio)
		{
			m_aspect = ratio;
			m_rebuildProjMatrix = true;
		}

		// Gets the camera's aspect ratio (width / height)
		float GetAspectRatio() const { return m_aspect; }

		// Returns camera position
		DirectX::SimpleMath::Vector3 GetPosition() const { return m_position; }
		// Sets the camera position
		void SetPosition(const DirectX::SimpleMath::Vector3& pos)
		{
			m_position = pos;
			m_rebuildViewMatrix = true;
		}

		// Returns camera rotation
		DirectX::SimpleMath::Vector3 GetRotation() const { return m_rotation; }
		// Sets the camera rotation (in degrees)
		void SetRotation(const DirectX::SimpleMath::Vector3& rot)
		{
			m_rotation = rot;
			m_rebuildViewMatrix = true;
		}

		// Rotates the camera towards a point
		void SetLookAt(const DirectX::SimpleMath::Vector3& at)
		{
			DirectX::SimpleMath::Vector3 v = at - m_position;
			v.Normalize();

			float pitch = asin(v.y);
			float yaw = -atan2(v.z, v.x) - float(M_PI_2);

			m_rotation = DirectX::SimpleMath::Vector3(pitch * float(RAD2DEG), yaw * float(RAD2DEG), 0);
			m_rebuildViewMatrix = true;
		}

		// Returns the camera's forward vector
		DirectX::SimpleMath::Vector3 GetForwardVector() const { return m_forward; }
		// Returns the camera's right vector
		DirectX::SimpleMath::Vector3 GetRightVector() const { return m_right; }
		// Returns the camera's up vector
		DirectX::SimpleMath::Vector3 GetUpVector() const { return m_up; }

		// Returns the camera's view matrix (Camera::Update must be called before)
		DirectX::SimpleMath::Matrix GetViewMatrix() const { return m_view_matrix; }
		// Returns the camera's projection matrix (Camera::Update must be called before)
		DirectX::SimpleMath::Matrix GetProjectionMatrix() const { return m_projection_matrix; }

	protected:
		DirectX::SimpleMath::Vector3 m_default_forward;
		DirectX::SimpleMath::Vector3 m_default_right;
		DirectX::SimpleMath::Vector3 m_default_up;

		DirectX::SimpleMath::Matrix m_rotation_matrix;

		DirectX::SimpleMath::Matrix m_view_matrix;
		DirectX::SimpleMath::Matrix m_projection_matrix;

		DirectX::SimpleMath::Vector3 m_position;
		DirectX::SimpleMath::Vector3 m_rotation;

		DirectX::SimpleMath::Vector3 m_forward;
		DirectX::SimpleMath::Vector3 m_up;
		DirectX::SimpleMath::Vector3 m_right;

		float m_near_clip_plane = 0.1f;
		float m_far_clip_plane = 50000.0f;
		float m_fov = 60.0f * float(DEG2RAD);
		float m_aspect = 1.0f;

		bool m_rebuildProjMatrix = false;
		bool m_rebuildViewMatrix = false;
	};
}
