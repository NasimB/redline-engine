cbuffer MatrixBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;
}

cbuffer MaterialBuffer : register(b1)
{
	float3 diffuseColor;
	float roughness;
	float metallic;
	bool hasDiffuseMap;
	bool hasNormalMap;
	bool hasSpecularMap;
}

cbuffer SkyboxPSBuffer : register(b2)
{
	float2 g; // X: G, Y: G^2
	float3 lightDirection;
	float3 cameraPosition;
}

struct VS_INPUT
{
	float4 Pos : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 Binormal : BINORMAL0;
};

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL0;
};

Texture2D diffuseTexture : register(t0);
SamplerState samplerLinear : register(s0);

VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output;
	output.Pos = mul(mul(mul(input.Pos, World), View), Projection);
	output.TexCoord = input.TexCoord;
	output.Normal = mul(input.Normal, World).xyz;
	return output;
}


float4 PS(VS_OUTPUT input) : SV_Target
{
	float4 diffuse = float4(diffuseColor, 1.0);
	if (hasDiffuseMap)
	{
		diffuse *= pow(diffuseTexture.Sample(samplerLinear, input.TexCoord), 2.2);
		clip(diffuse.a - 0.5);
	}
	return float4(diffuse.rgb * max(dot(lightDirection, input.Normal) * saturate(lightDirection.y * 8.0), 0.1), 1.0);
}
