//
// Atmospheric scattering "GroundFromAtmosphere" shader
//
// Author: Sean O'Neil
// Ported to HLSL by Nasim BOUGUERRA
//
// Copyright (c) 2004 Sean O'Neil
//

cbuffer ConstantBuffer : register(b0)
{
	matrix viewToWorldMatrix;
	bool useShadow;
}

cbuffer SkyboxVSBuffer : register(b1)
{
	float3 v3CameraPos;			// The camera's current position
	float4 v3InvWavelength;		// 1 / pow(wavelength, 4) for the red, green, and blue channels
	float fCameraHeight;		// The camera's current height
	float fCameraHeight2;		// fCameraHeight^2
	float fOuterRadius;			// The outer (atmosphere) radius
	float fOuterRadius2;		// fOuterRadius^2
	float fInnerRadius;			// The inner (planetary) radius
	float fInnerRadius2;		// fInnerRadius^2
	float fKrESun;				// Kr * ESun
	float fKmESun;				// Km * ESun
	float fKr4PI;				// Kr * 4 * PI
	float fKm4PI;				// Km * 4 * PI
	float fScale;				// 1 / (fOuterRadius - fInnerRadius)
	float fScaleDepth;			// Where the average atmosphere density is found
	float fScaleOverScaleDepth;	// (1.0f / (m_fOuterRadius - m_fInnerRadius)) / m_fRayleighScaleDepth
}

cbuffer SkyboxPSBuffer : register(b2)
{
	float2 g; // X: G, Y: G^2
	float3 lightDirection;
	float3 cameraPosition;
}

cbuffer ShadowsBuffer : register(b3)
{
	matrix lightView;
	matrix lightProjectionOne;
	matrix lightProjectionTwo;
	matrix lightProjectionThree;
	float shadowResolution;
	float distance;
}

Texture2D baseTexture : register(t0);
Texture2D positionTexture : register(t1);

Texture2D<float> shadowMapOne : register(t2);
Texture2D<float> shadowMapTwo : register(t3);
Texture2D<float> shadowMapThree : register(t4);

SamplerState samplerPoint : register(s0);
SamplerState samplerLinear : register(s1);


float ShadowContribution(float3 position)
{
	float4 ShadowProjectionOne = mul(mul(float4(position, 1.0), lightView), lightProjectionOne);
	float2 projectedTexCoords = float2((ShadowProjectionOne.x / ShadowProjectionOne.w / 2.0) + 0.5, (-ShadowProjectionOne.y / ShadowProjectionOne.w / 2.0) + 0.5);
	
	if (saturate(projectedTexCoords.x) == projectedTexCoords.x && saturate(projectedTexCoords.y) == projectedTexCoords.y)
	{
		float depth = ShadowProjectionOne.z / ShadowProjectionOne.w;
		float shadowMapDepth = shadowMapOne.Sample(samplerLinear, projectedTexCoords);

		if (depth - shadowMapDepth > 0.0001)
			return 0.0;
		
		return 1.0;
	}
	return 1.0;
}



// The number of sample points taken along the ray
static const int nSamples = 4;
static const float fSamples = (float)nSamples;

// The scale equation calculated by Vernier's Graphical Analysis
float scale(float fCos)
{
	float x = 1.0f - fCos;
	return fScaleDepth * exp(-0.00287f + x * (0.459f + x * (3.83f + x * (-6.80f + x * 5.25f))));
}


struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
	return output;
}


float4 PS(VS_OUTPUT input) : SV_Target
{
	float3 position = mul(float4(positionTexture.Sample(samplerPoint, input.TexCoord).xyz, 1.0f), viewToWorldMatrix).xyz;
	float3 color = baseTexture.Sample(samplerPoint, input.TexCoord).rgb;

	//return color.xyzz;
	
	// Get the ray from the camera to the vertex, and its length (which is the far point of the ray passing through the atmosphere)
	float3 v3Pos = (position - cameraPosition) * 2e-6;
	v3Pos.y += fInnerRadius;
	float3 v3Ray = v3Pos - v3CameraPos;
	float fFar = length(v3Ray);
	v3Ray /= fFar;

	// Calculate the ray's starting position, then calculate its scattering offset
	float3 v3Start = v3CameraPos;
	float fDepth = exp((fInnerRadius - fCameraHeight) / fScaleDepth);
	float fCameraAngle = 1.0f; // dot(-v3Ray, v3Pos) / length(v3Pos);
	float fLightAngle = dot(lightDirection, v3Pos) / length(v3Pos);
	float fCameraScale = scale(fCameraAngle);
	float fLightScale = scale(fLightAngle);
	float fCameraOffset = fDepth * fCameraScale;
	float fTemp = (fLightScale + fCameraScale);

	// Initialize the scattering loop variables
	float fSampleLength = fFar / fSamples;
	float fScaledLength = fSampleLength * fScale;
	float3 v3SampleRay = v3Ray * fSampleLength;
	float3 v3SamplePoint = v3Start + v3SampleRay * 0.5f;

	// Now loop through the sample rays
	float3 v3FrontColor = float3(0.0f, 0.0f, 0.0f);
	float3 v3Attenuate = float3(0.0f, 0.0f, 0.0f);

	float3 startWorldPos = cameraPosition;
	float3 sampleWorldPos = ((position - cameraPosition) / fSamples);

	for (float i = 0; i < fSamples; i++)
	{
		float3 worldPos = startWorldPos + sampleWorldPos * i;
		
		float sunShadow = useShadow ? ShadowContribution(worldPos) : 1.0;

		float fHeight = length(v3SamplePoint);
		float fDepth = exp(fScaleOverScaleDepth * (fInnerRadius - fHeight));
		float fScatter = fDepth * fTemp - fCameraOffset;
		v3Attenuate = exp(-fScatter * (v3InvWavelength.rgb * fKr4PI + fKm4PI));
		v3FrontColor += v3Attenuate * (fDepth * fScaledLength) * sunShadow;

		v3SamplePoint += v3SampleRay;
	}

	float3 gl_FrontColor = v3FrontColor * (v3InvWavelength.rgb * fKrESun + fKmESun);

	// Calculate the attenuation factor for the ground
	float3 gl_FragColor = gl_FrontColor + color * max(v3Attenuate, 1.0 - saturate(lightDirection.y * 10.0f));
	//float3 ambiant = float3(0.12, 0.12, 0.20) * color.rgb * (1 - v3Attenuate);
	return float4(gl_FragColor/* + ambiant*/, 1.0f);
}
