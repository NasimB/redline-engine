//
// Screen Space Ambient Occlusion shader
//
// Author: Nasim BOUGUERRA
// 

static const float fMXAONormalBias = 0.2; // Occlusion Cone bias to reduce self-occlusion of surfaces that have a low angle to each other.
static const float fMXAOBlurSharpness = 20.00; // MXAO sharpness, higher means AO blurs less across geometry edges but may leave some noisy areas.
static const int fMXAOBlurSteps = 3; // Offset count for MXAO bilateral blur filter. Higher means smoother but also blurrier AO.

static const float fMXAOFadeoutStart = 0.3; // Distance where MXAO starts to fade out. 0.0 = camera, 1.0 = sky. Must be less than Fade Out End.
static const float fMXAOFadeoutEnd = 0.5; // Distance where MXAO completely fades out. 0.0 = camera, 1.0 = sky. Must be greater than Fade Out Start.

cbuffer SSAOBuffer : register(b0)
{
	float halfWidth;
	float halfHeight;
	float aspectRatio;
	float dummy;
}

Texture2D positionTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D colorTexture : register(t2);

SamplerState samplerLinear : register(s0);

static const float3 sampleSphere[] = {
	float3(0.2024537f, 0.841204f, -0.9060141f),
	float3(-0.2200423f, 0.6282339f,-0.8275437f),
	float3(0.3677659f, 0.1086345f,-0.4466777f),
	float3(0.8775856f, 0.4617546f,-0.6427765f),
	float3(0.7867433f,-0.141479f, -0.1567597f),
	float3(0.4839356f,-0.8253108f,-0.1563844f),
	float3(0.4401554f,-0.4228428f,-0.3300118f),
	float3(0.0019193f,-0.8048455f, 0.0726584f),
	float3(-0.7578573f,-0.5583301f, 0.2347527f),
	float3(-0.4540417f,-0.252365f, 0.0694318f),
	float3(-0.0483353f,-0.2527294f, 0.5924745f),
	float3(-0.4192392f, 0.2084218f,-0.3672943f),
	float3(-0.8433938f, 0.1451271f, 0.2202872f),
	float3(-0.4037157f,-0.8263387f, 0.4698132f),
	float3(-0.6657394f, 0.6298575f, 0.6342437f),
	float3(-0.0001783f, 0.2834622f, 0.8343929f),
};


struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
	return output;
}


float3 randomNormal(float2 tex)
{
	float noiseX = (frac(sin(dot(tex, float2(15.8989f, 76.132f)*1.0f))*46336.23745f));
	float noiseY = (frac(sin(dot(tex, float2(11.9899f, 62.223f)*2.0f))*34748.34744f));
	float noiseZ = (frac(sin(dot(tex, float2(13.3238f, 63.122f)*3.0f))*59998.47362f));
	return normalize(float3(noiseX, noiseY, noiseZ));
}


float3 GetPosition(float2 coords)
{
	return positionTexture.Sample(samplerLinear, coords).xyz * -0.001;
}

float3 GetNormal(float2 coords)
{
	return normalize(normalTexture.Sample(samplerLinear, coords).xyz);
}

float4 PS(VS_OUTPUT input) : SV_Target
{
	float3 normal = GetNormal(input.TexCoord);
	if (length(normal.xyz) == 0.0f) discard; // No normal = no pixel to draw here

	float3 position = GetPosition(input.TexCoord);

	float3 randNorm = randomNormal(input.TexCoord);

	const unsigned int samples = 8;
	const float radius = 0.0005f / position.z;
	const float strength = 1.0f;    	    	    	//Per step strength 
	const float totalStrength = 1.5f;    	    	//Total strength 
	const float falloffMin = 0.0001f;    	    	//Depth falloff min value 
	const float falloffMax = 0.002f;    	    	//Depth falloff max value 

	float3 centerPos = float3(input.TexCoord, position.z);

	float occlusion = 0.0;
	for (unsigned int i = 0; i < samples; ++i)
	{
		// get sample position

		float3 offset = reflect(sampleSphere[i*2], randNorm); // Reflect sample to random direction. 
		offset = sign(dot(offset, normal)) * offset; // Convert to hemisphere. 
		offset.y = -offset.y;

		float3 ray = centerPos + offset * radius;

		// Linear depth at ray.xy 
		float occDepth = positionTexture.Sample(samplerLinear, ray.xy).z * -0.001;
		
		// Viewspace normal at ray.xy.
		float3 occNormal = GetNormal(ray.xy);
		
		float depthDifference = (centerPos.z - occDepth);

		float normalDifference = dot(occNormal, normal);

		//Occlusion dependet on angle between normals, smaller angle causes more occlusion.
		float normalOcc = (1.0f - saturate(normalDifference));
		
		//Occlusion dependent on depth difference, limited using falloffMin and falloffMax. 
		//helps to reduce self occlusion and halo-artifacts. Try a bit around with falloffMin/Max. 
		float depthOcc = step(falloffMin, depthDifference) * (1.0f - smoothstep(falloffMin, falloffMax, depthDifference));
		
		occlusion += saturate(depthOcc * normalOcc * strength);
	}

	occlusion /= samples; // Divide by number of samples to get the average occlusion. 
	occlusion = saturate(pow(1.0f - occlusion, totalStrength));

	return float4(0.0, 0.0, 0.0, occlusion);
}


/*
Calculates weights for bilateral AO blur. Using only
depth is surely faster but it doesn't really cut it, also
areas with a flat angle to the camera will have high depth
differences, hence blur will cause stripes as seen in many
AO implementations, even HBAO+. Taking view angle into
account greatly helps to reduce these problems.
*/
void GetBlurWeight(in float4 tempKey, in float4 centerKey, in float surfacealignment, inout float weight)
{
	float depthdiff = abs(tempKey.w - centerKey.w);
	float normaldiff = 1.0 - saturate(dot(normalize(tempKey.xyz), normalize(centerKey.xyz)));

	float depthweight = saturate(rcp(fMXAOBlurSharpness*depthdiff*5.0*surfacealignment));
	float normalweight = saturate(rcp(fMXAOBlurSharpness*normaldiff*10.0));

	weight = min(normalweight, depthweight) * 2;
}

/*
Fetches normal,depth and AO/IL data from the respective buffers.
AO only: backbuffer rgb - normal, backbuffer alpha - AO.
IL enabled: backbuffer rgb - IL, backbuffer alpha - AO.
*/
void GetBlurKeyAndSample(in float2 texcoord, inout float4 tempsample, inout float4 key)
{
	tempsample = colorTexture.Sample(samplerLinear, texcoord, 0);
	key = float4(GetNormal(texcoord), GetPosition(texcoord).z);
}


/*
Bilateral blur, exploiting bilinear filter
for sample count reduction by sampling 2 texels
at once.
*/
float4 GetBlurredAO(float2 texcoord, float2 axisscaled, int nSteps)
{
	float4 tempsample;
	float4 centerkey, tempkey;
	float  centerweight = 1.0, tempweight;
	float4 blurcoord = 0.0;

	GetBlurKeyAndSample(texcoord.xy, tempsample, centerkey);
	float surfacealignment = saturate(-dot(centerkey.xyz, normalize(float3(texcoord.xy*2.0 - 1.0, 1.0)*centerkey.w)));

	float AO = tempsample.w;

	[loop]
	for (int iStep = 1; iStep <= nSteps; iStep++)
	{
		float currentLinearstep = iStep * 2.0 - 0.5;

		GetBlurKeyAndSample(texcoord.xy + currentLinearstep * axisscaled, tempsample, tempkey);
		GetBlurWeight(tempkey, centerkey, surfacealignment, tempweight);

		AO += tempsample.w * tempweight;
		centerweight += tempweight;

		GetBlurKeyAndSample(texcoord.xy - currentLinearstep * axisscaled, tempsample, tempkey);
		GetBlurWeight(tempkey, centerkey, surfacealignment, tempweight);

		AO += tempsample.w * tempweight;
		centerweight += tempweight;
	}

	return float4(centerkey.xyz * 0.5 + 0.5, AO / centerweight);
}


float4 PS_BlurX(VS_OUTPUT input) : SV_Target
{
	return GetBlurredAO(input.TexCoord.xy, float2(halfWidth, 0.0), fMXAOBlurSteps);
}


float4 PS_BlurY(VS_OUTPUT input) : SV_Target
{
	float scenedepth = GetPosition(input.TexCoord).z;
	float occlusion = GetBlurredAO(input.TexCoord.xy, float2(0.0, halfHeight), fMXAOBlurSteps).w;
	occlusion = lerp(occlusion, 1.0, smoothstep(fMXAOFadeoutStart, fMXAOFadeoutEnd, scenedepth));
	return float4(0.0, 0.0, 0.0, occlusion);
}
