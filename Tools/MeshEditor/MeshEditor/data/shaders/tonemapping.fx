Texture2D baseTexture : register(t0);
Texture2D bloomTexture : register(t1);
Texture2D<float> exposureTexture : register(t2);

SamplerState baseSampler : register(s0);

cbuffer LuminanceBuffer : register(b0)
{
	float deltaTime;
	float minExposure;
	float maxExposure;
	float adaptationSpeed;
}


struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
	return output;
}


float Log2Exposure(in float avgLuminance)
{
	static const float KeyValue = 0.18;
	avgLuminance = max(avgLuminance, 0.00001f);
	float linearExposure = (KeyValue / avgLuminance);
	return log2(max(linearExposure, 0.00001f));
}

float LinearExposure(in float avgLuminance)
{
	static const float KeyValue = 0.18;
	avgLuminance = max(avgLuminance, 0.00001f);
	float linearExposure = (KeyValue / avgLuminance);
	return max(linearExposure, 0.00001f);
}

float3 ACESFilm(float3 x)
{
	float a = 2.51f;
	float b = 0.03f;
	float c = 2.43f;
	float d = 0.59f;
	float e = 0.14f;
	return saturate((x*(a*x + b)) / (x*(c*x + d) + e));
}

float CalcLuminance(float3 color)
{
	return max(dot(color, float3(0.2126, 0.7152, 0.0722)), 0.0001);
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
	float3 color = baseTexture.Sample(baseSampler, input.TexCoord).rgb;
	float3 bloom = bloomTexture.Sample(baseSampler, input.TexCoord).rgb;

	const float BloomAmmount = 0.26;
	color = lerp(color, bloom, BloomAmmount);

	float2 vtc = float2(input.TexCoord - 0.5);
	float vignette = saturate(pow(1 - (dot(vtc, vtc) * 0.5), 8.0));

	float exposure = exposureTexture.SampleLevel(baseSampler, float2(0.5, 0.5), 0);

	float3 ldrPixel = ACESFilm(color * vignette * exposure);

	return float4(ldrPixel, 1.0);
}

float PS_LuminanceAdaptation(VS_OUTPUT input) : SV_Target
{
	float lum = CalcLuminance(baseTexture.SampleLevel(baseSampler, float2(0.5, 0.5), 100).rgb);
	float exposure = LinearExposure(lum);
	float oldExposure = bloomTexture.SampleLevel(baseSampler, float2(0.5, 0.5), 0).r;

	float newExposure = lerp(oldExposure, exposure, 1.0 - exp(-adaptationSpeed * deltaTime));
	return clamp(newExposure, minExposure, maxExposure);
}