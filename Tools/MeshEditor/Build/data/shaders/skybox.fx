//
// Atmospheric scattering "SkyFromAtmosphere" shader
//
// Author: Sean O'Neil
// Ported to HLSL by Nasim BOUGUERRA
//
// Copyright (c) 2004 Sean O'Neil
//

cbuffer MatrixBuffer : register(b1)
{
	matrix World;
	matrix View;
	matrix Projection;
}

cbuffer SkyboxVSBuffer : register(b2)
{
	float3 v3CameraPos;			// The camera's current position
	float4 v3InvWavelength;		// 1 / pow(wavelength, 4) for the red, green, and blue channels
	float fCameraHeight;		// The camera's current height
	float fCameraHeight2;		// fCameraHeight^2
	float fOuterRadius;			// The outer (atmosphere) radius
	float fOuterRadius2;		// fOuterRadius^2
	float fInnerRadius;			// The inner (planetary) radius
	float fInnerRadius2;		// fInnerRadius^2
	float fKrESun;				// Kr * ESun
	float fKmESun;				// Km * ESun
	float fKr4PI;				// Kr * 4 * PI
	float fKm4PI;				// Km * 4 * PI
	float fScale;				// 1 / (fOuterRadius - fInnerRadius)
	float fScaleDepth;			// Where the average atmosphere density is found
	float fScaleOverScaleDepth;	// (1.0f / (m_fOuterRadius - m_fInnerRadius)) / m_fRayleighScaleDepth
}

cbuffer SkyboxPSBuffer : register(b3)
{
	float2 g; // X: G, Y: G^2
	float3 lightDirection;
	float3 cameraPosition;
}


// The number of sample points taken along the ray
static const int nSamples = 4;
static const float fInvSamples = 1 / (float)nSamples;

// The scale equation calculated by Vernier's Graphical Analysis
float scale(float fCos)
{
	float x = 1.0f - fCos;
	return fScaleDepth * exp(-0.00287f + x * (0.459f + x * (3.83f + x * (-6.80f + x * 5.25f))));
}

// Calculates the Mie phase function
float getMiePhase(float fCos, float fCos2, float g, float g2)
{
	return 1.5f * ((1.0f - g2) / (2.0f + g2)) * (1.0f + fCos2) / pow(1.0f + g2 - 2.0f * g * fCos, 1.5f);
}

// Calculates the Rayleigh phase function
float getRayleighPhase(float fCos2)
{
	return 0.75f + 0.75f * fCos2;
}

// Returns the near intersection point of a line and a sphere
float getNearIntersection(float3 v3Pos, float3 v3Ray, float fDistance2, float fRadius2)
{
	float B = 2.0 * dot(v3Pos, v3Ray);
	float C = fDistance2 - fRadius2;
	float fDet = max(0.0, B*B - 4.0f * C);
	return 0.5f * (-B - sqrt(fDet));
}

// Returns the far intersection point of a line and a sphere
float getFarIntersection(float3 v3Pos, float3 v3Ray, float fDistance2, float fRadius2)
{
	float B = 2.0f * dot(v3Pos, v3Ray);
	float C = fDistance2 - fRadius2;
	float fDet = max(0.0f, B*B - 4.0f * C);
	return 0.5f * (-B + sqrt(fDet));
}


struct VS_INPUT
{
	float4 Pos : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 Binormal : BINORMAL0;
};


struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float4 c0 : TEXCOORD0;
	float4 c1 : TEXCOORD1;
	float3 v3Direction : TEXCOORD2;
};


VS_OUTPUT VS(VS_INPUT input)
{
	float3 vertPos = input.Pos.xyz;
	if (vertPos.y < 0.0)
		vertPos.y = 0.0;

	vertPos = normalize(vertPos);

	// Get the ray from the camera to the vertex, and its length (which is the far point of the ray passing through the atmosphere)
	float3 v3Pos = vertPos;
	v3Pos.y += fInnerRadius;
	float3 v3Ray = v3Pos - v3CameraPos;
	float fFar = length(v3Ray);
	v3Ray /= fFar;

	// Calculate the ray's starting position, then calculate its scattering offset
	float3 v3Start = v3CameraPos;
	float fHeight = length(v3Start);
	float fDepth = exp(fScaleOverScaleDepth * (fInnerRadius - fCameraHeight));
	float fStartAngle = dot(v3Ray, v3Start) / fHeight;
	float fStartOffset = fDepth * scale(fStartAngle);

	// Initialize the scattering loop variables
	float fSampleLength = fFar * fInvSamples;
	float fScaledLength = fSampleLength * fScale;
	float3 v3SampleRay = v3Ray * fSampleLength;
	float3 v3SamplePoint = v3Start + v3SampleRay * 0.5f;

	// Now loop through the sample rays
	float3 v3FrontColor = float3(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < nSamples; i++)
	{
		float fHeight = length(v3SamplePoint);
		float fDepth = exp(fScaleOverScaleDepth * (fInnerRadius - fHeight));
		float fLightAngle = dot(lightDirection, v3SamplePoint) / fHeight;
		float fCameraAngle = dot(v3Ray, v3SamplePoint) / fHeight;
		float fScatter = (fStartOffset + fDepth * (scale(fLightAngle) - scale(fCameraAngle)));
		float3 v3Attenuate = exp(-fScatter * (v3InvWavelength.rgb * fKr4PI + fKm4PI));

		v3FrontColor += v3Attenuate * (fDepth * fScaledLength);
		v3SamplePoint += v3SampleRay;
	}

	// Finally, scale the Mie and Rayleigh colors and set up the varying variables for the pixel shader
	VS_OUTPUT output;
	output.Pos = mul(mul(mul(input.Pos, World), View), Projection);
	output.c0.xyz = v3FrontColor * (v3InvWavelength.rgb * fKrESun);
	output.c0.w = 1.0f;
	output.c1.xyz = v3FrontColor * fKmESun;
	output.c1.w = 1.0f;
	output.v3Direction = v3CameraPos - v3Pos;
	return output;
}


float4 PS(VS_OUTPUT input) : SV_Target
{
	if (input.v3Direction.y > 0.0)
		discard;

	float fCos = dot(lightDirection, input.v3Direction) / length(input.v3Direction);
	float fCos2 = fCos * fCos;

	float4 color = getRayleighPhase(fCos2) * input.c0 + getMiePhase(fCos, fCos2, g.x, g.y) * input.c1;
	color.a = 1.0f; // color.b;
	return color;
}
