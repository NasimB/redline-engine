cbuffer MatrixBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;
}

struct VS_INPUT
{
	float4 Pos : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 Binormal : BINORMAL0;
};

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float4 ColorDepth : COLOR0;
};


VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output;
	output.Pos = mul(mul(mul(input.Pos, World), View), Projection);
	output.ColorDepth = float4(input.Normal.rgb, output.Pos.z);
	return output;
}


float4 PS(VS_OUTPUT input) : SV_Target
{
	float depth = input.ColorDepth.a;
	float3 color = input.ColorDepth.rgb * depth;
	return float4(input.ColorDepth.rgb, 1.0f);
}
