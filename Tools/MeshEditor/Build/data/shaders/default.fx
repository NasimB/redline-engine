cbuffer ConstantBuffer : register(b0)
{
	matrix viewToWorldMatrix;
	bool useShadow;
}

#define MAX_LIGHTS 63

#define PI 3.14159265359

#define Directionnal 0
#define Point 1
#define Spot 2

struct Light 
{
	float3 color;
	float intensity;
	float3 position;
	int type;
	float3 direction;
	float cutoff; // for spot lights
};

cbuffer LightsBuffer : register(b1)
{
	Light lights[MAX_LIGHTS];
	int numLights;
}

cbuffer ShadowsBuffer : register(b2)
{
	matrix lightView;
	matrix lightProjectionOne;
	matrix lightProjectionTwo;
	matrix lightProjectionThree;
	float shadowResolution;
}

Texture2D positionTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D colorTexture : register(t2);
Texture2D occlusionTexture : register(t3);

Texture2D<float> shadowMapOne : register(t4);
Texture2D<float> shadowMapTwo : register(t5);
Texture2D<float> shadowMapThree : register(t6);

TextureCube skyboxTexture : register(t7);

SamplerState samplerPoint : register(s0);
SamplerState samplerLinear : register(s1);
SamplerState samplerShadow : register(s2);


struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
	return output;
}


float linstep(float min, float max, float v)
{
	return clamp((v - min) / (max - min), 0, 1);
}


float ShadowContribution(matrix projMat, Texture2D<float> shadowTex, float3 position)
{
	float4 ShadowProjection = mul(mul(float4(position, 1.0f), lightView), projMat);

	float2 projectedTexCoords = float2((ShadowProjection.x / ShadowProjection.w / 2.0f) + 0.5f, (-ShadowProjection.y / ShadowProjection.w / 2.0f) + 0.5f);

	if (saturate(projectedTexCoords.x) != projectedTexCoords.x || saturate(projectedTexCoords.y) != projectedTexCoords.y)
		return 1.0f;

	float depth = ShadowProjection.z / ShadowProjection.w;

	//if (depth <= 0.0 || depth >= 1.0f)
	//	return depth;

	float w;
	float h;
	shadowTex.GetDimensions(w, h);

	float pixelOffsetW = 1.0f / w;
	float pixelOffsetH = 1.0f / h;

	float shadow = 1.0f;

	float blockerDepth = 0.0;
	float blockerCount = 0.0;
	for (float y = -1.5; y <= 1.5; y += 1.0)
	{
		for (float x = -1.5; x <= 1.5; x += 1.0)
		{
			float shadowMapDepth = shadowTex.SampleLevel(samplerShadow, projectedTexCoords + float2(x * pixelOffsetW * 2.0, y * pixelOffsetH * 2.0), 1);

			if (shadowMapDepth < depth)
			{
				blockerDepth += shadowMapDepth;
				blockerCount += 1.0;
			}
		}
	}

	blockerDepth /= blockerCount;

	const float lightSize = 1.5;
	const float radius = max((depth - blockerDepth) * lightSize / blockerDepth, 0.05);
	
	for (y = -2.0; y <= 2.0; y += 1.0)
	{
		for (float x = -2.0; x <= 2.0; x += 1.0)
		{
			float4 ShadowProjectionPenumbra = mul(mul(float4(position + float3(x * radius, 0, y * radius), 1.0f), lightView), projMat);

			float2 projectedTexCoordsPenumbra = float2((ShadowProjectionPenumbra.x / ShadowProjectionPenumbra.w / 2.0f) + 0.5, (-ShadowProjectionPenumbra.y / ShadowProjectionPenumbra.w / 2.0f) + 0.5);

			if (projectedTexCoordsPenumbra.x < 0 || projectedTexCoordsPenumbra.x > 1 || projectedTexCoordsPenumbra.y < 0 || projectedTexCoordsPenumbra.y > 1)
				continue;
			
			projectedTexCoordsPenumbra.x = (round(projectedTexCoordsPenumbra.x / pixelOffsetW) * pixelOffsetW) + 0.5 * pixelOffsetW;
			projectedTexCoordsPenumbra.y = (round(projectedTexCoordsPenumbra.y / pixelOffsetH) * pixelOffsetH) + 0.5 * pixelOffsetH;

			float shadowMapDepth = shadowTex.SampleLevel(samplerShadow, projectedTexCoordsPenumbra, 0);

			if (depth - shadowMapDepth > 0.00001)
				shadow -= 1.0 / 25.0;
		}
	}

	return shadow;
}

float Specular_D(float a, float NdH)
{
	// Isotropic ggx.
	float a2 = a*a;
	float NdH2 = NdH * NdH;

	float denominator = NdH2 * (a2 - 1.0f) + 1.0f;
	denominator *= denominator;
	denominator *= PI;

	return a2 / denominator;
}

float3 Specular_F(float3 specularColor, float3 h, float3 v)
{
	return (specularColor + (1.0f - specularColor) * pow((1.0f - saturate(dot(v, h))), 5));
}

float3 Specular_F_Roughness(float3 specularColor, float a, float3 h, float3 v)
{
	// Sclick using roughness to attenuate fresnel.
	return (specularColor + (max(1.0f - a, specularColor) - specularColor) * pow((1 - saturate(dot(v, h))), 5));
}

float Specular_G(float a, float NdV, float NdL, float NdH, float VdH, float LdV)
{
	// Smith schlick-GGX.
	float k = a * 0.5f;
	float GV = NdV / (NdV * (1 - k) + k);
	float GL = NdL / (NdL * (1 - k) + k);

	return GV * GL;
}

float3 Specular(float3 specularColor, float3 h, float3 v, float3 l, float a, float NdL, float NdV, float NdH, float VdH, float LdV)
{
	return ((Specular_D(a, NdH) * Specular_G(a, NdV, NdL, NdH, VdH, LdV)) * Specular_F(specularColor, v, h)) / (4.0f * NdL * NdV + 0.0001f);
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
	float4 normal = normalTexture.SampleLevel(samplerPoint, input.TexCoord, 0); // RGB: normal A: metallic
	if (length(normal.xyz) == 0.0f) discard; // No normal = no pixel to draw here

	float4 position = positionTexture.SampleLevel(samplerPoint, input.TexCoord, 0);
	float4 color = colorTexture.SampleLevel(samplerPoint, input.TexCoord, 0); // RGB: color A: roughness

	float4 occlusion = occlusionTexture.SampleLevel(samplerPoint, input.TexCoord, 0);

	// set important material values
	float roughness = color.a; // 0: smooth, 1: rough
	float metallic = normal.a; // 0: plastic, 1: metal
	
	float pixelDistance = abs(position.z);
	float3 V = -normalize(position.xyz); // view vector
	float3 N = normalize(normal.rgb);

	static const float NumMipmap = 7.0;
	float MipmapIndex = (roughness-0.01) * NumMipmap;
	float3 reflectVector = normalize(mul(reflect(-V, N), viewToWorldMatrix)) * float3(-1.0, 1.0, 1.0);

	float3 envColor = saturate(skyboxTexture.SampleLevel(samplerLinear, reflectVector, MipmapIndex).xyz);

	float sunShadow = 1.0f;
	if(useShadow)
	{
		float3 worldPos = mul(float4(position.xyz, 1.0f), viewToWorldMatrix).xyz;
		if (pixelDistance < 47.5f)
		{
			sunShadow = ShadowContribution(lightProjectionOne, shadowMapOne, worldPos);
		}
		else if (pixelDistance < 52.5f)
		{
			sunShadow = lerp(ShadowContribution(lightProjectionOne, shadowMapOne, worldPos),
							ShadowContribution(lightProjectionTwo, shadowMapTwo, worldPos), (pixelDistance - 47.5f) * 0.2f);
		}
		else if (pixelDistance < 167.5f)
		{
			sunShadow = ShadowContribution(lightProjectionTwo, shadowMapTwo, worldPos);
		}
		else if (pixelDistance < 172.5f)
		{
			sunShadow = lerp(ShadowContribution(lightProjectionTwo, shadowMapTwo, worldPos), 
							ShadowContribution(lightProjectionThree, shadowMapThree, worldPos), (pixelDistance - 167.5f) * 0.2f);
		}
		else if (pixelDistance < 490.0f)
		{
			sunShadow = ShadowContribution(lightProjectionThree, shadowMapThree, worldPos);
		}
		else if (pixelDistance < 500.0f)
		{
			sunShadow = lerp(ShadowContribution(lightProjectionThree, shadowMapThree, worldPos), 1.0f, (pixelDistance - 490.0f) * 0.1f);
		}
	}
	

	// Lerp with metallic value to find the good diffuse and specular.
	float3 realAlbedo = color - color * metallic;

	// 0.03 default specular value for dielectric.
	float3 realSpecularColor = lerp(0.03f, color, metallic);

	float3 finalValue = float3(0, 0, 0);

	for (int i = 0; i < numLights; ++i)
	{
		float intensity = 0.0f;
		float3 lightColor = float3(0, 0, 0);
		float3 lightDirection = float3(0, 1, 0);

		if (lights[i].type == Directionnal) // Directional
		{
			lightDirection = normalize(lights[i].direction);
			intensity = max(lights[i].intensity * sunShadow, 0.0f);
			lightColor = lights[i].color;
		}
		else if (lights[i].type == Point) // Point
		{
			float r = lights[i].intensity;
			float3 L = lights[i].position - position.xyz;
			float distance = length(L);
			float d = max(distance - r, 0);
			L /= distance;

			// calculate basic attenuation
			float denom = d / r + 1;
			float attenuation = 1 / (denom*denom);

			// scale and bias attenuation such that:
			//   attenuation == 0 at extent of max influence
			//   attenuation == 1 when d == 0
			static const float cutoff = 0.005f;
			attenuation = (attenuation - cutoff) / (1 - cutoff);
			intensity = max(attenuation, 0);
			lightColor = lights[i].color;
			lightDirection = L;
		}
		else if (lights[i].type == Spot) // Spot
		{
			float r = lights[i].intensity;
			float3 L = lights[i].position - position.xyz;
			float distance = length(L);
			float d = max(distance - r, 0);
			L /= distance;

			// calculate basic attenuation
			float denom = d / r + 1;
			float attenuation = 1 / (denom*denom);

			// scale and bias attenuation such that:
			//   attenuation == 0 at extent of max influence
			//   attenuation == 1 when d == 0
			static const float cutoff = 0.005f;
			attenuation = (attenuation - cutoff) / (1 - cutoff);
			intensity = max(attenuation, 0);

			lightDirection = L;
			float angleAttenuation = acos(dot(normalize(-lights[i].direction), L));
			intensity = max((lights[i].cutoff - angleAttenuation), 0.0f) * lights[i].intensity * intensity;
			lightColor = lights[i].color;
		}

		if (intensity <= 0.0f)
			continue;

		
		// Compute some useful values.
		float NdL = saturate(dot(N, lightDirection));
		float NdV = saturate(dot(N, V));
		float3 h = normalize(lightDirection + V);
		float NdH = saturate(dot(N, h));
		float VdH = saturate(dot(V, h));
		float LdV = saturate(dot(lightDirection, V));
		float a = max(0.001f, roughness * roughness);

		float3 cDiff = realAlbedo / PI;
		float3 cSpec = Specular(realSpecularColor, h, V, lightDirection, a, NdL, NdV, NdH, VdH, LdV);

		finalValue += lightColor * NdL * (cDiff * (1.0f - cSpec) + cSpec) * intensity;
		
	}

	float3 envFresnel = Specular_F_Roughness(realSpecularColor, roughness * roughness, N, V) * occlusion.a;
	float3 irradiance = float3(0.12, 0.12, 0.20) * occlusion.a;
	float4 output = float4(finalValue + envFresnel * envColor + realAlbedo * irradiance, 1.0f);
	finalValue += float3(0.12, 0.12, 0.20) * color.rgb * occlusion.a;
	output.rgb += color.rgb * occlusion.rgb;
	return output;
}
