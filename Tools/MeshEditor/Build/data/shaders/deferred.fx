cbuffer MatrixBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;
}


cbuffer MaterialBuffer : register(b1)
{
	float3 diffuseColor;
	float roughness;
	float metallic;
	bool hasDiffuseMap;
	bool hasNormalMap;
	bool hasSpecularMap;
}

Texture2D diffuseTexture : register(t0);
Texture2D normalTexture : register(t1);
SamplerState samLinear : register(s0);


struct VS_INPUT
{
	float4 Pos : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 Binormal : BINORMAL0;
};


struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0; 
	float4 ViewPos : TEXCOORD1;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 Binormal : BINORMAL0;
};


struct PS_OUTPUT
{
	float4 Position : SV_Target0;
	float4 Normal : SV_Target1;
	float4 Color : SV_Target2;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output;
	output.TexCoord = input.TexCoord;
	output.ViewPos = mul(mul(input.Pos, World), View);
	output.Pos = mul(output.ViewPos, Projection);

	output.Normal = mul(mul(float4(input.Normal, 0.0f), World), View).xyz;
	output.Tangent = mul(mul(float4(input.Tangent, 0.0f), World), View).xyz;
	output.Binormal = mul(mul(float4(input.Binormal, 0.0f), World), View).xyz;
	
	return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
PS_OUTPUT PS(VS_OUTPUT input)
{
	PS_OUTPUT output;
	output.Position = input.ViewPos;

	if (hasNormalMap)
	{
		float3 normalTangentSpace = normalTexture.Sample(samLinear, input.TexCoord).xyz;
		output.Normal.xyz = normalize((normalTangentSpace * 2.0) - 1.0);
		output.Normal.xyz = output.Normal.x * normalize(input.Tangent) + output.Normal.y * normalize(input.Binormal) + output.Normal.z * normalize(input.Normal);
		output.Normal.w = metallic;
	}
	else
	{
		output.Normal = float4(normalize(input.Normal), metallic);
	}

	if (hasDiffuseMap) 
	{
		float4 diffuseTex = pow(diffuseTexture.Sample(samLinear, input.TexCoord), 2.2);

		clip(diffuseTex.a - 0.1);

		output.Color = float4(diffuseColor * diffuseTex.rgb, roughness);
	}
	else
	{
		output.Color = float4(diffuseColor, roughness);
	}

	return output;
}
