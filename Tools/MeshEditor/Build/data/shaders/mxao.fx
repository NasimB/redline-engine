//
// Ambient Obscurance with Indirect Lighting "MXAO" 2.1.001 by Marty McFly
// CC BY-NC-ND 3.0 licensed.
//

#define MXAO_MIPLEVEL_AO		0 // [0 to 2] Miplevel of AO texture. 0 = fullscreen, 1 = 1/2 screen width/height, 2 = 1/4 screen width/height and so forth. Best results: IL MipLevel = AO MipLevel + 2
#define MXAO_MIPLEVEL_IL		2 // [0 to 4] Miplevel of IL texture. 0 = fullscreen, 1 = 1/2 screen width/height, 2 = 1/4 screen width/height and so forth.
#define MXAO_ENABLE_IL          1 // [0 or 1] Enables Indirect Lighting calculation. Will cause a major fps hit.
#define MXAO_ENABLE_BACKFACE	1 // [0 or 1] Enables back face check so surfaces facing away from the source position don't cast light. Will cause a major fps hit.
#define MXAO_TWO_LAYER			1 // [0 or 1] Splits MXAO into two separate layers that allow for both large and fine AO.


static const float fMXAOAmbientOcclusionAmount = 4.00; // Intensity of AO effect. Can cause pitch black clipping if set too high.
static const float fMXAOIndirectLightingAmount = 1.00; // Intensity of IL effect. Can cause overexposured white spots if set too high.\nEnable SSIL in preprocessor section.
static const float fMXAOIndirectLightingSaturation = 1.00; // Controls color saturation of IL effect.\nEnable SSIL in preprocessor section.
static const float fMXAOSampleRadius = 4.00; // Sample radius of MXAO, higher means more large-scale occlusion with less fine-scale details.
static const float fMXAOSampleCount = 16.0; // Amount of MXAO samples. Higher means more accurate and less noisy AO at the cost of performance.

#if (MXAO_TWO_LAYER != 0)
static const float fMXAOSampleRadiusSecondary = 0.2; // Multiplier of Sample Radius for fine geometry. A setting of 0.5 scans the geometry at half the radius of the main AO.
static const float2 fMXAOMultFineCoarse = 1.0; // Intensity of large and small scale AO / IL.
#endif

static const float fMXAONormalBias = 0.2; // Occlusion Cone bias to reduce self-occlusion of surfaces that have a low angle to each other.
static const float fMXAOBlurSharpness = 2.00; // MXAO sharpness, higher means AO blurs less across geometry edges but may leave some noisy areas.
static const int fMXAOBlurSteps = 3; // Offset count for MXAO bilateral blur filter. Higher means smoother but also blurrier AO.

static const float fMXAOFadeoutStart = 0.3; // Distance where MXAO starts to fade out. 0.0 = camera, 1.0 = sky. Must be less than Fade Out End.
static const float fMXAOFadeoutEnd = 0.5; // Distance where MXAO completely fades out. 0.0 = camera, 1.0 = sky. Must be greater than Fade Out Start.


cbuffer SSAOBuffer : register(b0)
{
	float4 lightDirection;
	float halfWidth;
	float halfHeight;
	float aspectRatio;
	float lightIntensity;
}

Texture2D positionTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D colorTexture : register(t2);

SamplerState samplerLinear : register(s0);

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


VS_OUTPUT VS(uint id:SV_VertexID)
{
	VS_OUTPUT output;
	output.Pos.x = (float)(id / 2) * 4.0f - 1.0f;
	output.Pos.y = (float)(id % 2) * 4.0f - 1.0f;
	output.Pos.z = 0.0f;
	output.Pos.w = 1.0f;

	output.TexCoord.x = (float)(id / 2) * 2.0f;
	output.TexCoord.y = 1.0f - (float)(id % 2) * 2.0f;
	return output;
}


/*	
	Fetches position relative to camera. This is somewhat inaccurate
	as it assumes FoV == 90 degrees but yields good enough results.
	Axes are multiplied with far plane to better scale the occlusion
	falloff and save instruction in AO main pass. Also using a bigger
	data range seems to reduce precision artifacts for logarithmic
	depth buffer option. 
*/
float3 GetPosition(float2 coords)
{
	return positionTexture.Sample(samplerLinear, coords).xyz;
}


/* 
	Same as above, except linearized and scaled data is already stored
	in dedicated texture and we're sampling mipmaps here. 
*/
float3 GetPositionLOD(float2 coords, float mipLevel)
{
	return positionTexture.SampleLevel(samplerLinear, coords, mipLevel).xyz;
}


/*	
	Calculates the bayer dither pattern that's used to jitter
	the direction of the AO samples per pixel.
	Why this instead of precalculated texture? BECAUSE I CAN.
	Using this ordered jitter instead of a pseudorandom one
	has 3 advantages: it seems to be more cache-aware, the AO
	is (given a fitting AO sample distribution pattern) a lot less
	noisy (better variance, see Alchemy AO) and bilateral blur
	needs a much smaller kernel: from my tests a blur kernel
	of 5x5 is fine for most settings, but using a pseudorandom
	distribution still has noticeable grain with 12x12++.
	Smaller bayer matrix sizes have more obvious directional
	AO artifacts but are easier to blur. 
*/
float GetBayerFromCoordLevel(float2 pixelpos, int maxLevel)
{
	float finalBayer = 0.0;

	for (float i = 1 - maxLevel; i <= 0; i++)
	{
		float bayerSize = exp2(i);
		float2 bayerCoord = floor(pixelpos * bayerSize) % 2.0;
		float bayer = 2.0 * bayerCoord.x - 4.0 * bayerCoord.x * bayerCoord.y + 3.0 * bayerCoord.y;
		finalBayer += exp2(2.0*(i + maxLevel))* bayer;
	}

	float finalDivisor = 4.0 * exp2(2.0 * maxLevel) - 4.0;
	//raising all values by increment is false but in AO pass it makes sense. Can you see it?
	return finalBayer / finalDivisor + 1.0 / exp2(2.0 * maxLevel);
}

/* 
	Main AO pass. The samples are taken in an outward spiral,
	that way a simple rotation matrix is enough to compute
	the sample locations. The rotation angle is fine-tuned,
	it yields an optimal (optimal as in "I couldn't find a better one")
	sample distribution. Vogel algorithm uses the golden angle,
	and samples are more uniformly distributed over the disc but
	AO quality suffers a lot of samples are lining up (having the
	same sampling direction). Test it yourself: make angle depending
	on texcoord.x and you'll see that AO quality is highly depending
	on angle. Mara and McGuire solve this in their Alchemy AO approach
	by providing a hand-selected rotation for each sample count,
	however my angle seems to produce better results and doesn't require
	declaring a huge constant array or any CPU side code. 
*/
float4 GetMXAO(float2 POS, float2 UV, float3 N, float3 P, float nSamples, float radius, float falloffFactor, float sampleJitter)
{
	float4 AO_IL = 0.0;
	float2 sampleUV, Dir;

#if(MXAO_TWO_LAYER != 0)
	float enhanceDetails = (POS.x + POS.y) % 2;
	radius *= lerp(1.0, fMXAOSampleRadiusSecondary, enhanceDetails);
	falloffFactor *= lerp(1.0, 1.0 / (fMXAOSampleRadiusSecondary*fMXAOSampleRadiusSecondary), enhanceDetails);
#endif

	sincos(6.28318548*sampleJitter, Dir.y, Dir.x);
	Dir *= radius;

	[loop]
	for (float iSample = 0.0; iSample < nSamples; ++iSample)
	{
		Dir.xy = mul(Dir.xy, float2x2(0.575, 0.81815, -0.81815, 0.575));

		sampleUV = UV.xy + Dir.xy * float2(1.0, aspectRatio) * (iSample + sampleJitter);

		if (saturate(sampleUV.x) != sampleUV.x || saturate(sampleUV.y) != sampleUV.y)
			continue;

		float sampleMIP = saturate(radius * iSample * 20.0) * 5.0;
		
		float3 samplePosition = GetPositionLOD(sampleUV, sampleMIP);

		if (length(samplePosition) == 0.0) // Zero pos = no pixel data to process
			continue;

		float3 V = -P + samplePosition;

		float  VdotV = dot(V, V);

		float  VdotN = dot(V, N) * rsqrt(VdotV);

		float fAO = saturate(1.0 + falloffFactor * VdotV)  * saturate(VdotN - fMXAONormalBias);

#if(MXAO_ENABLE_IL != 0)
		if (fAO > 0.1)
		{
			float3 fIL = colorTexture.SampleLevel(samplerLinear, sampleUV, sampleMIP + MXAO_MIPLEVEL_IL).xyz;
#if(MXAO_ENABLE_BACKFACE != 0)
			float3 tN = normalize(normalTexture.SampleLevel(samplerLinear, sampleUV, sampleMIP + MXAO_MIPLEVEL_IL).xyz);

			fIL *= max(dot(tN, lightDirection.rgb) * lightIntensity, 0.1);
			fIL = fIL - fIL * saturate(dot(V, tN) * rsqrt(VdotV) * 2.0);
#endif

			AO_IL += float4(fIL * fAO, fAO - fAO * dot(fIL, 0.333));
		}
#else
		AO_IL.w += fAO;
#endif
	}

	AO_IL = saturate(AO_IL / ((1.0 - fMXAONormalBias)*nSamples));

#if(MXAO_TWO_LAYER != 0)
	AO_IL = pow(AO_IL, 1.0 / lerp(fMXAOMultFineCoarse.x, fMXAOMultFineCoarse.y, enhanceDetails));
#endif

	

	return AO_IL;
}



float4 PS(VS_OUTPUT input) : SV_Target
{
	float3 normalSample = normalize(normalTexture.Sample(samplerLinear, input.TexCoord).xyz);
	
	// Zero normal = no pixel to draw here
	if (length(normalSample) == 0.0f) 
		return float4(0.0, 0.0, 0.0, 1.0); 

	float3 ScreenSpaceNormals = normalize(normalSample);
	float3 ScreenSpacePosition = GetPosition(input.TexCoord);

	float scenedepth = abs(ScreenSpacePosition.z) / 1000.0;

	// Outside of range
	if (scenedepth > fMXAOFadeoutEnd)
		return float4(0.0, 0.0, 0.0, 1.0);

	ScreenSpacePosition += ScreenSpaceNormals * scenedepth;

	float SampleRadiusScaled = 0.25 * fMXAOSampleRadius / (fMXAOSampleCount * (ScreenSpacePosition.z + 2.0));
	
	float falloffFactor = -1.0 / (fMXAOSampleRadius * fMXAOSampleRadius);

	float bayer = GetBayerFromCoordLevel(input.Pos.xy, 4);
	
	float4 occlusion = GetMXAO(input.Pos.xy,
		input.TexCoord,
		ScreenSpaceNormals,
		ScreenSpacePosition,
		fMXAOSampleCount,
		SampleRadiusScaled,
		falloffFactor,
		bayer);

	occlusion = sqrt(abs(occlusion)); // AO denoise
	return occlusion;
}



/* 
	Calculates weights for bilateral AO blur. Using only
	depth is surely faster but it doesn't really cut it, also
	areas with a flat angle to the camera will have high depth
	differences, hence blur will cause stripes as seen in many
	AO implementations, even HBAO+. Taking view angle into
	account greatly helps to reduce these problems. 
*/
void GetBlurWeight(in float4 tempKey, in float4 centerKey, in float surfacealignment, inout float weight)
{
	float depthdiff = abs(tempKey.w - centerKey.w);
	float normaldiff = 1.0 - saturate(dot(normalize(tempKey.xyz), normalize(centerKey.xyz)));

	float depthweight = saturate(rcp(fMXAOBlurSharpness*depthdiff*5.0*surfacealignment));
	float normalweight = saturate(rcp(fMXAOBlurSharpness*normaldiff*10.0));

	weight = min(normalweight, depthweight) * 2;
}

/* 
	Fetches normal,depth and AO/IL data from the respective buffers.
	AO only: backbuffer rgb - normal, backbuffer alpha - AO.
	IL enabled: backbuffer rgb - IL, backbuffer alpha - AO. 
*/
void GetBlurKeyAndSample(in float2 texcoord, inout float4 tempsample, inout float4 key)
{
	tempsample = colorTexture.SampleLevel(samplerLinear, texcoord, 0);
#if(MXAO_ENABLE_IL != 0)
	key = float4(normalize(normalTexture.Sample(samplerLinear, texcoord).xyz), GetPosition(texcoord).z);
#else
	key = float4(tempsample.xyz * 2 - 1, GetPosition(texcoord).z);
#endif
}


/* 
	Bilateral blur, exploiting bilinear filter
	for sample count reduction by sampling 2 texels
	at once.
*/
float4 GetBlurredAO(float2 texcoord, float2 axisscaled, int nSteps)
{
	float4 tempsample;
	float4 centerkey, tempkey;
	float  centerweight = 1.0, tempweight;
	float4 blurcoord = 0.0;

	GetBlurKeyAndSample(texcoord.xy, tempsample, centerkey);
	float surfacealignment = saturate(-dot(centerkey.xyz, normalize(float3(texcoord.xy*2.0 - 1.0, 1.0)*centerkey.w)));

#if(MXAO_ENABLE_IL != 0)
	float4 AO_IL = tempsample;
#else
	float AO = tempsample.w;
#endif

	[loop]
	for (int iStep = 1; iStep <= nSteps; iStep++)
	{
		float currentLinearstep = iStep * 2.0 - 0.5;

		GetBlurKeyAndSample(texcoord.xy + currentLinearstep * axisscaled, tempsample, tempkey);
		GetBlurWeight(tempkey, centerkey, surfacealignment, tempweight);

#if(MXAO_ENABLE_IL != 0)
		AO_IL += tempsample * tempweight;
#else
		AO += tempsample.w * tempweight;
#endif
		centerweight += tempweight;

		GetBlurKeyAndSample(texcoord.xy - currentLinearstep * axisscaled, tempsample, tempkey);
		GetBlurWeight(tempkey, centerkey, surfacealignment, tempweight);

#if(MXAO_ENABLE_IL != 0)
		AO_IL += tempsample * tempweight;
#else
		AO += tempsample.w * tempweight;
#endif
		centerweight += tempweight;
	}

#if(MXAO_ENABLE_IL != 0)
	return float4(AO_IL / centerweight);
#else
	return float4(centerkey.xyz*0.5 + 0.5, AO / centerweight);
#endif
}


float4 PS_BlurX(VS_OUTPUT input) : SV_Target
{
	return GetBlurredAO(input.TexCoord.xy, float2(halfWidth, 0.0), fMXAOBlurSteps);
}


float4 PS_BlurY(VS_OUTPUT input) : SV_Target
{
	float4 occlusion = GetBlurredAO(input.TexCoord.xy, float2(0.0, halfHeight), fMXAOBlurSteps);

	occlusion *= occlusion; // AO denoise

	float3 ScreenSpacePosition = GetPosition(input.TexCoord);

	float scenedepth = abs(ScreenSpacePosition.z) / 1000.0;

	occlusion.xyz = lerp(dot(occlusion.xyz, 0.333), occlusion.xyz, fMXAOIndirectLightingSaturation) * fMXAOIndirectLightingAmount * 4;
	occlusion.w = 1.0 - pow(1.0 - occlusion.w, fMXAOAmbientOcclusionAmount * 4.0);

	occlusion.w = lerp(occlusion.w, 0.0, smoothstep(fMXAOFadeoutStart, fMXAOFadeoutEnd, scenedepth));
	occlusion.xyz = lerp(occlusion.xyz, 0.0.xxx, smoothstep(fMXAOFadeoutStart * 0.5, fMXAOFadeoutEnd * 0.5, scenedepth));

	occlusion.w = saturate(1.0 - occlusion.w);
	occlusion.xyz = max(0.0, occlusion.www * occlusion.xyz);
	return occlusion;
}